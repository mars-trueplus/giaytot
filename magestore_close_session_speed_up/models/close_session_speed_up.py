# -*- coding: utf-8 -*-
from odoo import models, fields, api, _, tools
from odoo.exceptions import UserError
import ast
import time
import logging
_logger = logging.getLogger(__name__)

class POSCloseSessionSpeedUp(models.Model):
    _name = "close.session.speed.up"

    pos_session_id = fields.Many2one('pos.session', "POS Session")
    state = fields.Boolean("Is Croned")
    context = fields.Char("Session Context")

    @api.model
    def post_entries(self):
        try:
            for item in self.env['close.session.speed.up'].search([]):
                if not item.state:
                    self = self.with_context(ast.literal_eval(item.context))
                    sessions = item.pos_session_id
                    for session in sessions:
                        company_id = session.config_id.company_id.id
                        ctx = dict(self.env.context, force_company=company_id, company_id=company_id)
                        self = self.with_context(ctx)
                        for st in session.statement_ids:
                            if abs(st.difference) > st.journal_id.amount_authorized_diff:
                                # The pos manager can close statements with maximums.
                                if not self.env['ir.model.access'].check_groups("point_of_sale.group_pos_manager"):
                                    raise UserError(_(
                                        "Your ending balance is too different from the theoretical cash closing (%.2f), the maximum allowed is: %.2f. You can contact your manager to force it.") % (
                                                    st.difference, st.journal_id.amount_authorized_diff))
                            if (st.journal_id.type not in ['bank', 'cash']):
                                raise UserError(
                                    _("The type of the journal for your payment method should be bank or cash "))
                            st.with_context(ctx).sudo().button_confirm_bank()

                        # company_id = session.config_id.journal_id.company_id.id
                        orders = session.order_ids.filtered(lambda order: order.state == 'paid')
                        journal_id = self.env['ir.config_parameter'].sudo().get_param(
                            'pos.closing.journal_id_%s' % company_id, default=session.config_id.journal_id.id)
                        if not journal_id:
                            raise UserError(_("You have to set a Sale Journal for the POS:%s") % (session.config_id.name,))

                        move = self.env['pos.order'].with_context(force_company=company_id)._create_account_move(session.start_at,
                                                                                                                 session.name,
                                                                                                                 int(journal_id),
                                                                                                                 company_id)
                        orders.with_context(force_company=company_id)._create_account_move_line(session, move)
                        for order in session.order_ids.filtered(lambda o: o.state not in ['done', 'invoiced']):
                            if order.state not in ('paid'):
                                raise UserError(
                                    _("You cannot confirm all orders of this session, because they have not the 'paid' status"))
                            order.action_pos_order_done()
                        # orders = session.order_ids.filtered(lambda order: order.state in ['invoiced', 'done'])
                        # orders.sudo()._reconcile_payments()

                    item.write({'state': True})
        except Exception as ex:
            _logger.exception('Update accounting pos cron - error: %s' % tools.ustr(ex))
        # self.env['pos.session'].with_context(ctx)._confirm_orders()
        return True

class PosSession(models.Model):
    _inherit = "pos.session"

    def check_statements(self, ctx):
        for session in self:
            for st in session.statement_ids:
                if abs(st.difference) > st.journal_id.amount_authorized_diff:
                    # The pos manager can close statements with maximums.
                    if not self.env['ir.model.access'].check_groups("point_of_sale.group_pos_manager"):
                        raise UserError(_(
                            "Your ending balance is too different from the theoretical cash closing (%.2f), the maximum allowed is: %.2f. You can contact your manager to force it.") % (
                                        st.difference, st.journal_id.amount_authorized_diff))
                if (st.journal_id.type not in ['bank', 'cash']):
                    raise UserError(_("The type of the journal for your payment method should be bank or cash "))
                st.with_context(ctx).sudo().check_button_confirm_bank_pos_speed_up()
        return True

    def check_order_pos_speed_up(self):
        for session in self:
            orders = session.order_ids.filtered(lambda order: order.state == 'paid')
            journal_id = self.env['ir.config_parameter'].sudo().get_param(
                'pos.closing.journal_id_%s' % session.config_id.company_id.id, default=session.config_id.journal_id.id)
            if not journal_id:
                raise UserError(_("You have to set a Sale Journal for the POS:%s") % (session.config_id.name,))

            orders.with_context(force_company=session.config_id.company_id.id).check_create_account_move_line_pos_speed_up(session)

            for order in session.order_ids.filtered(lambda o: o.state not in ['done', 'invoiced']):
                if order.state not in ('paid'):
                    raise UserError(
                        _("You cannot confirm all orders of this session, because they have not the 'paid' status"))
                order.check_create_account_move_line_pos_speed_up()

    @api.multi
    def action_pos_session_close(self):
        # Close CashBox
        close_session = self.env['close.session.speed.up']
        for session in self:
            company_id = session.config_id.company_id.id
            ctx = dict(self.env.context, force_company=company_id, company_id=company_id)
            self = self.with_context(ctx)
            self.check_statements(ctx)
            self.check_order_pos_speed_up()
            close_session.create({
                'pos_session_id': session.id,
                'state': False,
                'context': self.env.context
            })
        self.write({'state': 'closed'})
        return {
            'type': 'ir.actions.client',
            'name': 'Point of Sale Menu',
            'tag': 'reload',
            'params': {'menu_id': self.env.ref('point_of_sale.menu_point_root').id},
        }

class PosOrder(models.Model):
    _inherit = "pos.order"

    def check_create_account_move_line_pos_speed_up(self,session=None):

        if session and not all(session.id == order.session_id.id for order in self):
            raise UserError(_('Selected orders do not have the same session!'))

        for order in self.filtered(lambda o: not o.account_move or o.state == 'paid'):
            for line in order.lines:
                # Search for the income account
                if line.product_id.property_account_income_id.id:
                    income_account = 1
                elif line.product_id.categ_id.property_account_income_categ_id.id:
                    income_account = 2
                else:
                    raise UserError(_('Please define income '
                                      'account for this product: "%s" (id:%d).')
                                    % (line.product_id.name, line.product_id.id))

        return True

class AccountBankStatement(models.Model):
    _inherit = "account.bank.statement"

    def check_button_confirm_bank_pos_speed_up(self):
        self._balance_check()
        statements = self.filtered(lambda r: r.state == 'open')
        for statement in statements:
            moves = self.env['account.move']
            for st_line in statement.line_ids:
                if st_line.account_id and not st_line.journal_entry_ids.ids:
                    st_line.check_fast_counterpart_creation_pos_speed_up()
                elif not st_line.journal_entry_ids.ids:
                    raise UserError(
                        _('All the account entries lines must be processed in order to close the statement.'))
                moves = (moves | st_line.journal_entry_ids)
            if moves:
                moves.check_post_pos_speed_up()

class AccountBankStatementLine(models.Model):
    _inherit = "account.bank.statement.line"

    def check_fast_counterpart_creation_pos_speed_up(self):
        pos_store_dimension = 0
        if self.env.context.get('pos_store_dimension'):
            pos_store_dimension = self.env.context.get('pos_store_dimension')
        for st_line in self:
            # Technical functionality to automatically reconcile by creating a new move line
            vals = {
                'name': st_line.name,
                'debit': st_line.amount < 0 and -st_line.amount or 0.0,
                'credit': st_line.amount > 0 and st_line.amount or 0.0,
                'account_id': st_line.account_id.id,
                'store_dimension': pos_store_dimension or 0
            }
            st_line.check_process_reconciliation_pos_speed_up(new_aml_dicts=[vals])

    def check_process_reconciliation_pos_speed_up(self, counterpart_aml_dicts=None, payment_aml_rec=None, new_aml_dicts=None):

        counterpart_aml_dicts = counterpart_aml_dicts or []
        new_aml_dicts = new_aml_dicts or []

        counterpart_moves = self.env['account.move']

        if any(line.journal_entry_ids for line in self):
            raise UserError(_('A selected statement line was already reconciled with an account move.'))

        # Create move line(s). Either matching an existing journal entry (eg. invoice), in which
        # case we reconcile the existing and the new move lines together, or being a write-off.
        if counterpart_aml_dicts or new_aml_dicts:
            em = True
        elif self.move_name:
            raise UserError(_('Operation not allowed. Since your statement line already received a number, you cannot reconcile it entirely with existing journal entries otherwise it would make a gap in the numbering. You should book an entry and make a regular revert of it in case you want to cancel it.'))
        counterpart_moves.assert_balanced()
        return True

class AccountMove(models.Model):
    _inherit = "account.move"

    def check_post_pos_speed_up(self):
        # invoice = self._context.get('invoice', False)
        self.check_post_validate_pos_speed_up()
    #     for move in self:
    #         move.line_ids.create_analytic_lines()
    #         if move.name == '/':
    #             new_name = False
    #             journal = move.journal_id
    #
    #             if invoice and invoice.move_name and invoice.move_name != '/':
    #                 new_name = invoice.move_name
    #             else:
    #                 if journal.sequence_id:
    #                     # If invoice is actually refund and journal has a refund_sequence then use that one or use the regular one
    #                     sequence = journal.sequence_id
    #                     if invoice and invoice.type in ['out_refund', 'in_refund'] and journal.refund_sequence:
    #                         if not journal.refund_sequence_id:
    #                             raise UserError(_('Please define a sequence for the refunds'))
    #                         sequence = journal.refund_sequence_id
    #
    #                     new_name = sequence.with_context(ir_sequence_date=move.date).next_by_id()
    #                 else:
    #                     raise UserError(_('Please define a sequence on the journal.'))

        # return True

    def check_post_validate_pos_speed_up(self):
        for move in self:
            if move.line_ids:
                if not all([x.company_id.id == move.company_id.id for x in move.line_ids]):
                    raise UserError(_("Cannot create moves for different companies."))
        self.assert_balanced()
        return self._check_lock_date()