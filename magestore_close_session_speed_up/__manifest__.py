# -*- coding: utf-8 -*-

{
    'name': 'Magestore POS Close Session Speed Up',
    'version': '1.0.0',
    'category': 'Point Of Sale',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Magestore POS Close Session Speed Up',
    'description': """""",
    'depends': [
        'point_of_sale'
    ],
    'data': [
        'data/close_session_speed_up.xml',
        'security/ir.model.access.csv',
        # 'views/pos_speed_up_templates.xml',
    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
