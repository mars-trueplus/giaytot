# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class FieldCustomerInvoiceReminder(models.TransientModel):
    _name = "customer.invoice.field.reminder"

    invoice_id = fields.Many2one('account.invoice')

    @api.model
    def default_get(self, fields):
        res = super(FieldCustomerInvoiceReminder, self).default_get(fields)
        if not res.get('invoice_id') and self._context.get('active_id'):
            res['invoice_id'] = self._context['active_id']
        return res

    @api.multi
    def process(self):
        self.ensure_one()
        self.invoice_id.action_date_assign()
        self.invoice_id.action_move_create()
        self.invoice_id.invoice_validate()


class FieldVendorBillReminder(models.TransientModel):
    _name = "vendor.bill.field.reminder"

    invoice_id = fields.Many2one('account.invoice')

    @api.model
    def default_get(self, fields):
        res = super(FieldVendorBillReminder, self).default_get(fields)
        if not res.get('invoice_id') and self._context.get('active_id'):
            res['invoice_id'] = self._context['active_id']
        return res

    @api.multi
    def process(self):
        self.ensure_one()
        self.invoice_id.action_date_assign()
        self.invoice_id.action_move_create()
        self.invoice_id.invoice_validate()