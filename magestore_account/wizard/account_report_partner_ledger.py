# -*- coding: utf-8 -*-

from datetime import datetime
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
import time
from odoo import api, fields, models, _
from odoo.exceptions import UserError
import base64
import contextlib
import cStringIO
import csv
import xlwt

class AccountPartnerLedger(models.TransientModel):
    _inherit = "account.report.partner.ledger"

    export_to_excel = fields.Boolean('Export to excel')
    name = fields.Char(string="File Name", readonly=True)
    data = fields.Binary(string="File", readonly=True)
    file_type = fields.Selection([('xls', 'xls'), ('csv', 'csv')], default='xls', required=True)
    state = fields.Selection([('choose', 'choose'), ('get', 'get')], default='choose')
    account_move_line_id = fields.Many2many("account.move.line", string="Account Move Line")
    # partner_id = fields.Many2one('res.partner', string="Partner", related='account_move_line_id.partner_id', store=False)
    # partner_id = fields.Many2one('res.partner', string="Partner", domain=lambda self: [('partner_id', 'in', self._get_partner_id())])
    partner_id = fields.Many2one('res.partner', string="Partner")

    # def _get_partner_id(self):
    #     company_id = self.env.user.company_id.id
    #     partner_ids = self.env['account.move'].search([('company_id', '=', company_id)]).browse('partner_id')
    #     return partner_ids

    def _print_report(self, data):
        data = self.pre_print_report(data)
        data['form'].update({'reconciled': self.reconciled, 'amount_currency': self.amount_currency})
        data['form'].update({'export_to_excel': self.export_to_excel})
        data['form'].update({'partner_id': self.partner_id.id})
        if self.export_to_excel:
            return self._render_html(data)
        else:
            return self.env['report'].get_action(self, 'account.report_partnerledger', data=data)

    def _lines(self, data, partner):
        full_account = []
        query_get_data = self.env['account.move.line'].with_context(data['form'].get('used_context', {}))._query_get()
        reconcile_clause = "" if data['form']['reconciled'] else ' AND "account_move_line".reconciled = false '
        params = [partner.id, tuple(data['computed']['move_state']), tuple(data['computed']['account_ids'])] + query_get_data[2]
        query = """
            SELECT "account_move_line".id, "account_move_line".date, j.code, acc.code as a_code, acc.name as a_name, "account_move_line".ref, m.name as move_name, "account_move_line".name, "account_move_line".debit, "account_move_line".credit, "account_move_line".amount_currency,"account_move_line".currency_id, c.symbol AS currency_code
            FROM """ + query_get_data[0] + """
            LEFT JOIN account_journal j ON ("account_move_line".journal_id = j.id)
            LEFT JOIN account_account acc ON ("account_move_line".account_id = acc.id)
            LEFT JOIN res_currency c ON ("account_move_line".currency_id=c.id)
            LEFT JOIN account_move m ON (m.id="account_move_line".move_id)
            WHERE "account_move_line".partner_id = %s
                AND m.state IN %s
                AND "account_move_line".account_id IN %s AND """ + query_get_data[1] + reconcile_clause + """
                ORDER BY "account_move_line".date"""
        self.env.cr.execute(query, tuple(params))
        res = self.env.cr.dictfetchall()

        sum = 0.0
        lang_code = self.env.context.get('lang') or 'en_US'
        lang = self.env['res.lang']
        lang_id = lang._lang_get(lang_code)
        date_format = lang_id.date_format
        for r in res:
            r['date'] = datetime.strptime(r['date'], DEFAULT_SERVER_DATE_FORMAT).strftime(date_format)
            r['displayed_name'] = '-'.join(
                r[field_name] for field_name in ('move_name', 'ref', 'name')
                if r[field_name] not in (None, '', '/')
            )
            sum += r['debit'] - r['credit']
            r['progress'] = sum
            full_account.append(r)
        return full_account

    def _sum_partner(self, data, partner, field):
        if field not in ['debit', 'credit', 'debit - credit']:
            return
        result = 0.0
        query_get_data = self.env['account.move.line'].with_context(data['form'].get('used_context', {}))._query_get()
        reconcile_clause = "" if data['form']['reconciled'] else ' AND "account_move_line".reconciled = false '

        params = [partner.id, tuple(data['computed']['move_state']), tuple(data['computed']['account_ids'])] + query_get_data[2]
        query = """SELECT sum(""" + field + """)
                FROM """ + query_get_data[0] + """, account_move AS m
                WHERE "account_move_line".partner_id = %s
                    AND m.id = "account_move_line".move_id
                    AND m.state IN %s
                    AND m.account_id IN %s
                    AND """ + query_get_data[1] + reconcile_clause
        self.env.cr.execute(query, tuple(params))

        contemp = self.env.cr.fetchone()
        if contemp is not None:
            result = contemp[0] or 0.0
        return result

    def _render_html(self, data=None):
        data['computed'] = {}

        obj_partner = self.env['res.partner']
        query_get_data = self.env['account.move.line'].with_context(data['form'].get('used_context', {}))._query_get()
        data['computed']['move_state'] = ['draft', 'posted']
        if data['form'].get('target_move', 'all') == 'posted':
            data['computed']['move_state'] = ['posted']
        result_selection = data['form'].get('result_selection', 'customer')
        if result_selection == 'supplier':
            data['computed']['ACCOUNT_TYPE'] = ['payable']
        elif result_selection == 'customer':
            data['computed']['ACCOUNT_TYPE'] = ['receivable']
        else:
            data['computed']['ACCOUNT_TYPE'] = ['payable', 'receivable']
        self.env.cr.execute("""
                SELECT a.id
                FROM account_account a
                WHERE a.internal_type IN %s
                AND NOT a.deprecated""", (tuple(data['computed']['ACCOUNT_TYPE']),))
        data['computed']['account_ids'] = [a for (a,) in self.env.cr.fetchall()]
        params = [tuple(data['computed']['move_state']), tuple(data['computed']['account_ids'])] + query_get_data[2]
        reconcile_clause = "" if data['form']['reconciled'] else ' AND "account_move_line".reconciled = false '
        query = """
                SELECT DISTINCT "account_move_line".partner_id
                FROM """ + query_get_data[0] + """, account_account AS account, account_move AS am
                WHERE "account_move_line".partner_id IS NOT NULL
                    AND "account_move_line".account_id = account.id
                    AND am.id = "account_move_line".move_id
                    AND am.state IN %s
                    AND "account_move_line".account_id IN %s
                    AND NOT account.deprecated
                    AND """ + query_get_data[1] + reconcile_clause

        partner_id = data['form'].get('partner_id', None)
        if partner_id:
            query += """ AND "account_move_line".partner_id = %s""" %partner_id

        self.env.cr.execute(query, tuple(params))
        partner_ids = [res['partner_id'] for res in self.env.cr.dictfetchall()]
        if len(partner_ids) == 0:
            partner = self.env['res.partner'].browse(partner_id)
            if partner:
                raise UserError(_("Partner '%s' doesn't have transaction" %partner.name))
            else:
                raise UserError(_("There is no transaction"))

        partners = obj_partner.browse(partner_ids)
        partners = sorted(partners, key=lambda x: (x.ref, x.name))

        docargs = {
            'doc_ids': partner_ids,
            'doc_model': self.env['res.partner'],
            'data': data,
            'docs': partners,
            'time': time,
            'lines': self._lines,
            'sum_partner': self._sum_partner,
        }
        return self._do_export_to_excel(docargs)

    def _do_export_to_excel(self, docargs):
        lang_code = self.env.context.get('lang') or 'en_US'
        lang = self.env['res.lang']
        lang_id = lang._lang_get(lang_code)
        date_format = lang_id.date_format


        wb = xlwt.Workbook(encoding='utf8')
        company = self.env.user.company_id

        bold_head_title = "font: bold on;align: horizontal center;align: vertical center;"
        bold = "font: bold on;"
        border = "border: left thin, right thin, top thin, bottom thin;"
        background_color = "pattern: pattern solid, fore_colour gray25;"
        background_color_header = "pattern: pattern solid, fore_colour light_orange;"
        bold_head_title_border = bold_head_title
        bold_border = xlwt.easyxf(bold + border + background_color)
        bold_border_header = xlwt.easyxf(bold + border + background_color_header)
        bold_head_title_border = xlwt.easyxf(bold_head_title_border)
        bold = xlwt.easyxf(bold)


        sheet = wb.add_sheet('sheet')
        sheet.write(0, 0, "Company")
        sheet.write(0, 1, company.name)
        sheet.write(0, 6, "Create Date")
        sheet.write(0, 7, datetime.today().strftime(date_format))
        sheet.write_merge(1, 2, 0, 7, 'Partner Ledger', bold_head_title_border)

        index = 3

        if docargs['data']['form']['date_from']:
            sheet.write(index+1, 0, "From date", bold)
            sheet.write(index+1, 1, datetime.strptime(docargs['data']['form']['date_from'], DEFAULT_SERVER_DATE_FORMAT).strftime(date_format))
            index += 1
        if docargs['data']['form']['date_to']:
            sheet.write(index+1, 0, "To date", bold)
            sheet.write(index+1, 1, datetime.strptime(docargs['data']['form']['date_to'], DEFAULT_SERVER_DATE_FORMAT).strftime(date_format))
            index += 1

        index += 1
        sheet.write(index+1, 0, "Partner", bold_border_header)
        sheet.write(index+1, 1, "Date", bold_border_header)
        sheet.write(index+1, 2, "JRNL", bold_border_header)
        sheet.write(index+1, 3, "Account", bold_border_header)
        sheet.write(index+1, 4, "Ref", bold_border_header)
        sheet.write(index+1, 5, "Debit", bold_border_header)
        sheet.write(index+1, 6, "Credit", bold_border_header)
        sheet.write(index+1, 7, "Balance", bold_border_header)
        # sheet.write(0, 7, "Currency")

        index += 1
        if len(docargs['docs']):
            for partner in docargs['docs']:

                # sheet.write(index + 1, 0, (str(partner.ref) + " - " + partner.name).encode('utf8'))
                # sheet.write(index + 1, 0, partner.name.encode('utf8'), bold_border)
                # sheet.write(index + 1, 1, '')
                # sheet.write(index + 1, 2, '')
                # sheet.write(index + 1, 3, '')
                # sheet.write(index + 1, 4, '')
                sheet.write_merge(index+1, index+1, 0, 4, partner.name.encode('utf8'), bold_border)
                sheet.write(index + 1, 5, docargs['sum_partner'](docargs['data'], partner, 'debit'), bold_border)
                sheet.write(index + 1, 6, docargs['sum_partner'](docargs['data'], partner, 'credit'), bold_border)
                sheet.write(index + 1, 7, docargs['sum_partner'](docargs['data'], partner, 'debit - credit'), bold_border)
                # sheet.write(index + 1, 7, '')
                index += 1

                for line in docargs['lines'](docargs['data'], partner):

                    sheet.write(index + 1, 0, partner.name.encode('utf8'))
                    sheet.write(index + 1, 1, line['date'].encode('utf8'))
                    sheet.write(index + 1, 2, line['code'].encode('utf8'))
                    sheet.write(index + 1, 3, line['a_code'].encode('utf8'))
                    sheet.write(index + 1, 4, line['displayed_name'].encode('utf8'))
                    sheet.write(index + 1, 5, line['debit'])
                    sheet.write(index + 1, 6, line['credit'])
                    sheet.write(index + 1, 7, line['progress'])
                    # sheet.write(index + 1, 7, line['amount_currency'])
                    index += 1

        data = cStringIO.StringIO()
        wb.save(data)
        data.seek(0)
        out = base64.encodestring(data.read())
        data.close()

        self.write({
            'state': 'get',
            'data': out,
            'name': 'Partner Ledger.xls'
        })
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'account.report.partner.ledger',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': self.id,
            # 'views': [(False, 'form')],
            'view_id': self.env.ref('account.account_report_partner_ledger_view').id,
            'target': 'new',
        }
