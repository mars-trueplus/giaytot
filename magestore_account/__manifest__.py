# -*- coding: utf-8 -*-

{
    'name': 'Magestore Accounting',
    'version': '1.0.0',
    'category': 'Accounting',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Magestore Accounting',
    'description': """""",
    'depends': [
        'account_accountant',
        'account',
        'magestore_purchase',
        'magestore_stock',
        'magestore_store_dimension',
        'base',
        'web',
    ],
    'data': [
        'views/account_view.xml',
        'views/account_payment_view.xml',
        'views/account_invoice_view.xml',
        # 'views/sell_asset_view.xml',
        'views/account_journal_dashboard_view.xml',

        'data/print_pc_cash.xml',
        'data/print_pt_cash.xml',

        'report/report_account_payment_print_pc_cash.xml',
        'report/report_account_payment_print_pc_bank.xml',
        'report/report_account_payment_print_pkt.xml',

        'report/report_account_move_print_pc_cash.xml',
        'report/report_account_move_print_pc_bank.xml',
        'report/report_account_move_print_pkt.xml',
        'report/report_account_payment_print_pt_cash.xml',
        'report/report_account_move_print_pt_cash.xml',
        'report/report_account_payment_print_pt_bank.xml',
        'report/report_account_move_print_pt_bank.xml',

        'report/report_account_invoice_print_pkt.xml',

        'security/ir.model.access.csv',

        # Added by Mars
        'views/account_invoice_supplier_view.xml',

        'wizard/account_invoice_refund.xml',
        'wizard/account_invoice_field_remind.xml',
        'wizard/account_report_partner_ledger_view.xml',

        'views/crm_team_view.xml',
    ],
    'qweb':[
        # 'static/src/xml/base_inherit.xml',
    ],
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
