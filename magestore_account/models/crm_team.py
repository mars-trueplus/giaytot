# -*- coding: utf-8 -*-

from datetime import date

from odoo import api, fields, models


class CrmTeam(models.Model):
    _inherit = 'crm.team'

    is_show_dashboard = fields.Boolean(string="Dashboard", default=False)