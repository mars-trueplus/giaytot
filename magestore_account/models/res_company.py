# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class ResCompany(models.Model):
    _inherit = "res.company"

    company_address = fields.Char("Company Address", size=255, compute="_get_company_address", store=True)

    @api.depends("street", "street2", "city", "country_id")
    def _get_company_address(self):
        for item in self:
            item.company_address = ""
            if item.street:
                item.company_address = item.company_address + item.street
            if item.street2:
                item.company_address = item.company_address + ", " + item.street2
            if item.city:
                item.company_address = item.company_address + ", " + item.city
            # if item.country_id:
            #     item.company_address = item.company_address + ", " + item.country_id.name
