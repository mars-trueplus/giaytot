# -*- coding: utf-8 -*-

import json
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class AccountJournal(models.Model):
    _inherit = "account.journal"

    type = fields.Selection(selection_add=[('sell_asset', 'Sell Asset')], help="Select 'Sell Asset' for Fixed asset")
    is_stock_journal = fields.Boolean('Inventory', default=False)
    count_zero_unit_value = fields.Integer(compute='_compute_count_zero_unit_value')

    @api.one
    def _compute_count_zero_unit_value(self):
        self.count_zero_unit_value = len(self.env['stock.quant'].search([('unit_cost', '=', 0)]))



