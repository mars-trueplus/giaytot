import json
import time
from datetime import datetime, timedelta

from babel.dates import format_datetime, format_date

from odoo import models, api, _, fields
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT as DF
from odoo.tools.misc import formatLang


class account_journal(models.Model):
    _inherit = "account.journal"

    @api.multi
    def get_journal_dashboard_datas(self):
        result = super(account_journal, self).get_journal_dashboard_datas()
        if self.type == 'sale':
            overdue_website_team_ids = self.env['crm.team'].search([('is_show_dashboard', '=', True)]).ids
            count_value_overdue_website_sales = 0
            count_overdue_website_sales = []
            sum_overdue_website_sales = []
            name_overdue_website_sales = []
            for overdue_website_team_id in overdue_website_team_ids:
                overdue_website_sales = self.env['account.invoice'].search([
                    ('team_id.id', '=', overdue_website_team_id),
                    ('date_due', '<', time.strftime('%Y-%m-%d')),
                    ('state', '=', 'open'),
                    ('type', 'in', ['out_invoice', 'out_refund'])
                ])

                sum_overdue_website_sale = 0.0
                if len(overdue_website_sales) > 0:
                    count_value_overdue_website_sales = count_value_overdue_website_sales + 1
                    name_overdue_website_sales.append(self.env['crm.team'].browse(overdue_website_team_id).name)
                    count_overdue_website_sales.append(len(overdue_website_sales))
                    sum_overdue_website_sale = sum([o.residual_signed for o in overdue_website_sales])
                    sum_overdue_website_sales.append(formatLang(self.env, sum_overdue_website_sale or 0.0, currency_obj=self.currency_id or self.company_id.currency_id))

            result.update({
                'count_value_overdue_website_sales' : count_value_overdue_website_sales,
                'name_overdue_website_sales' : name_overdue_website_sales,
                'count_overdue_website_sales' : count_overdue_website_sales,
                'sum_overdue_website_sales' : sum_overdue_website_sales
                # 'sum_overdue_website_sales' : formatLang(self.env, sum_overdue_website_sale or 0.0, currency_obj=self.currency_id or self.company_id.currency_id)
            })
            print result
        return result