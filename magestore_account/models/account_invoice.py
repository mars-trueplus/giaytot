# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, RedirectWarning, ValidationError

import logging

_logger = logging.getLogger(__name__)


class AccountInvoice(models.Model):
    _inherit = "account.invoice"
    _description = "Invoice"
    _order = "date_invoice desc, number desc, id desc"

    # carrier_tracking_ref = fields.Char(compute="_get_carrier_tracking_ref", string="Tracking Reference")
    carrier_tracking_reference = fields.Char(compute="_get_carrier_tracking_ref", string="Carrier Tracking Reference", store=True)

    def _get_partner_address(self):
        """Added by Adam to process the partner's address in reports"""
        partner_address = ""
        if self.partner_id.street:
            partner_address = partner_address + self.partner_id.street
        if self.partner_id.street2:
            partner_address = partner_address + ", " + self.partner_id.street2
        if self.partner_id.city:
            partner_address = partner_address + ", " + self.partner_id.city
        # if item.country_id:
        #     item.partner_address = item.partner_address + ", " + item.country_id.name
        return partner_address

    def _get_company_address(self):
        """Added by Adam to process the company's address in reports"""
        company_address = ""
        if self.company_id.street:
            company_address = company_address + self.company_id.street
        if self.company_id.street2:
            company_address = company_address + ", " + self.company_id.street2
        if self.company_id.city:
            company_address = company_address + ", " + self.company_id.city
        # if self.company_id.country_id:
        #     company_address = company_address + ", " + self.company_id.country_id.name
        return company_address

    @api.multi
    def action_invoice_open(self):
        # lots of duplicate calls to action_invoice_open, so we remove those already open
        to_open_invoices = self.filtered(lambda inv: inv.state != 'open')
        if to_open_invoices.filtered(lambda inv: inv.state not in ['proforma2', 'draft']):
            raise UserError(_("Invoice must be in draft or Pro-forma state in order to validate it."))

        if self.type == 'out_invoice' and self.journal_id.type == 'sale' and not self._context.get('pos_invoice', False):
            if (not self.payment_term_id and not self.date_invoice) or not self.date_invoice or not self.payment_term_id:
                view = self.env.ref('magestore_account.view_customer_invoice_field_reminder')
                wiz = self.env['customer.invoice.field.reminder'].create({'invoice_id': to_open_invoices.id})
                return {
                    'name': _('Odoo Reminder'),
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'customer.invoice.field.reminder',
                    'views': [(view.id, 'form')],
                    'view_id': view.id,
                    'target': 'new',
                    'res_id': wiz.id,
                    'context': self.env.context,
                }

        if self.type == 'in_invoice' and self.journal_id.type == 'purchase' and not self._context.get('pos_invoice', False):
            if not self.date_invoice:
                view = self.env.ref('magestore_account.view_vendor_bill_field_reminder')
                wiz = self.env['vendor.bill.field.reminder'].create({'invoice_id': to_open_invoices.id})
                return {
                    'name': _('Odoo Reminder'),
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'vendor.bill.field.reminder',
                    'views': [(view.id, 'form')],
                    'view_id': view.id,
                    'target': 'new',
                    'res_id': wiz.id,
                    'context': self.env.context,
                }

        to_open_invoices.action_date_assign()
        to_open_invoices.action_move_create()
        to_open_invoices.invoice_validate()
        if self.type == 'out_refund'and not self._context.get('pos_invoice', False):
            return {
                'type': 'ir.actions.act_window',
                'name': _('Odoo Reminder'),
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'invoice.refund.reminder',
                'target': 'new',
            }
        # elif check_empty_field:
        #     return {
        #         'type': 'ir.actions.act_window',
        #         'name': _('Odoo Reminder'),
        #         'view_type': 'form',
        #         'view_mode': 'form',
        #         'res_model': 'invoice.field.reminder',
        #         'target': 'new',
        #     }
        # else:
        #     return check_empty_field
        return True

    @api.multi
    def _get_carrier_tracking_ref(self):
        for item in self:
            carrier_tracking_ref = ''

            if item.origin:
                stock_picking = self.env['stock.picking'].search([('origin', '=', item.origin)])

                if len(stock_picking) == 1:
                    carrier_tracking_ref = stock_picking.carrier_tracking_ref

                elif len(stock_picking) > 1:
                    carrier_tracking_ref = _("Multiples")
                else:
                    carrier_tracking_ref = ''

                item.write({'carrier_tracking_reference': carrier_tracking_ref})

    @api.multi
    def action_move_create(self):
        """ Creates invoice related analytics and financial move lines """
        account_move = self.env['account.move']

        for inv in self:
            if not inv.journal_id.sequence_id:
                raise UserError(_('Please define sequence on the journal related to this invoice.'))
            if not inv.invoice_line_ids:
                raise UserError(_('Please create some invoice lines.'))
            if inv.move_id:
                continue

            ctx = dict(self._context, lang=inv.partner_id.lang)

            if not inv.date_invoice:
                inv.with_context(ctx).write({'date_invoice': fields.Date.context_today(self)})
            date_invoice = inv.date_invoice
            company_currency = inv.company_id.currency_id

            # create move lines (one per invoice line + eventual taxes and analytic lines)
            iml = inv.invoice_line_move_line_get()
            iml += inv.tax_line_move_line_get()

            diff_currency = inv.currency_id != company_currency
            # create one move line for the total and possibly adjust the other lines amount
            total, total_currency, iml = inv.with_context(ctx).compute_invoice_totals(company_currency, iml)

            name = inv.name or '/'
            if inv.payment_term_id:
                totlines = \
                inv.with_context(ctx).payment_term_id.with_context(currency_id=company_currency.id).compute(total,
                                                                                                            date_invoice)[
                    0]
                res_amount_currency = total_currency
                ctx['date'] = date_invoice
                for i, t in enumerate(totlines):
                    if inv.currency_id != company_currency:
                        amount_currency = company_currency.with_context(ctx).compute(t[1], inv.currency_id)
                    else:
                        amount_currency = False

                    # last line: add the diff
                    res_amount_currency -= amount_currency or 0
                    if i + 1 == len(totlines):
                        amount_currency += res_amount_currency

                    iml.append({
                        'type': 'dest',
                        'name': name,
                        'price': t[1],
                        'account_id': inv.account_id.id,
                        'date_maturity': t[0],
                        'amount_currency': diff_currency and amount_currency,
                        'currency_id': diff_currency and inv.currency_id.id,
                        'invoice_id': inv.id
                    })
            else:
                iml.append({
                    'type': 'dest',
                    'name': name,
                    'price': total,
                    'account_id': inv.account_id.id,
                    'date_maturity': inv.date_due,
                    'amount_currency': diff_currency and total_currency,
                    'currency_id': diff_currency and inv.currency_id.id,
                    'invoice_id': inv.id
                })
            part = self.env['res.partner']._find_accounting_partner(inv.partner_id)
            line = [(0, 0, self.line_get_convert(l, part.id)) for l in iml]
            line = inv.group_lines(iml, line)

            journal = inv.journal_id.with_context(ctx)
            line = inv.finalize_invoice_move_lines(line)

            date = inv.date or date_invoice
            move_vals = {
                'ref': inv.reference,
                'line_ids': line,
                'journal_id': journal.id,
                'date': date,
                'narration': inv.comment,
                'account_id': inv.account_id.id,
                'team_id': inv.team_id.id,
            }
            ctx['company_id'] = inv.company_id.id
            ctx['invoice'] = inv
            ctx_nolang = ctx.copy()
            ctx_nolang.pop('lang', None)
            move = account_move.with_context(ctx_nolang).create(move_vals)
            # Pass invoice in context in method post: used if you want to get the same
            # account move reference when creating the same invoice after a cancelled one:
            move.post()
            # make the invoice point to that move
            vals = {
                'move_id': move.id,
                'date': date,
                'move_name': move.name,
            }
            inv.with_context(ctx).write(vals)
        return True