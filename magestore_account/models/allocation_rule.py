# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo import fields
from odoo.exceptions import UserError


class AccountAllocationRule(models.Model):
    _name = "account.allocation"

    name = fields.Char('Rule', size=10, required=True)
    gt_allocation_description = fields.Char('Description', size=200)
    gt_allocation_journal = fields.Many2one('account.journal', 'Journal', required=True)
    gt_allocation_method = fields.Selection([
        ('percent', "Percent"),
        ('amount', "Amount")
    ], string="Method", required=True)
    gt_allocation_active = fields.Boolean(default=True, string="Active")
    gt_allocation_source_chart_account = fields.Many2many('account.account', 'account_source_account_account_rel', 'account_source_id', 'account_account_id', string='Chart of Account')
    gt_allocation_desti_type = fields.Selection([
        ('ledger', "Ledger")
    ], string="Type", required=True, default='ledger')
    chart_line_ids = fields.One2many('allocation.chart.line', 'allocation_chart_id', string='Chart of Account Items', copy=True)
    dime_line_ids = fields.One2many('allocation.dime.line', 'allocation_dime_id', string='Store Dimension Items', copy=True)
    company_id = fields.Many2one('res.company', related='gt_allocation_journal.company_id', string='Company', store=True, readonly=True, default=lambda self: self.env.user.company_id)

    @api.model
    def create(self, values):
        if values.get('gt_allocation_desti_type') == 'ledger':
            if values.get('dime_line_ids'):
                values['dime_line_ids'] = []
        if values.get('gt_allocation_desti_type') == 'dimension':
            if values.get('chart_line_ids'):
                values['chart_line_ids'] = []
        res = super(AccountAllocationRule,self).create(values)
        return res

    @api.multi
    @api.depends('name', 'gt_allocation_description')
    def name_get(self):
        result = []
        for allocation in self:
            name = allocation.name + ' [' + allocation.gt_allocation_description + ']'
            result.append((allocation.id, name))
        return result

class AllocationRuleChartLine(models.Model):
    _name = "allocation.chart.line"

    allocation_chart_id = fields.Many2one('account.allocation', string='Chart of Account', required=True, index=True, ondelete="cascade")
    account_id = fields.Many2one('account.account', string='Code/Name')
    amount = fields.Float(string='Amount')


class AllocationRuleDimeLine(models.Model):
    _name = "allocation.dime.line"

    allocation_dime_id = fields.Many2one('account.allocation', string='Store Dimension', required=True, index=True, ondelete="cascade")
    dimension_id = fields.Many2one('store.dimension', string='Store Dimension')
    description = fields.Text(related='dimension_id.description', string='Description')
    amount = fields.Float(string='Amount')


class AllocationProcess(models.Model):
    _name = "allocation.process"

    name = fields.Char(related='allocation_id.name', String="Process Name")
    allocation_id = fields.Many2one('account.allocation', string="Allocation Name", required=True, states={'processed': [('readonly', True)]})
    allocation_description = fields.Char(related='allocation_id.gt_allocation_description', string="Allocation Description", size=200, readonly=True)
    from_date = fields.Date(string="From Date", states={'processed': [('readonly', True)]})
    to_date = fields.Date(string="To Date", states={'processed': [('readonly', True)]})
    posting = fields.Selection([
        ('draft', "Unposted"),
        ('posted', "Posted")
    ], string="Posting", required=True, states={'processed': [('readonly', True)]})
    posting_date = fields.Date(string="GL Posting Date", required=True, states={'processed': [('readonly', True)]})
    state = fields.Selection([('draft', 'Draft'), ('processed', 'Processed')], default='draft', string="Status")

    @api.multi
    def process(self):
        allocation_model = self.env['account.allocation']
        chart_model = self.env['allocation.chart.line']
        dime_Model = self.env['allocation.dime.line']
        moveline_model = self.env['account.move.line']
        journal_id = allocation_model.browse(self.allocation_id.id).gt_allocation_journal
        allocation_method = allocation_model.browse(self.allocation_id.id).gt_allocation_method
        source_account = allocation_model.browse(self.allocation_id.id).gt_allocation_source_chart_account
        destination_type = allocation_model.browse(self.allocation_id.id).gt_allocation_desti_type
        posting_type = self.posting
        from_date = self.from_date or 0
        to_date = self.to_date or 0
        posting_date = self.posting_date or ''
        destination_account = []
        if destination_type == 'ledger':
            destination_account = chart_model.search([('allocation_chart_id', '=', self.allocation_id.id)])
        # if destination_type == 'dimension':
        #     destination_account = dime_Model.search([('allocation_dime_id', '=', self.allocation_id.id)])
        total_credit = 0
        total_debit = 0
        total_move_credit = 0
        total_move_debit = 0
        line_ids = []
        # truong hop 1 source - nhieu destination
        if len(source_account) == 1:
            if len(destination_account) > 1:
                for destination_amount in destination_account:
                    if destination_amount.amount == 0:
                        raise UserError(_('Amount can not be 0.(one - many/one)'))
            journal_items = moveline_model.search([('account_id', '=', source_account.id), ('move_id.state', '=', 'posted')])
            for journal_item in journal_items:
                if from_date != 0 and to_date != 0:
                    if from_date <= journal_item.date and to_date >= journal_item.date:
                        total_credit += journal_item.credit
                        total_debit += journal_item.debit
                elif from_date != 0 and to_date == 0:
                    if from_date <= journal_item.date:
                        total_credit += journal_item.credit
                        total_debit += journal_item.debit
                elif from_date == 0 and to_date != 0:
                    if to_date >= journal_item.date:
                        total_credit += journal_item.credit
                        total_debit += journal_item.debit
                else:
                    total_credit += journal_item.credit
                    total_debit += journal_item.debit
            if total_credit <= 0 or total_debit <= 0:
                raise UserError(_('Debit or Credit of source\'s amount is not enough.(one - many/one)'))
            destination_total_amount = 0
            if allocation_method == 'amount':
                if len(destination_account) == 1 and destination_account.amount == 0:
                    total_move_debit = total_debit
                    total_move_credit = total_credit
                    line_ids.append((0, False, {
                        'account_id': destination_account.account_id.id,
                        'debit': 0,
                        'credit': total_debit,
                        'name': 'allocation process destination'
                    }))
                    line_ids.append((0, False, {
                        'account_id': destination_account.account_id.id,
                        'debit': total_credit,
                        'credit': 0,
                        'name': 'allocation process destination'
                    }))
                if len(destination_account) > 1:
                    for destination_amount in destination_account:
                        destination_total_amount += destination_amount.amount
                    if destination_total_amount > total_debit or destination_total_amount > total_credit:
                        raise UserError(_('Debit or Credit amount is not enough.(one - many/one)'))
                    for destination_amount in destination_account:
                        total_move_debit += destination_amount.amount
                        total_move_credit += destination_amount.amount
                        line_ids.append((0, False, {
                            'account_id': destination_amount.account_id.id,
                            'debit': 0,
                            'credit': destination_amount.amount,
                            'name': 'allocation process destination'
                        }))
                        line_ids.append((0, False, {
                            'account_id': destination_amount.account_id.id,
                            'debit': destination_amount.amount,
                            'credit': 0,
                            'name': 'allocation process destination'
                        }))
            if allocation_method == 'percent':
                debit = 0
                credit = 0
                if len(destination_account) == 1 and destination_account.amount == 0:
                    total_move_debit = total_debit
                    total_move_credit = total_credit
                    line_ids.append((0, False, {
                        'account_id': destination_account.account_id.id,
                        'debit': 0,
                        'credit': total_debit,
                        'name': 'allocation process destination'
                    }))
                    line_ids.append((0, False, {
                        'account_id': destination_account.account_id.id,
                        'debit': total_credit,
                        'credit': 0,
                        'name': 'allocation process destination'
                    }))
                if len(destination_account) > 1:
                    for destination in destination_account:
                        debit = total_credit * destination.amount / 100
                        credit = total_debit * destination.amount / 100
                        total_move_debit += credit
                        total_move_credit += debit
                        line_ids.append((0, False, {
                            'account_id': destination.account_id.id,
                            'debit': 0,
                            'credit': credit,
                            'name': 'allocation process destination'
                        }))
                        line_ids.append((0, False, {
                            'account_id': destination.account_id.id,
                            'debit': debit,
                            'credit': 0,
                            'name': 'allocation process destination'
                        }))
            line_ids.append((0, False, {
                'account_id': source_account.id,
                'debit': 0,
                'credit': total_move_credit,
                'name': 'allocation process source'
            }))
            line_ids.append((0, False, {
                'account_id': source_account.id,
                'debit': total_move_debit,
                'credit': 0,
                'name': 'allocation process source'
            }))
        # truong hop nhieu source - 1 destination
        elif len(source_account) > 1 and len(destination_account) == 1:
            for source in source_account:
                journal_items = moveline_model.search([('account_id', '=', source.id), ('move_id.state', '=', 'posted')])
                each_total_credit = 0
                each_total_debit = 0
                for journal_item in journal_items:
                    if from_date != 0 and to_date != 0:
                        if from_date <= journal_item.date and to_date >= journal_item.date:
                            each_total_credit += journal_item.credit
                            each_total_debit += journal_item.debit
                    elif from_date != 0 and to_date == 0:
                        if from_date <= journal_item.date:
                            each_total_credit += journal_item.credit
                            each_total_debit += journal_item.debit
                    elif from_date == 0 and to_date != 0:
                        if to_date >= journal_item.date:
                            each_total_credit += journal_item.credit
                            each_total_debit += journal_item.debit
                    else:
                        each_total_credit += journal_item.credit
                        each_total_debit += journal_item.debit
                if allocation_method == 'amount':
                    if destination_account.amount == 0:
                        if each_total_credit > 0:
                            total_credit += each_total_credit
                            line_ids.append((0, False, {
                                'account_id': source.id,
                                'debit': each_total_credit,
                                'credit': 0,
                                'name': 'allocation process source'
                            }))
                        if each_total_debit > 0:
                            total_debit += each_total_debit
                            line_ids.append((0, False, {
                                'account_id': source.id,
                                'debit': 0,
                                'credit': each_total_debit,
                                'name': 'allocation process source'
                            }))
                    if destination_account.amount != 0:
                        if each_total_debit < destination_account.amount or each_total_credit < destination_account.amount:
                            raise UserError(_('Debit or Credit amount is not enough.(many - one)'))
                        total_credit += each_total_credit
                        total_debit += each_total_debit
                        line_ids.append((0, False, {
                            'account_id': source.id,
                            'debit': destination_account.amount,
                            'credit': 0,
                            'name': 'allocation process source'
                        }))
                        line_ids.append((0, False, {
                            'account_id': source.id,
                            'debit': 0,
                            'credit': destination_account.amount,
                            'name': 'allocation process source'
                        }))
                if allocation_method == 'percent':
                    if destination_account.amount == 0:
                        if each_total_credit > 0:
                            total_credit += each_total_credit
                            line_ids.append((0, False, {
                                'account_id': source.id,
                                'debit': each_total_credit,
                                'credit': 0,
                                'name': 'allocation process source'
                            }))
                        if each_total_debit > 0:
                            total_debit += each_total_debit
                            line_ids.append((0, False, {
                                'account_id': source.id,
                                'debit': 0,
                                'credit': each_total_debit,
                                'name': 'allocation process source'
                            }))
                    if destination_account.amount != 0:
                        if each_total_credit > 0:
                            total_credit += each_total_credit * destination_account.amount / 100
                            line_ids.append((0, False, {
                                'account_id': source.id,
                                'debit': each_total_credit * destination_account.amount / 100,
                                'credit': 0,
                                'name': 'allocation process source'
                            }))
                        if each_total_debit > 0:
                            total_debit += each_total_debit * destination_account.amount / 100
                            line_ids.append((0, False, {
                                'account_id': source.id,
                                'debit': 0,
                                'credit': each_total_debit * destination_account.amount / 100,
                                'name': 'allocation process source'
                            }))
            # if allocation_method == 'amount':
            #     line_ids.append((0, False, {
            #         'account_id': destination_account.account_id.id,
            #         'debit': 0,
            #         'credit': destination_account.amount * len(source_account),
            #         'name': 'allocation process destination'
            #     }))
            #     line_ids.append((0, False, {
            #         'account_id': destination_account.account_id.id,
            #         'debit': destination_account.amount * len(source_account),
            #         'credit': 0,
            #         'name': 'allocation process destination'
            #     }))
            # if allocation_method == 'percent':
            line_ids.append((0, False, {
                'account_id': destination_account.account_id.id,
                'debit': 0,
                'credit': total_credit,
                'name': 'allocation process destination'
            }))
            line_ids.append((0, False, {
                'account_id': destination_account.account_id.id,
                'debit': total_debit,
                'credit': 0,
                'name': 'allocation process destination'
            }))
        else:
            raise UserError(_('Can only process when allocation rule is many source - one destination OR one source - one destination OR one source - many destination.'))
        account_move_param = {
            'name': journal_id.sequence_id.next_by_id(),
            'journal_id': journal_id.id,
            'date': posting_date,
            'line_ids':line_ids,
            'state': posting_type
        }
        account_move = self.env['account.move'].create(account_move_param)
        return self.write({
            'state': 'processed'
        })
