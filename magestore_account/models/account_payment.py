# -*- coding: utf-8 -*-

import math
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from . import num_to_word_vn
from odoo.tools import float_round, amount_to_text_en



class account_payment(models.Model):
    """Added By Adam
    """
    _inherit = "account.payment"

    check_amount_in_words_vn = fields.Char(string="Amount in Words in Vietnamese", compute="_onchange_amount_to_word_vn")
    payment_date_text = fields.Char(compute="_get_payment_date")
    payment_purpose = fields.Char(size=200, String="Payment Purpose")
    receipt_purpose = fields.Char(size=200, String="Receipt Purpose")

    @api.depends('amount')
    def _onchange_amount_to_word_vn(self):
        # TODO: merge, refactor and complete the amount_to_text and amount_to_text_en classes
        for item in self:
            check_amount_in_words_vn = num_to_word_vn.Num2Word_VN().number_to_text(math.floor(item.amount))
            if math.floor(item.amount) > 0:
                if math.floor(item.amount) % 1000 == 0:
                    check_amount_in_words_vn = check_amount_in_words_vn + u" đồng chẵn."
                else:
                    check_amount_in_words_vn = check_amount_in_words_vn + u" đồng."
            item.check_amount_in_words_vn = check_amount_in_words_vn.capitalize()
            self.write({'check_amount_in_words_vn': item.check_amount_in_words_vn})
            # print check_amount_in_words_vn
            # print item.check_amount_in_words_vn

    def _get_payment_date(self):
        for item in self:
            create_date_arr = item.payment_date.split(" ")[0].split("-")
            item.payment_date_text = "Ngày " + create_date_arr[2] + " tháng " + create_date_arr[1] + " năm " + create_date_arr[0]

    def _get_partner_address(self):
        """Added by Adam to process the partner's address in reports"""
        partner_address = ""
        if self.partner_id.street:
            partner_address = partner_address + self.partner_id.street
        if self.partner_id.street2:
            partner_address = partner_address + ", " + self.partner_id.street2
        if self.partner_id.city:
            partner_address = partner_address + ", " + self.partner_id.city
        # if item.country_id:
        #     item.partner_address = item.partner_address + ", " + item.country_id.name
        return partner_address

    def _get_company_address(self):
        """Added by Adam to process the company's address in reports"""
        company_address = ""
        if self.company_id.street:
            company_address = company_address + self.company_id.street
        if self.company_id.street2:
            company_address = company_address + ", " + self.company_id.street2
        if self.company_id.city:
            company_address = company_address + ", " + self.company_id.city
        # if self.company_id.country_id:
        #     company_address = company_address + ", " + self.company_id.country_id.name
        return company_address