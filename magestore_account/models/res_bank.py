# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class ResBank(models.Model):
    _inherit = "res.bank"

    bank_address = fields.Char("Bank Address", size=255, compute="_get_bank_address", store=True)

    @api.depends("street", "street2", "city", "country")
    def _get_bank_address(self):
        for item in self:
            item.bank_address = ""
            if item.street:
                item.bank_address = item.bank_address + item.street
            if item.street2:
                item.bank_address = item.bank_address + ", " + item.street2
            if item.city:
                item.bank_address = item.bank_address + ", " + item.city
            # if item.country:
            #     item.bank_address = item.bank_address + ", " + item.country.name
