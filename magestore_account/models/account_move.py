# -*- coding: utf-8 -*-

import math
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError
from . import num_to_word_vn
from odoo.tools import float_round, amount_to_text_en
from odoo.tools.misc import formatLang


class AccountMove(models.Model):
    _inherit = 'account.move'

    line_ids_02 = fields.One2many('account.move.line', 'move_id', string='Journal Items',
                                  states={'posted': [('readonly', True)]})

    invoice_date = fields.Date('Invoice date', help='')
    invoice_form = fields.Char('Invoice form', size=20, help='')
    invoice_serial = fields.Char('Invoice serial', size=20, help='')
    invoice_number = fields.Integer('Invoice number', size=10, help='')
    invoice_description = fields.Char('Invoice description', size=200, help='')
    vendor_name = fields.Char('Vendor name', size=200, help='')
    organization_tax = fields.Char('Organization tax', size=20, help='')

    # Added by Adam: convert date field into string
    payment_date_text = fields.Char(compute="_get_payment_date")

    # Added by Adam: GT_G.GL.03_FDD_AddFiedInJournalEntries_v0.2 - MH02 – Màn hình customize thông tin in chứng từ
    person_name = fields.Char(Sze=200, String="Person Name")
    person_address = fields.Char(Size=200, String="Person Address")
    description = fields.Text(String="Description")
    check_amount_in_words_vn = fields.Char(string="Amount in Words in Vietnamese",
                                           compute="_onchange_amount_to_word_vn")

    # account_id = fields.Many2one('account.account', string='Account', required=True,
    #                              domain=[('deprecated', '=', False)])
    team_id = fields.Many2one('crm.team', string='Sales Team')

    # Michael: edit dummy_account_id to group
    # dummy_account_id = fields.Many2one('account.account', related='line_ids.account_id', string='Account', store=True)

    @api.depends('amount')
    def _onchange_amount_to_word_vn(self):
        for item in self:
            check_amount_in_words_vn = num_to_word_vn.Num2Word_VN().number_to_text(math.floor(item.amount))
            if math.floor(item.amount) > 0:
                if math.floor(item.amount) % 1000 == 0:
                    check_amount_in_words_vn = check_amount_in_words_vn + u" đồng chẵn."
                else:
                    check_amount_in_words_vn = check_amount_in_words_vn + u" đồng."
            item.check_amount_in_words_vn = check_amount_in_words_vn.capitalize()
            self.write({'check_amount_in_words_vn': item.check_amount_in_words_vn})
            # print check_amount_in_words_vn
            # print item.check_amount_in_words_vn

    def _get_payment_date(self):
        for item in self:
            create_date_arr = item.date.split(" ")[0].split("-")
            item.payment_date_text = "Ngày " + create_date_arr[2] + " tháng " + create_date_arr[1] + " năm " + \
                                     create_date_arr[0]

    def _get_partner_address(self):
        """Added by Adam to process the partner's address in reports"""
        partner_address = ""
        if self.partner_id.street:
            partner_address = partner_address + self.partner_id.street
        if self.partner_id.street2:
            partner_address = partner_address + ", " + self.partner_id.street2
        if self.partner_id.city:
            partner_address = partner_address + ", " + self.partner_id.city
        # if item.country_id:
        #     item.partner_address = item.partner_address + ", " + item.country_id.name
        return partner_address

    def _get_company_address(self):
        """Added by Adam to process the company's address in reports"""
        company_address = ""
        if self.company_id.street:
            company_address = company_address + self.company_id.street
        if self.company_id.street2:
            company_address = company_address + ", " + self.company_id.street2
        if self.company_id.city:
            company_address = company_address + ", " + self.company_id.city
        # if self.company_id.country_id:
        #     company_address = company_address + ", " + self.company_id.country_id.name
        return company_address


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    # move_id = fields.Many2one('account.move', string='Journal Entry', ondelete="cascade", help="The move of this entry line.", index=True, required=True, auto_join=True)

    invoice_date = fields.Date('Invoice date', help='')
    invoice_form = fields.Char('Invoice form', size=20, help='')
    invoice_serial = fields.Char('Invoice serial', size=20, help='')
    invoice_number = fields.Integer('Invoice number', size=10, help='')
    invoice_description = fields.Char('Invoice description', size=200, help='')
    vendor_name = fields.Char('Vendor name', size=200, help='')
    organization_tax = fields.Char('Organization tax', size=20, help='')

    name = fields.Char(required=False, string="Label")

    # Added By Adam to filter the order by pos
    # @api.model
    # def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
    #     res = super(AccountMoveLine, self).search_read(domain, fields, offset, limit, order)
    #     if 'payment_id' in domain[0]:
    #         res = super(AccountMoveLine, self).search_read(domain, fields, offset, limit, order='debit desc')
    #     return res
    # user_config_ids = self.env.user.pos_config_ids.ids
    # data = []
    # for item in res:
    #     config_id = item.get('config_id')
    #     if config_id[0] in user_config_ids:
    #         data.append(item)
    # return data

    # Michael: fix bug res.partner read
    @api.model
    def get_data_for_manual_reconciliation_widget(self, partner_ids, account_ids):
        """ Returns the data required for the invoices & payments matching of partners/accounts.
            If an argument is None, fetch all related reconciliations. Use [] to fetch nothing.
        """
        return {
            'customers': self.sudo().get_data_for_manual_reconciliation('partner', partner_ids, 'receivable'),
            'suppliers': self.sudo().get_data_for_manual_reconciliation('partner', partner_ids, 'payable'),
            'accounts': self.sudo().get_data_for_manual_reconciliation('account', account_ids),
        }

    @api.multi
    def prepare_move_lines_for_reconciliation_widget(self, target_currency=False, target_date=False):
        """ Returns move lines formatted for the manual/bank reconciliation widget

            :param target_currency: currency (browse_record or ID) you want the move line debit/credit converted into
            :param target_date: date to use for the monetary conversion
        """
        context = dict(self._context or {})
        ret = []

        if target_currency:
            # re-browse in case we were passed a currency ID via RPC call
            target_currency = self.env['res.currency'].browse(int(target_currency))

        for line in self:
            if type(line.move_id.name) is not bool and line.name and line.name != '/':
                temp = line.move_id.name + ': ' + line.name
            else:
                temp = line.move_id.name

            company_currency = line.account_id.company_id.currency_id
            ret_line = {
                'id': line.id,
                'name': temp,
                'ref': line.move_id.ref or '',
                # For reconciliation between statement transactions and already registered payments (eg. checks)
                # NB : we don't use the 'reconciled' field because the line we're selecting is not the one that gets reconciled
                'already_paid': line.account_id.internal_type == 'liquidity',
                'account_code': line.account_id.code,
                'account_name': line.account_id.name,
                'account_type': line.account_id.internal_type,
                'date_maturity': line.date_maturity,
                'date': line.date,
                'journal_name': line.journal_id.name,
                'partner_id': line.partner_id.id,
                'partner_name': line.partner_id.name,
                'currency_id': (line.currency_id and line.amount_currency) and line.currency_id.id or False,
            }

            debit = line.debit
            credit = line.credit
            amount = line.amount_residual
            amount_currency = line.amount_residual_currency

            # For already reconciled lines, don't use amount_residual(_currency)
            if line.account_id.internal_type == 'liquidity':
                amount = abs(debit - credit)
                amount_currency = abs(line.amount_currency)

            # Get right debit / credit:
            target_currency = target_currency or company_currency
            line_currency = (line.currency_id and line.amount_currency) and line.currency_id or company_currency
            amount_currency_str = ""
            total_amount_currency_str = ""
            if line_currency != company_currency and target_currency == line_currency:
                # The payment currency is the invoice currency, but they are different than the company currency
                # We use the `amount_currency` computed during the invoice validation, at the invoice date
                # to avoid exchange gain/loss
                # e.g. an invoice of 100€ must be paid with 100€, whatever the company currency and the exchange rates
                total_amount = line.amount_currency
                actual_debit = debit > 0 and amount_currency or 0.0
                actual_credit = credit > 0 and -amount_currency or 0.0
                currency = line_currency
            else:
                # Either:
                #  - the invoice, payment, company currencies are all the same,
                #  - the payment currency is the company currency, but the invoice currency is different,
                #  - the invoice currency is the company currency, but the payment currency is different,
                #  - the invoice, payment and company currencies are all different.
                # For the two first cases, we can simply use the debit/credit of the invoice move line, which are always in the company currency,
                # and this is what the target need.
                # For the two last cases, we can use the debit/credit which are in the company currency, and then change them to the target currency
                total_amount = abs(debit - credit)
                actual_debit = debit > 0 and amount or 0.0
                actual_credit = credit > 0 and -amount or 0.0
                currency = company_currency
            if line_currency != target_currency:
                amount_currency_str = formatLang(self.env, abs(actual_debit or actual_credit),
                                                 currency_obj=line_currency)
                total_amount_currency_str = formatLang(self.env, total_amount, currency_obj=line_currency)
            if currency != target_currency:
                ctx = context.copy()
                ctx.update({'date': target_date or line.date})
                total_amount = currency.with_context(ctx).compute(total_amount, target_currency)
                actual_debit = currency.with_context(ctx).compute(actual_debit, target_currency)
                actual_credit = currency.with_context(ctx).compute(actual_credit, target_currency)
            amount_str = formatLang(self.env, abs(actual_debit or actual_credit), currency_obj=target_currency)
            total_amount_str = formatLang(self.env, total_amount, currency_obj=target_currency)

            ret_line['debit'] = abs(actual_debit)
            ret_line['credit'] = abs(actual_credit)
            ret_line['amount_str'] = amount_str
            ret_line['total_amount_str'] = total_amount_str
            ret_line['amount_currency_str'] = amount_currency_str
            ret_line['total_amount_currency_str'] = total_amount_currency_str
            ret.append(ret_line)
        return ret
