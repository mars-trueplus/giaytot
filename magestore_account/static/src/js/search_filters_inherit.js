odoo.define('magestore_account.search_filters', function (require) {
    "use strict";
    var search_filters = require('web.search_filters');
    var ExtendedSearchPropositionInherit = search_filters.ExtendedSearchProposition.include({
        start: function () {
            var href = location.href;
            var model = href.substring(href.lastIndexOf('model'));
            var model_name = model.substring(model.indexOf('=')+1, model.indexOf('&'))
            switch(model_name){
                case 'account.invoice':
                    $('.o_searchview_extended_prop_field').val('invoice_date');
                    break;

                case 'account.move':
                    $('.o_searchview_extended_prop_field').val('date');
                    break;
            }
            return this._super().done(this.proxy('changed'));
        },
    });
});