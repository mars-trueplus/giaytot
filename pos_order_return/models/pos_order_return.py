# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
#################################################################################
from odoo.tools.translate import _
import time
import logging
from odoo.tools import float_is_zero
from datetime import datetime
from odoo import netsvc
from odoo import api, fields,models
from odoo.exceptions import UserError, Warning, RedirectWarning
_logger = logging.getLogger(__name__)

class PosRefundPopup(models.TransientModel):
    _name = "pos.refund.popup"

    refund_reason = fields.Text(string='Refund Reason', required=True)

    @api.multi
    def create_refund(self):
        order_id = self.env.context.get('order_id')
        session_id = self.env.context.get('session_id')
        data = self.read()[0]
        reason_to_refund = 'Reason refund: ' + data['refund_reason']

        order = self.env['pos.order'].search([('id', '=', order_id)], limit=1)

        # PosOrder = self.env['pos.order']

        PosOrder = order.copy({
            # ot used, name forced by create
            'name': order.name + _(' REFUND'),
            # 'session_id': current_session.id,
            'session_id': session_id,
            'date_order': fields.Datetime.now(),
            'pos_reference': order.pos_reference,
            'note': reason_to_refund,
            # 'source_order': order_id,
            'return_order_id': order_id,
            'is_return_order': True
        })
        # PosOrder = clone
        # for clone in PosOrder:
        for order_line in PosOrder.lines:
			if order_line.line_qty_returned >= order_line.qty:
				order_line.unlink()

        count = 0
        for old_order_line in order.lines:
            if old_order_line.line_qty_returned < old_order_line.qty:
				PosOrder.lines[count].write({
                    'qty': -(old_order_line.qty - old_order_line.line_qty_returned),
                    'product_id': old_order_line.product_id.id,
                    'price_unit': old_order_line.price_unit,
                    'discount': old_order_line.discount,
                    'original_line_id': old_order_line.id,
                })
				old_order_line.write({'line_qty_returned': old_order_line.qty})
				count = count + 1

        self.check_order_return_status(order)

        return {
            'name': _('Return Products'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'pos.order',
            'res_id': PosOrder.ids[0],
            'view_id': False,
            'context': self.env.context,
            'type': 'ir.actions.act_window',
            'target': 'current',
        }

    @api.multi
    def check_order_return_status(self, order):
		order_line = order.lines

		return_order = False
		no_return = False
		return_status = '-'

		for line in order_line:
			if line.line_qty_returned > 0:
				return_order = True
				if line.line_qty_returned < line.qty:
					return_status = 'Partially-Returned'
			else:
				no_return = True

		if return_order == True and return_status != 'Partially-Returned' and no_return == False:
			return_status = 'Fully-Returned'
		if return_order == True and no_return == True:
			return_status = 'Partially-Returned'
		order.write({'return_status': return_status})

class ProductTemplate(models.Model):
	_inherit = 'product.template'

	not_returnable = fields.Boolean('Not Returnable')

class PosOrder(models.Model):
	_inherit = 'pos.order'

	source_order = fields.Many2one('pos.order', string='Source Order', index=True, readonly=True)

	is_return_order = fields.Boolean(string='Return Order',copy=False)
	return_order_id = fields.Many2one('pos.order','Return Order Of',readonly=True,copy=False)
	return_status = fields.Selection([('-','Not Returned'),('Fully-Returned','Trả hết hàng'),('Partially-Returned','Trả một phần'),('Non-Returnable','Không thể trả hàng')],default='-',copy=False,string='Return Status')

	@api.model
	def _process_order(self, pos_order):
		prec_acc = self.env['decimal.precision'].precision_get('Account')
		pos_session = self.env['pos.session'].browse(pos_order['pos_session_id'])
		if pos_session.state == 'closing_control' or pos_session.state == 'closed':
			pos_order['pos_session_id'] = self._get_valid_session(pos_order).id
		if pos_order['is_return_order']:
			pos_order['amount_paid'] = 0
			for line in pos_order['lines']:
				line_dict = line[2]
				line_dict['qty'] = line_dict['qty'] * 1
				original_line = self.env['pos.order.line'].browse(line_dict['original_line_id'])
				if line_dict['qty'] < 0:
					original_line.line_qty_returned += abs(line_dict['qty'])
			for statement in pos_order['statement_ids']:
				statement_dict = statement[2]
				statement_dict['amount'] = statement_dict['amount'] * 1
			pos_order['amount_tax'] = pos_order['amount_tax'] * 1
			# pos_order['amount_return'] = 0
			pos_order['amount_total'] = pos_order['amount_total'] * 1

		order = self.create(self._order_fields(pos_order))
		journal_ids = set()
		for payments in pos_order['statement_ids']:
			if not float_is_zero(payments[2]['amount'], precision_digits=prec_acc):
				order.add_payment(self._payment_fields(payments[2]))
			journal_ids.add(payments[2]['journal_id'])

		if pos_session.sequence_number <= pos_order['sequence_number']:
			pos_session.write({'sequence_number': pos_order['sequence_number'] + 1})
			pos_session.refresh()

		if not float_is_zero(pos_order['amount_return'], prec_acc):
			cash_journal_id = pos_session.cash_journal_id.id
			#payment to return
			if pos_order['amount_total'] < 0 and journal_ids > 0 and len(pos_order['statement_ids']) > 0:
				cash_journal_id = payments[2]['journal_id']

			if not cash_journal_id:
				# Select for change one of the cash journals used in this
				# payment
				cash_journal = self.env['account.journal'].search([
					('type', '=', 'cash'),
					('id', 'in', list(journal_ids)),
				], limit=1)
				if not cash_journal:
					# If none, select for change one of the cash journals of the POS
					# This is used for example when a customer pays by credit card
					# an amount higher than total amount of the order and gets cash back
					cash_journal = [statement.journal_id for statement in pos_session.statement_ids if statement.journal_id.type == 'cash']
					if not cash_journal:
						raise UserError(_("No cash statement found for this session. Unable to record returned cash."))
				cash_journal_id = cash_journal[0].id
			# pos_order['amount_return'] = pos_order['amount_return'] if pos_order['amount_total'] > 0 else -pos_order['amount_return']
			order.add_payment({
				'amount': -pos_order['amount_return'],
				'payment_date': fields.Datetime.now(),
				'payment_name': _('return'),
				'journal': cash_journal_id,
			})
		return order

	@api.model
	def _order_fields(self,ui_order):
		fields_return = super(PosOrder,self)._order_fields(ui_order)
		fields_return.update({
			'is_return_order':ui_order.get('is_return_order') or False,
			'return_order_id':ui_order.get('return_order_id') or False,
			'return_status':ui_order.get('return_status') or False,
			})
		return fields_return

	@api.multi
	def refund(self):

		"""Create a copy of order  for refund order"""
		current_session = self.env['pos.session'].search([('state', '!=', 'closed'), ('user_id', '=', self.env.uid)],
														 limit=1)

		if not current_session:
			raise UserError(
				_('To return product(s), you need to open a session that will be used to register the refund.'))

		session_id = 0
		ordered_location_id = self.location_id.id
		for session in current_session:
			if ordered_location_id == session.config_id.stock_location_id.id:
				session_id = current_session.id
				break
		if session_id == 0:
			raise UserError(_('You cannot return products because POS is closed'))

		ir_model_data = self.env['ir.model.data']
		compose_form_id = ir_model_data.get_object_reference('pos_order_return', 'view_pos_refund_product')[1]

		ctx = dict({
			'order_id': self.id,
			'session_id': session_id
		})

		# popup to fill refund reason
		return {
			'name': _('Refund Product'),
			'type': 'ir.actions.act_window',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'pos.refund.popup',
			'views': [(compose_form_id, 'form')],
			'view_id': compose_form_id,
			'target': 'new',
			'context': ctx,
		}


class PosOrderLine(models.Model):
    _inherit = 'pos.order.line'

    line_qty_returned = fields.Integer('Line Returned', default=0)
    original_line_id = fields.Many2one('pos.order.line', "Original line")

    @api.multi
    def write(self, vals):
		original_line_id = self.original_line_id.id
		pos_order_line = super(PosOrderLine, self).write(vals)
		if original_line_id:
			return_order_lines = self.env['pos.order.line'].search([('original_line_id', '=', original_line_id)])
			qty_returned = 0
			for return_order_line in return_order_lines:
				if return_order_line.qty < 0:
					qty_returned = qty_returned + return_order_line.qty

			order_line = self.env['pos.order.line'].browse(original_line_id)
			order_line.write({'line_qty_returned': -qty_returned})
			order = self.env['pos.order'].search([('id', '=', order_line.order_id.id)], limit=1)
			self.env['pos.refund.popup'].check_order_return_status(order)

    @api.model
    def unlink(self):
		order_id = self.order_id
		original_line_id = self.original_line_id.id
		super(PosOrderLine, self).unlink()
		if original_line_id:
			return_order_lines = self.env['pos.order.line'].search([('original_line_id', '=', original_line_id)])
			qty_returned = 0
			for return_order_line in return_order_lines:
				if return_order_line.qty < 0:
					qty_returned = qty_returned + return_order_line.qty
			order_line = self.env['pos.order.line'].browse(original_line_id)
			order_line.write({'line_qty_returned': -qty_returned})
			order = self.env['pos.order'].search([('id', '=', order_line.order_id.id)], limit=1)
			self.env['pos.refund.popup'].check_order_return_status(order)


    @api.model
    def _order_line_fields(self,line):
        fields_return = super(PosOrderLine,self)._order_line_fields(line)
        fields_return[2].update({'line_qty_returned':line[2].get('line_qty_returned','')})
        fields_return[2].update({'original_line_id':line[2].get('original_line_id','')})
        return fields_return