# -*- coding: utf-8 -*-

{
    'name': 'Export Product',
    'version': '1.0.0',
    'category': 'Tools',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Export Product',
    'description': """""",
    'depends': [
        'stock'
    ],
    'data': [
        'wizard/export_product_view.xml',
        'views/export_stock _issuing.xml',
    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
