# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import models

import cStringIO
import xlwt

import logging
_logger = logging.getLogger(__name__)

class Picking(models.Model):
    _inherit = "stock.picking"

    def export_stock_issuing(self):
        context = self._context
        active_ids = context.get('active_ids')
        return {
            'type': 'ir.actions.act_url',
            'url': '/web/export/export_stock_issuing?active_ids=%s'%(active_ids),
            'target': 'self',
        }

    def _export_to_xls(self, active_ids):
        items = self.env['stock.pack.operation'].search([('picking_id', 'in', active_ids)])

        wb = xlwt.Workbook(encoding='utf8')
        sheet = wb.add_sheet('sheet')
        sheet.write(0, 0, "product_sku")
        sheet.write(0, 1, "product_name")
        sheet.write(0, 2, "stock_issuing_qty")
        index = 0
        products = {}
        product_ids = []
        out_put_clgt = []
        for item in items:
            if item.product_id[0].id not in product_ids:
                product = self.env['product.product'].browse(item.product_id[0])
                if product:
                    product_sku = product.id.default_code.encode('utf8') if product.id.default_code else ''
                    product_name = product.id.name_get()[0][1].encode('utf8') if len(product.id.name_get()) > 0 else ''
                    product_name = product_name.replace('[%s]' % (product_sku), '').strip()
                    products[item.product_id[0].id] = {
                        'product_sku': product_sku,
                        'product_name': product_name,
                        'stock_issuing_qty': item.qty_done,
                    }
                    product_ids.append(item.product_id[0].id)
            else:
                products[item.product_id[0].id]['stock_issuing_qty'] = products[item.product_id[0].id]['stock_issuing_qty'] + item.qty_done
        if products:
            for deophai_out_put in products:
                sheet.write(index + 1, 0, products[deophai_out_put]['product_sku'])
                sheet.write(index + 1, 1, products[deophai_out_put]['product_name'])
                sheet.write(index + 1, 2, products[deophai_out_put]['stock_issuing_qty'])
                index += 1
            # if product:
            #     product_sku = product.id.default_code.encode('utf8') if product.id.default_code else ''
            #     product_name = product.id.name_get()[0][1].encode('utf8') if len(product.id.name_get()) > 0 else ''
            #     product_name = product_name.replace('[%s]' % (product_sku), '').strip()
            #     sheet.write(index + 1, 0, product_sku)
            #     sheet.write(index + 1, 1, product_name)
            #     sheet.write(index + 1, 2, item.qty_done)
            #     index += 1

        data = cStringIO.StringIO()
        wb.save(data)
        wb.save(data)
        data.seek(0)
        out = data.read()
        data.close()

        return out