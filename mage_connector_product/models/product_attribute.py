from odoo import api, fields, models, _


class ProductAttribute(models.Model):
    _inherit = 'product.attribute'

    label = fields.Char(string="Label", required=True)
    type = fields.Selection(string='Type',
                            required=True,
                            selection=[
                                ('text', 'Text Field'),
                                ('textarea', 'Text Area'),
                                ('date', 'Date'),
                                ('boolean', 'Yes/No'),
                                ('multiselect', 'Multiple Select'),
                                ('select', 'Dropdown'),
                                ('price', 'Price'),
                                ('media_image', 'Media Image'),
                                ('weee', 'Fixed Product Tax')
                            ])

    @api.model
    def create(self, vals):
        context = self._context
        res = super(ProductAttribute, self).create(vals)
        if res:
            mapping_obj = self.env['mage.mapping.product.attribute'].search(
                [('odoo_id', '=', res.id)])
            if not mapping_obj:
                self.env['mage.mapping.product.attribute'].with_context({
                    'mage_id': context.get('mage_id', 0)
                }).create({
                    'odoo_id': res.id,
                    'state': 'waiting'
                })
        return res


class ProductAttributeValue(models.Model):
    _inherit = 'product.attribute.value'

    @api.model
    def create(self, vals):
        context = self._context
        res = super(ProductAttributeValue, self).create(vals)
        if res:
            mapping_obj = self.env['mage.mapping.product.attribute.value'].search(
                [('odoo_id', '=', res.id)])
            if not mapping_obj:
                self.env['mage.mapping.product.attribute.value'].with_context({
                    'mage_id': context.get('mage_id', 0)
                }).create({
                    'odoo_id': res.id,
                    'state': 'waiting'
                })
        return res
