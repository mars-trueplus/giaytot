from odoo import models, fields, api, tools
import json

import logging
_logger = logging.getLogger(__name__)


class MageMappingProduct(models.Model):
    _name = "mage.mapping.product"
    _inherit = "mage.mapping"

    odoo_id = fields.Many2one('product.product', string="Odoo Product")
    mapping_line_id = fields.One2many('mage.mapping.product.line', 'mapping_ids', string="Mapping Line")

    @api.model
    def create(self, vals):
        mapping_obj = super(MageMappingProduct, self.with_context({
            'model_mapping_line': 'mage.mapping.product.line',
            'mage_id': self._context.get('mage_id', 0)
        })).create(vals)

        return mapping_obj

    @api.multi
    def write(self, vals):
        mapping_obj = super(MageMappingProduct, self.with_context({
            'mapping_id': self.id,
            'model_mapping_line': 'mage.mapping.product.line'
        })).write(vals)

        return mapping_obj

    @api.multi
    def do_mapping(self):
        mapping_lines_obj = self.env['mage.mapping.product.line'].search([
            ('mapping_ids', '=', self.id),
            ('state', '!=', 'done')
        ])
        for line in mapping_lines_obj:
            product_obj = self.odoo_id
            if product_obj:
                mapping_data = {
                    'id': product_obj.id,
                    'name': product_obj.name,
                    'sku': product_obj.default_code,
                    'price': product_obj.list_price,
                    'description': product_obj.description_sale,
                    'type': product_obj.magento_product_type,
                    'attribute_set': product_obj.attribute_set_id.id,
                    'attributes': product_obj.attribute_set_id.attribute_id.ids,
                    'categories': product_obj.magento_category_ids.ids,
                    'associates': product_obj.associated_ids.ids
                }

                data = json.dumps(mapping_data)
                response = line.instance_ids.connect('odooconnector.mapping_product', [data])
                if response['status']:
                    line.write({
                        'mage_id': response['data'],
                        'state': 'done'
                    })
                else:
                    line.write({'state': 'fail'})
                    mapping_missing_obj = self.env['mage.mapping.missing'].search([
                        ('instance_id', '=', line.instance_ids.id),
                        ('mapping_id', '=', self.id),
                        ('mapping_type', '=', 'product')
                    ])
                    if not mapping_missing_obj:
                        self.env['mage.mapping.missing'].create({
                            'instance_id': line.instance_ids.id,
                            'mapping_id': self.id,
                            'mapping_type': 'product',
                            'mapping_message': response['message']})
                    else:
                        mapping_missing_obj.write({'mapping_message': response['message']})

        self.write({'state': 'mapped'})
        return True

    @api.multi
    def do_mapping_auto(self):
        try:
            mappings_obj = self.env['mage.mapping.product'].search([('state', '=', 'waiting')], limit=5)
            for mapping in mappings_obj:
                mapping.do_mapping()
            return True
        except Exception as ex:
            _logger.exception('MageMappingProduct - do_mapping_auto: %s' % tools.ustr(ex))

    # create new product when get product from Magento
    @api.model
    def create_new_product(self, product_data):
        try:
            context = self._context
            if context.get('instance_type') == 'magento':
                vals = json.loads(product_data)
                simple = False
                configurable = False
                type = vals.get('type', False)
                if type == "simple":
                    simple = True
                elif type == "configurable":
                    configurable = True

                self.env['mage.instance'].do_sync_product_magento2odoo(simple, configurable)
                return True
                # instances_obj = self.env['mage.instance'].search([])
                # for instance in instances_obj:
                #     products_data = vals
                #     mapped_data = self.env['mage.instance'].create_product(products_data)
                #     if len(mapped_data) > 0:
                #         data_json = json.dumps(mapped_data)
                #         instance.connect('odooconnector.create_product_mapping', data_json)
            return True
        except Exception as ex:
            _logger.exception('Catalog Product - Error when create new product from Magento: %s' % tools.ustr(ex))
            return 0

class MageMappingProductLine(models.Model):
    _name = "mage.mapping.product.line"
    _inherit = "mage.mapping.line"

    mage_id = fields.Integer(string="Magento Product Configurable")
