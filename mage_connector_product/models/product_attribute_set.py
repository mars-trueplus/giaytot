from odoo import api, fields, models


class ProductAttributeSet(models.Model):
    _name = "product.attribute.set"

    name = fields.Char(string="Name", required=True)
    attribute_id = fields.Many2many('product.attribute', string="Product Attribute", default=None)

    @api.model
    def create(self, vals):
        context = self._context
        res = super(ProductAttributeSet, self).create(vals)
        if res:
            mapping_obj = self.env['mage.mapping.product.attribute.set'].search(
                [('odoo_id', '=', res.id)])
            if not mapping_obj:
                self.env['mage.mapping.product.attribute.set'].with_context({
                    'mage_id': context.get('mage_id', 0)
                }).create({
                    'odoo_id': res.id,
                    'state': 'waiting'
                })
        return res
