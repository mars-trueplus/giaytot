from odoo import models, fields, api, tools
import json

import logging
_logger = logging.getLogger(__name__)


class MageMappingProductCategory(models.Model):
    _name = "mage.mapping.product.category"
    _inherit = "mage.mapping"

    odoo_id = fields.Many2one('mage.product.category', string="Odoo Product Category")
    mapping_line_id = fields.One2many('mage.mapping.product.category.line', 'mapping_ids', string="Mapping Line")

    @api.model
    def create(self, vals):
        mapping_obj = super(MageMappingProductCategory, self.with_context({
            'model_mapping_line': 'mage.mapping.product.category.line',
            'mage_id': self._context.get('mage_id', 0)
        })).create(vals)

        return mapping_obj

    @api.multi
    def write(self, vals):
        mapping_obj = super(MageMappingProductCategory, self.with_context({
            'mapping_id': self.id,
            'model_mapping_line': 'mage.mapping.product.category.line'
        })).write(vals)

        return mapping_obj

    @api.multi
    def do_mapping(self):
        mapping_lines_obj = self.env['mage.mapping.product.category.line'].search([
            ('mapping_ids', '=', self.id), ('state', '!=', 'done')
         ])
        for line in mapping_lines_obj:
            product_category_obj = self.odoo_id
            if product_category_obj:
                mapping_data = {
                    'id': product_category_obj.id,
                    'name': product_category_obj.name
                }
                data = json.dumps(mapping_data)
                response = line.instance_ids.connect('odooconnector.mapping_product_category', [data])
                if response['status']:
                    line.write({
                        'mage_id': response['data'],
                        'state': 'done'
                    })
                else:
                    line.write({'state': 'fail'})

        self.write({'state': 'mapped'})

        return True

    @api.multi
    def do_mapping_auto(self):
        try:
            mappings_obj = self.env['mage.mapping.product.category'].search([('state', '=', 'waiting')], limit=10)
            for mapping in mappings_obj:
                mapping.do_mapping()
            return True
        except Exception as ex:
            _logger.exception('Product Category - Error cron job: %s' % tools.ustr(ex))


class MageMappingProductCategoryLine(models.Model):
    _name = "mage.mapping.product.category.line"
    _inherit = "mage.mapping.line"

    mage_id = fields.Integer(string="Magento Product Category")
