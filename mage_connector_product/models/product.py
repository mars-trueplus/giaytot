from odoo import api, fields, models, _


class ProductTemplate(models.Model):
    _inherit = "product.template"

    magento_product_type = fields.Selection(string='Type',
                                            selection=[
                                                ('configurable', 'Configurable Product'),
                                                ('simple', 'Simple Product')
                                            ])
    attribute_set_id = fields.Many2one('product.attribute.set', string='Attribute Set')
    magento_category_ids = fields.Many2many('mage.product.category')
    associated_ids = fields.Many2many('product.product', domain="[('magento_product_type','=','simple')]")
    source = fields.Char(string="Source")
    allow_sync = fields.Boolean(string="Allow Sync", default=True)


class ProductProduct(models.Model):
    _inherit = 'product.product'

    @api.model
    def create(self, vals):
        context = self._context
        res = super(ProductProduct, self).create(vals)
        if res and res.allow_sync:
            mapping_obj = self.env['mage.mapping.product'].search([('odoo_id', '=', res.id)])
            if not mapping_obj:
                mapping_data = {
                    'odoo_id': res.id,
                    'status': 'waiting'
                }
                self.env['mage.mapping.product'].with_context({
                    'mage_id': context.get('mage_id', 0)
                }).create(mapping_data)
        return res

    @api.multi
    def write(self, vals):
        product_obj = super(ProductProduct, self).write(vals)
        for product in self:
            if product.allow_sync:
                mapping_obj = self.env['mage.mapping.product'].search([('odoo_id', '=', product.id)])
                if mapping_obj:
                    mapping_obj.write({'state': 'waiting'})

        return product_obj
