from odoo import models, fields, api


class MageProductCategory(models.Model):
    _name = 'mage.product.category'

    name = fields.Char(string="Name", required=True)
    # parent_id = fields.Many2one("mage.product.category", string="Category Parent")

    @api.model
    def create(self, vals):
        context = self._context
        res = super(MageProductCategory, self).create(vals)
        if res:
            mapping_obj = self.env['mage.mapping.product.category'].search(
                [('odoo_id', '=', res.id)])
            if not mapping_obj:
                self.env['mage.mapping.product.category'].with_context({
                    'mage_id': context.get('mage_id', False)
                }).create({
                        'odoo_id': res.id,
                        'state': 'waiting'
                    })
        return res
