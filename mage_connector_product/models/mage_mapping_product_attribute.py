from odoo import models, fields, api, tools
import json

import logging
_logger = logging.getLogger(__name__)


class MageMappingProductAttributeSet(models.Model):
    _name = "mage.mapping.product.attribute.set"
    _inherit = "mage.mapping"

    odoo_id = fields.Many2one('product.attribute.set', string="Odoo Product Attribute Set")
    mapping_line_id = fields.One2many('mage.mapping.product.attribute.set.line', 'mapping_ids', string="Mapping Line")

    @api.model
    def create(self, vals):
        mapping_obj = super(MageMappingProductAttributeSet, self.with_context({
            'model_mapping_line': 'mage.mapping.product.attribute.set.line',
            'mage_id': self._context.get('mage_id', 0)
        })).create(vals)

        return mapping_obj

    @api.multi
    def write(self, vals):
        mapping_obj = super(MageMappingProductAttributeSet, self.with_context({
            'mapping_id': self.id,
            'model_mapping_line': 'mage.mapping.product.attribute.set.line'
        })).write(vals)

        return mapping_obj

    @api.multi
    def do_mapping(self):
        mapping_lines_obj = self.env['mage.mapping.product.attribute.set.line'].search([
            ('mapping_ids', '=', self.id), ('state', '!=', 'done')
        ])
        for line in mapping_lines_obj:
            product_attribute_set_obj = self.odoo_id
            if product_attribute_set_obj:
                product_attribute = []
                for attribute in product_attribute_set_obj.attribute_id:
                    option = []
                    product_attribute_value_obj = self.env['product.attribute.value'].search([
                        ('attribute_id', '=', attribute.id)
                    ])
                    for value in product_attribute_value_obj:
                        option.append({
                            'id': value.id,
                            'name': value.name
                        })

                    product_attribute.append({
                        'id': attribute.id,
                        'name': attribute.name,
                        'label': attribute.label,
                        'type': attribute.type,
                        'option': option
                    })

                mapping_data = {
                    'id': product_attribute_set_obj.id,
                    'name': product_attribute_set_obj.name,
                    'attributes': product_attribute
                }
                data = json.dumps(mapping_data)
                response = line.instance_ids.connect('odooconnector.mapping_product_attribute_set', [data])
                if response['status']:
                    line.write({
                        'mage_id': response['data'],
                        'state': 'done'
                    })
                else:
                    line.write({'state': 'fail'})

        self.write({'state': 'mapped'})

        return True

    @api.multi
    def do_mapping_auto(self):
        try:
            mappings_obj = self.env['mage.mapping.product.attribute.set'].search([('state', '=', 'waiting')], limit=10)
            for mapping in mappings_obj:
                mapping.do_mapping()
            return True
        except Exception as ex:
            _logger.exception('Product Attribute Set - Error cron job: %s' % tools.ustr(ex))


class MageMappingProductAttributeSetLine(models.Model):
    _name = "mage.mapping.product.attribute.set.line"
    _inherit = "mage.mapping.line"

    mage_id = fields.Integer(string="Magento Product Attribute Set")


class MageMappingProductAttribute(models.Model):
    _name = "mage.mapping.product.attribute"
    _inherit = "mage.mapping"

    odoo_id = fields.Many2one('product.attribute', string="Odoo Product Attribute")
    mapping_line_id = fields.One2many('mage.mapping.product.attribute.line', 'mapping_ids', string="Mapping Line")

    @api.model
    def create(self, vals):
        mapping_obj = super(MageMappingProductAttribute, self.with_context({
            'model_mapping_line': 'mage.mapping.product.attribute.line',
            'mage_id': self._context.get('mage_id', 0)
        })).create(vals)

        return mapping_obj

    @api.multi
    def write(self, vals):
        mapping_obj = super(MageMappingProductAttribute, self.with_context({
            'mapping_id': self.id,
            'model_mapping_line': 'mage.mapping.product.attribute.line'
        })).write(vals)

        return mapping_obj

    @api.multi
    def do_mapping(self):
        mapping_lines_obj = self.env['mage.mapping.product.attribute.line'].search([
            ('mapping_ids', '=', self.id), ('state', '!=', 'done')
        ])
        for line in mapping_lines_obj:
            product_attribute_obj = self.odoo_id
            if product_attribute_obj:
                mapping_data = {
                    'id': product_attribute_obj.id,
                    'name': product_attribute_obj.name,
                    'label': product_attribute_obj.label,
                    'type': product_attribute_obj.type
                }

                data = json.dumps(mapping_data)
                response = line.instance_ids.connect('odooconnector.mapping_product_attribute', [data])
                if response['status']:
                    line.write({
                        'mage_id': response['data'],
                        'state': 'done'
                    })
                else:
                    line.write({'state': 'fail'})

        self.write({'state': 'mapped'})

        return True

    @api.multi
    def do_mapping_auto(self):
        try:
            mappings_obj = self.env['mage.mapping.product.attribute'].search([('state', '=', 'waiting')], limit=10)
            for mapping in mappings_obj:
                mapping.do_mapping()
            return True
        except Exception as ex:
            _logger.exception('Product Attribute - Error cron job: %s' % tools.ustr(ex))


class MageMappingProductAttributeLine(models.Model):
    _name = "mage.mapping.product.attribute.line"
    _inherit = "mage.mapping.line"

    mage_id = fields.Integer(string="Magento Product Attribute")


class MageMappingProductAttributeValue(models.Model):
    _name = "mage.mapping.product.attribute.value"
    _inherit = "mage.mapping"

    odoo_id = fields.Many2one('product.attribute.value', string="Odoo Product Attribute Value")
    mapping_line_id = fields.One2many('mage.mapping.product.attribute.value.line', 'mapping_ids', string="Mapping Line")

    @api.model
    def create(self, vals):
        mapping_obj = super(MageMappingProductAttributeValue, self.with_context({
            'model_mapping_line': 'mage.mapping.product.attribute.value.line',
            'mage_id': self._context.get('mage_id', 0)
        })).create(vals)

        return mapping_obj

    @api.multi
    def write(self, vals):
        mapping_obj = super(MageMappingProductAttributeValue, self.with_context({
            'mapping_id': self.id,
            'model_mapping_line': 'mage.mapping.product.attribute.value.line'
        })).write(vals)

        return mapping_obj

    @api.multi
    def do_mapping(self):
        mapping_lines_obj = self.env['mage.mapping.product.attribute.value.line'].search([
            ('mapping_ids', '=', self.id), ('state', '!=', 'done')
        ])
        for line in mapping_lines_obj:
            product_attribute_value_obj = self.odoo_id
            if product_attribute_value_obj:
                mapping_data = {
                    'id': product_attribute_value_obj.id,
                    'value': product_attribute_value_obj.name,
                    'attribute_code': product_attribute_value_obj.attribute_id.name
                }

                data = json.dumps(mapping_data)
                response = line.instance_ids.connect('odooconnector.mapping_product_attribute_value', [data])
                if response['status']:
                    line.write({
                        'mage_id': response['data'],
                        'state': 'done'
                    })
                else:
                    line.write({'state': 'fail'})

        self.write({'state': 'mapped'})

        return True

    @api.multi
    def do_mapping_auto(self):
        try:
            mappings_obj = self.env['mage.mapping.product.attribute.value'].search([('state', '=', 'waiting')], limit=10)
            for mapping in mappings_obj:
                mapping.do_mapping()
            return True
        except Exception as ex:
            _logger.exception('Product Attribute Value - Error cron job: %s' % tools.ustr(ex))


class MageMappingProductAttributeValueLine(models.Model):
    _name = "mage.mapping.product.attribute.value.line"
    _inherit = "mage.mapping.line"

    mage_id = fields.Integer(string="Magento Product Attribute Value")
