from odoo import models, fields, api, tools
import json

import logging
_logger = logging.getLogger(__name__)


class MageInstance(models.Model):
    _inherit = "mage.instance"

    auto_sync_product = fields.Boolean('Auto Sync', default=True)
    # default_categ_id = fields.Many2one('product.category', string='Default category', required=True)

    @api.multi
    def do_sync(self):
        self.do_sync_product_attribute_magento2odoo()
        self.do_sync_product_category_magento2odoo()
        cron_sync_magento2odoo_obj = self.env['ir.cron'].sudo().search([
            ('function', '=', 'do_sync_product_magento2odoo'),
            ('model', '=', 'mage.instance'),
            ('active', '=', False)
        ])
        if cron_sync_magento2odoo_obj:
            cron_sync_magento2odoo_obj.write({'active': True})

        return super(MageInstance, self).do_sync()

    @api.multi
    def do_sync_product_category_magento2odoo(self):
        response = self.connect('odooconnector.get_categories')
        category_data = response['data']
        if category_data:
            mapped_data = []
            for category in category_data:
                category_obj = self.env['mage.product.category'].with_context({
                    'mage_id': category['id']
                }).create({
                    'name': category['name']
                })
                mapped_data.append({
                    'odoo_id': category_obj.id if category_obj else 0,
                    'mage_id': category['id']
                })

            data_json = json.dumps(mapped_data)
            self.connect('odooconnector.create_product_category_mapping', [data_json])
        return True

    @api.multi
    def do_sync_product_attribute_magento2odoo(self):
        response = self.connect('odooconnector.get_attribute_sets')
        attributes_set_data = response['data']
        if attributes_set_data:
            mapped_data = []
            for attributes_set in attributes_set_data:
                attributes_data = attributes_set['attributes']
                attribute_ids = []
                mapped_attribute_data = []
                for attribute in attributes_data:
                    attribute_obj = self.env['product.attribute'].search([('name', '=', attribute['code'])])
                    if not attribute_obj:
                        attribute_obj = self.env['product.attribute'].with_context({'mage_id': attribute['id']})\
                            .create({
                                'name': attribute['code'],
                                'label':  attribute['label'],
                                'type': attribute['type']
                            })
                    if attribute_obj:
                        attribute_ids.append(attribute_obj.id)
                        mapped_attribute_option_data = []
                        for value in attribute['option']:
                            attribute_value_obj = self.env['product.attribute.value'].search([('name', '=', value['label'])])
                            if attribute_value_obj:
                                self.env['product.attribute.value'].write({'attribute_id': attribute_obj.id})
                            else:
                                attribute_value_obj = self.env['product.attribute.value'].with_context({
                                    'mage_id': value['value']
                                }).create({
                                        'attribute_id': attribute_obj.id,
                                        'name': value['label']
                                    })
                            mapped_attribute_option_data.append({
                                'odoo_id': attribute_value_obj.id if attribute_value_obj else 0,
                                'mage_id': value['value']
                            })
                        mapped_attribute_data.append({
                            'odoo_id': attribute_obj.id if attribute_obj else 0,
                            'mage_id': attribute['id'],
                            'options': mapped_attribute_option_data
                        })
                attribute_set_obj = self.env['product.attribute.set'].search([('name', '=', attributes_set['name'])])
                if not attribute_set_obj:
                    attribute_set_obj = self.env['product.attribute.set'].with_context({'mage_id': attributes_set['id']})\
                        .create({
                            'name': attributes_set['name'],
                            'attribute_id': [
                                [6, False, attribute_ids]
                            ]
                        })

                mapped_data.append({
                    'odoo_id': attribute_set_obj.id if attribute_set_obj else 0,
                    'mage_id': attributes_set['id'],
                    'attributes': mapped_attribute_data
                })

            data_json = json.dumps(mapped_data)
            self.connect('odooconnector.create_product_attribute_set_mapping', [data_json])
        return True

    @api.multi
    def do_sync_product_magento2odoo(self, sync_simple=False, sync_configurable=False):
        try:
            product_type = None
            if sync_simple:
                product_type = "simple"
            elif sync_configurable:
                product_type = "configurable"
            if product_type:
                instances_obj = self.env['mage.instance'].search([])
                for instance in instances_obj:
                    response = instance.connect('odooconnector.get_products', [product_type])
                    products_data = response['data']
                    mapped_data = self.create_product(products_data)
                    if len(mapped_data) > 0:
                        data_json = json.dumps(mapped_data)
                        instance.connect('odooconnector.create_product_mapping', [data_json])
        except Exception as ex:
            _logger.exception('Sync Product - error: %s' % tools.ustr(ex))
        return True

    @api.multi
    def do_sync_product_stock_odoo2magento(self):
        try:
            products = self.env['product.product'].search([])
            product_data = []
            if len(products) > 0:
                for product in products:
                    product_data.append({
                        'odoo_product_id': product.id,
                        'qty_available': product.qty_available
                    })
                instances_obj = self.env['mage.instance'].search([])
                for instance in instances_obj:
                    if len(product_data) > 0:
                        data_json = json.dumps(product_data)
                        instance.connect('odooconnector.update_product_stock_from_odoo', [data_json])
        except Exception as ex:
            _logger.exception('Sync Product Stock - error: %s' % tools.ustr(ex))
        return True

    @api.multi
    def create_product(self, products_data):
        mapped_data = []
        if products_data:
            for product in products_data:
                product_sku = product.get('sku', False)
                if product_sku:
                    product_obj = self.env['product.product'].search([('default_code', '=', product_sku)])
                    if not product_obj:
                        product_data = {
                            'name': product['name'],
                            'default_code': product['sku'],
                            'price': product['price'],
                            'type': 'product',
                            'categ_id': 3,
                            'source': 'Magento',
                            'magento_product_type': product['type'],
                            'attribute_set_id': product['attribute_set_id'],
                            'attribute_value_ids': [[6, False, product['attributes']]],
                            'magento_category_ids': [[6, False, map(int, product['categories'])]],
                            'associated_ids': [[6, False, map(int, product['associated'])]]
                        }
                        product_obj = self.env['product.product'].with_context({
                            'mage_id': product['id']
                        }).create(product_data)

                    mapped_data.append({
                        'odoo_id': product_obj.id,
                        'mage_id': product['id']
                    })
        return mapped_data
