# -*- coding: utf-8 -*-

{
    'name': 'Magento Connector Product',
    'version': '1.0.0',
    'category': 'Connector Modules',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Magento Connector Product',
    'description': """""",
    'depends': [
        'base',
        'mage_connector_base',
        'sale',
        'product'
    ],
    'data': [
        'data/product_cron.xml',
        'data/product_attribute_cron.xml',
        'security/ir.model.access.csv',
        'views/mage_instance_view.xml',
        'views/mage_mapping_product_attribute_view.xml',
        'views/mage_mapping_product_category_view.xml',
        'views/mage_product_category_view.xml',
        'views/mage_mapping_product_view.xml',
        'views/product_attribute_set_view.xml',
        'views/product_attribute_view.xml',
        'views/product_view.xml'
    ],
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
