# -*- coding: utf-8 -*-

{
    'name': 'Magestore Translation',
    'version': '1.0.0',
    'category': 'Translation',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Magestore Translation into Vietnamese',
    'description': """Magestore Translation into Vietnamese""",
    'depends': [
        'magestore_account',
        'magestore_account_asset',
        'magestore_accounting_report',
        'magestore_delivery_carrier',
        'magestore_import',
        'magestore_import_adjustment_stock',
        'magestore_intercompany',
        'magestore_point_of_sale',
        'magestore_pos_promotion',
        'magestore_pos_speed_up',
        'magestore_purchase',
        'magestore_sale',
        'magestore_stock',
        'magestore_store_dimension',
        'magestore_user_group',
        'magestore_warranty',
        'web',
        'account',
        'purchase',
        'stock',

    ],
    'data': [

    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
