# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
  "name"                 :  "Point of Sale Reprint Receipt",
  "summary"              :  "Point of Sale Reprint Receipt",
  "category"             :  "point_of_sale",
  "version"              :  "1.0",
  "sequence"             :  1,
  "author"               :  "Magestore",
  "website"              :  "http://www.magestore.com",
  "description"          :  "Point of Sale Reprint Receipt",
  "depends"              :  ['pos_orders'],
  "data"                 :  [
                              'views/templates.xml',
                            ],
  "qweb"                 :  ['static/src/xml/reprint.xml'],
  "application"          :  True,
  "installable"          :  True,
  "auto_install"         :  False,
}