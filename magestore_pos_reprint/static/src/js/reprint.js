odoo.define('magestore_pos_reprint.pos_reprint', function (require) {
    "use strict";

    var devices = require('point_of_sale.devices');
    var screens = require('point_of_sale.screens');
    var core = require('web.core');

    var _t = core._t;
    var gui = require('point_of_sale.gui');
    var pos_orders = require('pos_orders.pos_orders');
    var ActionManager = require('web.ActionManager');
    var QWeb = core.qweb;

    screens.ReceiptScreenWidget.include({
        reprint_receipt: function (order, self) {
            this.$('.pos-receipt-container').html(QWeb.render('PosTicket',{
                widget:this,
                order: order,
                receipt: order.export_for_printing(),
                orderlines: order.get_orderlines(),
                paymentlines: order.get_paymentlines(),
            }));
            self.gui.show_screen('receipt');
        }
    });

    devices.ProxyDevice.include({
        print_receipt: function (receipt) {
            this._super(receipt);
            this.pos.old_receipt = receipt || this.pos.old_receipt;
        },
    });

    var ReprintOrder = screens.ScreenWidget.extend({
        template: 'ReprintTicketScreenWidget',
        show: function () {
            var self = this;
            self._super();
            $('.button.back').on("click", function () {
                self.gui.show_screen('products');
            });
            $('.button.print').on("click", function () {
                window.print();
            });
        }
    });
    gui.define_screen({name:'reprint_order', widget: ReprintOrder});
    // var ReprintButton = screens.ActionButtonWidget.extend({
    //     template: 'ReprintButton',
    //     button_click: function () {
    //         var self = this;
    //         // var order_list = this.pos.get_order_list();
    //         // var order = order_list[0];
    //         // var orders = this.pos.get('orders').models;
    //
    //         // SuperReceiptScreenWidget.reprint_receipt(order, self);
    //
    //         // this.$('.pos-receipt-container').html(QWeb.render('ReprintOrder',{
    //         //     widget:this,
    //         //     order: order,
    //         //     receipt: order.export_for_printing(),
    //         //     orderlines: order.get_orderlines(),
    //         //     paymentlines: order.get_paymentlines(),
    //         // }));
    //         // self.gui.show_screen('reprint_order');
    //
    //         // if (this.pos.old_receipt) {
    //         //     this.pos.proxy.print_receipt(this.pos.old_receipt);
    //         // } else {
    //         //     this.gui.show_popup('error', {
    //         //         'title': _t('Nothing to Print'),
    //         //         'body': _t('There is no previous receipt to print.'),
    //         //     });
    //         // }
    //     },
    // });

    // screens.define_action_button({
    //     'name': 'reprint',
    //     'widget': ReprintButton,
    //     'condition': function () {
    //         return true;
    //         // return this.pos.config.iface_reprint && this.pos.config.iface_print_via_proxy;
    //     },
    // });

    pos_orders.include({
		show: function() {
			var self = this;
			this._super();
			self.$('.wk-order-list-contents').delegate('.reprint_order', 'click', function(event) {
                event.stopPropagation();
			    self.reprint_order(event, $(this), parseInt(this.id));
            });
			// $('.reprint_order').on('click',function(event) {
			//     event.stopPropagation();
			//     self.reprint_order(event, $(this), parseInt(this.id));
			// });
		},
        // render_list: function(order, input_txt) {
		// 	var self = this;
		// 	this._super(order, input_txt);
		// 	$('.reprint_order').on('click',function(event) {
		// 	    event.stopPropagation();
		// 	    self.reprint_order(event, $(this), parseInt(this.id));
		// 	});
		// },
        reprint_order: function (event, $line, order_id) {
            var self = this;
			var order = self.pos.db.order_by_id[order_id];
			self.display_order_to_reprint(order);
        },
        display_order_to_reprint: function (order) {
		    var self = this;
            var orderlines = [];
            var statements = [];
            order.lines.forEach(function(line_id) {
                orderlines.push(self.pos.db.line_by_id[line_id]);
            });
            order.statement_ids.forEach(function(statement_id) {
                var statement = self.pos.db.statement_by_id[statement_id];
                statements.push(statement);
                // journal_ids_used.push(statement.journal_id[0]);
            });


            $('.pos-receipt-container').html(QWeb.render('ReprintOrder', {
                widget: self,
                order: order,
                statements: statements,
                orderlines: orderlines,
            }));
            self.gui.show_screen("reprint_order");
        },
        get_order_by_id: function (order_id) {
		    var self = this;
            return self.pos.db.order_by_id[order_id]
        }
	});


    return ReprintOrder;
});