# -*- coding: utf-8 -*-

{
    'name': 'Magestore Import Adjustment Stock',
    'version': '1.0.0',
    'category': 'Tools',
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Magestore Import Adjustment Stock',
    'description': """""",
    'depends': [
        'stock'
    ],
    'data': [
        'wizard/import_adjustment_stock_view.xml',
        'views/stock_inventory_views.xml',
    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
