from odoo import api, fields, models, _, tools
from odoo.exceptions import UserError
import base64
import xlwt, xlrd, cStringIO
from collections import Counter

import logging
_logger = logging.getLogger(__name__)

class ImportAdjustmentStock(models.TransientModel):
    _name = "import.adjustment.stock.wizard"

    data = fields.Binary('File', required=True)
    data_err = fields.Binary('File')
    state = fields.Selection([('choose', 'choose'), ('get', 'get'), ('successful', 'successful')], default='choose')
    filename = fields.Char('File Name', required=True)

    @api.multi
    def do_import(self):

        list_product = []
        context = self._context
        stock_inventory_id = context.get('active_id', False)
        inventory_obj = self.env['stock.inventory'].browse(stock_inventory_id)

        if inventory_obj:

            list_product = []

            vals = {
                'inventory_id': inventory_obj.id,
                'location_id': inventory_obj.location_id.id
            }
            data = base64.decodestring(self.data)
            file_type = self.filename.split('.')[1]
            if file_type == 'csv':
                data = data.replace('\r', '')
                lines = data.split('\n')

                default_code_list = []

                for item in lines[1:]:
                    if item != '':
                        error_code = ""
                        item_arr = item.split(';')

                        if len(item_arr) <= 1:
                            item_arr = item.split(',')

                        if item_arr[0] in default_code_list:
                            error_code += "Duplicated"
                        else:
                            default_code_list.append(item_arr[0])

                        number_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.']

                        for char_ in item_arr[2]:
                            if char_ not in number_list:
                                error_code += "Product qty is not number "
                                break
                            if char_ == ",":
                                error_code += "Product qty is not integer "
                                break

                    vals.update({
                        'default_code': item_arr[0],
                        'product_name': item_arr[1],
                        'product_qty': item_arr[2] if len(item_arr) > 2 else 0.0,
                        'error_code': error_code,
                    })
                    list_product.append(vals.copy())

                check = self._import_adjustment_product(list_product)

            else:
                book = xlrd.open_workbook(file_contents=data, encoding_override='utf8')
                sheet1 = book.sheet_by_index(0)

                # Added by walter

                default_code_list = []
                for row in xrange(1, sheet1.nrows):
                    error_code = ""
                    if sheet1.cell(row, 0).value in default_code_list:
                        error_code += "Duplicated "
                    else:
                        default_code_list.append(sheet1.cell(row, 0))

                    number_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.']
                    for char_ in str(sheet1.cell(row, 2).value):
                        if char_ not in number_list:
                            error_code += "Product qty is not number "
                            break
                        if char_ == ",":
                            error_code += "Product qty is not integer "
                            break

                    vals.update({
                        'default_code': sheet1.cell(row, 0).value,
                        'product_name': sheet1.cell(row, 1).value,
                        'product_qty': sheet1.cell(row, 2).value if sheet1.cell(row, 2).value else 0.0,
                        'error_code': error_code,
                    })
                    # Ending added by Walter
                    list_product.append(vals.copy())

                check = self._import_adjustment_product(list_product)

        if check:
            self.write({
                'state': 'get',
                'data_err': check,
                'filename': 'log_error.xls',
            })
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'import.adjustment.stock.wizard',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': self.id,
                'views': [(False, 'form')],
                'target': 'new',
            }
        else:
            self.write({
                'state': 'successful',
            })
            return {
                'type': 'ir.actions.act_window',
                'res_model': 'import.adjustment.stock.wizard',
                'view_mode': 'form',
                'view_type': 'form',
                'res_id': self.id,
                'views': [(False, 'form')],
                'target': 'new',
            }

    def _import_adjustment_product(self, list_product):

        product_error = []
        error_log = {}
        count = 0
        value = ''
        for vals in list_product:

            # product_obj = self.env['product.product'].search([('default_code', '=', vals.get('default_code'))])
            product_id = False
            # Michael
            # if len(product_obj) != 1:
            try:
                self.env.cr.execute('SELECT id,product_tmpl_id FROM product_product WHERE trim(lower(default_code)) = \'%s\'' % (str(vals.get('default_code')).lower().strip()))
                product_obj = self.env.cr.fetchall()
                if len(product_obj) == 1:
                    product_id = product_obj[0][0]
                    product_tmpl_id = product_obj[0][1]
                    self.env.cr.execute(
                        'SELECT uom_id FROM product_template WHERE id = \'%s\'' % (product_tmpl_id))
                    # product_template = self.env['product.template'].browse(product_tmpl_id)
                    product_template_obj = self.env.cr.fetchall()
                    product_uom_id = product_template_obj[0][0]
            except:
                continue
            # else:
            #     product_id = product_obj[0].id
            #     product_uom_id = product_obj[0].uom_id.id
            # Added by Walter
            if product_id and vals.get('error_code') == '':
                # stock_inventory_line = self.env['stock.inventory.line'].search([
                #     ('inventory_id', '=', vals.get('inventory_id')),
                #     ('location_id', '=', vals.get('location_id')),
                #     ('product_id', '=', product_id)
                # ])
                # if stock_inventory_line:
                #     stock_inventory_line.write({
                #         'product_qty': float(vals.get('product_qty'))
                #     })
                # else:
                count = count +1
                # _logger.info('Count Import: %s' % tools.ustr(count))
                inventory_id = vals.get('inventory_id')
                location_id = vals.get('location_id')
                product_qty = vals.get('product_qty')
                if value:
                    value += ',(%s, %s, %s, %s, %s)' % (inventory_id,location_id,product_id, product_qty, product_uom_id)
                else:
                    value += '(%s, %s, %s, %s, %s)' % (inventory_id,location_id,product_id, product_qty, product_uom_id)
                # value += ',' + '(' + inventory_id + ',' + location_id + ',' + product_id + ',' + product_qty + ',' + product_uom_id + ')'
                if count == 900:
                    self.env.cr.execute('INSERT INTO stock_inventory_line (inventory_id, location_id, product_id, product_qty, product_uom_id) VALUES %s' % (value))
                    value = ''
                    count = 0;
                # stock_inventory_values = {
                #     'inventory_id': inventory_id,
                #     'location_id': location_id,
                #     'product_id': product_id,
                #     'product_qty': product_qty
                # }
                # self.env['stock.inventory.line'].sudo().create(stock_inventory_values)
            # Added by Walter
            error_code = vals.get('error_code')

            if len(product_obj) == 0:
                error_code += "SKU doesn't exist"

            if vals.get('error_code') != '' or error_code != '':
                error_log.update({
                    'default_code': vals.get('default_code'),
                    'product_name': vals.get('product_name'),
                    'product_qty': vals.get('product_qty'),
                    'error_code': error_code,
                })
                product_error.append(error_log.copy())

        if value:
            self.env.cr.execute(
                'INSERT INTO stock_inventory_line (inventory_id, location_id, product_id, product_qty, product_uom_id) VALUES %s' % (
                value))
        err_out = self._export_xls_error(product_error)

        if product_error:
            return err_out

    # Added by Walter
    @staticmethod
    def _export_xls_error(vals):

        wb = xlwt.Workbook(encoding='utf8')
        sheet = wb.add_sheet('sheet')
        sheet.write(0, 0, "product_sku")
        sheet.write(0, 1, "product_name")
        sheet.write(0, 2, "product_qty")
        sheet.write(0, 3, "error_code")
        index = 0

        for item in vals:
            sheet.write(index + 1, 0, item.get('default_code'))
            sheet.write(index + 1, 1, item.get('product_name'))
            sheet.write(index + 1, 2, item.get('product_qty'))
            sheet.write(index + 1, 3, item.get('error_code'))
            index += 1

        data_err = cStringIO.StringIO()
        wb.save(data_err)
        data_err.seek(0)
        out = base64.encodestring(data_err.read())
        data_err.close()

        return out
