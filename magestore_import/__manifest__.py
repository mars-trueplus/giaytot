# -*- coding: utf-8 -*-

{
    'name': 'Magestore Importing',
    'version': '1.0.0',
    'category': 'Tools',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Multiple importing purpose',
    'description': """""",
    'depends': [
        'sale',
        'stock',
        'product',
        'purchase',
    ],
    'data': [
        'wizard/import_stock_movement_views.xml',
        'wizard/import_purchase_order_views.xml',
        'wizard/import_sale_order_views.xml',
        'wizard/import_account_invoice_form_views.xml',

        # Added by Adam: Import Journal Entries
        'wizard/import_account_move_view.xml',
        'wizard/import_account_invoice_views.xml',
        'wizard/import_fixed_assets_views.xml',

        'views/stock_movement_views.xml',
        'views/purchase_order_views.xml',
        'views/sale_order_views.xml',
        'views/templates.xml',
        'views/account_invoice_view.xml',

        # Added by Adam: Import Journal Entries
        'views/account_move_view.xml',

    ],
    'qweb': [
        'static/src/xml/button_import_account_invoice.xml',
        'static/src/xml/button_import_account_asset_asset.xml',
    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
