odoo.define('magestore_import.button_import_account_invoice', function(require){
    'use strict';

    var core = require('web.core');
    var Model = require('web.Model');
    var Widget = require('web.Widget');
    var ListView = require('web.ListView');
    var QWeb = core.qweb;

    var Extends = ListView.include({
        render_buttons: function() {
            this._super.apply(this, arguments);
            this.$buttons.on('click', '.o_list_button_free_import', this.proxy('import_account_invoice'));
        },
        import_account_invoice: function(){
            var self = this;
            var invoice_model = new Model('import.account.invoice.wizard');
            invoice_model.call('get_id_of_view_import').then(function(result){
                self.do_action({
                    type: 'ir.actions.act_window',
                    name: 'Import Wizard',
                    res_model: 'import.account.invoice.wizard',
                    views: [[result, 'form']],
                    view_type: "form",
                    view_mode: "form",
                    target: 'new'
                });
            });
        },
    });
});



