odoo.define('magestore_import.button_import_account_move', function(require){
    'use strict';

    var core = require('web.core');
    var Model = require('web.Model');
    var Widget = require('web.Widget');
    var ListView = require('web.ListView');
    var QWeb = core.qweb;

    var Extends = ListView.include({
        render_buttons: function() {
            this._super.apply(this, arguments);
            this.$buttons.on('click', '.o_list_button_account_move_import', this.proxy('magestore_import_account_move'));
        },
        magestore_import_account_move: function(){
            var self = this;
            var move_model = new Model('import.account.move.wizard');

            move_model.call('get_id_of_view_import').then(function(result){
                self.do_action({
                    type: 'ir.actions.act_window',
                    name: 'Source',
                    res_model: 'import.account.move.wizard',
                    views: [[result, 'form']],
                    view_type: "form",
                    view_mode: "form",
                    target: 'new'
                });
            });
        },
    });
});



