# -*- coding: utf-8 -*-

from odoo import api, fields, models, _, tools
from odoo.exceptions import UserError
import base64
import xlwt, xlrd, cStringIO
# from magestore_import.models.qty_validate import QtyValidate
import qty_validate

class ImportSaleOrder(models.TransientModel):
    _name = "import.sale.order.wizard"

    data = fields.Binary('File', required=True)
    data_err = fields.Binary('File Error')
    state = fields.Selection([('choose', 'choose'), ('get', 'get')], default="choose")
    filename = fields.Char('Filename', required=True)

    @api.multi
    def do_import(self):

        context = self._context
        sale_order_id = context.get('active_id', False)
        sale_order_obj = self.env['sale.order'].browse(sale_order_id)

        if sale_order_obj:

            list_product = []

            vals = {
                'order_id': sale_order_id,
            }
            data = base64.decodestring(self.data)
            file_type = self.filename.split('.')[1]

            ism = qty_validate.QtyValidate()

            if file_type == "csv":
                data = data.replace('\r', '')
                lines = data.split("\n")

                list_product = ism.qty_validate_csv(vals, lines)

            if file_type == "xls":
                book = xlrd.open_workbook(file_contents=data, encoding_override='utf8')
                sheet1 = book.sheet_by_index(0)

                list_product = ism.qty_validate_xls(vals, sheet1)

            check = self._import_stock_movement(list_product)

            if check:
                self.write({
                    'state': 'get',
                    'data_err': check,
                    'filename': 'log_error.xls',
                })
                return {
                    'type': 'ir.actions.act_window',
                    'res_model': 'import.sale.order.wizard',
                    'view_mode': 'form',
                    'view_type': 'form',
                    'res_id': self.id,
                    'views': [(False, 'form')],
                    'target': 'new',
                }
            else:
                return {
                    'warning': {
                        'title': "Complete",
                        'message': 'Import Complete.',
                    }
                }

    def _import_stock_movement(self, list_product):

        product_error = []
        error_log = {}

        for vals in list_product:

            product_obj = self.env['product.product'].search([('default_code', '=', vals.get('default_code'))])

            # Getting data back from validate
            product_price = vals.get('product_price')
            product_qty = vals.get('product_qty')
            product_default_code = vals.get('default_code')
            product_name = vals.get('product_name')
            order_id = vals.get('order_id')
            error_code = vals.get('error_code')

            # Fill data to sale_order_line
            if len(product_obj) == 1 and error_code == '':
                sale_order_line = self.env['sale.order.line'].search([
                    ('order_id', '=', order_id),
                    ('product_id', '=', product_obj[0].id),
                ])
                if sale_order_line:
                    sale_order_line.write({
                        'product_qty': float(product_qty)
                    })
                else:
                    sale_order_values = {
                        'product_id': product_obj[0].id,
                        'name': product_obj[0].name,
                        'price_unit': product_price if product_price != 0.0 and product_price != u'' else product_obj[0].lst_price,
                        'order_id': order_id,
                        'product_uom': product_obj[0].uom_id.id,
                        'product_uom_qty': product_qty,
                    }
                    self.env['sale.order.line'].create(sale_order_values)

            if len(product_obj) == 0:
                error_code += str(product_default_code) + " SKU doesn't exist"

            if vals.get('error_code') != '' or error_code != '':
                error_log.update({
                    'default_code': product_default_code,
                    'product_name': product_name,
                    'product_qty': product_qty,
                    'error_code': error_code,
                })
                product_error.append(error_log.copy())

        err_out = self._export_xls_error(product_error)

        if product_error:
            return err_out

    @staticmethod
    def _export_xls_error(vals):

        wb = xlwt.Workbook(encoding='utf8')
        sheet = wb.add_sheet('sheet')
        sheet.write(0, 0, "product_sku")
        sheet.write(0, 1, "product_name")
        sheet.write(0, 2, "product_qty")
        sheet.write(0, 3, "error_code")
        index = 0

        for item in vals:
            sheet.write(index + 1, 0, item.get('default_code'))
            sheet.write(index + 1, 1, item.get('product_name'))
            sheet.write(index + 1, 2, item.get('product_qty'))
            sheet.write(index + 1, 3, item.get('error_code'))
            index += 1

        data_err = cStringIO.StringIO()
        wb.save(data_err)
        data_err.seek(0)
        out = base64.encodestring(data_err.read())
        data_err.close()

        return out
