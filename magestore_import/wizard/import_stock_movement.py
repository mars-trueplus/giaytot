# -*- coding: utf-8 -*-

from odoo import api, fields, models, _, tools
from odoo.exceptions import UserError
import base64
import xlwt,xlrd, cStringIO
import qty_validate


class ImportStockMovement(models.TransientModel):
    _name = "import.stock.movement.wizard"

    data = fields.Binary('File', required=True)
    data_err = fields.Binary('File Error')
    state = fields.Selection([('choose', 'choose'), ('get', 'get')], default="choose")
    filename = fields.Char('Filename', required=True)

    @api.multi
    def do_import(self):

        context = self._context
        stock_picking_id = context.get('active_id', False)
        stock_picking_obj = self.env['stock.picking'].browse(stock_picking_id)

        if stock_picking_obj:

            list_product = []

            vals = {
                'location_id': stock_picking_obj.location_id.id,
                'location_dest_id': stock_picking_obj.location_dest_id.id,
                'picking_id': stock_picking_id,
                'picking_type_id': stock_picking_obj.picking_type_id,
            }
            data = base64.decodestring(self.data)
            file_type = self.filename.split('.')[1]

            ism = qty_validate.QtyValidate()

            if file_type == "csv":
                data = data.replace('\r', '')
                lines = data.split("\n")

                list_product = ism.qty_validate_csv(vals, lines)

            if file_type == "xls":
                book = xlrd.open_workbook(file_contents=data, encoding_override='utf8')
                sheet1 = book.sheet_by_index(0)

                list_product = ism.qty_validate_xls(vals, sheet1)

            check = self._import_stock_movement(list_product)

            if check:
                self.write({
                    'state': 'get',
                    'data_err': check,
                    'filename': 'log_error.xls',
                })
                return {
                    'type': 'ir.actions.act_window',
                    'res_model': 'import.stock.movement.wizard',
                    'view_mode': 'form',
                    'view_type': 'form',
                    'res_id': self.id,
                    'views': [(False, 'form')],
                    'target': 'new',
                }
            else:
                return {
                    'warning': {
                        'title': "Complete",
                        'message': 'Import Complete.',
                    }
                }

    def _import_stock_movement(self, list_product):

        product_error = []
        error_log = {}

        for item in list_product:

            product_obj = self.env['product.product'].search([('default_code', '=', item.get('default_code'))])

            # Added by Walter
            if len(product_obj) == 1 and item.get('error_code') == '':
                stock_moving_line = self.env['stock.move'].search([
                    ('picking_id', '=', item.get('picking_id')),
                    ('product_id', '=', product_obj[0].id)
                ])
                if stock_moving_line:
                    stock_moving_line.write({
                        'product_uom_qty': float(item.get('product_qty'))
                    })
                else:
                    location_dest_id = item.get('location_dest_id')
                    location_id = item.get('location_id')
                    product_qty = item.get('product_qty')
                    picking_id = item.get('picking_id')
                    picking_type_id = item.get('picking_type_id').id
                    stock_moving_values = {
                        'picking_id': picking_id,
                        'picking_type_id': picking_type_id,
                        'location_dest_id': location_dest_id,
                        'location_id': location_id,
                        'product_id': product_obj[0].id,
                        'product_uom_qty': float(product_qty),
                        'product_uom': product_obj[0].uom_id.id,
                        'state': 'draft',
                        'name': product_obj[0].name,
                    }
                    tmp = self.env['stock.move'].create(stock_moving_values)
                    print tmp.picking_id

            # Added by Walter
            error_code = item.get('error_code')

            if len(product_obj) == 0:
                error_code += "SKU doesn't exist"

            if item.get('error_code') != '' or error_code != '':
                error_log.update({
                    'default_code': item.get('default_code'),
                    'product_name': item.get('product_name'),
                    'product_qty': item.get('product_qty'),
                    'error_code': error_code,
                })
                product_error.append(error_log.copy())

        err_out = self._export_xls_error(product_error)

        if product_error:
            return err_out

    @staticmethod
    def _export_xls_error(vals):

        wb = xlwt.Workbook(encoding='utf8')
        sheet = wb.add_sheet('sheet')
        sheet.write(0, 0, "product_sku")
        sheet.write(0, 1, "product_name")
        sheet.write(0, 2, "product_qty")
        sheet.write(0, 3, "error_code")
        index = 0

        for item in vals:
            sheet.write(index + 1, 0, item.get('default_code'))
            sheet.write(index + 1, 1, item.get('product_name'))
            sheet.write(index + 1, 2, item.get('product_qty'))
            sheet.write(index + 1, 3, item.get('error_code'))
            index += 1

        data_err = cStringIO.StringIO()
        wb.save(data_err)
        data_err.seek(0)
        out = base64.encodestring(data_err.read())
        data_err.close()

        return out

