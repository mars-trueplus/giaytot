# -*- coding: utf-8 -*-
# Author: Mars
from datetime import datetime
import xlrd

class GetInvoice():
    @staticmethod
    def read_csv_file(lines):

        list_account_invoices = []

        title_row = lines[0]
        if not title_row:
            return 'Wrong file format'
        titles = title_row.split(',')
        if len(titles) <= 1:
            titles = title_row.split(';')
        first_title = titles[0]

        # if first column is Customer --> import Customer invoices
        if first_title == 'Customer':
            if len(titles) != 10:
                return 'Wrong file format'
            list_account_invoices.append('Customer')
            # read from first line to end of file
            for item in lines[1:]:
                customer_invoice = {}
                if item != '':
                    item_arr = item.split(',')
                    if len(item_arr) <= 1:
                        item_arr = item.split(';')
                    if len(item_arr) == 10:

                        if CheckDate.correct_date(item_arr[1]):
                            date_value = datetime.strptime(item_arr[1], '%m/%d/%y').strftime('%Y-%m-%d')
                        else:
                            date_value = ''

                        customer_invoice.update({
                            'partner_id': item_arr[0],
                            'date_invoice': date_value,
                            'currency_id': item_arr[2],
                            'journal_id': item_arr[3],
                            'account_infor_id': item_arr[4],
                            'company_id': item_arr[5],
                            'name': item_arr[6],
                            'account_lines_id': item_arr[7],
                            'quantity': item_arr[8],
                            'price_unit': item_arr[9],
                        })
                    else:
                        customer_invoice.update({
                            'errors': 'This line is wrong format; '
                        })
                    list_account_invoices.append(customer_invoice)
        # if first column is Vendor --> import Vendor bills
        elif first_title == 'Vendor':
            if len(titles) != 11:
                return 'Wrong file format'
            list_account_invoices.append('Vendor')
            for item in lines[1:]:
                vendor_bill = {}
                if item != '':
                    item_arr = item.split(',')
                    if len(item_arr) <= 1:
                        item_arr = item.split(';')
                    if len(item_arr) == 11:
                        if CheckDate.correct_date(item_arr[5]):
                            date_value = datetime.strptime(item_arr[5], '%m/%d/%y').strftime('%Y-%m-%d')
                        else:
                            date_value = ''

                        if CheckDate.correct_date(item_arr[10]):
                            date_invoice = datetime.strptime(item_arr[10], '%m/%d/%y').strftime('%Y-%m-%d')
                        else:
                            date_invoice = ''
                        vendor_bill.update({
                            'partner_id': item_arr[0],
                            'currency_id': item_arr[1],
                            'journal_id': item_arr[2],
                            'account_infor_id': item_arr[3],
                            'company_id': item_arr[4],
                            'date':  date_value,
                            'name': item_arr[6],
                            'account_bill_id': item_arr[7],
                            'quantity': item_arr[8],
                            'price_unit': item_arr[9],
                            'date_invoice': date_invoice
                        })
                    else:
                        vendor_bill.update({
                            'errors': 'This line is wrong format; '
                        })
                    list_account_invoices.append(vendor_bill)
        else:
            return 'Wrong file format'

        return list_account_invoices

    @staticmethod
    def read_xls_file(sheet1, book):

        list_account_invoices = []

        # if first row has wrong number column --> wrong format file
        first_row = sheet1._cell_values[0]
        if not len(first_row):
            return 'Wrong file format'
        if first_row[0] == 'Customer':
            list_account_invoices.append('Customer')
            # read from first row to end of file
            for row in xrange(1, sheet1.nrows):
                customer_invoice = {}
                if len(sheet1._cell_values[row]) == 10:
                    date_value = sheet1.cell(row, 1).value
                    if type(date_value) is int or type(date_value) is float:
                        date_value = datetime(*xlrd.xldate_as_tuple(sheet1.cell(row, 1).value, book.datemode)).date()
                    customer_invoice.update({
                        'partner_id': sheet1.cell(row, 0).value,
                        'date_invoice': date_value,
                        'currency_id': sheet1.cell(row, 2).value,
                        'journal_id': sheet1.cell(row, 3).value,
                        'account_infor_id': sheet1.cell(row, 4).value,
                        'company_id': sheet1.cell(row, 5).value,
                        'name': sheet1.cell(row, 6).value,
                        'account_lines_id': sheet1.cell(row, 7).value,
                        'quantity': sheet1.cell(row, 8).value,
                        'price_unit': sheet1.cell(row, 9).value,
                    })
                else:
                    customer_invoice.update({
                        'errors': 'This line is wrong format; '
                    })
                list_account_invoices.append(customer_invoice)
        elif first_row[0] == 'Vendor':
            list_account_invoices.append('Vendor')
            for row in xrange(1, sheet1.nrows):
                vendor_bill = {}
                if len(sheet1._cell_values[row]) == 11:
                    date_value = sheet1.cell(row,5).value
                    if type(date_value) is int or type(date_value) is float:
                        date_value = datetime(*xlrd.xldate_as_tuple(sheet1.cell(row, 5).value,book.datemode)).date()

                    date_invoice = sheet1.cell(row, 10).value
                    if type(date_invoice) is int or type(date_invoice) is float:
                        date_invoice = datetime(*xlrd.xldate_as_tuple(sheet1.cell(row, 10).value,book.datemode)).date()

                    vendor_bill.update({
                        'partner_id': sheet1.cell(row, 0).value,
                        'currency_id': sheet1.cell(row, 1).value,
                        'journal_id': sheet1.cell(row, 2).value,
                        'account_infor_id': sheet1.cell(row, 3).value,
                        'company_id': sheet1.cell(row, 4).value,
                        'date': date_value,
                        'name': sheet1.cell(row, 6).value,
                        'account_bill_id': sheet1.cell(row, 7).value,
                        'quantity': sheet1.cell(row, 8).value,
                        'price_unit': sheet1.cell(row, 9).value,
                        'date_invoice': date_invoice
                    })
                else:
                    vendor_bill.update({
                        'errors': 'This line is wrong format; '
                    })
                list_account_invoices.append(vendor_bill)
        else:
            return 'Wrong file format'

        return list_account_invoices

class CheckDate():
    @staticmethod
    def correct_date(date_text):
        try:
            datetime.strptime(date_text, '%m/%d/%y')
            return True
        except ValueError:
            return False