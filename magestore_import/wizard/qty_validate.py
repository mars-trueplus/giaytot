# -*- coding: utf-8 -*-


class QtyValidate():

    @staticmethod
    def qty_validate_csv(vals, lines):

        list_product = []
        default_code_list = []

        for item in lines[1:]:
            if item != '':
                error_code = ""
                item_arr = item.split(';')
                if len(item_arr) <= 1:
                    item_arr = item.split(',')

                product_code = item_arr[0].value
                product_name = item_arr[1].value
                product_qty = item_arr[2].value
                product_price = item_arr[3].value

                if product_code in default_code_list:
                    error_code += str(product_code) + " Duplicated "
                else:
                    default_code_list.append(product_code)

                number_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.']
                dot_list = ['1', '2', '3', '4', '5', '6', '7', '8', '9']
                check_dot = False

                for char_ in str(product_qty):

                    if char_==".":
                        check_dot = True
                        continue
                    if check_dot:
                        if char_ in dot_list:
                            error_code += "Product " + str(product_code) + " qty is not number"
                            break
                    if char_ not in number_list:
                        error_code += "Product " + str(product_code) + " qty is not number "
                        break
                    if char_ == ",":
                        error_code += "Product " + str(product_code) + " qty is not integer "
                        break
                    if char_ == "-":
                        error_code += "Product " + str(product_code) + " qty is not positive integer "
                        break
                    check_dot = False

                if str(product_price) == '':
                    product_price = 0.0

                for char_ in str(product_price):

                    if char_==".":
                        check_dot = True
                        continue
                    if check_dot:
                        if char_ in dot_list:
                            error_code += "Price " + str(product_code) + " qty is not number"
                            break
                    if char_ not in number_list:
                        error_code += "Price " + str(product_code) + " qty is not number "
                        break
                    if char_ == ",":
                        error_code += "Price " + str(product_code) + " qty is not integer "
                        break
                    if char_ == "-":
                        error_code += "Price " + str(product_code) + " qty is not positive integer "
                        break
                    check_dot = False

            vals.update({
                'default_code': product_code,
                'product_name': product_name,
                'product_qty': product_qty if product_qty else 0.0,
                'product_price': product_price,
                'error_code': error_code,
            })
            list_product.append(vals.copy())
            error_code = ""

        return list_product

    @staticmethod
    def qty_validate_xls(vals, sheet1):

        list_product = []
        default_code_list = []

        for row in xrange(1, sheet1.nrows):
            error_code = ""
            product_code = sheet1.cell(row, 0).value
            product_name = sheet1.cell(row, 1).value
            product_qty = sheet1.cell(row, 2).value
            product_price = sheet1.cell(row, 3).value

            if sheet1.cell(row, 0).value in default_code_list:
                error_code += str(product_code) + " Duplicated "
            else:
                default_code_list.append(product_code)

            number_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.']
            dot_list = ['1', '2', '3', '4', '5', '6', '7', '8', '9']

            check_dot = False

            for char_ in str(product_qty):
                if char_ == ".":
                    check_dot = True
                    continue
                if check_dot:
                    if char_ in dot_list:
                        error_code += "Product " + str(product_code) + " qty is not number"
                        break
                if char_ not in number_list:
                    error_code += "Product " + str(product_code) + " qty is not number "
                    break
                if char_ == ",":
                    error_code += "Product " + str(product_code) + " qty is not integer "
                    break
                if char_ == "-":
                    error_code += "Product " + str(product_code) + " qty is not positive integer "
                    break
                check_dot = False

            if str(product_price)=='':
                product_price = 0.0

            for char_ in str(product_price):

                if char_==".":
                    check_dot = True
                    continue
                if check_dot:
                    if char_ in dot_list:
                        error_code += "Price " + str(product_code) + " qty is not number"
                        break
                if char_ not in number_list:
                    error_code += "Price " + str(product_code) + " qty is not number "
                    break
                if char_==",":
                    error_code += "Price " + str(product_code) + " qty is not integer "
                    break
                if char_=="-":
                    error_code += "Price " + str(product_code) + " qty is not positive integer "
                    break
                check_dot = False

            vals.update({
                'default_code': product_code,
                'product_name': product_name,
                'product_qty': product_qty if product_qty else 0.0,
                'product_price': product_price,
                'error_code': error_code,
            })
            # Ending added by Walter
            list_product.append(vals.copy())

        return list_product

    @staticmethod
    def qty_validate_invoice_form_xls(vals, sheet1):

        list_product = []
        default_code_list = []

        for row in xrange(1, sheet1.nrows):
            error_code = ""
            product_code = sheet1.cell(row, 0).value
            product_name = sheet1.cell(row, 1).value
            product_store_dimension = sheet1.cell(row, 2).value
            product_qty = sheet1.cell(row, 3).value
            product_price = sheet1.cell(row, 4).value
            product_discount = sheet1.cell(row, 5).value

            if sheet1.cell(row, 0).value in default_code_list:
                if error_code != "":
                    error_code = ','.join((error_code, str(product_code) + " Duplicated "))
                else:
                    error_code += " Duplicated "
            else:
                default_code_list.append(product_code)

            number_list = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.']
            dot_list = ['1', '2', '3', '4', '5', '6', '7', '8', '9']

            check_dot = False

            for char_ in str(product_qty):
                if char_ == ".":
                    check_dot = True
                    continue
                if check_dot:
                    if char_ in dot_list:
                        if error_code != "":
                            error_code = ','.join((error_code, "Product " + str(product_code) + " qty is not number"))
                        else:
                            error_code += "Product " + str(product_code) + " qty is not number"
                        break
                if char_ not in number_list:
                    if error_code != "":
                        error_code = ','.join((error_code, "Product " + str(product_code) + " qty is not number "))
                    else:
                        error_code += "Product " + str(product_code) + " qty is not number "
                    break
                if char_ == ",":
                    if error_code != "":
                        error_code = ','.join((error_code, "Product " + str(product_code) + " qty is not integer "))
                    else:
                        error_code += "Product " + str(product_code) + " qty is not integer "
                    break
                if char_ == "-":
                    if error_code != "":
                        error_code = ','.join((error_code, "Product " + str(product_code) + " qty is not positive integer "))
                    else:
                        error_code =+ "Product " + str(product_code) + " qty is not positive integer "
                    break
                check_dot = False

            if str(product_price) == '':
                product_price = 0.0

            for char_ in str(product_price):

                if char_ == ".":
                    check_dot = True
                    continue
                if check_dot:
                    if char_ in dot_list:
                        if error_code != "":
                            error_code = ','.join((error_code, "Price " + str(product_code) + " qty is not number"))
                        else:
                            error_code += "Price " + str(product_code) + " qty is not number"
                        break
                if char_ not in number_list:
                    if error_code != "":
                        error_code = ','.join((error_code, "Price " + str(product_code) + " qty is not number "))
                    else:
                        error_code += "Price " + str(product_code) + " qty is not number "
                    break
                if char_ == ",":
                    if error_code != "":
                        error_code = ','.join((error_code, "Price " + str(product_code) + " qty is not integer "))
                    else:
                        error_code += "Price " + str(product_code) + " qty is not integer "
                    break
                if char_ == "-":
                    if error_code != "":
                        error_code = ','.join((error_code, "Price " + str(product_code) + " qty is not positive integer "))
                    else:
                        error_code += "Price " + str(product_code) + " qty is not positive integer "
                    break
                check_dot = False

            vals.update({
                'default_code': product_code,
                'product_name': product_name,
                'product_store_dimension': product_store_dimension,
                'product_qty': product_qty if product_qty else 0.0,
                'product_price': product_price,
                'product_discount': product_discount,
                'error_code': error_code,
            })
            # Ending added by Walter
            list_product.append(vals.copy())

        return list_product