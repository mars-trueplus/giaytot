# -*- coding: utf-8 -*-

from odoo import api, fields, models, _, tools
from odoo.exceptions import UserError
import base64
import xlwt,xlrd, cStringIO
import qty_validate


# class AccountInvoice(models.Model):
#     _inherit = "account.invoice"
#
#     @api.multi
#     def write(self, vals):
#         res = super(AccountInvoice, self).write(vals)
#         if vals.get('invoice_line_ids'):
#
#             for line in self:
#                 if not line.store_dimension:
#                     line.store_dimension = vals.get('store_dimension')
#         return res

class ImportAccountInvoiceLineMovement(models.TransientModel):
    _name = "import.account.invoice.line.wizard"

    data = fields.Binary('File', required=True)
    data_err = fields.Binary('File Error')
    data_success = fields.Boolean(default=False)
    state = fields.Selection([('choose', 'choose'), ('get', 'get'), ('success', 'success')], default="choose")
    filename = fields.Char('Filename', required=True)

    @api.multi
    def do_import(self):

        context = self._context
        invoice_form_id = context.get('active_id', False)
        invoice_form_obj = self.env['account.invoice'].browse(invoice_form_id)

        if invoice_form_obj:

            list_product = []

            # vals = {
            #     'location_id': invoice_form_obj.location_id.id,
            #     'location_dest_id': invoice_form_obj.location_dest_id.id,
            #     'picking_id': invoice_form_id,
            #     'picking_type_id': invoice_form_obj.picking_type_id,
            # }
            vals = {
                'invoice_id': invoice_form_id
            }
            data = base64.decodestring(self.data)
            file_type = self.filename.split('.')[1]

            ism = qty_validate.QtyValidate()

            # if file_type == "csv":
            #     data = data.replace('\r', '')
            #     lines = data.split("\n")
            #
            #     list_product = ism.qty_validate_csv(vals, lines)

            if file_type == "xls":
                book = xlrd.open_workbook(file_contents=data, encoding_override='utf8')
                sheet1 = book.sheet_by_index(0)

                list_product = ism.qty_validate_invoice_form_xls(vals, sheet1)

            check = self._import_stock_movement(list_product)

            if check:
                self.write({
                    'state': 'get',
                    'data_err': check,
                    'filename': 'log_error.xls',
                })
                return {
                    'type': 'ir.actions.act_window',
                    'res_model': 'import.account.invoice.line.wizard',
                    'view_mode': 'form',
                    'view_type': 'form',
                    'res_id': self.id,
                    'views': [(False, 'form')],
                    'target': 'new',
                }
            else:
                self.write({
                    'state': 'success',
                    'data_success': True
                })
                return {
                    'type': 'ir.actions.act_window',
                    'res_model': 'import.account.invoice.line.wizard',
                    'view_mode': 'form',
                    'view_type': 'form',
                    'res_id': self.id,
                    'views': [(False, 'form')],
                    'target': 'new',
                }
                # return {
                #     'warning': {
                #         'title': "Complete",
                #         'message': 'Import Complete.',
                #     }
                # }

    def _import_stock_movement(self, list_product):

        product_error = []
        error_log = {}
        customer_invoice = []

        for item in list_product:
            customer_invoice = self.env['account.invoice'].browse(item.get('invoice_id'))
            customer_invoice_lines = customer_invoice.invoice_line_ids
            check_imported = False
            for customer_invoice_line in customer_invoice_lines:
                if customer_invoice_line.product_id.default_code == item.get('default_code'):
                    check_imported = True
                    break
            if check_imported:
                continue

            product_obj = self.env['product.product'].search([('default_code', '=', item.get('default_code'))])

            # Added by Walter
            error_code = item.get('error_code')

            if item.get('product_store_dimension'):
                product_store_dimension = item.get('product_store_dimension').replace(" ","")
                check_store_dimension = False
                for dimension_item in self.env['store.dimension'].search([]):
                    if product_store_dimension == dimension_item.name.replace(" ",""):
                        check_store_dimension = True
                        break
                if not check_store_dimension:
                    if error_code != "":
                        error_code = ','.join((error_code,"Store dimension dosen't exist"))
                    else:
                        error_code += "Store dimension dosen't exist"

            # Added by Walter
            if len(product_obj) == 1 and item.get('error_code') == '' and error_code == '':
                dimensions = [(dimension_item.id) for dimension_item in self.env['store.dimension'].search([]) if dimension_item.name.replace(" ","") == item.get('product_store_dimension').replace(" ","")]
                if len(dimensions) > 0:
                    dimension = dimensions[0]
                else:
                    dimension = ''
                invoice_lines = {
                    'product_id': product_obj.id,
                    'name': item.get('product_name'),
                    'price_unit': item.get('product_price'),
                    'account_id': customer_invoice.invoice_line_ids.with_context(journal_id=customer_invoice.journal_id.id)._default_account(),
                    'store_dimension': dimension,
                    'quantity': item.get('product_qty'),
                    'discount': item.get('product_discount')
                }
                customer_invoice.write({'invoice_line_ids': [(0, 0, invoice_lines)]})

            if len(product_obj) == 0:
                if error_code != "":
                    error_code = ','.join((error_code,"SKU doesn't exist"))
                else:
                    error_code += "SKU doesn't exist"

            if item.get('error_code') != '' or error_code != '':
                error_log.update({
                    'default_code': item.get('default_code'),
                    'product_name': item.get('product_name'),
                    'product_store_dimension': item.get('product_store_dimension'),
                    'product_qty': item.get('product_qty'),
                    'product_price': item.get('product_price'),
                    'product_discount': item.get('product_discount'),
                    'error_code': error_code,
                })
                product_error.append(error_log.copy())

        err_out = self._export_xls_error(product_error)

        if product_error:
            return err_out

    @staticmethod
    def _export_xls_error(vals):

        wb = xlwt.Workbook(encoding='utf8')
        sheet = wb.add_sheet('sheet')
        sheet.write(0, 0, "product_sku")
        sheet.write(0, 1, "product_name")
        sheet.write(0, 2, "product_store_dimension")
        sheet.write(0, 3, "product_qty")
        sheet.write(0, 4, "price")
        sheet.write(0, 5, "discount")
        sheet.write(0, 6, "error_code")
        index = 0

        for item in vals:
            sheet.write(index + 1, 0, item.get('default_code'))
            sheet.write(index + 1, 1, item.get('product_name'))
            sheet.write(index + 1, 2, item.get('product_store_dimension'))
            sheet.write(index + 1, 3, item.get('product_qty'))
            sheet.write(index + 1, 4, item.get('price'))
            sheet.write(index + 1, 5, item.get('discount'))
            sheet.write(index + 1, 6, item.get('error_code'))
            index += 1

        data_err = cStringIO.StringIO()
        wb.save(data_err)
        data_err.seek(0)
        out = base64.encodestring(data_err.read())
        data_err.close()

        return out

