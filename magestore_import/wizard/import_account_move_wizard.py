# -*- coding: utf-8 -*-
# Author: Adam - Magestore

from odoo import api, fields, models, _, tools
from odoo.exceptions import UserError
import base64
import xlwt, xlrd, cStringIO


class ImportAccountMoveWizard(models.TransientModel):
    _name = "import.account.move.wizard"

    data = fields.Binary('File', required=True)
    data_err = fields.Binary('File Error')
    state = fields.Selection([('choose', 'choose'), ('get', 'get')], default="choose")
    filename = fields.Char('Filename', required=True)

    # @api.model
    # def get_id_of_view_import(self):
    #     return self.env.ref('magestore_import.import_account_move_form').id

    @api.multi
    def do_import(self):
        context = self._context
        account_move_id = context.get('active_id', False)
        account_move_obj = self.env['account.move'].browse(account_move_id)
        user = self.env.user

        if account_move_obj:
            data = base64.decodestring(self.data)
            file_type = self.filename.split('.')[1]
            journals = []


            if file_type == "csv":
                data = data.replace('\r', '')
                lines = data.split("\n")
                journal_items_error = self._getJournalCSV(account_move_obj, lines, user)

            if file_type == "xls":
                book = xlrd.open_workbook(file_contents=data, encoding_override='utf8')
                sheet1 = book.sheet_by_index(0)
                journal_items_error = self._getJournalXLS(account_move_obj, sheet1, user)

            if journal_items_error:
                err_out = self._export_xls_error(journal_items_error)
                self.write({
                    'state': 'get',
                    'data_err': err_out,
                    'filename': 'import_journal_entries_error_log.xls',
                })
                return {
                    'type': 'ir.actions.act_window',
                    'res_model': 'import.account.move.wizard',
                    'view_mode': 'form',
                    'view_type': 'form',
                    'res_id': self.id,
                    'views': [(False, 'form')],
                    'target': 'new',
                }
            else:
                return {
                    'warning': {
                        'title': _('Warning'),
                        'message': _('Import Journal Entries Complete.'),
                    },
                }

    def _getJournalCSV(self, account_move_obj, lines, user):
        journal_items = []
        journal_items_error = []
        error_log = {}
        for item in lines[1:]:
            if item != '':
                error_code = ""
                error_messsage = ''
                error = False
                account_id = False
                partner_id = False
                currency_id = False

                item_arr = item.split(';')

                if len(item_arr) <= 1:
                    item_arr = item.split(',')
                    lenght = len(item_arr)
                    main_account = ''
                    partner = ''
                    label = ''
                    currency = ''
                    debit = 0.0
                    credit = 0.0

                    if lenght > 5:
                        if item_arr[5].strip():
                            credit = float(item_arr[5].strip())
                    if lenght > 4:
                        if item_arr[4].strip():
                            debit = float(item_arr[4].strip())
                    if lenght > 3:
                        currency = str(item_arr[3]).strip()
                    if lenght > 2:
                        label = str(item_arr[2])
                    if lenght > 1:
                        partner = str(item_arr[1]).strip()
                    if lenght:
                        main_account = str(item_arr[0]).strip()

                    if main_account:
                        account_id = self._check_data('account.account', 'code', main_account, user)
                        if not account_id:
                            error = True
                            error_messsage = "The main account %s is not available" %main_account
                    else:
                        error = True
                        error_messsage = "The main account cannot be empty"

                    if not error and partner:
                        partner_id = self._check_data('res.partner', 'name', partner, user)
                        if not partner_id:
                            partner_id = self.env['res.users'].search(
                                [('name', '=', partner), ('company_id', '=', user.company_id.id)]).partner_id
                            if not partner_id:
                                error = True
                                error_messsage = "The partner '%s' is not available" % partner

                    if not error:
                        if label == False:
                            error = True
                            error_messsage = "The lable cannot be empty"

                    if not error and currency:
                        currency_id = self._check_data('res.currency', 'name', currency, user)
                        if not currency_id:
                            error = True
                            error_messsage = "The currency '%s' is not available" %currency

                    if not error:
                        account_move_values = [0, False, {
                            'account_id': account_id,
                            'partner item_arr[5].strip():_id': partner_id,
                            'label': label,
                            'currency_id': currency_id,
                            'debit': debit,
                            'credit': credit,
                            'name': label,
                            'move_id': account_move_obj
                        }]
                        journal_items.append(account_move_values)
                    else:
                        error_log.update({
                            'account_id': main_account,
                            'partner item_arr[5].strip():_id': partner,
                            'label': label,
                            'currency_id': currency,
                            'debit': debit,
                            'credit': credit,
                            'error_message': error_messsage
                        })
                        journal_items_error.append(error_log.copy())
        if journal_items:
            account_move_obj.write({'line_ids': journal_items})

        if journal_items_error:
            return journal_items_error
        return False

    def _process_str_field(self, field):
        type_str = str(type(field))
        if 'unicode' in type_str:
            return field.strip()
        if 'float' in type_str:
            return str(int(field))
        return str(field).strip()


    def _process_number_field(self, field):
        type_str = str(type(field))
        if 'float' not in type_str and 'int' not in type_str:
            return 0.0
        return float(field)

    def _getJournalXLS(self, account_move_obj, sheet1, user):
        journal_items = []
        journal_items_error = []
        journal_item = {}
        error_log = {}
        for row in xrange(1, sheet1.nrows):
            error_messsage = ''
            error = False
            account_id = False
            partner_id = False
            currency_id = False

            main_account = self._process_str_field(sheet1.cell(row, 0).value)
            partner = self._process_str_field(sheet1.cell(row, 1).value)
            label = self._process_str_field(sheet1.cell(row, 2).value)
            currency = self._process_str_field(sheet1.cell(row, 3).value)
            debit = self._process_number_field(sheet1.cell(row, 4).value)
            credit = self._process_number_field(sheet1.cell(row, 5).value)

            if main_account:
                account_id = self._check_data('account.account', 'code', main_account, user)
                if not account_id:
                    error = True
                    error_messsage = "The main account %s is not available" %main_account
            else:
                error = True
                error_messsage = "The main account cannot be empty"

            if not error and partner:
                partner_id = self._check_data('res.partner', 'name', partner, user)
                if not partner_id:
                    partner_id = self.env['res.users'].search([('name', '=', partner), ('company_id', '=', user.company_id.id)]).partner_id
                    if not partner_id:
                        error = True
                        error_messsage = "The partner '%s' is not available" %partner

            if not error:
                if label == False:
                    error = True
                    error_messsage = "The lable cannot be empty"

            if not error and currency:
                currency_id = self._check_data('res.currency', 'name', currency, user)
                if not currency_id:
                    error = True
                    error_messsage = "The currency '%s' is not available" %currency

            if not error:
                account_move_values = [0, False, {
                    'account_id': account_id,
                    'partner_id': partner_id,
                    'label': label,
                    'currency_id': currency_id,
                    'debit': debit,
                    'credit': credit,
                    'name': label,
                    'move_id': account_move_obj
                }]
                journal_items.append(account_move_values)
                # self.env['account.move.line'].create(account_move_values)
            else:

                error_log.update({
                    'account_id': main_account,
                    'partner_id': partner,
                    'label': label,
                    'currency_id': currency,
                    'debit': debit,
                    'credit': credit,
                    'error_message': error_messsage
                })
                journal_items_error.append(error_log.copy())
        if journal_items:
            account_move_obj.write({'line_ids': journal_items})

        if journal_items_error:
            return journal_items_error
        return False

    def _export_xls_error(self, vals):

        wb = xlwt.Workbook(encoding='utf8')
        sheet = wb.add_sheet('sheet')
        sheet.write(0, 0, "Main Account")
        sheet.write(0, 1, "Partner")
        sheet.write(0, 2, "Label")
        sheet.write(0, 3, "Currency")
        sheet.write(0, 4, "Debit")
        sheet.write(0, 5, "Credit")
        sheet.write(0, 6, "Error Message")
        index = 0

        for item in vals:
            sheet.write(index + 1, 0, item.get('account_id'))
            sheet.write(index + 1, 1, item.get('partner_id'))
            sheet.write(index + 1, 2, item.get('label'))
            sheet.write(index + 1, 3, item.get('currency_id'))
            sheet.write(index + 1, 4, item.get('debit'))
            sheet.write(index + 1, 5, item.get('credit'))
            sheet.write(index + 1, 6, item.get('error_message'))
            index += 1

        data_err = cStringIO.StringIO()
        wb.save(data_err)
        data_err.seek(0)
        out = base64.encodestring(data_err.read())
        data_err.close()

        return out

    def _check_data(self, model, field, value, user):
        if model == 'res.currency':
            if user.currency_id.name == value:
                model = self.env[model].search([(field, '=', value)])
            else:
                return False
        else:
            model = self.env[model].search([(field, '=', value), ('company_id', '=', user.company_id.id)])
        return model.id


