# -*- coding: utf-8 -*-
{
    'name': "Magestore accounting report",

    'summary': """
        Magestore accounting report""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Magestore",
    'website': "http://www.magestore.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Accounting',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base',
                'point_of_sale',
                'account',
                'magestore_store_dimension',
                ],

    'data': [
        'views/accounting_report_view_inherit_2.xml',
        'views/profit_loss_report_view.xml',
    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
}
