# -*- coding: utf-8 -*-
from odoo import fields, models, api
import time


class AccountProfitLossReport(models.TransientModel):
    _inherit = "accounting.report"


    @api.multi
    def check_report(self):

        res = super(AccountProfitLossReport, self).check_report()
        data = {}
        _journal_ids = []
        data['form'] = \
        self.read(['account_report_id', 'date_from_cmp', 'date_to_cmp', 'journal_ids', 'filter_cmp', 'target_move'])[0]
        for field in ['account_report_id']:
            if isinstance(data['form'][field], tuple):
                data['form'][field] = data['form'][field][0]
        comparison_context = self._build_comparison_context(data)
        store_dimension_list = []

        if self.store_dimension:
            for journal_id in self.journal_ids:
                _check = False
                _journal_entries = self.env['account.move'].search([('journal_id', '=', journal_id.id)])
                for item in _journal_entries:
                    for line in item.line_ids:
                        if line.store_dimension:
                            _check = True
                            _journal_ids.append(journal_id.id)
                            break
                    if _check:
                        break

            for rec in self.store_dimension:
                store_dimension_list.append(rec.id)

            comparison_context.update({
                'journal_ids': _journal_ids,
                'store_dimension': store_dimension_list,
            })
            res['data']['form']['comparison_context'] = comparison_context
            return res
        else:
            res['data']['form']['comparison_context'] = comparison_context
            return res


class ProfitLossReportFinancial(models.AbstractModel):
    _inherit = "report.account.report_financial"

    def _compute_account_balance(self, accounts, item_store_dimension):
        """ compute the balance, debit and credit for the provided accounts
        """
        mapping = {
            'balance': "COALESCE(SUM(debit),0) - COALESCE(SUM(credit), 0) as balance",
            'debit': "COALESCE(SUM(debit), 0) as debit",
            'credit': "COALESCE(SUM(credit), 0) as credit",
        }

        res = {}
        for account in accounts:
            res[account.id] = dict((fn, 0.0) for fn in mapping.keys())
        if accounts:
            if item_store_dimension != -1:
                tables, where_clause, where_params = self.env['account.move.line']._query_get()
                params = (tuple(accounts._ids),) + tuple(where_params)
                new_params = []
                lst_param = list(params)
                lst_param_0 = list(lst_param[0])

                for item in lst_param_0:
                    tmp = self.env['account.move.line'].search([('account_id', '=', item)])
                    count = 0
                    for item_tmp in tmp:
                        sd = item_tmp.store_dimension
                        if len(sd)==0:
                            count += 1
                    if count==len(tmp):
                        continue
                    else:
                        new_params.append(item)
                # params[0] = new_params
                context = dict(self._context or {})
                addition_str = ""
                if context.get("date_to") and context.get("date_from"):
                    addition_str += "AND ((account_move_line.date <= %s) AND (account_move_line.date >= %s))"
                elif context.get("date_to"):
                    addition_str += "AND (account_move_line.date <= %s)"
                elif context.get("date_from"):
                    addition_str += "AND (account_move_line.date >= %s)"
                else:
                    addition_str = ""

                request_part_select = "SELECT account_move_line.account_id as id, COALESCE(SUM(credit), 0) as credit, COALESCE(SUM(debit),0) - COALESCE(SUM(credit), 0) as balance, COALESCE(SUM(debit), 0) as debit, COALESCE(account_move_line.store_dimension,0) as sd "
                request_part_from = "FROM account_move as account_move_line__move_id,account_move_line "
                request_part_where = "WHERE account_move_line.account_id IN %s AND ((account_move_line.move_id=account_move_line__move_id.id) AND (COALESCE(account_move_line.store_dimension,0)!=0)) "
                request_part_journal = "AND ((account_move_line.journal_id in ("
                if self._context.get("state") == "all":
                    request_part_where_next = ")) "
                else:
                    request_part_where_next = ")) AND (account_move_line__move_id.state = %s) "
                request_part_group = ") GROUP BY account_move_line.account_id,account_move_line.store_dimension"

                lst_param[0] = tuple(new_params)
                count = 0
                for item_lst_param in lst_param:
                    if 'int' in str(type(item_lst_param)):
                        count += 1
                for i in range(count):
                    request_part_journal += "%s,"
                if count > 0:
                    request = request_part_select + \
                              request_part_from + \
                              request_part_where + \
                              addition_str + \
                              request_part_journal[:-1] + \
                              request_part_where_next + \
                              request_part_group
                else:
                    request = request_part_select + \
                              request_part_from + \
                              request_part_where + \
                              addition_str + \
                              request_part_journal + "NULL" + \
                              request_part_where_next + \
                              request_part_group

                self.env.cr.execute(request, params)
                fetch_all = self.env.cr.dictfetchall()
                credit = 0.0
                debit = 0.0
                balance = 0.0
                for row in fetch_all:
                    if row.get('sd')==item_store_dimension:
                        res[row['id']] = row
            else:
                tables, where_clause, where_params = self.env['account.move.line']._query_get()
                tables = tables.replace('"', '') if tables else "account_move_line"
                wheres = [""]
                if where_clause.strip():
                    wheres.append(where_clause.strip())
                filters = " AND ".join(wheres)
                request = "SELECT account_move_line.account_id as id, " + ', '.join(mapping.values()) + \
                          " FROM " + tables + \
                          " WHERE account_move_line.account_id IN %s " \
                          + filters + \
                          " GROUP BY account_move_line.account_id"
                params = (tuple(accounts._ids),) + tuple(where_params)
                self.env.cr.execute(request, params)
                for row in self.env.cr.dictfetchall():
                    res[row['id']] = row
        return res

    def _compute_report_balance(self, reports, item_store_dimension):
        '''returns a dictionary with key=the ID of a record and value=the credit, debit and balance amount
           computed for this record. If the record is of type :
               'accounts' : it's the sum of the linked accounts
               'account_type' : it's the sum of leaf accoutns with such an account_type
               'account_report' : it's the amount of the related report
               'sum' : it's the sum of the children of this record (aka a 'view' record)'''
        res = {}
        fields = ['credit', 'debit', 'balance']
        for report in reports:
            if report.id in res:
                continue
            res[report.id] = dict((fn, 0.0) for fn in fields)
            if report.type == 'accounts':
                # it's the sum of the linked accounts
                res[report.id]['account'] = self._compute_account_balance(report.account_ids, item_store_dimension)
                for value in res[report.id]['account'].values():
                    for field in fields:
                        res[report.id][field] += value.get(field)
            elif report.type == 'account_type':
                # print report.name
                # it's the sum the leaf accounts with such an account type
                accounts = self.env['account.account'].search([('user_type_id', 'in', report.account_type_ids.ids)])
                tmp = self._compute_account_balance(accounts, item_store_dimension)
                res[report.id]['account'] = tmp
                for value in res[report.id]['account'].values():
                    for field in fields:
                        res[report.id][field] += value.get(field)
            elif report.type == 'account_report' and report.account_report_id:
                # it's the amount of the linked report
                res2 = self._compute_report_balance(report.account_report_id, item_store_dimension)
                for key, value in res2.items():
                    for field in fields:
                        res[report.id][field] += value[field]
            elif report.type == 'sum':
                # it's the sum of the children of this account.report
                # print report.name
                res2 = self._compute_report_balance(report.children_ids, item_store_dimension)
                for key, value in res2.items():
                    for field in fields:
                        res[report.id][field] += value[field]
        return res

    def get_account_lines(self, data):
        lines = []

        # Getting store_dimension to filter
        # journal_ids_filtered = []
        store_dimensions = data.get('comparison_context').get('store_dimension')
        filtered_journal_ids = data.get('comparison_context').get('journal_ids')

        if not store_dimensions:
            store_dimensions = [-1]

        for item_store_dimension in store_dimensions:
            account_report = self.env['account.financial.report'].search([('id', '=', data['account_report_id'][0])])
            search_store_dimension = self.env['store.dimension'].search([('id', '=', item_store_dimension)])

            if not search_store_dimension:
                store_dimension = ""
            else:
                store_dimension = search_store_dimension.name

            child_reports = account_report._get_children_by_order()

            # Filtered store dimension for journal id
            list_journal_ids = []
            for item in filtered_journal_ids:
                _search_journal_id = self.env['account.move'].search([('journal_id', '=', item)])
                for item_search_journal_id in _search_journal_id:
                    if item_search_journal_id.store_dimension == store_dimension:
                        list_journal_ids.append(item)
                        break

            # Update used_context journal_ids
            data.get('used_context').update({
                'journal_ids': list_journal_ids,
            })
            res = self.with_context(data.get('used_context'))._compute_report_balance(child_reports, item_store_dimension)

            if data['enable_filter']:
                comparison_res = self.with_context(data.get('comparison_context'))._compute_report_balance(child_reports)
                for report_id, value in comparison_res.items():
                    res[report_id]['comp_bal'] = value['balance']
                    report_acc = res[report_id].get('account')
                    if report_acc:
                        for account_id, val in comparison_res[report_id].get('account').items():
                            report_acc[account_id]['comp_bal'] = val['balance']

            for report in child_reports:
                if report.type == 'sum':
                    vals = {
                        'name': report.name,
                        'balance': res[report.id]['balance'] * report.sign,
                        'type': 'report',
                        'level': bool(report.style_overwrite) and report.style_overwrite or report.level,
                        'account_type': report.type or False,  # used to underline the financial report balances
                        'store_dimension': store_dimension,
                    }
                else:
                    vals = {
                        'name': report.name,
                        'balance': res[report.id]['balance'] * report.sign,
                        'type': 'report',
                        'level': bool(report.style_overwrite) and report.style_overwrite or report.level,
                        'account_type': report.type or False,  # used to underline the financial report balances
                    }

                if data['debit_credit']:
                    vals['debit'] = res[report.id]['debit']
                    vals['credit'] = res[report.id]['credit']

                if data['enable_filter']:
                    vals['balance_cmp'] = res[report.id]['comp_bal'] * report.sign

                lines.append(vals)
                if report.display_detail == 'no_detail':
                    # the rest of the loop is used to display the details of the financial report, so it's not needed here.
                    continue

                if res[report.id].get('account'):
                    sub_lines = []
                    for account_id, value in res[report.id]['account'].items():
                        # if there are accounts to display, we add them to the lines with a level equals to their level in
                        # the COA + 1 (to avoid having them with a too low level that would conflicts with the level of data
                        # financial reports for Assets, liabilities...)
                        flag = False
                        account = self.env['account.account'].browse(account_id)
                        vals = {
                            'name': account.code + ' ' + account.name,
                            'balance': value['balance'] * report.sign or 0.0,
                            'type': 'account',
                            'level': report.display_detail == 'detail_with_hierarchy' and 4,
                            'account_type': account.internal_type,
                        }
                        if data['debit_credit']:
                            vals['debit'] = value['debit']
                            vals['credit'] = value['credit']
                            if not account.company_id.currency_id.is_zero(
                                    vals['debit']) or not account.company_id.currency_id.is_zero(vals['credit']):
                                flag = True
                        if not account.company_id.currency_id.is_zero(vals['balance']):
                            flag = True
                        if data['enable_filter']:
                            vals['balance_cmp'] = value['comp_bal'] * report.sign
                            if not account.company_id.currency_id.is_zero(vals['balance_cmp']):
                                flag = True
                        if flag:
                            sub_lines.append(vals)
                    lines += sorted(sub_lines, key=lambda sub_line: sub_line['name'])

        return lines
