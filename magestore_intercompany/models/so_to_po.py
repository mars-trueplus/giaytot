# -*- coding: utf-8 -*-
from odoo import api, models, fields


class SaleOrderToPurchaseOrder ( models.Model ):
    _inherit = "sale.order"

    # warehouse_id = fields.Many2one()

    confirm = fields.Boolean(default=False)

    @api.model
    def create(self, vals):
        if not vals.get('origin'):
            vals.update({
                'confirm': True,
            })
        return super(SaleOrderToPurchaseOrder, self).create(vals)

    @api.multi
    def action_confirm(self):
        self.ensure_one()
        
        if not self.confirm or not self.partner_id.is_company:
            return super(SaleOrderToPurchaseOrder, self).action_confirm()

        else:
            check_parent_company = self.env['res.company'].sudo().search([('partner_id', '=', self.partner_id.id)])
            if not check_parent_company:
                return super(SaleOrderToPurchaseOrder, self).action_confirm()

            partner_id = self.company_id
            date_order = self.date_order
            company_id = self.partner_id.company_id
            currency_id = self.currency_id
            date_planned = date_order
            # picking_type_id = self.env['stock.picking.type'].sudo().search([('code', '=', 'incoming')])[0].id or False
            picking_type_id = self.env['stock.picking.type'].sudo().search([
                ('warehouse_id.company_id', 'in', [company_id.id, False])],
                limit=1).id

            purchase_order_lines = []
            for sale_order_line in self.order_line.sudo():
                purchase_order_line = [0, False]
                order_line_vals = {}
                order_line_vals['name'] = sale_order_line.name
                order_line_vals['product_id'] = sale_order_line.product_id.id
                order_line_vals['product_qty'] = sale_order_line.product_uom_qty
                order_line_vals['product_uom'] = 1
                order_line_vals['price_unit'] = sale_order_line.price_unit
                order_line_vals['date_planned'] = date_order
                purchase_order_line.append(order_line_vals)
                purchase_order_lines.append(purchase_order_line)

            purchase_vals = {
                'partner_id': partner_id.partner_id.id,
                'date_order': date_order,
                'company_id': company_id.id,
                'currency_id': currency_id.id,
                'date_planned': date_planned,
                'picking_type_id': picking_type_id,
                'order_line': purchase_order_lines,
                'state': 'draft',
                'origin': self.name,
            }

            self.env['purchase.order'].sudo().create(purchase_vals)

            return super(SaleOrderToPurchaseOrder, self).action_confirm()