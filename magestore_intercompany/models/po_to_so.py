# -*- coding:utf-8 -*-
from odoo import api, models, fields
from datetime import datetime


class PurchaseOrderToSaleOrder(models.Model):
    _inherit = "purchase.order"

    confirm_purchase_order = fields.Boolean(default=False)

    @api.model
    def create(self, vals):
        if not vals.get('origin'):
            vals.update({
                'confirm_purchase_order': True,
            })
        # changed by Mars
        # because use sudo() so responsible of all purchase order are Administrator
        # ===> wrong business
        return super(PurchaseOrderToSaleOrder, self).create(vals)
        # return super(PurchaseOrderToSaleOrder, self.sudo()).create(vals)

    @api.multi
    def button_approve(self):
        self.ensure_one()

        if not self.confirm_purchase_order or not self.partner_id.is_company:
            return super(PurchaseOrderToSaleOrder, self).button_approve()

        else:
            check_parent_company = self.env['res.company'].sudo().search([('partner_id', '=', self.partner_id.id)])
            if not check_parent_company:
                return super(PurchaseOrderToSaleOrder, self).button_approve()

            # Getting warehouse_id
            warehouse_id = 0
            search_partner_id = self.env['res.partner'].sudo().search([('id', '=', self.company_id.partner_id.id)])
            search_comp_id = self.env['res.partner'].sudo().search([('id', '=', self.partner_id.id)])
            if search_comp_id:
                warehouse_id = self.env['stock.warehouse'].sudo().search(
                    [('company_id', '=', search_comp_id.company_id.id)], limit=1).id

            # Getting partner_address
            partner_address = search_partner_id.address_get(['delivery', 'invoice'])

            # Setting sale.order.line
            sale_order_lines = []

            for item in self.order_line.sudo():

                # Calculating the different time between delivered and ordered day
                planned_day = str(self.date_planned).split(" ")[0]
                order_day = str(self.date_order).split(" ")[0]
                if order_day is None or planned_day is None:
                    customer_lead = 0.0
                else:
                    d1 = datetime.strptime(planned_day, "%Y-%m-%d")
                    d2 = datetime.strptime(order_day, "%Y-%m-%d")
                    timedelta = d1 - d2
                    customer_lead = timedelta.days + float(timedelta.seconds) / 86400

                sale_order_line = [0, False]

                sale_order_line.append({
                    'price_unit': item.price_unit,
                    'product_uom_qty': float(item.product_qty),
                    'product_uom': item.product_uom.id,
                    'product_id': item.product_id.id,
                    'customer_lead': customer_lead,
                    'name': item.name,
                })

                sale_order_lines.append(sale_order_line)

            rfq_vals = {
                'currency_id': self.currency_id.id,
                'date_order': self.date_order,
                'warehouse_id': warehouse_id,
                'company_id': self.partner_id.company_id.id,
                'partner_id': self.company_id.partner_id.id,
                'validity_date': self.date_planned,
                'partner_invoice_id': partner_address.get('invoice'),
                'partner_shipping_id': partner_address.get('delivery'),
                'order_line': sale_order_lines,
                'state': 'draft',
                'picking_policy': 'direct',
                'origin': self.name,
            }

            self.env['sale.order'].sudo().create(rfq_vals)

            return super(PurchaseOrderToSaleOrder, self).button_approve()
