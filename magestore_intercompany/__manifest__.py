# -*- coding: utf-8 -*-

{
    'name': 'Magestore Inter Company',
    'version': '1.0.0',
    'category': 'Tools',
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Magestore Inter Company',
    'description': """""",
    'depends': [
        'purchase',
        'sale',
        'stock',
        'delivery',
    ],
    'data': [
        # 'security/ir.model.access.csv',
    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
