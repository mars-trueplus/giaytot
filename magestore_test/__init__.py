from odoo import api, SUPERUSER_ID

#update shipping processing
def post_init_hook1(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    sale_orders = env['sale.order'].search([])
    for sale_order in sale_orders:
        try:
            total_qty_ordered = 0
            total_qty_delivered = 0
            shipping_processing = ''
            for item_order_line in sale_order.order_line:
                if item_order_line.product_id.product_tmpl_id.type == 'service':
                    continue
                total_qty_ordered += item_order_line.product_uom_qty
                total_qty_delivered += item_order_line.qty_delivered

            if total_qty_delivered == 0:
                shipping_processing = 'not_delivery'
            if total_qty_delivered > 0 and total_qty_delivered < total_qty_ordered:
                shipping_processing = 'partial'
            if total_qty_delivered == total_qty_ordered:
                shipping_processing = 'done'

            sale_order.write({
                'total_qty_ordered': total_qty_ordered,
                'total_qty_delivered': total_qty_delivered,
                'delivery_state': shipping_processing,
            })
        except Exception as e:
            continue

#update product standard price
def post_init_hook(cr, registry):
    list_product = [
        {'default_code': '1460-den-daran-38', 'standard_price': 377400},
        {'default_code': '1460-den-daran-41', 'standard_price': 377400},
        {'default_code': '1460-den-daran-43', 'standard_price': 377400},
        {'default_code': '1460-den-dasan-38', 'standard_price': 377400},
        {'default_code': '1460-nau-dasap-43', 'standard_price': 377400},
        {'default_code': '211-den-38', 'standard_price': 390000},
        {'default_code': '6068-xam-42', 'standard_price': 320000},
        {'default_code': '6102-nau-dalon-39', 'standard_price': 300000},
        {'default_code': '7054-den-41', 'standard_price': 230000},
        {'default_code': '9067-xam-44', 'standard_price': 314873},
        {'default_code': 'ask-962-den-40', 'standard_price': 340000},
        {'default_code': 'ask-962-den-dasap-43', 'standard_price': 340000},
        {'default_code': 'ask-962-nau-39', 'standard_price': 340000},
        {'default_code': 'ask-962-nau-cothap-43', 'standard_price': 340000},
        {'default_code': 'ask-962-naudam-41', 'standard_price': 340000},
        {'default_code': 'bt-004-den-38', 'standard_price': 370000},
        {'default_code': 'bt-004-nau-39', 'standard_price': 370000},
        {'default_code': 'bt-009-den-datra-39', 'standard_price': 390000},
        {'default_code': 'bt-009-den-datra-40', 'standard_price': 390000},
        {'default_code': 'bt-50-den-s41', 'standard_price': 390000},
        {'default_code': 'f-016-cam-42', 'standard_price': 500000},
        {'default_code': 'gv-302-nau-39', 'standard_price': 375000},
        {'default_code': 'hl-404-den-s39', 'standard_price': 350000},
        {'default_code': 'hl-404-den-s42', 'standard_price': 350000},
        {'default_code': 'hl-404-nau-s40', 'standard_price': 350000},
        {'default_code': 'hl-404-nau-s42', 'standard_price': 350000},
        {'default_code': 'hm-097-nau-43', 'standard_price': 337578},
        {'default_code': 'hm-099-nau-43', 'standard_price': 420000},
        {'default_code': 'hm-099-xanh-40', 'standard_price': 420000},
        {'default_code': 'hm-099-xanh-42', 'standard_price': 420000},
        {'default_code': 'hm-242-nau-38', 'standard_price': 320000},
        {'default_code': 'hm-242-nau-39', 'standard_price': 320000},
        {'default_code': 'hm-242-nau-41', 'standard_price': 320000},
        {'default_code': 'hm-242-nau-42', 'standard_price': 320000},
        {'default_code': 'hm-246-nau-43', 'standard_price': 320000},
        {'default_code': 'hm-248-den-40', 'standard_price': 320000},
        {'default_code': 'hm-3558-xanh-40', 'standard_price': 320000},
        {'default_code': 'hv-4313-nau-39', 'standard_price': 380000},
        {'default_code': 'jb-005-den-42', 'standard_price': 500000},
        {'default_code': 'jb-691-canhgian-39', 'standard_price': 500000},
        {'default_code': 'jb-790-den-40', 'standard_price': 420000},
        {'default_code': 'jb-790-den-41', 'standard_price': 420000},
        {'default_code': 'kg-03-den-42', 'standard_price': 320000},
        {'default_code': 'kh-136-den-deden-40', 'standard_price': 390000},
        {'default_code': 'kh-1522-nau-40', 'standard_price': 370000},
        {'default_code': 'kh-1522-naunhat-40', 'standard_price': 370000},
        {'default_code': 'kh-1523-naunhat-40', 'standard_price': 370000},
        {'default_code': 'kh-388-7-nau-40', 'standard_price': 360000},
        {'default_code': 'kh-388-nau-39', 'standard_price': 360000},
        {'default_code': 'kh-388-nau-41', 'standard_price': 300000},
        {'default_code': 'kt-02-973-den-43', 'standard_price': 400000},
        {'default_code': 'kt-650-nautron-41', 'standard_price': 370000},
        {'default_code': 'lx-990-densap-39', 'standard_price': 450000},
        {'default_code': 'mp-107-dentron-38', 'standard_price': 280000},
        {'default_code': 'mp-107-dentron-41', 'standard_price': 280000},
        {'default_code': 'pc-125-den-dethap-40', 'standard_price': 360000},
        {'default_code': 'th-040-nau-40', 'standard_price': 606450},
        {'default_code': 'va-222-be-38', 'standard_price': 180000},
        {'default_code': 'va-335-den-42', 'standard_price': 360000},
        {'default_code': 'xq-26-nau-38', 'standard_price': 330000},
        {'default_code': 'z2-xanh-39', 'standard_price': 312762},
        {'default_code': 'z6-dabo-datron-chuong-39', 'standard_price': 410000},
        {'default_code': 'z6-den-datron-chuong-38', 'standard_price': 410000},
        {'default_code': 'z6-den-datron-chuong-43', 'standard_price': 410000},
        {'default_code': 'z6-nau-datron-chuong-39', 'standard_price': 410000},
        {'default_code': 'z6-nau-datron-chuong-42', 'standard_price': 410000},
        {'default_code': 'z6-nau-datron-chuong-43', 'standard_price': 410000},
        {'default_code': 'hl-405-den-s38', 'standard_price': 350000},
        {'default_code': 'VN-1202-den-s43', 'standard_price': 1000000},
        {'default_code': 'z9-den-dalon-38', 'standard_price': 380000}
    ]
    env = api.Environment(cr, SUPERUSER_ID, {})
    for product in list_product:
        product_model = env['product.product'].search([('default_code','=',product['default_code'])],limit=1)
        product_model.write({'standard_price': product['standard_price']})

