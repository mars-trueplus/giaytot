# -*- coding: utf-8 -*-

{
    'name': 'Magestore Test',
    'version': '1.0.0',
    'category': 'Tools',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Magestore Test',
    'description': """""",
    'depends': [
        'magestore_sale'
    ],
    'data': [
        'views/stock_quant_views.xml',
    ],
    'sequence': 1,
    'application': False,
    'installable': True,
    'auto_install': False,
    'post_init_hook': "post_init_hook",
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
