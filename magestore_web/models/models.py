# -*- coding: utf-8 -*-

from odoo import models, fields, api


class MagestoreWebStockQuant(models.Model):
    _inherit = 'stock.quant'

    @api.model
    def get_total_item_product_id(self, group_field):
        if group_field:
            self.env.cr.execute("select count(DISTINCT "+group_field+") from stock_quant;")
            return self.env.cr.fetchall()
        return []

class MagestoreWebStockHistory(models.Model):
    _inherit = 'stock.history'

    @api.model
    def get_total_item_product_id(self, group_field):
        self.env.cr.execute("select count(DISTINCT "+group_field+") from stock_history;")
        return self.env.cr.fetchall()
