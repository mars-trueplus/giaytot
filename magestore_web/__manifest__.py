# -*- coding: utf-8 -*-
{
    'name': "Magestore Web",
    'version': '1.0.0',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Magestore Web',
    'description': """""",
    'depends': [
        'base',
        'web',
        'stock',
        'stock_account',
    ],
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
    ],
    'qweb': ['static/src/xml/*.xml',],
    'application': True,
    'installable': True,
    'auto_install': False,
}