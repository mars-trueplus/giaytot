# -*- coding: utf-8 -*-

{
    'name': 'POS Store Demension',
    'version': '1.0.0',
    'category': 'POS',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'POS Store Demension',
    'description': """""",
    'depends': [
        'point_of_sale',
        'account',
    ],
    'data': [
        # 'security/magestore_store_dimension.xml',
        'security/ir.model.access.csv',
        'views/store_dimension.xml',
        'views/account_invoice_view.xml',
        'views/account_view.xml',
        'views/pos_config_view_inherit.xml',
        'views/account_financial_report_view_inherit.xml',
    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
