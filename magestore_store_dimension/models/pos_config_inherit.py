# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import uuid
from odoo import api, fields, models, _
from odoo.exceptions import UserError

class PosConfigInherit(models.Model):
    _inherit = 'pos.config'

    store_dimension = fields.Many2one('store.dimension', String="Store Dimension")

class PosSession(models.Model):
    _inherit = 'pos.session'

    @api.multi
    def action_pos_session_close(self):
        # Close CashBox
        # for session in self:
        #     # Tab
        #     pos_store_dimension = session.config_id.store_dimension.id
        #     company_id = session.config_id.company_id.id
        #     ctx = dict(self.env.context, force_company=company_id, company_id=company_id,
        #                pos_store_dimension=pos_store_dimension)
        #     for st in session.statement_ids:
        #         if abs(st.difference) > st.journal_id.amount_authorized_diff:
        #             # The pos manager can close statements with maximums.
        #             if not self.env['ir.model.access'].check_groups("point_of_sale.group_pos_manager"):
        #                 raise UserError(_(
        #                     "Your ending balance is too different from the theoretical cash closing (%.2f), the maximum allowed is: %.2f. You can contact your manager to force it.") % (
        #                                 st.difference, st.journal_id.amount_authorized_diff))
        #         if (st.journal_id.type not in ['bank', 'cash']):
        #             raise UserError(_("The type of the journal for your payment method should be bank or cash "))
        #         st.with_context(ctx).sudo().button_confirm_bank()
        # self.with_context(ctx)._confirm_orders()
        # self.write({'state': 'closed'})
        # return {
        #     'type': 'ir.actions.client',
        #     'name': 'Point of Sale Menu',
        #     'tag': 'reload',
        #     'params': {'menu_id': self.env.ref('point_of_sale.menu_point_root').id},
        # }
        for session in self:
            pos_store_dimension = session.config_id.store_dimension.id
            self = self.with_context(dict(pos_store_dimension = pos_store_dimension))
        super(PosSession,self).action_pos_session_close()