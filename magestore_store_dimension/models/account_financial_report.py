# -*- coding: utf-8 -*-

from odoo import fields, models, api

class AccountingReport(models.TransientModel):
    _inherit = "accounting.report"

    store_dimension = fields.Many2many('store.dimension', string="Store Dimension", help='')
