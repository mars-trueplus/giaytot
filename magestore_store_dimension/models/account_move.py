# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class AccountMoveLine(models.Model):
    _inherit = 'account.move.line'

    store_dimension = fields.Many2one('store.dimension', 'Store dimension', help='')


# Added by Adam 05/05/2017
class AccountMoveInherit(models.Model):
    _inherit = 'account.move'

    store_dimension = fields.Char(String="Store Dimension", size=200, compute="_get_store_dimension", store=True)

    @api.depends("line_ids")
    def _get_store_dimension(self):
        line_arr = []
        for rec in self:
            if rec.line_ids:
                for line in rec.line_ids:
                    rec.store_dimension = line.store_dimension.name
                    if line.store_dimension not in line_arr:
                        line_arr.append(line.store_dimension)
                    if len(line_arr) > 1:
                        rec.store_dimension = "see more"
                        break

    @api.model
    def create(self, vals):
        if vals.get('line_ids'):
            for val in vals['line_ids']:
                if val[2].get('ref'):
                    stocking_id = self.env['stock.picking'].search([('name', 'ilike', val[2].get('ref'))]).id or ''
                    pos_session = self.env['pos.order'].search([('picking_id', '=', stocking_id)])
                    if pos_session:
                        pos_session_id = pos_session.session_id.id or ''
                        pos_conf_id = self.env['pos.session'].browse(pos_session_id).config_id or ''
                        pos_dimension = self.env['pos.config'].browse(pos_conf_id.id).store_dimension.id or 0
                        val[2]['store_dimension'] = pos_dimension
        move = super(AccountMoveInherit,
                     self.with_context(check_move_validity=False, partner_id=vals.get('partner_id'))).create(vals)
        move.assert_balanced()
        return move