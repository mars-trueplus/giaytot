# -*- coding: utf-8 -*-

from odoo import models


class AccountBankStatementLine(models.Model):
    _inherit = 'account.bank.statement.line'

    def _prepare_reconciliation_move_line(self, move, amount):
        fields = super(AccountBankStatementLine, self)._prepare_reconciliation_move_line(move, amount)
        pos_store_dimension = 0
        if self.env.context.get('pos_store_dimension'):
            pos_store_dimension = self.env.context.get('pos_store_dimension')
        fields['store_dimension'] = pos_store_dimension or 0
        return fields

    def fast_counterpart_creation(self):
        pos_store_dimension = 0
        if self.env.context.get('pos_store_dimension'):
            pos_store_dimension = self.env.context.get('pos_store_dimension')
        for st_line in self:
            # Technical functionality to automatically reconcile by creating a new move line
            vals = {
                'name': st_line.name,
                'debit': st_line.amount < 0 and -st_line.amount or 0.0,
                'credit': st_line.amount > 0 and st_line.amount or 0.0,
                'account_id': st_line.account_id.id,
                'store_dimension': pos_store_dimension or 0
            }
            st_line.process_reconciliation(new_aml_dicts=[vals])