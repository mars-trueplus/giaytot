# -*- coding: utf-8 -*-
from odoo import api, fields, models

class StoreDimension(models.Model):
    _name = "store.dimension"
    _description = "Store dimention"

    name = fields.Char(string="Store dimension", required=True, translate=True)
    description = fields.Text(string="Description", translate=True)
    company = fields.Many2one('res.company', string='Company', translate=True)
