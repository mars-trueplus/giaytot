# -*- coding: utf-8 -*-

{
    'name': 'Magestore User Group',
    'version': '1.0.0',
    'category': 'User Group',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'User Group Limitation',
    'description': """""",
    'depends': [
        'base',
        'account',
        'point_of_sale',
    ],
    'data': [
        # 'security/magestore_store_dimension.xml',
        'security/ir.model.access.csv',

        'views/user_group_view.xml',
        'views/account_view_inherit.xml',
        'views/pos_payment_inherit.xml',
    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
