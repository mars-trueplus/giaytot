# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class MagestoreGroup(models.Model):
    _name = "magestore.group"
    _description = "User Group Module"

    name = fields.Char(required=True, size=255)
    user_ids = fields.Many2many('res.users', String="User")

    # _sql_constraints = [
    #     ('name_uniq', 'unique (name)', "Tag name already exists !"),
    # ]

