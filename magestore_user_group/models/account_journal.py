# -*- coding: utf-8 -*-

from odoo import api, fields, models, _

class AccountJournalInherit(models.Model):
    _inherit = "account.journal"

    # group_id
    private_user_group = fields.Many2one('magestore.group', "Private User Group")


class PosMakePayment(models.TransientModel):
    _inherit = 'pos.make.payment'

    def _get_user_groups(self):
        user = self.env.user
        groups = self.env['magestore.group'].search([('user_ids.id', '=', user.id)])
        return groups
    @api.model
    def _search_my_model_types(self):
        groups = self._get_user_groups()
        if groups:
            return [('private_user_group.id', 'in', groups.ids)]

        # return ['|',('private_user_group.id', 'in', groups.ids), ('private_user_group', '=', False)]

    # Remove default Journal Entry as BA Requirement
    def _default_journal(self):
        active_id = self.env.context.get('active_id')
        if active_id:
            session = self.env['pos.order'].browse(active_id).session_id
            groups = self._get_user_groups()
            if groups:
                return False
            else:
                return session.config_id.journal_ids and session.config_id.journal_ids.ids[0] or False
        return False

    journal_id = fields.Many2one('account.journal', string='Payment Mode', required=True,
                                 domain=_search_my_model_types, default=_default_journal)


