# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

#
# Please note that these reports are not multi-currency !!!
#

from odoo import api, fields, models, tools


class VendorReport(models.Model):
    _name = "vendor.report"
    _description = "Vendor Orders"
    _auto = False
    _order = 'date_order desc'

    date_order = fields.Datetime('Order Date', readonly=True, help="Date on which this document has been created")
    partner_id = fields.Many2one('res.partner', 'Vendor', readonly=True)
    picking_id = fields.Many2one('stock.picking', 'Stock Picking', readonly=True)
    group_id = fields.Many2one('procurement.group', 'Procurement Group',readonly=True)
    purchase_id = fields.Many2one('purchase.order', 'Purchase Order', readonly=True)
    product_id = fields.Many2one('product.product', 'Product', readonly=True)
    return_qty_request = fields.Integer('Return Quantity Request', readonly=True)
    return_qty_done = fields.Integer('Return Quantity Complete', readonly=True)
    return_reason = fields.Char("Return Reason", readonly=True)

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'vendor_report')
        self._cr.execute("""
            create view vendor_report as (
                select
                    min(sm.id) as id,
                    min(sp.id) as picking_id,
                    sp.create_date as date_order,
                    sp.partner_id as partner_id,
                    sp.group_id as group_id,
                    sm.product_id as product_id,
                    sp.return_reason as return_reason,
                    spo.product_qty as return_qty_request,
                    spo.qty_done as return_qty_done
                from stock_move sm
                left join stock_picking sp on (sm.picking_id = sp.id)
                left join stock_picking_type spt on (sp.picking_type_id = spt.id)
                join stock_pack_operation spo on (spo.picking_id = sp.id)
                where 
                    spt.code = 'outgoing' and sp.state in ('done','assigned') and 
                    sm.origin ilike 'po%' and spo.product_id = sm.product_id
                group by
                    sp.create_date,
                    sp.partner_id,
                    sm.product_id,
                    sm.purchase_line_id,
                    spo.product_qty,
                    spo.qty_done,
                    sp.return_reason,
                    sp.group_id
                order by sp.group_id DESC
            )
        """ )
