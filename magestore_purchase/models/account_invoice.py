# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class AccountInvoice(models.Model):
    _inherit = 'account.invoice'

    invoice_date = fields.Date('Invoice date', help='')
    invoice_form = fields.Char('Invoice form', size=20, help='')
    invoice_serial = fields.Char('Invoice serial', size=20, help='')
    invoice_number = fields.Char('Invoice number', size=10, help='')
    invoice_description = fields.Char('Invoice description', size=200, help='')
    organization_tax = fields.Char('Organization tax', size=20, help='')

    @api.onchange('partner_id')
    def _onchange_address(self):
        self.organization_tax = self.partner_id.organization_tax