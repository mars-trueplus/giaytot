# -*- coding: utf-8 -*-
from odoo import fields, api, models
from odoo.tools.float_utils import float_is_zero, float_compare


class PurchaseInvoice(models.Model):
    _inherit = "purchase.order"

    @api.depends('state', 'order_line.qty_invoiced', 'order_line.qty_received', 'order_line.product_qty')
    def _get_invoiced(self):
        precision = self.env['decimal.precision'].precision_get('Product Unit of Measure')
        for order in self:
            if order.state not in ('purchase', 'done'):
                order.invoice_status = 'no'
                continue

            if any(float_compare(line.qty_invoiced,
                                 line.product_qty if line.product_id.purchase_method == 'purchase' else line.qty_received,
                                 precision_digits=precision) == -1 for line in order.order_line):
                order.invoice_status = 'to invoice'
            elif all(float_compare(line.qty_invoiced,
                                   line.product_qty if line.product_id.purchase_method == 'purchase' else line.qty_received,
                                   precision_digits=precision) >= 0 for line in order.order_line):
                if len(order.invoice_ids.ids) != 0:
                    order.invoice_status = 'invoiced'
                else:
                    order.invoice_status = 'to invoice'
            else:
                order.invoice_status = 'no'