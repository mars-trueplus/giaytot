# -*- coding: utf-8 -*-

from odoo import api, fields, models

class Allocation(models.Model):
    _name = 'allocation.line'

    sku_code = fields.Char('SKU')
    required_quantity = fields.Float('Số lượng yêu cầu')
    purchase_order_id = fields.Many2one('purchase.order')
