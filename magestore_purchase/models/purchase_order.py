# -*- coding: utf-8 -*-

import json
import logging
import re

from operator import attrgetter, add
from lxml import etree
from lxml.builder import E

from odoo import api, fields, models, _, exceptions, registry, SUPERUSER_ID, _


class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'
    _order = 'date_planned desc, date_order desc, id desc'

    # Implement by Walter
    total_qty_ordered = fields.Integer(default=0, compute="_compute_qty_ordered")
    total_qty_received = fields.Integer(default=0, compute="_compute_qty_received")

    # Added by Adam to store the confirm user information
    user_id = fields.Many2one("res.users", String="Request By")
    supplier_phone = fields.Char('Số điện thoại nhà cung cấp', related='partner_id.phone')

    @api.constrains('order_line')
    def check_duplicate_order(self):
        all_products = []
        all_same_products = []
        for order_line in self.order_line:
            cur_product = order_line.product_id
            if cur_product in all_products:
                all_same_products.append(cur_product.name)
            all_products.append(order_line.product_id)

        all_same_products = self.remove_duplicate_element(all_same_products)
        if all_same_products:
            raise exceptions.ValidationError(_('Trùng lặp {} trong Order lines.'.format(', '.join(all_same_products))))

    def remove_duplicate_element(self, seq):
        set = {}
        map(set.__setitem__, seq, [])
        return set.keys()

    # Implement by Walter
    @api.multi
    def _compute_qty_ordered(self):
        for item_purchase_order in self:
            total_qty_ordered = 0
            for item_purchase_line in item_purchase_order.order_line:
                total_qty_ordered += item_purchase_line.product_qty

            item_purchase_order.total_qty_ordered = total_qty_ordered

    @api.multi
    def _compute_qty_received(self):
        for item_purchase_order in self:
            total_qty_received = 0
            for item_purchase_line in item_purchase_order.order_line:
                total_qty_received += item_purchase_line.qty_received

            # Changed By Adam to fixed the problem PO02-01
            item_purchase_order.total_qty_received = total_qty_received

    # Added by Adam to store the confirm user information
    @api.multi
    def button_confirm(self):
        for order in self:
            if order.state not in ['draft', 'sent']:
                continue
            order.user_id = self.env.user.id
            order._add_supplier_to_product()
            # Deal with double validation process
            if order.company_id.po_double_validation == 'one_step' \
                    or (order.company_id.po_double_validation == 'two_step' \
                                and order.amount_total < self.env.user.company_id.currency_id.compute(
                            order.company_id.po_double_validation_amount, order.currency_id)) \
                    or order.user_has_groups('purchase.group_purchase_manager'):
                order.button_approve()
            else:
                order.write({'state': 'to approve'})
        return True

    # @api.model
    # def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
    #     res = super(PurchaseOrder, self).fields_view_get(view_id=view_id, view_type=view_type, toolbar=toolbar,
    #                                                      submenu=submenu)
    #     # view = self.env.ref('magestore_purchase.magestore_purchase_order_form_inherit', raise_if_not_found=False)
    #     view = self.env.ref('purchase.purchase_order_form', raise_if_not_found=False)
    #     if view and view.exists() and view._name == 'ir.ui.view' and view_type == 'form':
    #         if 'fields' in res and 'company_id' in res['fields']:
    #             # change title of field company_id
    #             res['fields']['company_id']['string'] = 'Mars Company'
    #         res['fields']['allocation_ids']['views']['tree']['fields']['sku_code']['string'] = 'Hahahaahahahahah'
    #
    #         eview = etree.fromstring(res['arch'])
    #         old_field = eview.xpath("//field[@name='partner_id']")
    #         if len(old_field):
    #             old_field = old_field[0]
    #             # can't add field don't exist in any model
    #             old_field.addnext(etree.Element('newline'))
    #             old_field.addnext(etree.Element('label', {'string': 'Warning'}))
    #             old_field.addnext(etree.Element('newline'))
    #             old_field.addnext(etree.Element('field', {'name': 'allocation_ids','string': 'Day la field'}))
    #         res['arch'] = etree.tostring(eview)
    #     return res

    allocation_ids = fields.One2many('allocation.line', 'purchase_order_id', string='Allocation Lines')
