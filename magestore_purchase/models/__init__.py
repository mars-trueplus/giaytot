# -*- coding: utf-8 -*-

from . import res_partner
from . import account_invoice
from . import purchase_order # Added by Mars
from . import purchase_invoice
from . import purchase_to_inventory
