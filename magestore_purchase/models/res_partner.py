# -*- coding: utf-8 -*-

from odoo import api, fields, models, _


class ResPartner(models.Model):
    _inherit = 'res.partner'

    organization_tax = fields.Char('Organization tax', size=20, help='')

