# -*- coding: utf-8 -*-

{
    'name': 'Magestore Purchase Management',
    'version': '1.0.0',
    'category': 'Purchases',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Magestore Purchase Management',
    'description': """""",
    'depends': [
        'account',
        'purchase',
        'magestore_stock',
    ],
    'data': [
        'views/res_partner_view.xml',
        'views/account_invoice_view.xml',
        'report/purchase_order_templates.xml',
        'report/purchase_report_views.xml',
        'views/view_supplier.xml',
        'views/purchase_order_view.xml',
        'security/ir.model.access.csv'
    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
