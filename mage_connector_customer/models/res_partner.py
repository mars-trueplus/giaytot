from odoo import api, fields, models, _
from odoo.exceptions import UserError
import json
import hashlib
import datetime


class ResPartner(models.Model):
    _inherit = 'res.partner'

    source = fields.Char(string="Source")
    allow_sync = fields.Boolean(string="Allow Sync", default=True)

    @api.model
    def create(self, vals):
        context = self._context
        # res = self
        # if vals.get('email', False):
        #     res = self.search([('email', '=', vals.get('email'))])
        #     if not res:
        #         res = super(ResPartner, self).create(vals)
        #         if res and res.customer and not res.parent_id and res.allow_sync:
        #             mapping_obj = self.env['mage.mapping.customer'].search([('odoo_id', '=', res.id)])
        #             if not mapping_obj:
        #                 mapping_customer_data = {
        #                     'odoo_id': res.id,
        #                     'state': 'waiting'
        #                 }
        #                 self.env['mage.mapping.customer'].with_context({
        #                     'mage_id': context.get('mage_id', 0)
        #                 }).create(mapping_customer_data)
        #     else:
        #         if not context.get('instance_type') == 'magento':
        #             raise UserError('This email already exists!')
        # else:
        #     if not context.get('instance_type') == 'magento':
        #         raise UserError('Please input email field!')

        # Created by walter
        raw_email_customer = vals.get('email', False)
        if not raw_email_customer:
            # process_email_customer = vals.get('name').decode("utf-8") + str(datetime.datetime.now())
            process_email_customer = u' '.join((vals.get('name'), str(datetime.datetime.now()))).encode('utf-8').strip()
            email_customer = hashlib.md5(process_email_customer).hexdigest()
            vals['email'] = email_customer + "@gmail.com"
        # End

        res = super(ResPartner, self).create(vals)
        if res and res.customer and not res.parent_id and res.allow_sync:
            mapping_obj = self.env['mage.mapping.customer'].search([('odoo_id', '=', res.id)])
            if not mapping_obj:
                mapping_customer_data = {
                    'odoo_id': res.id,
                    'state': 'waiting'
                }
                self.env['mage.mapping.customer'].with_context({
                    'mage_id': context.get('mage_id', 0)
                }).create(mapping_customer_data)
            else:
                if not context.get('instance_type') == 'magento':
                    raise UserError('This email already exists!')

        return res

    @api.multi
    def write(self, vals):
        context = self._context
        if context.get('instance_type') == 'magento':
            if not isinstance(vals, dict):
                vals = json.loads(vals)
        customer_obj = super(ResPartner, self).write(vals)

        for customer in self:
            if customer.allow_sync:
                mapping_obj = self.env['mage.mapping.customer'].search([('odoo_id', '=', customer.id)])
                if mapping_obj:
                    mapping_obj.write({'state': 'waiting'})

        return customer_obj

