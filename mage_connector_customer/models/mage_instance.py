from odoo import models, fields, api, tools
import datetime
import json

import logging
_logger = logging.getLogger(__name__)


class MageInstance(models.Model):
    _inherit = "mage.instance"

    auto_sync_customer = fields.Boolean('Auto Sync', default=True)

    @api.multi
    def do_sync(self):
        self.do_sync_country_region_magento2odoo()
        cron_sync_magento2odoo_obj = self.env['ir.cron'].sudo().search([
            ('function', '=', 'do_sync_customer_magento2odoo'),
            ('model', '=', 'mage.instance'),
            ('active', '=', False)
        ])
        if cron_sync_magento2odoo_obj:
            cron_sync_magento2odoo_obj.write({'active': True})

        return super(MageInstance, self).do_sync()

    @api.multi
    def do_sync_country_region_magento2odoo(self):
        try:
            response = self.connect('odooconnector.get_country_regions')
            customers_data = response['data']
            mapped_data = []
            if customers_data:
                for region in customers_data:
                    odoo_country_id = False
                    mage_country_id = region.get('country_id', False)
                    if mage_country_id:
                        res_country_obj = self.env['res.country'].search([('code', '=', mage_country_id)])
                        if res_country_obj:
                            odoo_country_id = res_country_obj.id

                    res_country_state_obj = self.env['res.country.state'].search([
                        ('country_id', '=', odoo_country_id),
                        ('code', '=', region.get('code', False))
                    ])
                    if res_country_state_obj:
                        self.env['res.country.state'].write({'name': region.get('name', False)})
                        res_country_state_id = res_country_state_obj.id if res_country_state_obj else 0
                        mapped_data.append({
                            'odoo_id': res_country_state_id,
                            'mage_id': region.get('id', 0)
                        })
                    else:
                        res_country_state_obj = self.env['res.country.state'].with_context({
                            'mage_id': region.get('id', 0)
                        }).create({
                            'country_id': odoo_country_id,
                            'code': region.get('code', False),
                            'name': region.get('name', False)
                        })
                    res_country_state_id = res_country_state_obj.id if res_country_state_obj else 0
                    mapped_data.append({
                        'odoo_id': res_country_state_id,
                        'mage_id': region.get('id', 0)
                    })
            data_json = json.dumps(mapped_data)
            self.connect('odooconnector.create_region_mapping', [data_json])
        except Exception as ex:
            _logger.exception('Sync Region - error: %s' % tools.ustr(ex))

        return True

    @api.multi
    def do_sync_customer_magento2odoo(self):
        try:
            instances_obj = self.env['mage.instance'].search([])
            for instance in instances_obj:
                response = instance.connect('odooconnector.get_customers')
                customers_data = response['data']
                mapped_data = []
                if customers_data:
                    for customer in customers_data:
                        child_ids = []
                        customer_vals = {
                            'name': customer.get('name', 'empty'),
                            'email': customer.get('email', False),
                            'source': 'Magento',
                            'customer': True
                        }
                        customer_address = customer.get('address')
                        for index, contact in enumerate(customer_address):
                            odoo_country_id = False
                            res_country_obj = self.env['res.country'].search([('code', '=', contact.get('country_id', False))])
                            if res_country_obj:
                                odoo_country_id = res_country_obj.id

                            if index == 0:
                                customer_vals.update({
                                    'phone': contact.get('telephone', False),
                                    'zip': contact.get('postcode', False),
                                    'street': contact.get('street', False),
                                    'city': contact.get('city', False),
                                    'state_id': contact.get('state_id', False),
                                    'country_id': odoo_country_id
                                })
                            else:
                                child_ids.append([0, False, {
                                    'name': contact.get('contact_name', 'empty'),
                                    'email': 'contact-%s@gmail.com' % tools.ustr(datetime.datetime.now().strftime("%Y%m%d%H%M%S%f")),
                                    'phone': contact.get('telephone', False),
                                    'street': contact.get('street', False),
                                    'city': contact.get('city', False),
                                    'zip': contact.get('postcode', False),
                                    'state_id': contact.get('state_id', False),
                                    'country_id': odoo_country_id,
                                    'type': 'other',
                                    'allow_sync': False,
                                    'source': 'Magento',
                                    'customer': True
                                }])
                        customer_vals.update({'child_ids': child_ids})

                        res_partner_obj = self.env['res.partner'].with_context({
                            'instance_id': instance.id,
                            'instance_type': 'magento',
                            'mage_id': customer.get('id', 0)
                        }).create(customer_vals)
                        _logger.exception('Sync Customer - create success')
                        mapped_data.append({
                            'odoo_id': res_partner_obj.id if res_partner_obj else 0,
                            'mage_id': customer.get('id', 0)
                        })

                    data_json = json.dumps(mapped_data)
                    instance.connect('odooconnector.create_customer_mapping', [data_json])
        except Exception as ex:
            _logger.exception('Sync Customer - error: %s' % tools.ustr(ex))
        return True
