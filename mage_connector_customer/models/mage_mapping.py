from odoo import models, fields, api, tools
import json

import logging
_logger = logging.getLogger(__name__)


class MageMappingCustomer(models.Model):
    _name = "mage.mapping.customer"
    _inherit = "mage.mapping"

    mapping_line_id = fields.One2many('mage.mapping.customer.line', 'mapping_ids', string="Mapping Line")
    odoo_id = fields.Many2one('res.partner', string="Odoo Customer")

    @api.model
    def create(self, vals):
        mapping_obj = super(MageMappingCustomer, self.with_context({
            'model_mapping_line': 'mage.mapping.customer.line',
            'mage_id': self._context.get('mage_id', 0)
        })).create(vals)

        return mapping_obj

    @api.multi
    def write(self, vals):
        mapping_obj = super(MageMappingCustomer, self.with_context({
            'mapping_id': self.id,
            'model_mapping_line': 'mage.mapping.customer.line'
        })).write(vals)

        return mapping_obj

    @api.multi
    def do_mapping(self):
        mapping_lines_obj = self.env['mage.mapping.customer.line'].search([('mapping_ids', '=', self.id), ('state', '!=', 'done')])
        for line in mapping_lines_obj:
            customer_obj = self.odoo_id
            customer_address = [{
                'first_name': customer_obj.name,
                'last_name': ' ',
                'phone': customer_obj.phone if customer_obj.phone else customer_obj.mobile,
                'street': customer_obj.street,
                'city': customer_obj.city,
                # 'state_id': customer_obj.state_id.code,
                'country_id': customer_obj.country_id.code
            }]

            contacts_obj = self.env['res.partner'].search([('parent_id', '=', customer_obj.id)])
            for contact in contacts_obj:
                contact_data = {
                        'first_name': contact.name,
                        'last_name': ' ',
                        'phone': contact.phone,
                        'street': contact.street,
                        'city': contact.city,
                        # 'state_id': contact.state_id.code,
                        'country_id': contact.country_id.code
                    }
                customer_address.append(contact_data)

            customer_mapping_data = {
                'customer_id': customer_obj.id,
                'first_name': customer_obj.name,
                'last_name': ' ',
                'email': customer_obj.email,
                'address': customer_address
            }
            data_json = json.dumps(customer_mapping_data)
            response = line.instance_ids.connect('odooconnector.mapping_customer', [data_json])
            customer_mapping_missing_obj = self.env['mage.mapping.missing'].search([
                ('instance_id', '=', line.instance_ids.id),
                ('mapping_id', '=', self.id),
                ('mapping_type', '=', 'customer')
            ])
            if response['status']:
                line.write({
                    'mage_id': response['data'],
                    'state': 'done'
                })
                if customer_mapping_missing_obj:
                    customer_mapping_missing_obj.unlink()
            else:
                line.write({'state': 'fail'})
                if not customer_mapping_missing_obj:
                    self.env['mage.mapping.missing'].create({
                        'instance_id': line.instance_ids.id,
                        'mapping_id': self.id,
                        'mapping_type': 'customer',
                        'mapping_message':  response['message']
                    })
                else:
                    customer_mapping_missing_obj.write({'mapping_message':  response['message']})
        self.write({
            'state': 'mapped'
        })
        return True

    @api.multi
    def do_mapping_auto(self):
        try:
            mappings_obj = self.env['mage.mapping.customer'].search([('state', '=', 'waiting')], limit=10)
            for mapping in mappings_obj:
                mapping.do_mapping()
            return True
        except Exception as ex:
            _logger.exception('Customer - cron job mapping error: %s' % tools.ustr(ex))

    @api.model
    def create_customer(self, vals):
        try:
            context = self._context
            customer_id = 0
            if context.get('instance_type') == 'magento':
                if not isinstance(vals, dict):
                    vals = json.loads(vals)
                    customer_vals = {
                        'name': vals.get('name'),
                        'email': vals.get('email', False),
                        'allow_sync': True,
                        'customer': True
                    }
                    customer_id = self.env['res.partner'].with_context({
                        'instance_url': context.get('instance_url'),
                        'instance_type': 'magento'
                    }).create(customer_vals)
            return customer_id
        except Exception as ex:
            _logger.exception('Customer - create error: %s' % tools.ustr(ex))


class MageMappingCustomerLine(models.Model):
    _name = "mage.mapping.customer.line"
    _inherit = "mage.mapping.line"

    mage_id = fields.Integer(string="Magento Customer")


class MageMappingResConutryState(models.Model):
    _name = "mage.mapping.res.country.state"
    _inherit = "mage.mapping"

    odoo_id = fields.Many2one('res.country.state', string="Odoo Country State")
    mapping_line_id = fields.One2many('mage.mapping.res.country.state.line', 'mapping_ids', string="Mapping Line")

    @api.model
    def create(self, vals):
        mapping_obj = super(MageMappingResConutryState, self.with_context({
            'model_mapping_line': 'mage.mapping.res.country.state.line',
            'mage_id': self._context.get('mage_id', 0)
        })).create(vals)

        return mapping_obj

    @api.multi
    def write(self, vals):
        mapping_obj = super(MageMappingResConutryState, self.with_context({
            'mapping_id': self.id,
            'model_mapping_line': 'mage.mapping.res.country.state.line'
        })).write(vals)

        return mapping_obj


class MageMappingResConutryStateLine(models.Model):
    _name = "mage.mapping.res.country.state.line"
    _inherit = "mage.mapping.line"

    mage_id = fields.Integer(string="Magento Country State")
