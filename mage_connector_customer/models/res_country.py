from odoo import api, fields, models, _


class ResCountryState(models.Model):
    _inherit = 'res.country.state'

    @api.model
    def create(self, vals):
        context = self._context
        res = super(ResCountryState, self).create(vals)
        if res:
            mapping_obj = self.env['mage.mapping.res.country.state'].search([('odoo_id', '=', res.id)])
            if not mapping_obj:
                self.env['mage.mapping.res.country.state'].with_context({
                    'mage_id': context.get('mage_id', 0)
                }).create({
                    'odoo_id': res.id,
                    'state': 'waiting'
                })
        return res
