# -*- coding: utf-8 -*-

{
    'name': 'Magento Connector Customer',
    'version': '1.0.0',
    'category': 'Connector Modules',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Magento Connector Customer',
    'description': """""",
    'depends': [
        'base',
        'mage_connector_base'
    ],
    'data': [
        'security/ir.model.access.csv',
        'data/customer_cron.xml',
        'views/mage_mapping_view.xml',
        'views/mage_instance_view.xml',
        'views/res_partner_view.xml'
    ],
    'sequence': 2,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
