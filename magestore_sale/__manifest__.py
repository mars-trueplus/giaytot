# -*- coding: utf-8 -*-

{
    'name': 'Magestore Sale',
    'version': '1.0.0',
    'category': 'Sales',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Quotations, Sales Orders, Invoicing',
    'description': """""",
    'depends': [
        'sale',
        'stock',
        'product',
        'sale_stock'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/product_tags.xml',
        'views/product_view.xml',
        'views/sale_order_views.xml',
        'views/website_sale_orders_inherit.xml',
        'views/stock_return.xml',
        'static/src/xml/sale_report_templates.xml',
        'views/sale_order_report.xml'
    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
    'post_init_hook': "post_init_hook",
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
