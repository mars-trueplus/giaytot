import models
import report # Added by Mars

from odoo import api, SUPERUSER_ID

#update shipping processing
def post_init_hook(cr, registry):
    env = api.Environment(cr, SUPERUSER_ID, {})
    sale_orders = env['sale.order'].search([])
    for sale_order in sale_orders:
        try:
            total_qty_ordered = 0
            total_qty_delivered = 0
            shipping_processing = ''
            for item_order_line in sale_order.order_line:
                if item_order_line.product_id.product_tmpl_id.type == 'service':
                    continue
                total_qty_ordered += item_order_line.product_uom_qty
                total_qty_delivered += item_order_line.qty_delivered

            if total_qty_delivered == 0:
                shipping_processing = 'not_delivery'
            if total_qty_delivered > 0 and total_qty_delivered < total_qty_ordered:
                shipping_processing = 'partial'
            if total_qty_delivered == total_qty_ordered:
                shipping_processing = 'done'

            sale_order.write({
                'total_qty_ordered': total_qty_ordered,
                'total_qty_delivered': total_qty_delivered,
                'delivery_state': shipping_processing,
            })
        except Exception as e:
            continue