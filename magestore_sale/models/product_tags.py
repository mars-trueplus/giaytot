# -*- coding: utf-8 -*-
from odoo import api, fields, models, _

class ProductTags(models.Model):
    _name = "product.tags"
    _description = "Product Tags"

    name = fields.Char(required=True, size=255)
    color = fields.Integer(string='Color Index')

    _sql_constraints = [
        ('name_uniq', 'unique (name)', "Tag name already exists !"),
    ]

    # tag_ids = fields.Many2many('project.tags', string='Tags', oldname='categ_ids')