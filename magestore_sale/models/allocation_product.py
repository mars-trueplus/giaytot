# -*- coding: utf-8 -*-

from odoo import fields, api, models,_


class AllocationProduct(models.Model):
    _name = 'allocation.product'

    name = fields.Char(string='Allocation Product')
    warehouse_ids = fields.Many2many('stock.warehouse', string='Kho')
    allocation_product_lines = fields.One2many('allocation.product.line', 'allocation_product_id',
                                               string='Phân bổ hàng')
    company_id = fields.Many2one('res.company', string=_(u'Company'),
                                 default=lambda self: self.env['res.company']._company_default_get('allocation.product'))

    @api.model
    def create(self, vals):
        res = super(AllocationProduct, self).create(vals)
        return res

class AllocationProductLine(models.Model):
    _name = 'allocation.product.line'

    allocation_product_id = fields.Many2one('allocation.product', string='Allocation Product')
    warehouse_name = fields.Char('Kho')
    sku = fields.Char('SKU')
    qty_stock = fields.Float('Số lượng tồn kho')
    qty_shipping = fields.Float('Số lượng xuất hàng')
    qty_sale = fields.Float('Tổng số lượng bán')
