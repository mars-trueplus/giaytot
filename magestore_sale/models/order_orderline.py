# -*- coding: utf-8 -*-
from odoo import api, fields, models


class OrderOrderLine(models.Model):
    _inherit = "sale.order"

    total_qty_ordered = fields.Float('Total Ordered', default=0.0)
    total_qty_delivered = fields.Float('Total Delivered', default=0.0)

    # @api.multi
    # def _compute_total_qty_ordered(self):
    #
    #     for item in self:
    #         _sum = 0.0
    #         for record in item.order_line:
    #             _sum += record.product_uom_qty
    #             item.total_qty_ordered = _sum
    #
    # @api.multi
    # def _compute_total_qty_delivered(self):
    #
    #     for item in self:
    #         _sum = 0.0
    #         for record in item.order_line:
    #             _sum += record.qty_delivered
    #             item.total_qty_delivered = _sum
