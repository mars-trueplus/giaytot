# -*- coding: utf-8 -*-

from odoo import fields, api, models

class ResPartner(models.Model):
    _inherit = 'res.partner'

    collaborator = fields.Boolean(string='Là Cộng tác viên')