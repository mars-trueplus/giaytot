# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
# Created by Mars
from datetime import date
import datetime
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, _, exceptions
from odoo.exceptions import UserError


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    # Implement by Walter
    total_qty_delivered = fields.Integer(default=0)
    total_qty_ordered = fields.Integer(default=0, compute='_compute_qty_ordered')
    # delivery_state = fields.Selection([('done', 'Done'),('partial','Partial Delivery'), ('not_delivery','Not Delivery')], string="Delivery State", compute="_get_delivery_state")
    delivery_state = fields.Selection([
        ('not_delivery', 'Not Ship'),
        ('partial', 'Ship Partial'),
        ('done', 'Shipped')
    ], readonly=True, required=True, string='Shipping Processing', default='not_delivery')
    # override team_id attribute of sale.order in module sale
    team_id = fields.Many2one('crm.team', 'Sales Team', change_default=True, default=None,
                              oldname='section_id', required=True)

    # Added by Adam
    # def _get_delivery_state(self):
    #     for item in self:
    #         if item.total_qty_delivered == item.total_qty_ordered:
    #             item.delivery_state = 'done'
    #         else:
    #             if item.total_qty_delivered == 0:
    #                 item.delivery_state = 'not_delivery'
    #             else:
    #                 item.delivery_state = 'partial'

    # Added by Mars
    def _default_domain_collaborator(self):
        return [('collaborator', '=', True)]

    @api.one
    @api.depends('team_id')
    def _compute_show_collaborator(self):
        name_sale_team = 'Bán cộng tác viên'
        if self.team_id:
            self.show_collaborator = True if self.team_id.name.encode('utf-8') == name_sale_team else False

    collaborator_id = fields.Many2one('res.partner', string='Cộng tác viên',
                                      domain=lambda self: self._default_domain_collaborator())
    show_collaborator = fields.Boolean(string='Show Collaborator', compute='_compute_show_collaborator')
    customer_phone = fields.Char('Số điện thoại khách hàng', related='partner_id.phone')
    allocation_product_id = fields.Many2one('allocation.product', string='Allocation Product')

    @api.constrains('order_line')
    def check_duplicate_order(self):
        all_products = []
        all_same_products = []
        for order_line in self.order_line:
            cur_product = order_line.product_id
            if cur_product in all_products:
                all_same_products.append(cur_product.name)
            all_products.append(order_line.product_id)

        all_same_products = self.remove_duplicate_element(all_same_products)
        if all_same_products:
            raise exceptions.ValidationError(_(u'Trùng lặp {} trong Order lines.'.format(', '.join(all_same_products))))

    def remove_duplicate_element(self, seq):
        set = {}
        map(set.__setitem__, seq, [])
        return set.keys()

    # Implement by Walter
    @api.multi
    def _compute_qty_ordered(self):
        for item_sale_order in self:
            total_qty_ordered = 0
            for item_order_line in item_sale_order.order_line:
                if not item_order_line.product_id.product_tmpl_id.type == 'service':
                    total_qty_ordered += item_order_line.product_uom_qty

            item_sale_order.total_qty_ordered = total_qty_ordered

    # @api.multi
    # def _compute_qty_delivered(self):
    #     for item_sale_order in self:
    #         total_qty_delivered = 0
    #         for item_order_line in item_sale_order.order_line:
    #             total_qty_delivered += item_order_line.qty_delivered
    #
    #         item_sale_order.total_qty_delivered = total_qty_delivered

    @api.multi
    def action_confirm(self):
        if not self.team_id or self.team_id == '':
            raise UserError(_("Trường đội ngũ bán hàng không được để trống!"))
        else:
            return super(SaleOrder, self).action_confirm()

    @api.multi
    def allocate_product(self):
        view = self.env.ref('magestore_sale.allocation_product_form')
        current_company = self.env['res.company']._company_default_get('allocation.product')
        # list warehouse of current company
        current_warehouses = self.env['stock.warehouse'].search([('company_id', '=', current_company.id)])
        # get all products of sale order
        all_product = self.order_line

        list_allocation_lines = []
        for index, warehouse in enumerate(current_warehouses):
            for product in all_product:
                allocation_line = [0, False]
                vals = {}
                if not index:
                    vals['sku'] = product
                    vals['qty_stock'] = product.product_uom_qty
                    vals['qty_shipping'] = 1



                    # sku = fields.Char('SKU')
                    # qty_stock = fields.Float('Số lượng tồn kho')
                    # qty_shipping = fields.Float('Số lượng xuất hàng')
                    # qty_sale = fields.Float('Tổng số lượng bán')


        return {
            'name': _('Allocation Product'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'allocation.product',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'current',
            'res_id': False,
            'context': {'sale_order_id': self.id}
        }

    # Added by Mars
    apply_require_carrier_supplier = fields.Boolean('Apply require', compute='_apply_require_attribute', default=False)

    @api.depends('team_id')
    def _apply_require_attribute(self):
        name_team = None
        if self.team_id:
            name_team = self.team_id.name.lower()
        if name_team and ('online' in name_team or 'website' in name_team):
            self.apply_require_carrier_supplier = True
        else:
            self.apply_require_carrier_supplier = False

