# -*- coding: utf-8 -*-

from . import allocation_product
from . import product_tags
from . import product_template
# from . import order_orderline
from . import sale_order # Added by Mars
from . import res_partner
from . import stock_picking