# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
# Created by Michael
from odoo import api, models


class Picking(models.Model):
    _inherit = "stock.picking"

    @api.multi
    def do_transfer(self):
        super(Picking, self).do_transfer()
        sale_order = self.sale_id
        # sale_order = self.env['sale.order'].browse(sale_id)
        # total_qty_ordered = 0
        total_qty_delivered = 0
        shipping_processing = ''
        for item_order_line in sale_order.order_line:
            if item_order_line.product_id.product_tmpl_id.type == 'service':
                continue
            # total_qty_ordered += item_order_line.product_uom_qty
            total_qty_delivered += item_order_line.qty_delivered

        # sale_order.total_qty_ordered = total_qty_ordered
        # sale_order.total_qty_delivered = total_qty_delivered

        if total_qty_delivered == 0:
            shipping_processing = 'not_delivery'
        if total_qty_delivered > 0 and total_qty_delivered < sale_order.total_qty_ordered:
            shipping_processing = 'partial'
        if total_qty_delivered == sale_order.total_qty_ordered:
            shipping_processing = 'done'

        sale_order.write({
            # 'total_qty_ordered': total_qty_ordered,
            'total_qty_delivered': total_qty_delivered,
            'delivery_state': shipping_processing,
        })
        return True