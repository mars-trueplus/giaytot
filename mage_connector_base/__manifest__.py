# -*- coding: utf-8 -*-

{
    'name': 'Connector Base',
    'version': '1.0.0',
    'category': 'Connector Modules',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Connector Base',
    'description': """""",
    'depends': [
        'stock'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/mage_instance_view.xml',
        'views/mage_mapping_view.xml',
        'views/mage_mapping_missing_view.xml',
        'wizard/message_wizard_view.xml'
    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
