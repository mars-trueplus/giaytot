from odoo import models, fields, api


class MageMapping(models.AbstractModel):
    _name = "mage.mapping"

    state = fields.Selection([('waiting', 'Waiting'), ('mapped', 'Mapped')], string='Status', default='waiting')

    @api.model
    def create(self, vals):
        context = self._context
        res = super(MageMapping, self).create(vals)
        model_mapping_line = self._context.get('model_mapping_line')
        if model_mapping_line:
            instances_obj = self.env['mage.instance'].search([('state', '!=', 'draft')])
            mage_id = context.get('mage_id', 0)
            for ins in instances_obj:
                mapping_line_data = {
                    'mapping_ids': res.id,
                    'mage_id': mage_id,
                    'instance_ids': ins.id,
                    'state': 'done' if mage_id else 'waiting'
                }
                self.env[model_mapping_line].create(mapping_line_data)

            if len(instances_obj) and mage_id:
                res.write({'state': 'mapped'})
        return res

    @api.multi
    def write(self, vals):
        if vals.get('state') == 'waiting':
            model_mapping_line = self._context.get('model_mapping_line')
            if model_mapping_line:
                self.env[model_mapping_line].search([
                    ('mapping_ids', '=', self._context.get('mapping_id')),
                    ('state', '!=', 'waiting')
                ]).write({'state': 'waiting'})

        return super(MageMapping, self).write(vals)


class MageMappingLine(models.AbstractModel):
    _name = "mage.mapping.line"

    mapping_ids = fields.Many2one('mage.mapping', string="Mapping")
    instance_ids = fields.Many2one('mage.instance', string="Instance")
    state = fields.Selection([
        ('waiting', 'Waiting'),
        ('fail', 'Fail'),
        ('done', 'Done')
    ], string='Status', default='waiting')


