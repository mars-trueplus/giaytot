from odoo import models, fields, api


class MageMappingMissing(models.Model):
    _name = "mage.mapping.missing"

    instance_id = fields.Many2one('mage.instance', string="Instance")
    mapping_id = fields.Many2one('mage.mapping', string="Mapping")
    mapping_type = fields.Selection([
        ('customer', 'Customer'),
        ('product', 'Product'),
        ('sale_order', 'Sale Order')
    ], string="Type")
    mapping_message = fields.Char(string="Message")
