from odoo import models, fields, api, tools
import xmlrpclib

import logging
_logger = logging.getLogger(__name__)


class MageInstance(models.Model):
    _name = "mage.instance"
    _description = "Connection for all XML RPC"

    name = fields.Char(string='Name', required=True)
    base_url = fields.Char(string='Base URL', required=True)
    user = fields.Char(string='API User Name', required=True)
    pwd = fields.Char(string='API Password', required=True, size=50)
    active = fields.Boolean(string="Active", default=False)
    state = fields.Selection([
        ('draft', 'Draft'),
        ('connected', 'Connected'),
        ('synchronizing', 'Synchronizing'),
        ('synchrozed', 'Synchrozed')
    ], string='Status', track_visibility='onchange', help='Status of the instance', default='draft')
    # stock_location = fields.Many2one('stock.warehouse', string="Stock Location")
    stock_location = fields.Many2one('stock.warehouse', 'Warehouse', required=True)
    _sql_constraints = [
        ('value_base_url_uniq', 'unique (base_url)', 'This base url already exists!')
    ]

    @api.model
    def connect(self, method=False, params=None):
        if params is None:
            params = []
        status = False
        data = None
        if self.active:
            try:
                server = xmlrpclib.Server(self.base_url)
                session = server.login(self.user, self.pwd)
                if session and method:
                    data = server.call(session, method, params)
                status = True
                message = "Connect successful!"
            except Exception as ex:
                _logger.exception('Mage Instance - Connect Magento error: %s' % tools.ustr(ex))
                message = "%s" % tools.ustr(ex)
        else:
            message = "Please active this instance to connect!"
        return {
            'status': status,
            'message': message,
            'data': data
        }

    @api.multi
    def do_connect(self):
        connection = self.connect()

        status = connection.get("status")
        if status:
            self.write({'state': 'connected'})

        message = connection.get("message")
        partial = self.env['message.wizard'].create({'message': message})

        return {
            'name': ("Test Connection"),
            'view_mode': 'form',
            'view_type': 'form',
            'res_model': 'message.wizard',
            'view_id': self.env.ref('mage_connector_base.message_wizard_mage_instance_form').id,
            'res_id': partial.id,
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
        }

    @api.model
    def check_connection(self, vals):
        connector_url = vals.get('connector_url')
        if connector_url:
            instance_id = self.search([('active', '=', True), ('base_url', '=', '%sapi/xmlrpc' % connector_url)])
            if instance_id:
                return True
        return False

    @api.model
    def get_instance_by_url(self, instance_url):
        instance_obj = 0
        if instance_url:
            instance_obj = self.search([
                ('active', '=', True),
                ('base_url', '=', '%sindex.php/api/xmlrpc' % instance_url)
            ])
            if not instance_obj:
                instance_obj = self.search([
                    ('active', '=', True),
                    ('base_url', '=', '%sapi/xmlrpc' % instance_url)
                ])
        return instance_obj


    @api.multi
    def do_sync(self):
        self.write({'state': 'synchronizing'})

        return True
