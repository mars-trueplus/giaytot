from urllib import unquote_plus


def unescape(text):
    try:
        text = unquote_plus(text.encode('utf8'))
        return text
    except Exception:
        return text
