from odoo import api, fields, models


class MessageWizard(models.TransientModel):
    _name = "message.wizard"

    message = fields.Text(string='Message', readonly=True, translate=True)

