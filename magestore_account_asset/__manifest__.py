# -*- coding: utf-8 -*-

{
    'name': 'Magestore Account Asset',
    'version': '1.0.0',
    'category': 'Assets Management',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Magestore Account Asset',
    'description': """""",
    'depends': [
        'account_asset',
        'magestore_account',
    ],
    'data': [
        'wizard/asset_depreciation_confirmation_wizard_views_inherit.xml',
        'views/account_asset_views_inherit.xml',
        'views/asset_number_sequence_view.xml',
        'views/asset_number_view.xml'
    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
