# -*- coding: utf-8 -*-
from odoo import api, fields, models


class AssetNumber(models.Model):
    _inherit = "account.asset.asset"

    asset_number = fields.Char('Asset Number')

    @api.model
    def create(self, vals):
        _category = vals.get('category_id')
        _asset_number_sequence = self.env['account.asset.category'].search([('id', '=', _category)])

        if _asset_number_sequence:
            _asset_number_sequence_id = _asset_number_sequence.number_sequence_id

            _asset_prefix = _asset_number_sequence_id.prefix if _asset_number_sequence_id.prefix else ''
            _asset_number_next_actual = _asset_number_sequence_id.number_next_actual
            _asset_padding = _asset_number_sequence_id.padding

            _asset_number = _asset_prefix
            for i in range(int(_asset_padding)):
                _asset_number += '0'

            vals.update({
                'asset_number': _asset_number + str(_asset_number_next_actual),
            })

            # Raise number sequence
            _sequence = self.env['ir.sequence'].search([('id', '=', _asset_number_sequence_id.id)])
            _sequence.write({
                'number_next_actual': _asset_number_next_actual + 1,
            })
            # print _sequence.number_next_actual

        return super(AssetNumber, self).create(vals)
