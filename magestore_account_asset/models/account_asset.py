# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, _

class AccountAssetCategory(models.Model):
    _inherit = 'account.asset.category'

    account_chart_id = fields.Many2one("account.account", String="Sell entries: Expense Lose")