# -*- coding: utf-8 -*-

from odoo import api, fields, models


class AssetNumberSequence(models.Model):
    _inherit = "account.asset.category"

    number_sequence_id = fields.Many2one('ir.sequence')
