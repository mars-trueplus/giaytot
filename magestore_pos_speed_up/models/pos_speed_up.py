# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import base64
from ast import literal_eval

from odoo import models, fields, api
from odoo.tools import pickle as cPickle


class pos_speed_up(models.Model):
    _name = 'pos.speed.up'

    cache = fields.Binary(attachment=True)
    type = fields.Selection([('product', 'Product'), ('customer', 'Customer')])
    domain_cache = fields.Text(required=True)
    fields_cache = fields.Text(required=True)

    config_id = fields.Many2one('pos.config', ondelete='cascade', required=True)
    # compute_user_id = fields.Many2one('res.users', 'Cache compute user', required=True)

    @api.model
    def refresh_all_caches(self):
        # self.env['pos.speed.up'].search([]).refresh_cache()
        for cache in self.env['pos.speed.up'].search([]):
            cache.refresh_cache(cache.type)

    @api.one
    def refresh_cache(self, type):
        if type == 'product':
            products = self.env['product.product'].search(self.get_domain_cache())
            data_ctx = products.with_context(pricelist=self.config_id.pricelist_id.id, display_default_code=False)
        if type == 'customer':
            customers = self.env['res.partner'].search(self.get_domain_cache())
            data_ctx = customers.with_context()

        res = data_ctx.read(self.get_fields_cache())
        if type == 'product':
            if len(res) > 0:
                for product in res:
                    if len(product['taxes_id']) > 0:
                        l = []
                        for tax in product['taxes_id']:
                            if self.env['account.tax'].browse(tax).company_id.id == self.config_id.company_id.id:
                                l.append(tax)
                        product['taxes_id'] = l
        datas = {
            'cache': base64.encodestring(cPickle.dumps(res)),
        }

        self.write(datas)

    @api.model
    def get_domain_cache(self):
        return literal_eval(self.domain_cache)

    @api.model
    def get_fields_cache(self):
        return literal_eval(self.fields_cache)

    @api.model
    def get_cache(self, domain, fields, type):
        if not self.cache or domain != self.get_domain_cache() or fields != self.get_fields_cache():
            self.domain_cache = str(domain)
            self.fields_cache = str(fields)
            self.type = type
            self.refresh_cache(type)
        cache = base64.decodestring(self.cache)
        return cPickle.loads(cache)


class pos_config(models.Model):
    _inherit = 'pos.config'

    @api.one
    @api.depends('cache_ids')
    def _get_oldest_cache_time(self):
        pos_speed_up = self.env['pos.speed.up']
        oldest_cache = pos_speed_up.search([('config_id', '=', self.id)], order='write_date', limit=1)
        if oldest_cache:
            self.oldest_cache_time = oldest_cache.write_date

    # Use a related model to avoid the load of the cache when the pos load his config
    cache_ids = fields.One2many('pos.speed.up', 'config_id')
    oldest_cache_time = fields.Datetime(compute='_get_oldest_cache_time', string='Oldest cache time', readonly=True)

    def _get_cache_for_user(self, type):
        pos_speed_up = self.env['pos.speed.up']
        cache_for_user = pos_speed_up.search([('id', 'in', self.cache_ids.ids), ('type', '=', type)])

        if cache_for_user:
            return cache_for_user[0]
        else:
            return None

    @api.multi
    def get_data_from_cache(self, fields, domain, type):
        cache_for_user = self._get_cache_for_user(type)

        if cache_for_user:
            return cache_for_user.get_cache(domain, fields, type)
        else:
            pos_speed_up = self.env['pos.speed.up']
            pos_speed_up.create({
                'config_id': self.id,
                'domain_cache': str(domain),
                'fields_cache': str(fields),
                'type': type
            })
            new_cache = self._get_cache_for_user(type)
            return new_cache.get_cache(domain, fields, type)

    # @api.multi
    # def get_customers_from_cache(self, fields, domain):


    @api.one
    def delete_cache(self):
        # throw away the old caches
        self.cache_ids.unlink()
