odoo.define('magestore_pos_speed_up.magestore_pos_speed_up', function (require) {
    "use strict";
    var core = require('web.core');
    var models = require('point_of_sale.models');
    var Model = require('web.DataModel');
    var _t = core._t;
    var posmodel_super = models.PosModel.prototype;
    var customer_fields = '';
    models.PosModel = models.PosModel.extend({
        load_server_data: function () {
            var self = this;

            var product_index = _.findIndex(this.models, function (model) {
                return model.model === "product.product";
            });

            var customer_index = _.findIndex(this.models, function (model) {
                return model.model === "res.partner";
            });

            var product_model = this.models[product_index];
            var customer_model = this.models[customer_index];

            if (product_index !== -1) {
                this.models.splice(product_index, 1);
            }

            if (customer_index !== -1) {
                this.models.splice(customer_index, 1);
            }
            self.customer_fields = customer_model.fields;
            return posmodel_super.load_server_data.apply(this, arguments).then(function () {

                var customer_fields = customer_model.fields;
                var customer_domain = customer_model.domain;
                var records = new Model('pos.config').call('get_data_from_cache',
                                                               [self.pos_session.config_id[0], customer_fields, customer_domain, 'customer']);

                self.chrome.loading_message(_t('Loading') + ' res.partner', 1);
                records.then(function (customer) {
                    self.partners = customer;
                    self.db.add_partners(customer);
                });

                var product_fields = product_model.fields;
                var product_domain = product_model.domain;
                var records = new Model('pos.config').call('get_data_from_cache',
                                                               [self.pos_session.config_id[0], product_fields, product_domain, 'product']);

                self.chrome.loading_message(_t('Loading') + ' product.product', 1);
                return records.then(function (product) {
                    self.db.add_products(product);
                });
                // self.load_product_data(product_model);
                // self.load_customer_data(customer_model);
            });
        },

        load_product_data: function(product_model) {
            var self = this;
            var product_fields = product_model.fields;
            var product_domain = product_model.domain;
            var records = new Model('pos.config').call('get_data_from_cache',
                                                           [self.pos_session.config_id[0], product_fields, product_domain, 'product']);

            self.chrome.loading_message(_t('Loading') + ' product.product', 1);
            return records.then(function (product) {
                self.db.add_products(product);
            });
        },

        load_customer_data: function(customer_model) {
            var self = this;
            var customer_fields = customer_model.fields;
            var customer_domain = customer_model.domain;
            var records = new Model('pos.config').call('get_data_from_cache',
                                                           [self.pos_session.config_id[0], customer_fields, customer_domain, 'customer']);

            self.chrome.loading_message(_t('Loading') + ' res.partner', 1);
            return records.then(function (customer) {
                self.db.add_partners(customer);
            });
        },

        load_new_partners: function() {
            var self = this;
            var def  = new $.Deferred();
            // var customer_index = _.findIndex(this.models, function (model) {
            //     return model.model === "res.partner";
            // })
            // var fields = customer_index.fields;
            var fields = self.customer_fields;
            var customer_create_date = this.db.get_partner_write_date();
            if (customer_create_date == '1970-01-01 00:00:00') {
                var date_from = new Date();
                date_from.setMinutes(date_from.getMinutes() - 10);

                var dd = date_from.getDate();
                var mm = date_from.getUTCMonth()+1; //January is 0!

                var yyyy = date_from.getUTCFullYear();
                var hh = date_from.getUTCHours();
                var tt = date_from.getUTCMinutes();
                var ss = date_from.getUTCSeconds();

                if (dd<10) {
                    dd='0'+dd;
                }
                if (mm<10) {
                    mm='0'+mm;
                }
                if (hh<10) {
                    hh='0'+hh;
                }
                if (tt<10) {
                    tt='0'+tt;
                }
                if (ss<10) {
                    ss='0'+ss;
                }
                var date_from_utc = yyyy+'-'+mm+'-'+dd+' '+hh+':'+tt+':'+ss;
                customer_create_date = date_from_utc;
            }
            new Model('res.partner')
                .query(fields)
                .filter([['customer','=',true],['write_date','>',customer_create_date]])
                .all({'timeout':10000, 'shadow': true})
                .then(function(partners){
                    if (self.db.add_partners(partners)) {   // check if the partners we got were real updates
                        def.resolve();
                    } else {
                        def.reject();
                    }
                }, function(err,event){ event.preventDefault(); def.reject(); });
            return def;
        },
    });
});
