# -*- coding: utf-8 -*-

import base64
import cStringIO
import xlwt
from datetime import datetime
from odoo import api, models, fields, _


class WizardValuationMovementSummary(models.TransientModel):
    _name = 'wizard.valuation.movement.summary'
    _description = 'Wizard that opens the stock valuation movement summary table'

    name = fields.Char(string="File Name", readonly=True)
    from_date = fields.Datetime('From Date', default=fields.Datetime.now, required=True)
    to_date = fields.Datetime('To Date', default=fields.Datetime.now, required=True)
    location_id = fields.Many2one("stock.location", string="Location", required=True, domain=[('usage', 'ilike', 'internal')])
    data = fields.Binary(string="File", readonly=True)
    state = fields.Selection([('choose', 'choose'), ('get', 'get')], default='choose')
    file_type = fields.Selection([('xls', 'xls')], default='xls', required=True)

    @api.multi
    def do_export(self):
        if self.location_id:
            self._export_to_xls()
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'wizard.valuation.movement.summary',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': self.id,
            'views': [(False, 'form')],
            'target': 'new',
        }

    def _export_to_xls(self):
        stock_move = self.env['stock.move']
        products = self.env['product.product'].search([]).with_context({'location': self.location_id.id})
        done_movings = stock_move.search([('state', '=', 'done'), ('date', '<=', self.to_date), ('date', '>=', self.from_date)])
        sheet_dict = []
        for product in products:
            init_quantity = 0
            init_done_movings = stock_move.search([('state', '=', 'done'), ('date', '<', self.from_date), ('product_id.id', '=', product.id), '|', ('location_dest_id.id', '=', self.location_id.id), ('location_id.id', '=', self.location_id.id)])
            for init_done_moving in init_done_movings:
                if init_done_moving.location_dest_id.id == self.location_id.id:
                    init_quantity += init_done_moving.product_uom_qty
                if init_done_moving.location_id.id == self.location_id.id:
                    init_quantity -= init_done_moving.product_uom_qty

            purchase_import_quantity = 0
            return_import_quantity = 0
            transfer_import_quantity = 0

            sale_export_quantity = 0
            return_export_quantity = 0
            transfer_export_quantity = 0

            for done_moving in done_movings:
                if done_moving.product_id.id == product.id:

                    if done_moving.location_dest_id.id == self.location_id.id:
                        if done_moving.location_id.usage == 'supplier':
                            purchase_import_quantity += done_moving.product_uom_qty
                        if done_moving.location_id.usage == 'customer':
                            return_import_quantity += done_moving.product_uom_qty
                        if done_moving.location_id.usage == 'internal' or done_moving.location_id.usage == 'inventory':
                            transfer_import_quantity += done_moving.product_uom_qty

                    if done_moving.location_id.id == self.location_id.id:
                        if done_moving.location_dest_id.usage == 'customer':
                            sale_export_quantity += done_moving.product_uom_qty
                        if done_moving.location_dest_id.usage == 'supplier':
                            return_export_quantity += done_moving.product_uom_qty
                        if done_moving.location_dest_id.usage == 'internal' or done_moving.location_dest_id.usage == 'inventory':
                            transfer_export_quantity += done_moving.product_uom_qty

            total_import_quantity = purchase_import_quantity + return_import_quantity + transfer_import_quantity
            total_export_quantity = sale_export_quantity + return_export_quantity + transfer_export_quantity
            total = init_quantity + total_import_quantity - total_export_quantity

            default_code = product.default_code
            if not default_code:
                default_code = ''

            sheet_dict.append({
                "Mã sản phẩm": default_code,
                "Tên sản phẩm": product.name,
                "Tồn kho đầu kỳ": init_quantity,
                "Nhập mua": purchase_import_quantity,
                "Nhập trả": return_import_quantity,
                "Nhập chuyển": transfer_import_quantity,
                "Tổng nhập": total_import_quantity,
                "Xuất bán": sale_export_quantity,
                "Xuất trả": return_export_quantity,
                "Xuất chuyển": transfer_export_quantity,
                "Tổng xuất": total_export_quantity,
                "Tồn kho cuối kỳ": total
            })

        # sheet.write_merge(top_row, bottom_row, left_column, right_column, 'Long Cell')

        wb = xlwt.Workbook(encoding='utf8')
        bold = "font: bold on;align: horizontal center;align: vertical center;"
        border = "border: left thin, right thin, top thin, bottom thin;"
        background_color = "pattern: pattern solid, fore_colour bright_green;"
        bold_border = bold + border + background_color
        bold = xlwt.easyxf(bold)
        border = xlwt.easyxf(border)
        bold_border = xlwt.easyxf(bold_border)
        title = xlwt.easyxf("font: bold on,color red;align: horizontal center;align: vertical center;")
        sheet = wb.add_sheet('sheet', cell_overwrite_ok=True)

        sheet.write_merge(0, 0, 4, 7, "BÁO CÁO XUẤT - NHẬP - TỒN", bold)
        sheet.write_merge(1, 1, 4, 7, self.location_id.name_get()[0][1], title)
        sheet.write_merge(2, 2, 4, 7, datetime.strptime(self.from_date, '%Y-%m-%d %H:%M:%S').strftime('%d-%m-%Y %H:%M:%S') + ' đến ' + datetime.strptime(self.to_date, '%Y-%m-%d %H:%M:%S').strftime('%d-%m-%Y %H:%M:%S'), title)


        sheet.write_merge(4, 5, 0, 0, "Mã sản phẩm", bold_border)
        sheet.write_merge(4, 5, 1, 1, "Tên sản phẩm", bold_border)
        sheet.write_merge(4, 5, 2, 2, "Tồn kho đầu kỳ", bold_border)

        sheet.write_merge(4, 4, 3, 6, "Nhập trong kỳ", bold_border)
        sheet.write(5, 3, "Nhập mua", bold_border)
        sheet.write(5, 4, "Nhập trả", bold_border)
        sheet.write(5, 5, "Nhập chuyển", bold_border)
        sheet.write(5, 6, "Tổng nhập", bold_border)

        sheet.write_merge(4, 4, 7, 10, "Xuất trong kỳ", bold_border)
        sheet.write(5, 7, "Xuất bán", bold_border)
        sheet.write(5, 8, "Xuất trả", bold_border)
        sheet.write(5, 9, "Xuất chuyển", bold_border)
        sheet.write(5, 10, "Tổng xuất", bold_border)

        sheet.write_merge(4, 5, 11, 11, "Tồn kho cuối kỳ", bold_border)
        index = 5
        for item in sheet_dict:

            sheet.write(index + 1, 0, item['Mã sản phẩm'], border)
            sheet.write(index + 1, 1, item['Tên sản phẩm'], border)
            sheet.write(index + 1, 2, item['Tồn kho đầu kỳ'], border)

            sheet.write(index + 1, 3, item['Nhập mua'], border)
            sheet.write(index + 1, 4, item['Nhập trả'], border)
            sheet.write(index + 1, 5, item['Nhập chuyển'], border)
            sheet.write(index + 1, 6, item['Tổng nhập'], border)

            sheet.write(index + 1, 7, item['Xuất bán'], border)
            sheet.write(index + 1, 8, item['Xuất trả'], border)
            sheet.write(index + 1, 9, item['Xuất chuyển'], border)
            sheet.write(index + 1, 10, item['Tổng xuất'], border)

            sheet.write(index + 1, 11, item['Tồn kho cuối kỳ'], border)

            index += 1

        total_style = xlwt.easyxf('font: bold on,color red;border: left thin, right thin, top thin, bottom thin;pattern: pattern solid, fore_colour yellow;')
        sheet.write(index + 1, 1, "Tổng", total_style)
        sheet.write(index + 1, 2, xlwt.Formula('SUM(C3:C' + str(index+1) + ')'), total_style)
        sheet.write(index + 1, 3, xlwt.Formula('SUM(D3:D' + str(index+1) + ')'), total_style)
        sheet.write(index + 1, 4, xlwt.Formula('SUM(E3:E' + str(index+1) + ')'), total_style)
        sheet.write(index + 1, 5, xlwt.Formula('SUM(F3:F' + str(index+1) + ')'), total_style)
        sheet.write(index + 1, 6, xlwt.Formula('SUM(G3:G' + str(index+1) + ')'), total_style)
        sheet.write(index + 1, 7, xlwt.Formula('SUM(H3:H' + str(index+1) + ')'), total_style)
        sheet.write(index + 1, 8, xlwt.Formula('SUM(I3:I' + str(index+1) + ')'), total_style)
        sheet.write(index + 1, 9, xlwt.Formula('SUM(J3:J' + str(index+1) + ')'), total_style)
        sheet.write(index + 1, 10, xlwt.Formula('SUM(K3:K' + str(index+1) + ')'), total_style)
        sheet.write(index + 1, 11, xlwt.Formula('SUM(L3:L' + str(index+1) + ')'), total_style)

        data = cStringIO.StringIO()
        wb.save(data)
        data.seek(0)
        out = base64.encodestring(data.read())
        data.close()

        self.write({
            'state': 'get',
            'data': out,
            'name': 'Movement_summary_%s.xls' % (self.location_id.name_get()[0][1])
        })
