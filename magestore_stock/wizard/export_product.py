# -*- coding: utf-8 -*-
from odoo import api, fields, models
import base64
import contextlib
import cStringIO
import csv
import xlwt


class ExportProduct(models.TransientModel):
    _name = "export.location.product.wizard"

    name = fields.Char(string="File Name", readonly=True)
    data = fields.Binary(string="File", readonly=True)
    file_type = fields.Selection([('xls', 'xls'), ('csv', 'csv')], default='xls', required=True)
    stock_location_id = fields.Many2one('stock.location', string="Stock Location", required=True)
    state = fields.Selection([('choose', 'choose'), ('get', 'get')], default='choose')

    @api.multi
    def do_export(self):
        if self.stock_location_id:
            if self.file_type == 'csv':
                self._export_to_csv()
            else:
                self._export_to_xls()
        return {
            'type': 'ir.actions.act_window',
            'res_model': 'export.location.product.wizard',
            'view_mode': 'form',
            'view_type': 'form',
            'res_id': self.id,
            'views': [(False, 'form')],
            'target': 'new',
        }

    def _export_to_csv(self):
        products = self.env['product.product'].search([]).with_context({'location': self.stock_location_id.id})
        with contextlib.closing(cStringIO.StringIO()) as buf:
            # buf = cStringIO.StringIO()
            writer = csv.writer(buf, delimiter=";", quotechar='"')
            writer.writerow(("product_sku", "product_name", "product_qty"))
            for item in products:
                product_sku = item.default_code.encode('utf8') if item.default_code else ''
                product_name = item.name_get()[0][1].encode('utf8') if len(item.name_get()) > 0 else ''
                product_name = product_name.replace('[%s]' % (product_sku), '').strip()
                qty_available = item.qty_available

                writer.writerow((
                    product_sku,
                    product_name,
                    item.qty_available
                ))
            out = base64.encodestring(buf.getvalue())
            self.write({
                'state': 'get',
                'data': out,
                'name': 'Stock_location_%s.csv' % (self.stock_location_id.name_get()[0][1])
            })

    def _export_to_xls(self):
        products = self.env['product.product'].search([]).with_context({'location': self.stock_location_id.id})
        wb = xlwt.Workbook(encoding='utf8')
        sheet = wb.add_sheet('sheet')
        sheet.write(0, 0, "product_sku")
        sheet.write(0, 1, "product_name")
        sheet.write(0, 2, "product_qty")
        index = 0
        for item in products:
            product_sku = item.default_code.encode('utf8') if item.default_code else ''
            product_name = item.name_get()[0][1].encode('utf8') if len(item.name_get()) > 0 else ''
            product_name = product_name.replace('[%s]' % (product_sku), '').strip()
            qty_available = item.qty_available

            sheet.write(index + 1, 0, product_sku)
            sheet.write(index + 1, 1, product_name)
            sheet.write(index + 1, 2, qty_available)
            index += 1

        data = cStringIO.StringIO()
        wb.save(data)
        data.seek(0)
        out = base64.encodestring(data.read())
        data.close()

        self.write({
            'state': 'get',
            'data': out,
            'name': 'Stock_location_%s.xls' % (self.stock_location_id.name_get()[0][1])
        })
