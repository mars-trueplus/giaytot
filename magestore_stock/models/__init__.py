# -*- coding: utf-8 -*-

from . import stock_picking_return
from . import stock_picking
from . import stock_quant
# Added by Mars
from . import res_users
from . import stock_picking_inventory
from . import stock_return_picking_line
# from . import stock_movement_summary
import stock_inventory
from . import stock_warehouse
