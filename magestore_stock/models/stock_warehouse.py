# -*- coding: utf-8 -*-
from odoo import api, fields, models, _


class Warehouse(models.Model):
    _inherit = "stock.warehouse"
    _order = 'sequence, id'

    sequence = fields.Integer('Sequence', help="Used to order the 'All Operations' kanban view")