from datetime import datetime

from odoo import api, fields, models, tools

import logging

_logger = logging.getLogger(__name__)


class Location(models.TransientModel):
    _name = "stock.location.inventory"

    @api.model
    def fields_view_get(self, view_id=None, view_type='form', toolbar=False, submenu=False):
        fields_view = super(Location, self).fields_view_get(view_id, view_type, toolbar=toolbar, submenu=False)
        cr = self._cr
        table_name = self._table
        cr.execute('truncate "%s" RESTART IDENTITY;' % table_name )
        stock_loc = self.env['stock.location']
        product_loc = self.env['product.product']
        internal_locations = stock_loc.search([('usage', '=', 'internal')])
        internal_location_ids = [internal_location.id for internal_location in internal_locations]
        operators = ['>', '<']
        value = dict()
        for internal_location_id in internal_location_ids:
            value[internal_location_id] = {}
            value[internal_location_id]['ids'] = []
            value[internal_location_id]['product_list'] = {}
            for operator in operators:
                product_ids_searchs = product_loc.with_context(
                    {'location': internal_location_id})._search_qty_available_new(operator, 0,
                                                                                  self._context.get('lot_id'),
                                                                                  self._context.get('owner_id'),
                                                                                  self._context.get('package_id')) or []
                if product_ids_searchs:
                    for product_ids_search in product_ids_searchs:
                        value[internal_location_id]['ids'].append(product_ids_search)
            if value[internal_location_id]['ids']:
                rec = product_loc.with_context(
                    {'location': internal_location_id, 'stock_picking_inventory': 1,
                     'ids': value[internal_location_id]['ids']})._product_available() or []
                value[internal_location_id]['product_list'] = rec

        for internal_location in internal_locations:
            for product in value[internal_location.id]['product_list']:

                products = self.env['product.product'].browse(product)

                # variable_attributes = products.attribute_line_ids.filtered(lambda l: len(l.value_ids) > 1).mapped(
                #     'attribute_id')
                # variant = products.attribute_value_ids._variant_name(variable_attributes)
                # print 'variant'
                # print variant
                # name = variant and "%s (%s)" % (products.name, variant) or products.name
                # print 'name'
                # print name
                # print 'products.attribute_value_ids'
                # print products.attribute_value_ids
                # attributes = "%s,"*len(products.attribute_value_ids) % tuple([("%s: %s" % (values.attribute_id.name, values.name)).encode('utf-8') for values in products.attribute_value_ids])
                # attributes = ', '.join([("%s: %s" % (values.attribute_id.name, values.name)).encode('utf-8') for values in products.attribute_value_ids])
                # print 'attributes'
                # print attributes

                product_forecasted_quantity = value[internal_location.id]['product_list'][product]['qty_available'] + \
                                              value[internal_location.id]['product_list'][product]['incoming_qty'] - \
                                              value[internal_location.id]['product_list'][product]['outgoing_qty']

                self.create({
                    'location_id': internal_location.id,
                    'location_product_id': product,
                    'location_product_qty': value[internal_location.id]['product_list'][product][
                        'qty_available'],
                    'gt_product_default_code': product_loc.browse(product).default_code,
                    'gt_product_name': product_loc.browse(product).name,
                    'name': product_loc.browse(product).name,
                    'gt_product_lst_price': product_loc.browse(product).list_price,
                    'gt_product_barcode': product_loc.browse(product).barcode,
                    'gt_product_forecasted_quantity': product_forecasted_quantity,
                    'gt_product_quantity_on_hand': value[internal_location.id]['product_list'][product][
                        'qty_available'],
                    'gt_product_attribute_value': ', '.join([("%s: %s" % (values.attribute_id.name, values.name)).encode('utf-8') for values in products.attribute_value_ids])
                    # 'company_id':self.env['stock.location'].browse(idss).company_id.id
                })
        return fields_view

    location_id = fields.Many2one('stock.location', 'Location Name')
    location_product_id = fields.Many2one('product.product', 'Product')
    location_product_qty = fields.Float('Quantity')
    gt_product_default_code = fields.Char('Internal Reference', index=True)
    gt_product_name = fields.Char('Name', index=True)
    # Adam added to display name in the form
    name = fields.Char('Name')
    gt_product_attribute_value = fields.Char('Attributes', index=True)
    gt_product_lst_price = fields.Float('Sale Price', index=True)
    gt_product_quantity_on_hand = fields.Float('Quantity On Hand', index=True)
    gt_product_forecasted_quantity = fields.Float('Forecasted Quantity', index=True)
    gt_product_barcode = fields.Char('Barcode', index=True)
    # company_id = fields.Many2one(
    #     'res.company', 'Company',
    #     default=lambda self: self.env['res.company']._company_default_get('stock.location'), index=True,
    #     help='Let this field empty if this location is shared between companies')


class ProductProduct(models.Model):
    _inherit = 'product.product'

    @api.multi
    def _product_available(self, field_names=None, arg=False):
        """ Compatibility method """
        if self._context.get('stock_picking_inventory'):
            self._ids = self._context.get('ids')
        return self._compute_quantities_dict(self._context.get('lot_id'), self._context.get('owner_id'),
                                             self._context.get('package_id'), self._context.get('from_date'),
                                             self._context.get('to_date'))
