from datetime import datetime

from odoo import api, fields, models
from odoo.tools.float_utils import float_compare, float_round
from odoo.tools.translate import _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.exceptions import UserError

import logging

_logger = logging.getLogger(__name__)


class Quant(models.Model):
    """ Added by Adam: G.IM 9 """
    _inherit = "stock.quant"

    unit_cost = fields.Float('Unit Value', compute='_compute_unit_cost', readonly=True, store=False, group_operator='avg')

    @api.depends("inventory_value", "qty")
    def _compute_unit_cost(self):
        for quant in self:
            if quant.qty:
                quant.unit_cost = quant.inventory_value / quant.qty
            else:
                quant.unit_cost = 0
                # quant.unit_cost = quant.product_id.standard_price
