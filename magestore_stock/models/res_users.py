# -*- coding: utf-8 -*-
# Author: Mars
from odoo import api, fields, models


class ResUsers(models.Model):
    _inherit = 'res.users'

    location_ids = fields.Many2many("stock.location", string="Inventory IDs")

    @api.model
    def check_user_has_setting_privilege(self, uid):
        cr = self._cr
        cr.execute('SELECT * FROM res_groups_users_rel WHERE gid = %s and uid = %s' % (4, uid))
        result = dict(cr.fetchall())
        return True if result else False
