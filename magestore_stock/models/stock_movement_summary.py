# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import api, fields, models, tools
from odoo.fields import Datetime as fieldsDatetime


class StockMovementSummary(models.Model):
    _name = "stock.movement.summary"
    _auto = False
    _order = 'date asc'

    product_id = fields.Many2one('product.product', 'Product', required=True)
    move_id = fields.Many2one('stock.move', 'Stock Move', required=True)
    location_id = fields.Many2one('stock.location', 'Location', required=True)

    # Tồn kho đầu kỳ
    init_quantity = fields.Float('Init Quantity', readonly=True, compute="_get_init_quantity")
    # Nhập mua
    purchase_import_quantity = fields.Float('Purchase Import Quantity')
    # Nhập trả
    return_import_quantity = fields.Float('Return Import Quantity')
    # Nhập chuyển
    transfer_import_quantity = fields.Float('Transfer Import Quantity')
    # Tổng nhâp: total_quantity_import = purchase_import_quantity + return_import_quantity + transfer_import_quantity
    total_import_quantity = fields.Float('Total Import Quantity')

    # Xuất bán
    sale_export_quantity = fields.Float('Sale Export Quantity')
    # Xuất trả
    return_export_quantity = fields.Float('Return Export Quantity')
    # Xuất chuyển
    transfer_export_quantity = fields.Float('Transfer Export Quantity')
    # Xuất phế liệu
    scrap_export_quantity = fields.Float('Scrap Export Quantity')
    # Tổng xuất: total_quantity_export = sale_export_quantity + return_export_quantity + transfer_export_quantity + scrap_export_quantity
    total_export_quantity = fields.Float('Total Export Quantity')

    # Tồn kho cuối kỳ: ending_quantity = init_quantity + total_import_quantity - total_export_quantity
    ending_quantity = fields.Float("End Quantity")
    # Tồn kho cuối kỳ sau điều chỉnh: ton kho tai thoi diem to_date
    ending_quantity_adjusted = fields.Float("End Quantity Adjusted")
    # Khác biệt: different_quantity = abs(ending_quantity_adjusted - ending_quantity) (luôn lớn hơn 0)
    different_quantity = fields.Float("Difference Quantity")

    # from_date = fields.Datetime("From Date")
    # to_date = fields.Datetime("To Date")
    date = fields.Datetime("Operation Date")

    @api.model_cr
    def init(self):
        tools.drop_view_if_exists(self._cr, 'stock_movement_summary')
        self._cr.execute("""
        CREATE VIEW stock_movement_summary AS (
            SELECT MIN(id) as id,
                location_id,
                product_id
            FROM stock_quant
            GROUP BY location_id, product_id
            )
        """)
