# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from collections import namedtuple
import json
import time

from odoo import api, fields, models, _
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT
from odoo.tools.float_utils import float_compare
from odoo.addons.procurement.models import procurement
from odoo.exceptions import UserError, AccessError, ValidationError


class PickingInherit(models.Model):
    _inherit = "stock.picking"
    _order = 'min_date desc'

    return_reason = fields.Char("Return Reason", size=255)
    picking_type_code_default = fields.Char('Code')
    # rewrite picking_type_id
    picking_type_id = fields.Many2one('stock.picking.type', 'Picking Type', required=True,
                                      states={'done': [('readonly', True)], 'cancel': [('readonly', True)]},
                                      domain="[('code', 'like', picking_type_code_default)]")
    user_transfer_id = fields.Many2one("res.users", String="Người chuyển")
    user_id = fields.Many2one("res.users", String="Người xác nhận")

    @api.multi
    def action_confirm(self):
        for item in self:
            item.user_transfer_id = item.env.user.id
        self.filtered(lambda picking: not picking.move_lines).write({'launch_pack_operations': True})
        # TDE CLEANME: use of launch pack operation, really useful ?
        self.mapped('move_lines').filtered(lambda move: move.state == 'draft').action_confirm()
        self.filtered(
            lambda picking: picking.location_id.usage in ('supplier', 'inventory', 'production')).force_assign()
        return True

    @api.multi
    def do_new_transfer(self):
        self.can_validate = True # Added by Mars for check user can validate or not
        for pick in self:
            pick.user_id = self.env.user.id
            pack_operations_delete = self.env['stock.pack.operation']
            if not pick.move_lines and not pick.pack_operation_ids:
                raise UserError(_('Please create some Initial Demand or Mark as Todo and create some Operations. '))
            # In draft or with no pack operations edited yet, ask if we can just do everything
            if pick.state == 'draft' or all([x.qty_done == 0.0 for x in pick.pack_operation_ids]):
                # If no lots when needed, raise error
                picking_type = pick.picking_type_id
                if (picking_type.use_create_lots or picking_type.use_existing_lots):
                    for pack in pick.pack_operation_ids:
                        if pack.product_id and pack.product_id.tracking != 'none':
                            raise UserError(
                                _('Some products require lots/serial numbers, so you need to specify those first!'))
                view = self.env.ref('stock.view_immediate_transfer')
                wiz = self.env['stock.immediate.transfer'].create({'pick_id': pick.id})
                # TDE FIXME: a return in a loop, what a good idea. Really.
                return {
                    'name': _('Immediate Transfer?'),
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'stock.immediate.transfer',
                    'views': [(view.id, 'form')],
                    'view_id': view.id,
                    'target': 'new',
                    'res_id': wiz.id,
                    'context': self.env.context,
                }

            # Check backorder should check for other barcodes
            if pick.check_backorder():
                view = self.env.ref('stock.view_backorder_confirmation')
                wiz = self.env['stock.backorder.confirmation'].create({'pick_id': pick.id})
                # TDE FIXME: same reamrk as above actually
                return {
                    'name': _('Create Backorder?'),
                    'type': 'ir.actions.act_window',
                    'view_type': 'form',
                    'view_mode': 'form',
                    'res_model': 'stock.backorder.confirmation',
                    'views': [(view.id, 'form')],
                    'view_id': view.id,
                    'target': 'new',
                    'res_id': wiz.id,
                    'context': self.env.context,
                }
            for operation in pick.pack_operation_ids:
                if operation.qty_done < 0:
                    raise UserError(_('No negative quantities allowed'))
                if operation.qty_done > 0:
                    operation.write({'product_qty': operation.qty_done})
                else:
                    pack_operations_delete |= operation
            if pack_operations_delete:
                pack_operations_delete.unlink()
        self.do_transfer()
        return

    @api.onchange('location_dest_id')
    def _change_location_dest_correct(self):
        if self.picking_type_code == 'internal' and self.location_dest_id.id != self.picking_type_id.default_location_dest_id.id and self.location_dest_id != '':
            self.location_dest_id = self.picking_type_id.default_location_dest_id

    # Added by Mars
    @api.constrains('location_id', 'location_dest_id')
    def _check_locations_of_user(self):
        picking_type_code = self.picking_type_id.code
        src_location = self.location_id
        # des_location = self.location_dest_id
        user_locations = self.env.user.location_ids.ids
        picking_type_default_src_location = self.picking_type_id.default_location_src_id
        picking_type_default_des_location = self.picking_type_id.default_location_dest_id

        error_bol = True
        error_message = ""
        if picking_type_code == 'internal' and src_location.id not in user_locations and not self.can_validate:
            error_message = src_location.display_name
        elif picking_type_code == 'incoming' and picking_type_default_des_location.id not in user_locations:
            error_message = picking_type_default_des_location.display_name
        elif picking_type_code == 'outgoing' and picking_type_default_src_location.id not in user_locations:
            error_message = picking_type_default_src_location.display_name
        else:
            error_bol = False
        if error_bol and error_message:
            raise ValidationError(_(u"You don't have privilege for {} location(s)".format(error_message)))


    source_location_internal = fields.Boolean('Source Location of User - Internal picking',
                                              compute='calculate_to_show_hide_button')
    dest_location_internal = fields.Boolean('Destination Location of User - Internal picking',
                                            compute='calculate_to_show_hide_button')
    dest_location_incoming = fields.Boolean('Destination Location of User - Incoming picking',
                                            compute='calculate_to_show_hide_button')
    source_location_outgoing = fields.Boolean('Source Location of User - Outgoing picking',
                                              compute='calculate_to_show_hide_button')
    # when user click Validate button --> this field = True --> user assigned to destination location
    # and not assigned to source location can validate
    can_validate = fields.Boolean('Assign to Destination Location can Validate', default=False)

    @api.one
    @api.depends('picking_type_id', 'location_id', 'location_dest_id')
    def calculate_to_show_hide_button(self):
        user_locations = self.env.user.location_ids.ids
        picking_type_code = self.picking_type_id.code
        src_internal = False
        des_internal = False
        des_incoming = False
        src_outgoing = False
        if user_locations:
            if picking_type_code == 'internal':
                src_internal = self.location_id.id in user_locations
                des_internal = self.picking_type_id.default_location_dest_id.id in user_locations
            elif picking_type_code == 'incoming':
                des_incoming = self.picking_type_id.default_location_dest_id.id in user_locations
            elif picking_type_code == 'outgoing':
                src_outgoing = self.picking_type_id.default_location_src_id.id in user_locations

        self.source_location_internal = src_internal
        self.dest_location_internal = des_internal
        self.dest_location_incoming = des_incoming
        self.source_location_outgoing = src_outgoing


    @api.multi
    def set_full_completed(self):
        for line in self.pack_operation_product_ids:
            line.qty_done = line.product_qty

    @api.onchange('picking_type_id')
    def reset_domain_location_destination(self):
        domain = []
        if self.picking_type_id.code == 'internal':
            domain = [('usage', '=', 'internal')]
        return {'domain': {'location_id': domain, 'location_dest_id':domain}}