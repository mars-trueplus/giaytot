# -*- coding: utf-8 -*-

{
    'name': 'Magestore Stock',
    'version': '1.0.0',
    'category': 'Warehouse',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Magestore Stock',
    'description': """""",
    'depends': [
        'stock',
        'product',
        'report',
        'stock_account',
        'delivery',
        'decimal_precision',
    ],
    'data': [
        'views/export_button_template.xml',
        'security/hide_menu.xml',
        'views/user_settings.xml',
        'views/stock_picking_filter.xml',
        'views/hide_menu.xml',
        'views/stock_picking_return_views_inherit.xml',
        'views/stock_picking_views_inherit.xml',
        'views/stock_picking_delivery_order_view.xml',
        'views/stock_picking_receive_view.xml',
        'views/stock_picking_transfer_view.xml',
        'views/stock_picking_inventory_view.xml',
        'views/stock_picking_views_inherit2.xml',
        'views/stock_warehouse_views.xml',
        'views/stock_move_views.xml',

        # Michael remove force avaibility button
        'views/remove_force_avaibility_button.xml',

        # Added by Adam: G.IM 9  – Thay đổi giao diện của định giá tồn kho
        'views/stock_quant_views.xml',
        'report/layout_templates.xml',
        'report/report_deliveryslip.xml',

        'wizard/wizard_valuation_movement_summary_views.xml',
        'wizard/export_product_view.xml',
    ],
    'qweb': [
        'static/src/xml/export_button.xml',
    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
