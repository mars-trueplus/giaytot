odoo.define('magestore_stock.HideImportButton', function(require){
    'use strict';

    var ListView = require('web.ListView');
    var session = require('web.session');
    var Model = require('web.Model');

    ListView.include({
        render_buttons: function(){
            this._super.apply(this, arguments);
            var self = this;
            var user_obj = new Model('res.users');
            user_obj.call('check_user_has_setting_privilege', [session.uid]).then(function(result){
                var m = self.model;
                if((m == 'stock.picking' || m == 'stock.inventory' || m == 'sale.order' || m == 'purchase.order') && result == false){
                    // hide Import button
                    self.$buttons.find('.o_button_import').css({"display":"None"});
                }
            });
        }
    });
});