odoo.define('magestore_stock.hide_edit_button', function (require) {
"use strict";

    var FormView = require('web.FormView');

    FormView.include({
        load_record: function(record) {
            this._super.apply(this, arguments);
            if (this.model=='stock.picking'){
                if ((this.get_fields_values().picking_type_code == 'internal' && this.get_fields_values().source_location_internal == true) ||
                 (this.get_fields_values().picking_type_code == 'incoming' && this.get_fields_values().dest_location_incoming == true) ||
                 (this.get_fields_values().picking_type_code == 'outgoing' && this.get_fields_values().source_location_outgoing == true)){
                    this.$buttons.find('.o_form_button_edit').css({"display":""});
                }
                else{
                    this.$buttons.find('.o_form_button_edit').css({"display":"none"});
                }
            }
        },
    });

});