odoo.define('export.button', function(require) {
    'use strict';

    var core = require('web.core');
    var Model = require('web.Model');
    var Widget = require('web.Widget');
    var ListView = require('web.ListView');
    var QWeb = core.qweb;

    var Extends = ListView.include({
        render_buttons: function() {
            this._super.apply(this, arguments);
            this.$buttons.on('click', '.inventory-export-button', this.proxy('on_click_export'));

        },
        on_click_export:function(){
            var self = this;
            self.do_action({
                type: 'ir.actions.act_window',
                name: 'Export Product',
                res_model: 'export.location.product.wizard',
                views: [[false, 'form']],
                view_type : "form",
                view_mode : "form",
                target: 'new',
            });
        },
    });

});
