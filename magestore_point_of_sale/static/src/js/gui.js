odoo.define('magestore_point_of_sale.gui', function (require) {
    'use strict';

    var gui = require('point_of_sale.gui');

    gui.Gui.include({
        show_popup: function (name, options) {
            if (this.current_popup) {
                this.close_popup();
            }
            this.current_popup = this.popup_instances[name];
            if (this.current_popup) {
                return this.current_popup.show(options);
            }
        }
    });
});