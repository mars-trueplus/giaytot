odoo.define('magestore_point_of_sale.models', function (require) {
    "use strict";

    var gui = require('point_of_sale.gui');
    var models = require('point_of_sale.models');
    var alldays = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
    var nowDate = new Date().getTime();
    var utils = require('web.utils');
    var core = require('web.core');
    var Model = require('web.DataModel');
    var SuperOrderline =  models.Orderline;
    var SuperOrder = models.Order;

    var _t = core._t;

    var round_di = utils.round_decimals;
    var round_pr = utils.round_precision;

    models.Order = models.Order.extend({

        // initialize: function(attributes,options){
		// 	var self = this;
		// 	self.pos_promotion_note = '';
		// 	self.pos_promotion_message = '';
		// 	self.new_order_free_product = [];
		// 	self.new_free_product_chosen_title = [];
		// 	self.new_free_product_chosen_count = [];
		// 	SuperOrder.prototype.initialize.call(this,attributes,options);
		// },
        export_as_JSON: function() {
			var self = this;
			var loaded = SuperOrder.prototype.export_as_JSON.call(this);
            loaded.pos_promotion_note = this.pos_promotion_note ? this.pos_promotion_note : '';
            loaded.pos_promotion_message = this.pos_promotion_message ? this.pos_promotion_message : '';
            loaded.new_order_free_product = this.new_order_free_product ? this.new_order_free_product : [];
            loaded.new_free_product_chosen_title = this.new_free_product_chosen_title ? this.new_free_product_chosen_title : [];
            loaded.new_free_product_chosen_count = this.new_free_product_chosen_count ? this.new_free_product_chosen_count : [];
            loaded.program_used_by_coupon_code = this.program_used_by_coupon_code ? this.program_used_by_coupon_code : -1;
            loaded.before_use_coupon = this.before_use_coupon ? this.before_use_coupon : [];
            loaded.is_used_coupon_code = this.is_used_coupon_code ? this.is_used_coupon_code : false;
            loaded.new_order_free_product_coupon = this.new_order_free_product_coupon ? this.new_order_free_product_coupon : [];
            loaded.new_free_product_chosen_title_coupon = this.new_free_product_chosen_title_coupon ? this.new_free_product_chosen_title_coupon : [];
            loaded.new_free_product_chosen_count_coupon = this.new_free_product_chosen_count_coupon ? this.new_free_product_chosen_count_coupon : [];
			return loaded;
		},

        add_product: function(product, options){
            if(this._printed){
                this.destroy();
                return this.pos.get_order().add_product(product, options);
            }
            var self = this;
            this.assert_editable();
            options = options || {};
            var attr = JSON.parse(JSON.stringify(product));
            attr.pos = this.pos;
            attr.order = this;
            var line = new SuperOrderline({}, {pos: this.pos, order: this, product: product});

            if(options.quantity !== undefined){
                line.set_quantity(options.quantity);
            }
            if(options.price !== undefined){
                line.set_unit_price(options.price);
            }
            if(options.discount !== undefined){
                line.set_discount(options.discount);
            }

            if(options.extras !== undefined){
                for (var prop in options.extras) {
                    line[prop] = options.extras[prop];
                }
            }

            // var last_orderline = this.get_last_orderline();
            // if( last_orderline && last_orderline.can_be_merged_with(line) && options.merge !== false){
            //     last_orderline.merge(line);
            // }else{
            //     this.orderlines.add(line);
            // }
            this.orderlines.add(line);
            this.select_orderline(this.get_last_orderline());

            if(line.has_product_lot){
                this.display_lot_popup();
            }
        }
    });

});
