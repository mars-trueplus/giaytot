/**
 * Created by michael on 26/06/2017.
 */
odoo.define('magestore_point_of_sale.screen', function (require) {
    "use strict";

    var screens = require('point_of_sale.screens');
    var pos_promotion = require('pos_promotion.pos_promotion');
    var models = require('point_of_sale.models');
    var core = require('web.core');
    var Model = require('web.DataModel');
    var BaseWidget = require('point_of_sale.BaseWidget');
    var MageBaseWidget = require('magestore_point_of_sale.BaseWidget');
    var _t = core._t;
    var QWeb = core.qweb;

    // Tab13 auto check pos_promotion
    var _super_posmodel = models.PosModel.prototype;
    models.PosModel = models.PosModel.extend({
        // initialize: function(session, attributes) {
            // this.click_pos_promotion = false;
            // this.show_pop_up_promotion = false;
            // this.note_content = '';
            // return _super_posmodel.initialize.call(this,session,attributes);
        // }
    });

    screens.OrderWidget.include({
        init: function(parent, options) {
            var self = this;
            // this._super.apply(this, arguments);
            this._super(parent,options);

            var search_timeout  = null;
            this.note_handler = function(event){
                if(event.type == "keypress" || event.keyCode === 46 || event.keyCode === 8){
                    clearTimeout(search_timeout);

                    var searchbox = this;

                    search_timeout = setTimeout(function(){
                        self.perform_note(searchbox.value);
                    },70);
                }
            };
        },
        perform_note: function (searchbox) {
            this.pos.get_order().set_pos_promotion_note(searchbox);
        },
        renderElement: function(scrollbottom){
            this._super();
            if(this.pos.get_order().get_orderlines().length > 0){
                this.el.querySelector('.pos-promotion-note input').addEventListener('keypress',this.note_handler);
                this.el.querySelector('.pos-promotion-note input').addEventListener('keydown',this.note_handler);
            }
        },
        // orderline_change: function(line){
        //     this.rerender_orderline(line);
        //     this.update_summary();
        // },
        set_value: function(val) {
            // console.log('set_value');
            var order = this.pos.get_order();
            if (order.get_selected_orderline()) {
                var mode = this.numpad_state.get('mode');
                if( mode === 'quantity'){
                    // this.pos.click_pos_promotion = false;
                    this.pos.get_order().click_pos_promotion = false;
                    this.pos.get_order().is_used_coupon_code = false;
                    order.get_selected_orderline().set_quantity(val);
                }else if( mode === 'discount'){
                    this.pos.get_order().click_pos_promotion = false;
                    this.pos.get_order().is_used_coupon_code = false;
                    // this.pos.click_pos_promotion = false;
                    order.get_selected_orderline().set_discount(val);
                }else if( mode === 'price'){
                    this.pos.get_order().click_pos_promotion = false;
                    this.pos.get_order().is_used_coupon_code = false;
                    // this.pos.click_pos_promotion = false;
                    order.get_selected_orderline().set_unit_price(val);
                }
            }
            // console.log(this.pos.get_order().is_used_coupon_code);
        },
        orderline_add: function(){
            // console.log('orderline_add');
            this.pos.get_order().is_used_coupon_code = false;
            this.pos.get_order().click_pos_promotion = false;
            // this.pos.click_pos_promotion = false;
            this.numpad_state.reset();
            this.renderElement('and_scroll_to_bottom');
            // console.log(this.pos.get_order().is_used_coupon_code);
        },
        orderline_remove: function(line){
            // console.log('orderline_remove');
            this.pos.get_order().is_used_coupon_code = false;
            this.pos.get_order().click_pos_promotion = false;
            // this.pos.click_pos_promotion = false;
            this.remove_orderline(line);
            this.numpad_state.reset();
            this.update_summary();
            // console.log(this.pos.get_order().is_used_coupon_code);
        }
    });

    screens.NumpadWidget.include({
        clickDeleteLastChar: function() {
            return this.pos.get_order().remove_orderline(this.pos.get_order().get_selected_orderline());
            // return this.state.deleteLastChar();
        }
    });

    screens.PaymentScreenWidget.include({
        validate_order: function(force_validation) {
            this._super();
            var self = this;
            if (this.order_is_valid(force_validation)) {
                var order = this.pos.get_order();
                var lines = order.get_orderlines();
                for(var i = 0 ; i < lines.length ; i++){
                    if(lines[i].pos_promotion_message.length > 0){
                        for (var j = 0 ; j < lines[i].pos_promotion_message.length ; j++){
                            if (order.pos_promotion_message.indexOf(lines[i].pos_promotion_message[j].message) < 0 ){
                                order.pos_promotion_message.push(lines[i].pos_promotion_message[j].message);
                            }
                        }
                        // if(order.pos_promotion_message.indexOf(lines[i].pos_promotion_message) < 0){
                        //     order.pos_promotion_message.push(lines[i].pos_promotion_message);
                        // }
                    }
                }
                // console.log('validate_order');
                // console.log(order.is_used_coupon_code);
                if(order.is_used_coupon_code){
                    if(order.get_client()){
                        new Model('pos.promotion.coupon.code').call('check_customer_with_coupon_code',[order.get_client().id,this.pos.get_order().program_used_by_coupon_code]).then(function (result) {
                            if(result == 1){
                                self.finalize_validation();
                            }
                            if(result == -1){
                                alert('Can not use coupon code!!!');
                                return;
                            }
                        })
                    }else{
                        self.finalize_validation();
                    }
                }else{
                    this.finalize_validation();
                }
            }
        }
    });

    //Michael add note to order
    screens.ActionpadWidget.include({
        renderElement: function() {
            var self = this;
            this._super();
            var promotion = new pos_promotion.PosPromotionButton(this,{});
            this.$('.pay').click(function(){
                // console.log('renderElement');
                // console.log(self.pos.get_order().click_pos_promotion);
                // console.log(self.pos.get_order().show_pop_up_promotion);
                var posPromotionConfigModel = new Model('pos.promotion.config.settings');

                posPromotionConfigModel.call('get_allow_second_time_sellection').then(function (return_value) {
                    if ($('#pos_promotion_note_id')[0] && $('#pos_promotion_note_id')[0].value) {
                        var order = self.pos.get_order();
                        order.pos_promotion_note = $('#pos_promotion_note_id')[0].value;
                    }
                    if(return_value === 1){
                        // if(!self.pos.click_pos_promotion || !self.pos.show_pop_up_promotion){
                        // if(!self.pos.get_order().click_pos_promotion || !self.pos.get_order().show_pop_up_promotion){
                        if(!self.pos.get_order().click_pos_promotion){
                            promotion.button_click();
                            return self.gui.show_screen('products',null,true,null);
                        }
                        // if(self.pos.click_pos_promotion && self.pos.show_pop_up_promotion){
                        // if(self.pos.get_order().click_pos_promotion && self.pos.get_order().show_pop_up_promotion){
                        if(self.pos.get_order().click_pos_promotion){
                            // var note = document.getElementById('note').value;
                            var order = self.pos.get_order();
                            // order.note = note;
                            var has_valid_product_lot = _.every(order.orderlines.models, function(line){
                                return line.has_valid_product_lot();
                            });
                            if(!has_valid_product_lot){
                                self.gui.show_popup('confirm',{
                                    'title': _t('Empty Serial/Lot Number'),
                                    'body':  _t('One or more product(s) required serial/lot number.'),
                                    confirm: function(){
                                        self.gui.show_screen('payment');
                                    },
                                });
                            }else{
                                self.gui.show_screen('payment');
                            }
                        }
                    }else{
                        var order = self.pos.get_order();
                        var has_valid_product_lot = _.every(order.orderlines.models, function(line){
                            return line.has_valid_product_lot();
                        });
                        if(!has_valid_product_lot){
                            self.gui.show_popup('confirm',{
                                'title': _t('Empty Serial/Lot Number'),
                                'body':  _t('One or more product(s) required serial/lot number.'),
                                confirm: function(){
                                    self.gui.show_screen('payment');
                                },
                            });
                        }else{
                            self.gui.show_screen('payment');
                        }
                    }
                });

            });
            this.$('.set-customer').click(function(){
                self.gui.show_screen('clientlist');
            });
        }
    });

    screens.ClientListScreenWidget.include({
        //Michael fix, dupplicate phone
        // what happens when we save the changes on the client edit form -> we fetch the fields, sanitize them,
        // send them to the backend for update, and call saved_client_details() when the server tells us the
        // save was successfull.
        save_client_details: function(partner) {
            var self = this;

            var fields = {};
            this.$('.client-details-contents .detail').each(function(idx,el){
                fields[el.name] = el.value || false;
            });

            if (!fields.name) {
                this.gui.show_popup('error',_t('A Customer Name Is Required'));
                return;
            }

            if (this.uploaded_picture) {
                fields.image = this.uploaded_picture;
            }

            fields.id           = partner.id || false;
            fields.country_id   = fields.country_id || false;

            var partners = self.pos.partners;

            if (fields.phone && !fields.id) {
                var result = $.grep(partners, function(e){ return e.phone == fields.phone; });
                if (result.length > 0) {
                    alert(_t('Số điện thoại đã tồn tịa trong hệ thống'));
                    return;
                }
            }

            new Model('res.partner').call('create_from_ui',[fields]).then(function(partner_id){
                self.saved_client_details(partner_id);
            },function(err,event){
                event.preventDefault();
                self.gui.show_popup('error',{
                    'title': _t('Error: Could not Save Changes'),
                    'body': _t('Your Internet connection is probably down.'),
                });
            });
        },
    });

    // Left menu
    screens.ProductScreenWidget.include({
        start: function () {
            this.left_menu = new MageBaseWidget.PromotionLeftMenu(this, {});
            this.left_menu.replace(this.$('.placeholder-PromotionLeftMenu'));
            this._super();
        }
    });



    // screens.ProductScreenWidget.include({
        // click_product: function(product) {
        //     var self = this;
        //     this._super.apply(this, arguments);

            // console.log(this.pos.get_order().get_orderlines());
            // console.log(product.id);
            // console.log(this.pos.specialprice[0].product_id[0]);

            // var orderlines = this.pos.get_order().get_orderlines();
            // var new_orderlines = [];
            //
            // var check = true;
            // new_orderlines.push(orderlines[0]);
            // for (var i = 1; i < orderlines.length; i++) {
            //     for (var j = 0; j < new_orderlines.length; j++) {
            //         if (new_orderlines[j].product.id == orderlines[i].product.id) {
            //             new_orderlines[j].quantity += orderlines[i].quantity;
            //             check = false;
            //         }
            //         if (j == new_orderlines.length - 1 && check) {
            //             new_orderlines.push(orderlines[i]);
            //         }
            //         check = true;
            //     }
            // }
            // new_orderlines.push(orderlines[i]);
            // for(var j = 0 ; j < this.pos.bxgy_bx.length ; j++){
            //     if(orderlines[i] == this.pos.bxgy_bx[j].product_id[0]){
            //         console.log('adsasd');
            //     }
            // }
        // }
    // });

    // screens.OrderWidget.include({
    //     init: function (parent, options) {
    //         this._super(parent, options);
    //         var sefl = this;
    //
    //         var order = this.pos.get('selectedOrder');
    //         console.log(order);
    //     }
    // });

    // screens.OrderWidget.include({
    //     update_summary: function(){
    //         var order = this.pos.get_order();
    //         if (!order.get_orderlines().length) {
    //             return;
    //         }
    //
    //         var total     = order ? order.get_total_with_tax() : 0;
    //         var taxes     = order ? order.get_total_tax() : 0;
    //         var promotion_discount = order ? order.get_pos_promotion_discount() : 0;
    //         if(promotion_discount > 0){
    //             this.el.querySelector('.summary .total .discount .value').textContent = this.format_currency(promotion_discount);
    //         }else{
    //             this.el.querySelector('.summary .total .discount').style.display = 'none';
    //         }
    //         this.el.querySelector('.summary .total > .value').textContent = this.format_currency(total);
    //         // this.el.querySelector('.summary .total .discount .value').textContent = this.format_currency(promotion_discount);
    //         this.el.querySelector('.summary .total .subentry .value').textContent = this.format_currency(taxes);
    //     },
    // });
});

