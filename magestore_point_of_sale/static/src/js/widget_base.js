odoo.define('magestore_point_of_sale.BaseWidget', function (require) {
    "use strict";

    var PosBaseWidget = require('point_of_sale.BaseWidget');

    var PromotionLeftMenu = PosBaseWidget.extend({
        template: 'PromotionLeftMenu',
        renderElement: function () {
            var self = this;
            this._super();
        }
    });

    return {
        PromotionLeftMenu: PromotionLeftMenu
    }

});
