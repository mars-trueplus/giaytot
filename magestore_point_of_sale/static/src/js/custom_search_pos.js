/**
 * Created by mrt on 13/09/2017.
 */
odoo.define('magestore_point_of_sale.custom_search_pos', function (require) {
    "use strict";

    var core = require('web.core');
    var pos_DB = require('point_of_sale.DB');
    var screens = require('point_of_sale.screens');

    function vn_un_accent(input) {
        var SPECIAL_CHARACTERS = ['!', '"', '#', '$', '%', '*', '+', ',',
				':', '<', '=', '>', '?', '@', '[', '\\', ']', '^', '`', '|',
				'~', 'À', 'Á', 'Â', 'Ã', 'È', 'É', 'Ê', 'Ì', 'Í', 'Ò', 'Ó',
				'Ô', 'Õ', 'Ù', 'Ú', 'Ý', 'à', 'á', 'â', 'ã', 'è', 'é', 'ê',
				'ì', 'í', 'ò', 'ó', 'ô', 'õ', 'ù', 'ú', 'ý', 'Ă', 'ă', 'Đ',
				'đ', 'Ĩ', 'ĩ', 'Ũ', 'ũ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ạ', 'ạ', 'Ả',
				'ả', 'Ấ', 'ấ', 'Ầ', 'ầ', 'Ẩ', 'ẩ', 'Ẫ', 'ẫ', 'Ậ', 'ậ', 'Ắ',
				'ắ', 'Ằ', 'ằ', 'Ẳ', 'ẳ', 'Ẵ', 'ẵ', 'Ặ', 'ặ', 'Ẹ', 'ẹ', 'Ẻ',
				'ẻ', 'Ẽ', 'ẽ', 'Ế', 'ế', 'Ề', 'ề', 'Ể', 'ể', 'Ễ', 'ễ', 'Ệ',
				'ệ', 'Ỉ', 'ỉ', 'Ị', 'ị', 'Ọ', 'ọ', 'Ỏ', 'ỏ', 'Ố', 'ố', 'Ồ',
				'ồ', 'Ổ', 'ổ', 'Ỗ', 'ỗ', 'Ộ', 'ộ', 'Ớ', 'ớ', 'Ờ', 'ờ', 'Ở',
				'ở', 'Ỡ', 'ỡ', 'Ợ', 'ợ', 'Ụ', 'ụ', 'Ủ', 'ủ', 'Ứ', 'ứ', 'Ừ',
				'ừ', 'Ử', 'ử', 'Ữ', 'ữ', 'Ự', 'ự'];

        var REPLACEMENTS =  ['\0', '\0', '\0', '\0', '\0', '\0', '_', '\0',
				'_', '\0', '\0', '\0', '\0', '\0', '\0', '_', '\0', '\0', '\0',
				'\0', '\0', 'A', 'A', 'A', 'A', 'E', 'E', 'E', 'I', 'I', 'O',
				'O', 'O', 'O', 'U', 'U', 'Y', 'a', 'a', 'a', 'a', 'e', 'e',
				'e', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u', 'y', 'A', 'a',
				'D', 'd', 'I', 'i', 'U', 'u', 'O', 'o', 'U', 'u', 'A', 'a',
				'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a',
				'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'E', 'e',
				'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e',
				'E', 'e', 'I', 'i', 'I', 'i', 'O', 'o', 'O', 'o', 'O', 'o',
				'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o',
				'O', 'o', 'O', 'o', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u',
				'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u'];

        var input_char_array = input.split('');
        for(var i = 0; i < input_char_array.length; i++){
            var index = SPECIAL_CHARACTERS.indexOf(input_char_array[i]);
            if(index !== -1){
                input_char_array[i] = REPLACEMENTS[index];
            }
        }
        return input_char_array.join('');
    }

    pos_DB.include({
        _product_search_string: function(product){
            var str = vn_un_accent(product.display_name);
            if (product.barcode) {
                str += '|' + product.barcode;
            }
            if (product.default_code) {
                str += '|' + vn_un_accent(product.default_code);
            }
            if (product.description) {
                str += '|' + vn_un_accent(product.description);
            }
            if (product.description_sale) {
                str += '|' + vn_un_accent(product.description_sale);
            }
            str  = product.id + ':' + str.replace(/:/g,'') + '\n';
            return str;
        },
    })

    screens.ProductCategoriesWidget.include({
        perform_search: function(category, query, buy_result){
            var products;
            if(query){
                products = this.pos.db.search_product_in_category(category.id,vn_un_accent(query));
                if(buy_result && products.length === 1){
                        this.pos.get_order().add_product(products[0]);
                        this.clear_search();
                }else{
                    this.product_list_widget.set_product_list(products);
                }
            }else{
                products = this.pos.db.get_product_by_category(this.category.id);
                this.product_list_widget.set_product_list(products);
            }
         }
    });

    return {
        vn_un_accent: vn_un_accent
    }
});