# -*- coding: utf-8 -*-

{
    'name': 'Magestore Point Of Sale',
    'version': '1.0.0',
    'category': 'Point Of Sale',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Magestore Point Of Sale',
    'description': """""",
    'depends': [
        'base', 'sale', 'point_of_sale',
        'stock_account',
        'magestore_stock',
        'magestore_pos_speedup_v2',
    ],
    'data': [
        # 'views/pos_refund_product.xml',
        'views/res_users_view_inherit.xml',
        'views/pos_config_view_inherit.xml',
        'security/sale_velocity_security.xml',
        'security/ir.model.access.csv',
        'views/pos_giaytot_header.xml',
        'report/report_sale_velocity.xml',
        'views/sale_velocity_view.xml',
        'views/pos_order_report.xml',
        'views/pos_order_view_inherit.xml',
    ],
    'qweb': [
        'static/src/xml/pos_templates.xml',
    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
