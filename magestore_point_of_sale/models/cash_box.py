# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.addons.point_of_sale.wizard.pos_box import PosBox

# Added by Mars: for odoo api
class PosBoxInInh(PosBox):
    _inherit = 'cash.box.in'

    @api.multi
    def run(self, context={}):
        active_model = self.env.context.get('active_model', False) or context.get('active_model', False)
        active_ids = self.env.context.get('active_ids', []) or context.get('active_ids', False)
        if active_model == 'pos.session':
            bank_statements = [session.cash_register_id for session in self.env[active_model].browse(active_ids) if
                               session.cash_register_id]
            if not bank_statements:
                raise UserError(_("There is no cash register for this PoS Session"))
            return self._run(bank_statements)
        else:
            return super(PosBox, self).run()


class PosBoxOutInh(PosBox):
    _inherit = 'cash.box.out'

    @api.multi
    def run(self, context={}):
        active_model = self.env.context.get('active_model', False) or context.get('active_model', False)
        active_ids = self.env.context.get('active_ids', []) or context.get('active_ids', False)
        if active_model == 'pos.session':
            bank_statements = [session.cash_register_id for session in self.env[active_model].browse(active_ids) if
                               session.cash_register_id]
            if not bank_statements:
                raise UserError(_("There is no cash register for this PoS Session"))
            return self._run(bank_statements)
        else:
            return super(PosBox, self).run()


class PosSession(models.Model):
    _inherit = 'pos.session'

    @api.multi
    def action_pos_session_closing_control(self):
        res = super(PosSession, self).action_pos_session_closing_control()
        if res:
            return res
        else:
            return True


class AccountBankWizard(models.Model):
    _inherit = 'account.bank.statement.cashbox'

    @api.multi
    def validate_cashbox(self, context={}):
        bnk_stmt_id = context.get('bank_statement_id', False) or context.get('active_id', False)
        bnk_stmt = self.env['account.bank.statement'].browse(bnk_stmt_id)
        total = 0.0
        for lines in self.cashbox_lines_ids:
            total += lines.subtotal
        if context.get('balance', False) == 'start':
            # starting balance
            bnk_stmt.write({'balance_start': total, 'cashbox_start_id': self.id})
        else:
            # closing balance
            bnk_stmt.write({'balance_end_real': total, 'cashbox_end_id': self.id})
        return {'type': 'ir.actions.act_window_close'}
