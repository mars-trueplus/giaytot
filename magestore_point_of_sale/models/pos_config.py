# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models, _, SUPERUSER_ID, api


class PosConfigInherit(models.Model):
    _inherit = 'pos.config'

    user_ids = fields.Many2many('res.users', string='Res Users')

    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        res = super(PosConfigInherit, self).search_read(domain, fields, offset, limit, order)
        if self.env.user.id == SUPERUSER_ID:
            return res
        else:
            data = []
            user_config_ids = self.env.user.pos_config_ids.ids
            for item in res:
                if item.get('id') in user_config_ids:
                    data.append(item)
            return data