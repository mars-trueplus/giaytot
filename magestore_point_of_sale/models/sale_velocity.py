# -*- coding: utf-8 -*-
from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models, _, tools



class SaleVelocity(models.Model):
    _name = 'pos.sale.velocity'

    def _get_default_all_warehouse_ids(self):
        return self.env['stock.warehouse'].search(
            [('company_id', '=', self.env['res.company']._company_default_get('pos.sale.velocity').id)])

    name = fields.Char(string=_(u'Report Name'), required=True)
    date_from = fields.Date(
        string=_(u'Date from'), required=True, default=str(datetime.now() + relativedelta(days=-1)))
    date_to = fields.Date(string=_(u'Date to'), required=True,
                          default=str(datetime.now()))
    warehouse_id = fields.Many2one(
        'stock.warehouse', string=_(u'Warehouse'), required=True,
        default=lambda self: self.env['stock.warehouse'].search(
            [('company_id', '=', self.env['res.company']._company_default_get('pos.sale.velocity').id)], limit=1))
    sale_velocity_line_ids = fields.One2many(
        'pos.sale.velocity.line', 'sale_velocity_id', string=_(u'Sale Velocity Lines'))
    company_id = fields.Many2one('res.company', string=_(u'Company'),
                                 default=lambda self: self.env['res.company']._company_default_get('pos.sale.velocity'))

    warehouse_company = fields.Many2one(string='Company of warehouse', related='warehouse_id.company_id')
    warehouse_ids = fields.Many2many('stock.warehouse', string=_(u'Warehouse(s)'),
                                     default=_get_default_all_warehouse_ids)

    def compute_period_tiem(self):
        date_from = str(self.date_from).split('-')
        date_to = str(self.date_to).split('-')
        date_from = list(map(int, date_from))
        date_to = list(map(int, date_to))
        d0 = date(date_from[0], date_from[1], date_from[2])
        d1 = date(date_to[0], date_to[1], date_to[2])
        return abs((d1 - d0).days) + 1

    @api.multi
    @api.onchange('date_from', 'date_to', 'warehouse_ids')
    def _search_all_sale_velocity_line(self):
        if self.date_from > self.date_to:
            self.date_from = datetime.now() + relativedelta(days=-1)
            self.date_to = datetime.now()
            return {
                'warning': {
                    'title': _("Chọn sai ngày"),
                    'message': _('Ngày kết thúc phải đứng sau ngày bắt đầu'),
                },
            }

        # hold velocity line data also don't need to create them into database
        list_sale_velocity_lines = []

        # if user not choose warehouse, field sale_velocity_line_ids will be empty
        if not self.warehouse_ids:
            self.sale_velocity_line_ids = []
            return

        # get list location from list warehouses
        list_locations = []
        temp1 = self.env['stock.location'].search([])
        for location in temp1:
            if location.get_real_warehouse_id().id in self.warehouse_ids.ids:
                list_locations.append(location.id)

        lst_stock_quants = self.env['stock.quant'].search(
            [('location_id', 'in', list_locations)])

        # calculate sum qty for all same stock_quant
        removed_dup_stocks = []
        lst_stock_quants = list(lst_stock_quants)
        lst_stock_quants.sort(key=lambda x: x.product_id.default_code)

        for quant in lst_stock_quants:
            temp1 = [q1 for q1 in lst_stock_quants if
                     q1.product_id == quant.product_id and q1.location_id.get_real_warehouse_id() == quant.location_id.get_real_warehouse_id()]
            sum = 0
            for a in temp1:
                sum = sum + a.qty
                lst_stock_quants.remove(a)

            if temp1:
                removed_dup_stocks.append({
                    'product_id': temp1[0].product_id.id,
                    'default_code': temp1[0].product_id.default_code,
                    'current_inventory': sum,
                    'company_id': temp1[0].company_id.id,
                    'location_id': temp1[0].location_id.id,
                    'warehouse_name': temp1[0].location_id.get_real_warehouse_id().name
                })

        period = self.compute_period_tiem()

        for index, stock_quant in enumerate(removed_dup_stocks):
            line_vals = {}
            # sku_code
            sku_code = stock_quant.get('default_code')
            # current_inventory
            current_inventory = stock_quant.get('current_inventory')
            # pos_order
            pos_order_lines = self.env['report.pos.order'].search([
                ('company_id', '=', stock_quant.get('company_id')),
                ('product_id', '=',stock_quant.get('product_id')),
                ('location_id', '=', stock_quant.get('location_id')),
                ('date', '>=', self.date_from),
                ('date', '<=', self.date_to)
            ])
            pos_order = 0
            if pos_order_lines:
                for li in pos_order_lines:
                    pos_order += li.product_qty
            if pos_order:
                # velocity
                velocity = float("{0:.2f}".format(float(pos_order) / float(period)))
                # date_out_stock
                if not velocity:
                    date_out_stock = 0
                else:
                    date_out_stock = float("{0:.2f}".format(float(current_inventory) / float(velocity)))

                line_vals.update({'sku_code': sku_code,
                                  'current_inventory': current_inventory,
                                  'pos_order': pos_order,
                                  'velocity': velocity,
                                  'date_out_stock': date_out_stock,
                                  'warehouse': stock_quant.get('warehouse_name')
                                  })

                list_sale_velocity_lines.append(line_vals)

        list_sale_velocity_lines = sorted(list_sale_velocity_lines, key=lambda line: line['warehouse'])
        final_sale_lines = []
        # sort all line has same warehouse name by pos_order
        for warehouse in sorted(self.warehouse_ids, key=lambda x: x.name):
            warehouse_name = warehouse.name
            ls_temp = [line for line in list_sale_velocity_lines if line['warehouse'] == warehouse_name]
            final_sale_lines.extend(sorted(ls_temp, key=lambda line: line['pos_order'], reverse=True))

        self.sale_velocity_line_ids = final_sale_lines


class SaleVelocityLine(models.Model):
    _name = 'pos.sale.velocity.line'
    _order = 'warehouse,pos_order desc'

    product_id = fields.Many2one('product.template')
    sale_velocity_id = fields.Many2one(
        'pos.sale.velocity', string=_(u'Sale Velocity'))
    sku_code = fields.Char(string=_(u'SKU Code'))
    current_inventory = fields.Integer(string=_(u'Current Inventory'))
    pos_order = fields.Integer(string=_(u'POS Order'))
    velocity = fields.Float(string=_(u'Velocity'))
    date_out_stock = fields.Float(string=_(u'Date Out Stock'))
    status = fields.Char(string=_(u'Status'))
    note_for_sale = fields.Text(string=_(u'Note for sale'))
    note_for_marketing = fields.Text(string=_(u'Note for marketing'))
    warehouse = fields.Char(string='Kho')


class StockLocation(models.Model):
    _inherit = 'stock.location'

    def get_real_warehouse_id(self):
        return self.get_warehouse()
