# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.exceptions import UserError, ValidationError


class MagestorePosSessionInherit(models.Model):
    _inherit = 'pos.session'

    # Added By Adam to filter the order by pos
    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        # Edited by Mars
        if order and 'from_odoo_api' in order:
            order = order.replace('from_odoo_api', '')
            return super(MagestorePosSessionInherit, self).search_read(domain, fields, offset=offset or 0,
                                                                       limit=limit or False, order=order or False)
        res = super(MagestorePosSessionInherit, self).search_read(domain, fields, offset=offset or 0,
                                                                  limit=limit or False, order=order or False)
        if not fields:
            fields = ['config_id']
        else:
            fields.append('config_id')
        if self.env.user.id == SUPERUSER_ID:
            return res
        else:
            user_config_ids = self.env.user.pos_config_ids.ids
            data = []
            for item in res:
                config_id = item.get('config_id', False)
                if config_id and config_id[0] in user_config_ids:
                    data.append(item)
            return data
