# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
from odoo import api, fields, models, _
from odoo.exceptions import UserError


class ResUsers(models.Model):
    _inherit = 'res.users'

    pos_config_ids = fields.Many2many("pos.config", String="POS")

    # @api.multi
    # def write(self, vals):
    #     print "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
    #     print self.pos_config_ids.ids
    #     res = super(ResUsers, self).write(vals)
    #     return res



