# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.
import logging
from functools import partial
import psycopg2

from odoo import api, fields, models, tools, _, SUPERUSER_ID
from odoo.exceptions import UserError

_logger = logging.getLogger(__name__)


# class PosRefundPopup(models.TransientModel):
#     _name = "pos.refund.popup"
#
#     refund_reason = fields.Text(string='Refund Reason', required=True)
#
#     @api.multi
#     def create_refund(self):
#         order_id = self.env.context.get('order_id')
#         session_id = self.env.context.get('session_id')
#         data = self.read()[0]
#         reason_to_refund = 'Reason refund: ' + data['refund_reason']
#
#         order = self.env['pos.order'].search([('id', '=', order_id)])
#
#         # PosOrder = self.env['pos.order']
#
#         PosOrder = order[0].copy({
#             # ot used, name forced by create
#             'name': order.name + _(' REFUND'),
#             # 'session_id': current_session.id,
#             'session_id': session_id,
#             'date_order': fields.Datetime.now(),
#             'pos_reference': order.pos_reference,
#             'note': reason_to_refund,
#             'source_order': order_id
#         })
#         # PosOrder = clone
#         # for clone in PosOrder:
#         for order_line in PosOrder.lines:
#             order_line.write({'qty': -order_line.qty})
#         return {
#             'name': _('Return Products'),
#             'view_type': 'form',
#             'view_mode': 'form',
#             'res_model': 'pos.order',
#             'res_id': PosOrder.ids[0],
#             'view_id': False,
#             'context': self.env.context,
#             'type': 'ir.actions.act_window',
#             'target': 'current',
#         }

class PosOrder(models.Model):
    _inherit = "pos.order"

    source_order = fields.Many2one('pos.order', string='Source Order', index=True, readonly=True)
    customer_phone = fields.Char('Số điện thoại khách hàng', related='partner_id.phone')
    qty_return = fields.Float('Số lượng trả')

    # pos_promotion_note = fields.Char("Ghi chú")
    note = fields.Char("Ghi chú")
    pos_promotion_message = fields.Html("Pos promoiton message")

    # Added by Mars (odoo api)
    @api.model
    def get_name_order(self, session_id):
        session = self.env['pos.session'].browse(session_id)
        return session.config_id.sequence_id._next()

    # Michael comment, change source code to pos_order_return
    # @api.multi
    # def refund(self):
    #
    #     """Create a copy of order  for refund order"""
    #     current_session = self.env['pos.session'].search([('state', '!=', 'closed'), ('user_id', '=', self.env.uid)],
    #                                                      limit=1)
    #
    #     if not current_session:
    #         raise UserError(
    #             _('To return product(s), you need to open a session that will be used to register the refund.'))
    #
    #     session_id = 0
    #     ordered_location_id = self.location_id.id
    #     for session in current_session:
    #         if ordered_location_id == session.config_id.stock_location_id.id:
    #             session_id = current_session.id
    #             break
    #     if session_id == 0:
    #         raise UserError(_('You cannot return products because POS is closed'))
    #
    #     ir_model_data = self.env['ir.model.data']
    #     compose_form_id = ir_model_data.get_object_reference('magestore_point_of_sale', 'view_pos_refund_product')[1]
    #
    #     ctx = dict({
    #         'order_id': self.id,
    #         'session_id': session_id
    #     })
    #
    #     # popup to fill refund reason
    #     return {
    #         'name': _('Refund Product'),
    #         'type': 'ir.actions.act_window',
    #         'view_type': 'form',
    #         'view_mode': 'form',
    #         'res_model': 'pos.refund.popup',
    #         'views': [(compose_form_id, 'form')],
    #         'view_id': compose_form_id,
    #         'target': 'new',
    #         'context': ctx,
    #     }

    # Added By Adam to filter the order by pos
    @api.model
    def search_read(self, domain=None, fields=None, offset=0, limit=None, order=None):
        # Added by Mars: xmlrpc call to this function
        # if order and 'from_odoo_api' in order:
        #     order = order.replace('from_odoo_api', '')
        # else:
        if not fields:
            fields = ['config_id']
        else:
            fields.append('config_id')
        # Michael
        # check user is not super admin, add more filter by view as pos
        if self.env.user.id != SUPERUSER_ID:
            user_config_ids = self.env.user.pos_config_ids.ids
            domain.append(('config_id', 'in', user_config_ids))

        return super(PosOrder, self).search_read(domain, fields, offset=offset or 0, limit=limit or False,
                                                 order=order or False)

    # Michael: fix bug print invoice pos (account.invoice read)
    @api.model
    def create_from_ui(self, orders):
        try:
            # Keep only new orders
            submitted_references = [o['data']['name'] for o in orders]
            pos_order = self.search([('pos_reference', 'in', submitted_references)])
            existing_orders = pos_order.read(['pos_reference'])
            existing_references = set([o['pos_reference'] for o in existing_orders])
            orders_to_save = [o for o in orders if o['data']['name'] not in existing_references]
            order_ids = []

            for tmp_order in orders_to_save:
                to_invoice = tmp_order['to_invoice']
                order = tmp_order['data']
                if to_invoice:
                    self._match_payment_to_invoice(order)
                pos_order = self._process_order(order)
                order_ids.append(pos_order.id)

                try:
                    pos_order.action_pos_order_paid()
                except psycopg2.OperationalError:
                    # do not hide transactional errors, the order(s) won't be saved!
                    raise
                except Exception as e:
                    _logger.error('Could not fully process the POS Order: %s', tools.ustr(e))

                if to_invoice:
                    pos_order.sudo().action_pos_order_invoice()
                    pos_order.invoice_id.with_context({'pos_invoice': True}).sudo().action_invoice_open()
                    pos_order.account_move = pos_order.sudo().invoice_id.move_id
            return order_ids, orders

        except Exception as e:
            print (unicode(e))

    @api.model
    def _order_fields(self, ui_order):
        fields_return = super(PosOrder, self)._order_fields(ui_order)
        pos_promotion_message = ''
        if ui_order['pos_promotion_message']:
            for message in ui_order['pos_promotion_message']:
                pos_promotion_message += unicode(message).encode('utf-8') + '<br/>'
        fields_return.update({
            'note': ui_order['pos_promotion_note'],
            'pos_promotion_message': pos_promotion_message,
        })
        return fields_return
