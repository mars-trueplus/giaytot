# -*- coding: utf-8 -*-

{
    'name': 'Magestore Stock Allocation',
    'version': '1.0.0',
    'category': 'Warehouse',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Magestore Stock Allocation',
    'description': """""",
    'depends': [
        'base',
        'sale',
        'purchase',
        'stock',
        'sale_stock'
    ],
    'data': [
        'security/ir.model.access.csv',
        'views/stock_allocation_view.xml',
        'views/sale_view.xml',
        'views/purchase_view.xml'
    ],
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
