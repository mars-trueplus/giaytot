from odoo import api, fields, models, _


class SaleOrder(models.Model):
    _inherit = "sale.order"

    @api.model
    def _default_warehouse_id(self):
        company = self.env.user.company_id.id
        warehouse_ids = self.env['stock.warehouse'].search([('company_id', '=', company)], limit=1)
        return warehouse_ids

    stock_allocation_id = fields.Many2one('stock.allocation')

    @api.multi
    def stock_allocation(self):
        ctx = self._context.copy()
        ctx.update({
            'stock_allocation_type': 'sale',
            'order_id': self.id
        })
        view = self.env.ref('magestore_stock_allocation.view_magestore_stock_allocation_form')
        return {
            'name': _('Stock Allocation'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'stock.allocation',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'context': ctx,
            'target': 'new'
        }

    @api.multi
    def action_view_stock_allocation(self):
        view = self.env.ref('magestore_stock_allocation.view_magestore_stock_allocation_form')
        if self.stock_allocation_id:
            return {
                'name': _('Stock Allocation'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'stock.allocation',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'res_id': self.stock_allocation_id.id,
                'target': 'new'
            }
        else:
            return {'type': 'ir.actions.act_window_close'}
