from odoo import api, fields, models, _


class PurchaseOrder(models.Model):
    _inherit = "purchase.order"

    stock_allocation_id = fields.Many2one('stock.allocation')

    @api.multi
    def stock_allocation(self):
        ctx = self._context.copy()
        ctx.update({
            'stock_allocation_type': 'purchase',
            'order_id': self.id
        })
        view = self.env.ref('magestore_stock_allocation.view_magestore_stock_allocation_form')
        return {
            'name': _('Stock Allocation'),
            'type': 'ir.actions.act_window',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'stock.allocation',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'context': ctx,
            'target': 'new'
        }

    @api.multi
    def action_view_stock_allocation(self):
        view = self.env.ref('magestore_stock_allocation.view_magestore_stock_allocation_form')
        if self.stock_allocation_id:
            return {
                'name': _('Stock Allocation'),
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'stock.allocation',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'res_id': self.stock_allocation_id.id,
                'target': 'new'
            }
        else:
            return {'type': 'ir.actions.act_window_close'}
