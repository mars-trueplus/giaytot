# -*- coding: utf-8 -*-

from odoo import fields, api, models, _
from odoo.exceptions import UserError
from datetime import datetime


class StockAllocation(models.Model):
    _name = 'stock.allocation'

    def _get_all_warehouse_ids(self):
        self.warehouse_ids = self.env['stock.warehouse'].search([('company_id', '=', self.env.user.company_id.id)])

    def _get_default_warehouse_ids(self):
        return self.env['stock.warehouse'].search([('company_id', '=', self.env.user.company_id.id)])

    warehouse_ids = fields.Many2many('stock.warehouse', string=_('Warehouse'), required=True, store=True, compute='_get_all_warehouse_ids', default=_get_default_warehouse_ids)
    stock_allocation_lines = fields.One2many('stock.allocation.line', 'stock_allocation_id', string=_('Stock Allocation Lines'))
    state = fields.Selection([('allocating', 'Allocating'), ('done', 'Done')], string='State', default='allocating')

    @api.model
    def create(self, values):
        context = self._context
        sku_errors = ''
        stock_allocation_lines = values.get('stock_allocation_lines')
        selected_stock_allocation_lines = []
        selected_warehouse_ids = []

        for stock_allocation_line in stock_allocation_lines:
            tmp_list = filter(lambda x: x[2]['product_id'] == stock_allocation_line[2]['product_id'], stock_allocation_lines)
            sum_qty_order = 0.0
            sum_qty_shipping = 0.0
            for p in tmp_list:
                if p[2]['qty_order'] != '':
                    sum_qty_order += float(p[2]['qty_order'])
                sum_qty_shipping += p[2]['qty_shipping']

            if sum_qty_order != sum_qty_shipping and stock_allocation_line[2]['product_sku'] != '':
                sku_errors += '\n- [%s] %s' % (
                    stock_allocation_line[2]['product_sku'], stock_allocation_line[2]['product_name'])

            if stock_allocation_line[2]['qty_shipping'] != 0:
                selected_stock_allocation_lines.append(stock_allocation_line)
                selected_warehouse_ids.append(stock_allocation_line[2]['warehouse_id'])

        if len(sku_errors) > 0:
            raise UserError(_(
                "The quantity shipping of following products is not equal to quantity order:%s\n\nPlease check again!") % sku_errors)

        values.update({
            'warehouse_ids': [[6, False, selected_warehouse_ids]],
            'stock_allocation_lines': selected_stock_allocation_lines
        })
        res = super(StockAllocation, self).create(values)

        order_id = context.get('active_id', 0)
        model_name = 'sale.order' if context.get('stock_allocation_type', False) == 'sale' else 'purchase.order'
        order_obj = self.env[model_name].browse(order_id)
        if order_obj:
            order_obj.stock_allocation_id = res.id
        return res

    @api.multi
    @api.onchange('warehouse_ids')
    def onchange_warehouse(self):
        context = self._context
        stock_allocation_lines = []
        order_id = self._context.get('active_id', 0)
        model_name = 'sale.order.line' if context.get('stock_allocation_type', False) == 'sale' else 'purchase.order.line'
        order_line_obj = self.env[model_name].search([('order_id', '=', order_id)])

        for so_line in order_line_obj:
            if so_line.product_id.type != 'service':
                for wh in self.warehouse_ids:
                    data = {
                        'product_id': so_line.product_id.id,
                        'product_sku': so_line.product_id.default_code,
                        'product_name': so_line.product_id.name,
                        'price_unit': so_line.price_unit,
                        'order_line_id': so_line.id,
                        'warehouse_id': wh.id,
                        'warehouse_name': wh.name,
                        'qty_order': str(so_line.product_uom_qty) if context.get('stock_allocation_type', False) == 'sale' else str(so_line.product_qty)
                    }
                    product_exits = filter(lambda x: x['product_id'] == so_line.product_id.id, stock_allocation_lines)
                    if len(product_exits) > 0:
                        data.update({
                            'product_sku': '',
                            'product_name': '',
                            'qty_order': '',
                        })
                    stock_allocation_lines.append(data)
        self.stock_allocation_lines = stock_allocation_lines

    @api.multi
    def allocation(self):
        context = self._context
        order_id = context.get('active_id')
        order_name = ''
        partner_id = 0
        if context.get('stock_allocation_type', False) == 'sale':
            stock_picking_type_code = 'outgoing'
            sale_order_obj = self.env['sale.order'].browse(order_id)
            if sale_order_obj:
                order_name = sale_order_obj.name
                partner_id = sale_order_obj.partner_id.id
                sale_order_obj.sudo().action_confirm()
        else:
            stock_picking_type_code = 'incoming'
            purchase_order_obj = self.env['purchase.order'].browse(order_id)
            if purchase_order_obj:
                order_name = purchase_order_obj.name
                partner_id = purchase_order_obj.partner_id.id
                purchase_order_obj.sudo().button_confirm()

        del_stock_picking_objs = self.env['stock.picking'].search([('origin', '=', order_name)])
        for sp in del_stock_picking_objs:
            del_stock_pack_operation_objs = self.env['stock.pack.operation'].search([('picking_id', '=', sp.id)])
            for spo in del_stock_pack_operation_objs:
                spo.unlink()
            sp.unlink()

        group_id = False
        procurement_group_obj = self.env['procurement.group'].search([('name', '=', order_name)])
        if procurement_group_obj:
            group_id = procurement_group_obj.id

        for wh in self.warehouse_ids:
            move_lines = []
            stock_picking_type_obj = self.env['stock.picking.type'].search([
                ('warehouse_id', '=', wh.id),
                ('code', '=', stock_picking_type_code)
            ])

            location_src_id = 0
            location_dest_id = 0
            if stock_picking_type_code == 'incoming':
                location_src_id = self.env.ref('stock.stock_location_suppliers').id
                location_dest_id = wh.lot_stock_id.id
            elif stock_picking_type_code == 'outgoing':
                location_src_id = wh.lot_stock_id.id
                location_dest_id = self.env.ref('stock.stock_location_customers').id

            for line in self.stock_allocation_lines:
                if line.qty_shipping != 0 and wh.id == line.warehouse_id.id:
                    move_line_values = {
                        'origin': order_name,
                        'product_id': line.product_id.id,
                        'name': line.product_id.name,
                        'product_uom_qty': line.qty_shipping,
                        'product_uom': 1,
                        'price_unit': line.price_unit,
                        'group_id': group_id,
                        'location_id': location_src_id,
                        'location_dest_id': location_dest_id,
                        'picking_type_id': stock_picking_type_obj.id,
                        'state': 'draft'
                    }
                    if stock_picking_type_code == 'incoming':
                        move_line_values.update({
                            'purchase_line_id': line.order_line_id
                        })
                    else:
                        procurement_order_obj = self.env['procurement.order'].search([('origin', '=', order_name)])
                        if procurement_order_obj:
                            move_line_values.update({
                                'procurement_id': procurement_order_obj.id
                            })
                    move_lines.append([0, False, move_line_values])

            if order_name and partner_id and location_src_id and location_dest_id and len(move_lines) > 0:
                stock_picking_values = {
                    'origin': order_name,
                    'partner_id': partner_id,
                    'picking_type_id': stock_picking_type_obj.id,
                    'company_id': self.env.user.company_id.id,
                    'location_id': location_src_id,
                    'location_dest_id': location_dest_id,
                    'move_type': 'direct',
                    'min_date': datetime.now(),
                    'move_lines': move_lines
                }
                stock_picking_obj = self.env['stock.picking'].create(stock_picking_values)
                stock_picking_obj.sudo().action_confirm()

        self.write({'state': 'done'})
        return True


class StockAllocationLine(models.Model):
    _name = 'stock.allocation.line'

    def _get_qty_available_by_warehouse(self):
        for line in self:
            qty_available = line.product_id.with_context({'warehouse': line.warehouse_id.id}).qty_available
            line.qty_on_hand = qty_available

    stock_allocation_id = fields.Many2one('stock.allocation', string=_('Stock Allocation'))
    product_id = fields.Many2one('product.product', string=_('Product ID'))
    product_sku = fields.Char(string=_('Product SKU'))
    product_name = fields.Char(string=_('Product Name'))
    warehouse_id = fields.Many2one(string=_('Warehouse'))
    warehouse_name = fields.Char(string=_('Warehouse Name'))
    order_line_id = fields.Integer(string=_('Order Line Id'))
    price_unit = fields.Float(string=_('Price Unit'))
    qty_order = fields.Char(string=_('Qty order'))
    qty_on_hand = fields.Float(string=_('Qty on hand'), compute='_get_qty_available_by_warehouse')
    qty_shipping = fields.Float(string=_('Qty shipping'))

    @api.multi
    @api.onchange('qty_shipping')
    def onchange_qty(self):
        context = self._context
        stock_allocation_lines = context.get('stock_allocation_lines')
        tmp_list = filter(lambda x: x[2]['product_id'] == self.product_id.id, stock_allocation_lines)
        sum_qty_order = 0.0
        sum_qty_shipping = self.qty_shipping

        for p in tmp_list:
            if p[2]['qty_order'] != '':
                sum_qty_order += float(p[2]['qty_order'])
            sum_qty_shipping += p[2]['qty_shipping']

        if sum_qty_order < sum_qty_shipping:
            self.qty_shipping = self._origin.qty_shipping
            warning_mess = {
                'title': _('Warning!'),
                'message': _('The quantity shipping is not equal to quantity order. Please check again!')
            }
            return {'warning': warning_mess}


class Test123(models.Model):
    _inherit = "stock.picking"

    @api.model
    def create(self, vals):
        res = super(Test123, self).create(vals)
        return res


class StockMove(models.Model):
    _inherit = "stock.move"

    @api.model
    def create(self, vals):
        res = super(StockMove, self).create(vals)
        return res
