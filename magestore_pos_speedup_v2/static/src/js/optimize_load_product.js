odoo.define('magestore_pos_speedup_v2.optimize', function (require) {
    "use strict";
    var models = require('point_of_sale.models');
    var db = require('point_of_sale.DB');
    var Model = require('web.Model');

    // check browser is supported indexDB
    var indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB || window.shimIndexedDB;
    if (!indexedDB) {
        window.alert("Your browser doesn't support a stable version of IndexedDB.")
    }

    // override PosDB
    db.include({
        init: function (options) {
            this.all_products = [];
            this._super(options);
        },
        add_products: function (products) {
            if (!localStorage['cached_products']) {
                this.save_products_to_indexDB(products);
            }
            this._super(products);
        },
        save_product_model: function (product_model) {
            localStorage.setItem('product_model', JSON.stringify(product_model));
        },
        save_products_to_indexDB: function (products) {
            this.all_products = products;
            // open indexDB
            var request = indexedDB.open("ACE", 1);
            // on error
            request.onerror = function () {
                alert("You not allow my web app to use IndexedDB!");
            };
            // fire when database change
            request.onupgradeneeded = function (event) {
                var db = event.target.result;
                db.createObjectStore("Products", {keyPath: "id"});
            };
            // on connect success
            request.onsuccess = function (event) {
                var db = event.target.result;
                var transaction = db.transaction(["Products"], "readwrite");
                var store = transaction.objectStore("Products");

                // add all products to indexDB
                for (var i = 0, n = products.length; i < n; i++) {
                    store.put(products[i]);
                }

                // check all products is cached
                localStorage['cached_products'] = 'true';
                // close indexDB
                transaction.oncomplete = function () {
                    db.close();
                };
            }
        },
        get_products_from_indexDB: function () {
            var self = this;
            // open indexDB with database name
            var request = indexedDB.open("ACE", 1);
            // if success get all products
            request.onsuccess = function (event) {
                var db = event.target.result;
                var transaction = db.transaction(["Products"], "readwrite");
                var store = transaction.objectStore("Products");

                // get all products
                var get_products = store.getAll();
                // success
                get_products.onsuccess = function (event) {
                    self.all_products = event.target.result || [];
                    self.add_products(event.target.result);
                };
                // error
                get_products.onerror = function () {
                    console.error("Can't get products from indexDB!");
                };

                // close indexDB
                transaction.oncomplete = function () {
                    db.close();
                };
            };
        },
        update_version: function () {
            var old_version = localStorage['index_version'];
            if (!/^\d+$/.test(old_version)) {
                old_version = 0;
            }
            new Model('product.index').call('get_latest_version', [old_version]).then(function (res) {
                localStorage['index_version'] = res;
            });
        },
        synchronize: function () {
            var self = this;
            var client_version = localStorage['index_version'];
            if (!/^\d+$/.test(client_version)) {
                client_version = 0;
            }
            new Model('product.index').call('synchronize', [client_version]).then(function (res) {
                // open indexDB with database name
                var request = indexedDB.open("ACE", 1);
                request.onsuccess = function (event) {
                    var db = event.target.result;
                    var transaction = db.transaction(["Products"], "readwrite");
                    var store = transaction.objectStore("Products");


                    if (res['created']) {
                        var created_products = res['created'];
                        _.each(res['created'], function (product) {

                            var req = store.put(product);

                            req.onerror = function (event) {
                                console.error(event.target.result);
                                // remove product error
                                _.reject(created_products, function (e) {
                                    return e.id === product.id;
                                });
                                // reset index_version
                                localStorage['index_version'] = 0;
                            };
                        });

                        // add products to pos
                        self.add_products(created_products);
                    }

                    if (res['updated']) {
                        var updated_products = res['updated'];
                        _.each(res['updated'], function (product) {

                            var req = store.put(product);

                            req.onerror = function (event) {
                                console.error(event.target.result);
                                // remove product error
                                _.reject(updated_products, function (e) {
                                    return e.id === product.id;
                                });
                                // reset index_version
                                localStorage['index_version'] = 0;
                            };
                        });
                        // update products in pos
                        _.each(updated_products, function (product) {
                            self.remove_product(product);
                        });
                        self.add_products(updated_products);
                    }

                    if (res['deleted']) {
                        var deleted_products = res['deleted'];
                        _.each(res['deleted'], function (product) {

                            var req = store.delete(product.id);

                            req.onerror = function (event) {
                                _.reject(deleted_products, function (e) {
                                    return e.id === product.id;
                                });
                                // reset index_version
                                localStorage['index_version'] = 0;
                            };
                        });

                        _.each(deleted_products, function (product) {
                            self.remove_product(product);
                        })
                    }


                    localStorage['index_version'] = res['latest_version'];

                    // close indexDB
                    transaction.oncomplete = function () {
                        db.close();
                    };
                };
            });
        },
        remove_product: function (product) {
            var stored_categories = this.product_by_category_id;

            var search_string = this._product_search_string(product);
            var categ_id = product.pos_categ_id ? product.pos_categ_id[0] : this.root_category_id;

            // remove product in category
            if (stored_categories[categ_id]) {
                stored_categories[categ_id] = _.reject(stored_categories[categ_id], function (e) {
                    return e === product.id;
                });
            }

            // remove search string
            if (this.category_search_string[categ_id]) {
                this.category_search_string[categ_id].replace(search_string, '');
            }

            // remove product in ancestors
            var ancestors = this.get_category_ancestors_ids(categ_id) || [];

            for (var j = 0, jlen = ancestors.length; j < jlen; j++) {
                var ancestor = ancestors[j];
                if (stored_categories[ancestor]) {
                    stored_categories[ancestor] = _.reject(stored_categories[ancestor], function (e) {
                        return e === product.id;
                    });
                }

                // remove search string
                if (this.category_search_string[ancestor]) {
                    this.category_search_string[ancestor].replace(search_string, '');
                }

            }
        }
    });
    // -----------------------------------------------------------------------------------------------------------------

    var _super_pos = models.PosModel.prototype;
    models.PosModel = models.PosModel.extend({
        initialize: function (session, attributes) {
            this.index = this.models.map(function (e) {
                return e.model;
            }).indexOf('product.product');
            this.product_model = this.models[this.index];

            _super_pos.initialize.call(this, session, attributes);

            if (localStorage['cached_products']) {
                this.db.synchronize();
            }

        },
        load_server_data: function () {
            if (localStorage['cached_products']) {
                this.disable_load_product();
                this.db.get_products_from_indexDB();
            } else {
                this.enable_load_product();
            }
            return _super_pos.load_server_data.call(this);
        },
        disable_load_product: function () {
            if (this.index > -1) {
                this.models.splice(this.index, 1)
            }
        },
        enable_load_product: function () {
            this.db.update_version();
            this.db.save_product_model(this.product_model);
        }
    });
    //------------------------------------------------------------------------------------------------------------------

    // override load_fields() for check model 'product.product' add new fields
    models.load_product_fields = function (fields) {

        // reused load_fields() of core
        models.load_fields('product.product', fields);

        // check if new field, enable load product from server for update indexDB
        var _product_model = localStorage.getItem('product_model');
        if (_product_model) {
            var product_model = JSON.parse(_product_model);
            fields.forEach(function (field) {
                if (product_model.fields.indexOf(field) === -1) {
                    localStorage['cached_products'] = '';
                }
            });
        }
    };
});