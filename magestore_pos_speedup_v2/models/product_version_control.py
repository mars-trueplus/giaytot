import json

from odoo import api, fields, models, _


class ProductIndex(models.Model):
    _name = 'product.index'

    updated = fields.Char('Updated', default='')
    created = fields.Char('Created', default='')
    deleted = fields.Char('Deleted', default='')

    @api.model
    def get_latest_version(self, old_version):
        if str(old_version).isdigit():
            self.env.cr.execute('select id from product_index WHERE id >= %s order by id desc limit 1' % (old_version,))
        else:
            self.env.cr.execute('select id from product_index order by id desc limit 1')
        version = self.env.cr.fetchone()
        if not version:
            version = 0
        return version

    @api.model
    def synchronize(self, client_version):
        if str(client_version).isdigit():
            all_changed = self.search([('id', '>', client_version)])

            updated = ''
            created = ''
            deleted = ''

            for o in all_changed:
                updated += o.updated
                created += o.created
                deleted += o.deleted

            if updated != "":
                updated = self.env['product.product'].search_read([
                    ('id', 'in', [int(x) for x in updated.split(',') if x.isdigit()])
                ])
            if created != "":
                created = self.env['product.product'].search_read([
                    ('id', 'in', [int(x) for x in created.split(',') if x.isdigit()])
                ])
            if deleted != "":
                deleted = self.env['product.product'].search_read([
                    ('id', 'in', [int(x) for x in deleted.split(',') if x.isdigit()])
                ])

            return {
                'updated': updated,
                'created': created,
                'deleted': deleted,
                'latest_version': self.get_latest_version(client_version)
            }


class ProductLog(models.Model):
    _inherit = 'product.product'

    @api.model
    def create(self, values):
        res = super(ProductLog, self).create(values)
        if res.ids:
            self.env['product.index'].create({'created': ','.join([str(x) for x in res.ids])})
        return res

    @api.multi
    def write(self, values):
        if self.ids:
            self.env['product.index'].create({'updated': ','.join([str(x) for x in self.ids])})
        return super(ProductLog, self).write(values)

    @api.multi
    def unlink(self):
        if self.ids:
            self.env['product.index'].create({'deleted': ','.join([str(x) for x in self.ids])})
        return super(ProductLog, self).unlink()


class ProductTemplateLog(models.Model):
    _inherit = 'product.template'

    @api.multi
    def write(self, vals):
        for o in self:
            if o.product_variant_id.ids:
                self.env['product.index'].create({'updated': ','.join([str(x) for x in o.product_variant_id.ids])})
        return super(ProductTemplateLog, self).write(vals)
