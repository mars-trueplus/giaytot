# -*- coding: utf-8 -*-
{
    'name': "Magestore POS SpeedUp",
    'summary': """
    """,
    'description': """
    """,
    'author': "Magestore",
    'website': "https://www.magestore.com",
    'category': 'Point of Sale',
    'version': '1.0',
    'depends': ['base', 'point_of_sale'],
    'data': [
        'security/ir.model.access.csv',
        'views/templates.xml',
    ],
    'application': True,
}
