# -*- coding: utf-8 -*-

from odoo import api, models, fields, _
from odoo.exceptions import UserError


class ProductTemplate(models.Model):
    _inherit = "product.template"

    def _get_default_category_id(self):
        if self._context.get('categ_id') or self._context.get('default_categ_id'):
            return self._context.get('categ_id') or self._context.get('default_categ_id')
        category = self.env.ref('product.product_category_all', raise_if_not_found=False)

        # Checking category All
        if not category:
            default_config = self.env['ir.values'].sudo().get_default('sale.config.settings', 'service_category')
            if not default_config:
                category = self.env['product.category'].search([])[0]
            else:
                category = self.env['product.category'].search([('id', '=', default_config.id)])

        return category and category.id or False

    categ_id = fields.Many2one(
        'product.category', 'Internal Category',
        change_default=True, default=_get_default_category_id, domain="[('type','=','normal')]",
        required=True, help="Select category for the current product")


class DeliverySupplier(models.Model):
    _inherit = "delivery.carrier"

    supplier = fields.Many2one("res.partner", domain="[('is_deliver', '=', True), ('supplier', '=', True)]", string="Supplier")
    get_account_entries = fields.Boolean(default=False, string="Get account entries")


class SaleOrderWithDeliveryMethod(models.Model):
    _inherit = "sale.order"

    delivery_supplier = fields.Many2one('res.partner', domain="[('is_deliver', '=', True)]")
    delivery_type = fields.Selection([('cod', 'Giao tận nơi'), ('store', 'Giao tại cửa hàng')], default='cod')
    pos_id = fields.Many2one("pos.config", String="Store Address")
    invoice_address = fields.Char()
    shipping_address = fields.Char()

    @api.onchange("delivery_type", "partner_id")
    def _compute_address(self):
        partner_address = self.partner_id.address_get(['delivery', 'invoice'])
        if self.delivery_type=='cod':
            tmp = self.env['res.partner'].search([('id', '=', partner_address.get('invoice'))])
            if tmp:
                _street = tmp.street if tmp.street else ''
                _city = tmp.city if tmp.city else ''
                _state = tmp.state_id.name if tmp.state_id.name else ''
                _country = tmp.country_id.name if tmp.country_id.name else ''
                self.invoice_address = _street + ', ' + _city + ', ' + _state + ', ' + _country
            else:
                self.invoice_address = "None"

            tmp = self.env['res.partner'].search([('id', '=', partner_address.get('delivery'))])
            if tmp:
                _street = tmp.street if tmp.street else ''
                _city = tmp.city if tmp.city else ''
                _state = tmp.state_id.name if tmp.state_id.name else ''
                _country = tmp.country_id.name if tmp.country_id.name else ''
                self.shipping_address = _street + ', ' + _city + ', ' + _state + ', ' + _country
            else:
                self.shipping_address = "None"

        if self.delivery_type=='store':
            tmp = self.env['res.partner'].search([('id', '=', partner_address.get('invoice'))])
            if tmp:
                _street = tmp.street if tmp.street else ''
                _city = tmp.city if tmp.city else ''
                _state = tmp.state_id.name if tmp.state_id.name else ''
                _country = tmp.country_id.name if tmp.country_id.name else ''
                self.invoice_address = _street + ', ' + _city + ', ' + _state + ', ' + _country
            else:
                self.invoice_address = "None"

            self.shipping_address = self.pos_id.stock_location_id.name

    @api.model
    def create(self, vals):
        # Checking website_sale option
        sale_config = self.env['ir.values'].sudo().get_default('sale.config.settings', 'website_sale_id')
        team_id = vals.get('team_id')

        # Checking delivery method setting
        _carrier_id = vals.get("carrier_id")
        if _carrier_id and sale_config == team_id:
            delivery_carrier = self.env["delivery.carrier"].search([('id', '=', _carrier_id)])
            if delivery_carrier:
                _get_account_entries = delivery_carrier.get_account_entries
                if _get_account_entries:
                    # print "Get account entries is enable"
                    _supplier_id = delivery_carrier.supplier
                    _supplier_account_receivable = self.env['res.partner'].search([('id', '=', _supplier_id.id)])
                    if _supplier_account_receivable:
                        supplier_account_receivable = _supplier_account_receivable.property_account_receivable_id
                        partner_id = vals.get('partner_id')
                        _partner = self.env['res.partner'].search([('id', '=', partner_id)])
                        if _partner:
                            # partner_account_receivable = _partner.property_account_receivable_id
                            _partner.write({
                                'property_account_receivable_id': supplier_account_receivable,
                            })
                            # print _partner.property_account_receivable_id
                            # print "anh"
                else:
                    print "Get account entries is disable"

        return super(SaleOrderWithDeliveryMethod, self).create(vals)

    @api.multi
    def write(self, values):
        if values.has_key('project_id'):
            return super(SaleOrderWithDeliveryMethod, self).write(values)

        # Check delivery method
        sale_config = self.env['ir.values'].sudo().get_default('sale.config.settings', 'website_sale_id')
        _carrier_id = values.get('carrier_id')

        if (_carrier_id is not None or self.carrier_id is not None) and \
                (self.team_id.id == sale_config or values.get('team_id') == sale_config):

            if _carrier_id is None and self.carrier_id is None:
                raise UserError(_("You need to set delivery method"))

            # Check get account entries
            if _carrier_id is not None:
                _delivery_carrier = self.env['delivery.carrier'].search([('id', '=', _carrier_id)])
            if self.carrier_id is not None:
                _delivery_carrier = self.env['delivery.carrier'].search([('id', '=', self.carrier_id.id)])

            _get_account_entries = _delivery_carrier.get_account_entries
            if _get_account_entries:
                _supplier_id = _delivery_carrier.supplier
                _supplier_account_receivable = self.env['res.partner'].search([('id', '=', _supplier_id.id)])
                if _supplier_account_receivable:
                    supplier_account_receivable = _supplier_account_receivable.property_account_receivable_id
                    partner_id = self.partner_id.id
                    _partner = self.env['res.partner'].search([('id', '=', partner_id)])
                    #
                    # print _partner.property_account_receivable_id
                    #
                    if _partner:
                        _partner.write({
                            'property_account_receivable_id': supplier_account_receivable,
                        })
                        #
                        # print _partner.property_account_receivable_id

        return super(SaleOrderWithDeliveryMethod, self).write(values)