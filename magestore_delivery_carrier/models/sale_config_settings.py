# -*- coding: utf-8 -*-

from odoo import api, fields, models


class SaleConfigSetting(models.TransientModel):

    _inherit = 'sale.config.settings'

    website_sale_id = fields.Many2one('crm.team', string="Website Sale")
    service_category = fields.Many2one("product.category")

    @api.multi
    def set_website_sale_id_defaults(self):
        return self.env['ir.values'].sudo().set_default(
            'sale.config.settings', 'website_sale_id', self.website_sale_id.id)