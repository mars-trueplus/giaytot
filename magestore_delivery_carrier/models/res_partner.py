# -*- coding:utf-8 -*-
from odoo import api, models, fields


class ResPartnerDeliver(models.Model):
    _inherit = "res.partner"

    is_deliver = fields.Boolean("Is deliver partner")