# -*- coding: utf-8 -*-
{
    'name': "Magestore Delivery Carrier",

    'summary': """
        Magestore delivery carrier""",

    'description': """
        Magestore delivery carrier
    """,

    'author': "Magestore",
    'website': "http://www.magestore.com",
    'category': 'Tools',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base',
                'delivery',
                'account',
                'sale',
                ],

    'data': [
        'views/delivery_supplier_view.xml',
        'views/delivery_config_settings.xml',
        'views/sale_order_view.xml',
        'views/view_partner_form_inherit.xml',
    ],
    'sequence': 1,
    'application': True,
}
