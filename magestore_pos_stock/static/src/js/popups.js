odoo.define("magestore_pos_stock.popups", function(require){
    'use strict';

    var core = require('web.core');
    var gui = require('point_of_sale.gui');
    var PopupWidget = require('point_of_sale.popups');

    var OutOfStockNotificationPopupWidget = PopupWidget.extend({
        template: "OutOfStockNotificationPopupWidget",
        events: _.extend({}, PopupWidget.prototype.events, {
            'click .popup-noti-outstock-check': 'click_on_check_stock_button',
            'click .popup-noti-outstock-confirm': 'click_on_confirm_button',
            'click .popup-noti-outstock-cancel': 'click_on_cancel_button',
            'click .popup-noti-outstock-ok': 'click_on_ok_button'
        }),

        click_on_check_stock_button: function(){
            // display checkstockscreen
            this.gui.show_screen('checkstockscreen', {name: this.options.context.product.barcode || this.options.context.product.display_name, isCheck: true, cid: this.cid, product_id: this.options.product_id});
            if(this.options.context){
                this.options.context.order.remove_orderline(this.options.context);
                this.options.context.order.is_stock_checking = false;
            }
        },
        click_on_confirm_button: function(){
            if(this.options.context){
                this.options.context.order.is_stock_checking = false;
            }
             if (this.options.my_confirm && this.options.max_available) {
                this.options.my_confirm.call(this.options.context, this.options.max_available);
             }else if(this.options.out_stock){
                 // add orderline
                 this.options.context.order.add_orderline(this.options.context);
                 this.options.context.order.enable_out_of_stock.push(this.options.product_id);
                 this.options.my_confirm.call(this.options.context, this.options.out_stock);
             }
        },
        click_on_cancel_button: function(){
            if(this.options.context){
                this.options.context.order.remove_orderline(this.options.context);
                this.options.context.order.is_stock_checking = false;
            }
        },
        click_on_ok_button: function(){
            this.click_on_cancel_button();
        }
    });
    gui.define_popup({name: 'out_of_stock_popup', widget: OutOfStockNotificationPopupWidget});

    return {
        OutOfStockNotificationPopupWidget: OutOfStockNotificationPopupWidget
    };
});