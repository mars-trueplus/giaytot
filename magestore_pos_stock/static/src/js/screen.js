odoo.define('magestore_pos_stock.screen', function (require) {
    'use strict';

    var core = require('web.core');
    var gui = require('point_of_sale.gui');
    var screens = require('point_of_sale.screens');


    var QWeb = core.qweb;
    var _t = core._t;

    /*--------------------------------------*\
     |         THE CHECK STOCK SCREEN       |
     \*======================================*/
    var CheckStockScreenWidget = screens.ScreenWidget.extend({
        template: "CheckStockScreen",
        events: _.extend({}, screens.ScreenWidget.prototype.events, {
            'click .checkstock-back': 'checkstock_back',
            'click .search-clear': 'clear_checkstock_search'
        }),
        init: function (parent, options) {
            var self = this;
            this._super(parent, options);
            this.partner_cache = new screens.DomCache();
        },
        auto_back: true,
        show: function (options) {
            var self = this;
            options = options || {};
            this._super(options);

            this.renderElement();

            this.$('.checkstock-search input').focus();

            self.render_product_list([]);
            this.param_name = this.gui.get_current_screen_param('name');
	        if (this.param_name && typeof this.param_name !== 'undefined') {
	            this.perform_search(this.param_name);
	            this.$('.checkstock-search input')[0].value = this.param_name;
	            this.$('.checkstock-search input').focus();
	        }

            this.$('.checkstock-product-contents').delegate('.checkstock-product-line', 'click', function (event) {
                self.product_line_select(event, $(this), parseInt($(this).data('id')));
            });

            if (this.pos.config.iface_vkeyboard && this.chrome.widget.keyboard) {
                this.chrome.widget.keyboard.connect(this.$('.checkstock-search input'));
            }

            this.$('.checkstock-search input').on('keyup', function (event) {
                self.perform_search(this.value);
            });

            this.details_visible = false;
            this.selected_tr_element = null;
        },
        renderElement: function () {
            this._super();
        },
        checkstock_back: function () {
            this.gui.back();
        },
        // show product list on check stock page
        render_product_list: function (products) {
            var content = this.$el[0].querySelector('.checkstock-product-contents');
            content.innerHTML = "";
            var len = Math.min(products.length, 1000);
            for (var i = 0; i < len; i++) {
                var product = products[i];
                var attr_label = "";
                if (product.attribute_value_ids) {
                    var attribute_value_ids = product.attribute_value_ids;
                    // Added by Adam: set attribute value label to product
                    var value_len = attribute_value_ids.length;
                    for (var j = 0; j < value_len; j++) {
                        var value = this.pos.db.get_attribute_value_by_id(attribute_value_ids[j]);
                        if (j < value_len - 1) {
                            attr_label += value['attribute_id'][1] + ": " + value.name + ", "
                        } else {
                            attr_label += value['attribute_id'][1] + ": " + value.name;
                        }
                    }
                    product.attr_label = attr_label;
                }
                var productline = this.partner_cache.get_node('p' + product.id);
                if (!productline) {
                    var productline_html = QWeb.render('CheckStockProductLine', {widget: this, product: product});
                    productline = document.createElement('tbody');
                    productline.innerHTML = productline_html;
                    productline = productline.childNodes[1];
                    this.partner_cache.cache_node('p' + product.id, productline);
                }
                content.appendChild(productline);
            }
        },
        perform_search: function (query) {
            var custom_search = require('magestore_point_of_sale.custom_search_pos');
            var products;
            if (query) {
            // Changed by Adam: Fix issue display two or more product with the same name when click on product to check stock
                var product_id = this.gui.get_current_screen_param('product_id');
                if(product_id){
                    products = [this.pos.db.get_product_by_id(product_id)];
                } else{
                    products = this.pos.db.search_product_in_category(0, custom_search.vn_un_accent(query));
                }
                this.display_stock_content('hide');
                this.render_product_list(products);
                if (products.length === 1) {
                    this.display_stock_content('show', products[0], 0);
                }
            } else {
                this.render_product_list([]);
            }
        },
        clear_checkstock_search: function () {
            //products: this.pos.db.get_product_by_category(0)
//            this.render_product_list(products);
            this.render_product_list([]);
            this.$('.checkstock-search input')[0].value = '';
            this.$('.checkstock-search input').focus();
        },
        display_stock_content: function (visibility, product, clickpos) {
            var self = this;
            var contents = this.$('.checkstock-product-details');
            var parent = this.$('.checkstock-product-list').parent();
            var scroll = parent.scrollTop();
            var height = contents.height();

            if (visibility === 'show') {
                contents.empty();
                var all_locations = self.pos.allLocationWarehourse;
                var real_stock = self.get_stock_quant_by_barcode(product.barcode, product.id, product.display_name);
                for(var i = 0; i < all_locations.length; i++){
                    all_locations[i].qty = 0;
                    if(real_stock.length){
                        for(var j = 0; j<real_stock.length; j++){
                            if(real_stock[j].location_id === all_locations[i].location_id){
                                all_locations[i].qty += real_stock[j].qty;
                            }
                        }
                    }
                }
                contents.append($(QWeb.render('CheckStockProductDetails', {
                    widget: this,
                    product: product,
                    stocks: all_locations
                })));
                var new_height = contents.height();

                if (!this.details_visible) {
                    parent.height('-=' + new_height);
                    if (clickpos < scroll + new_height + 20) {
                        parent.scrollTop(clickpos - 20);
                    } else {
                        parent.scrollTop(parent.scrollTop() + new_height);
                    }
                } else {
                    parent.scrollTop(parent.scrollTop() - height + new_height);
                }
                self.details_visible = true;
                this.$("#close_checkstock_popup").on('click', function () {
                    if (self.selected_tr_element) {
                        self.selected_tr_element.removeClass('highlight');
                        self.selected_tr_element.addClass('lowlight');
                    }
                    self.details_visible = false;
                    self.display_stock_content('hide', null);
                });

            } else if (visibility === 'hide') {
                contents.empty();
                parent.height('100%');
                if (height > scroll) {
                    contents.css({height: height + 'px'});
                    contents.animate({height: 0}, 400, function () {
                        contents.css({height: ''});
                    });
                } else {
                    parent.scrollTop(parent.scrollTop() - height);
                }
                this.details_visible = false;
            }
        },
        product_line_select: function (event, $line, id) {
            var product = this.pos.db.get_product_by_id(id);

            this.$(".checkstock-product-list .lowlight").removeClass('lowlight');
            if ($line.hasClass('highlight')) {
                $line.removeClass('highlight');
                $line.addClass('lowlight');
                this.display_stock_content('hide', product);

            } else {
                this.$('.checkstock-product-list .highlight').removeClass('highlight');
                $line.addClass('highlight');
                var y = event.pageY - $line.parent().offset().top;
                this.display_stock_content('show', product, y);
            }
        },
        // Changed by Adam: Search product by barcode or product id or product name
        get_stock_quant_by_barcode: function (barcode, product_id, product_name) {
            if (barcode) {
                return Object.values(this.pos.stock_quant_by_id).filter(function (t) {
                    return t.barcode === barcode;
                });
            } else if(product_id){
                return Object.values(this.pos.stock_quant_by_id).filter(function (t) {
                    return t.product_id === product_id;
                });
            } else {
                return Object.values(this.pos.stock_quant_by_id).filter(function (t) {
                    return t.product_name === product_name;
                });
            }
        }
    });

    gui.define_screen({name: 'checkstockscreen', widget: CheckStockScreenWidget});

    screens.ProductScreenWidget.include({
        click_product: function(product) {
            this._super(product);
            if(!this.pos.config.check_stock_mode){
                var lines = Object.values(this.pos.get_order().orderlines.models).filter(function (t) {
                    return t.product.id === product.id;
                });
                var qty = 0;
                for(var line in lines){
                    qty += line.quantity;
                }
                if(product.qty_available <= 0 || product.qty_available < qty){
                    this.pos.get_order().enable_out_of_stock.push(product.id)
                }
            }
        },
    });

    return {CheckStockScreenWidget: CheckStockScreenWidget}
});