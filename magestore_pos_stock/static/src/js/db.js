odoo.define("magestore_pos_stock.DB", function(require){
    "use strict";

    var pos_DB = require("point_of_sale.DB");

    var pos_db = pos_DB.include({
        init: function(options){
            this._super(options);

            this.product_search_string = "";
            this.product_sorted = [];
            this.product_by_id = {};
            this.product_by_barcode = {};

            this.product_attributes = [];
            this.product_attribute_by_id = {};

            this.product_attribute_values = [];
            this.product_attribute_value_by_id = {};
        },
        _product_search_string: function(product){
            var self = this;
            var str = self._super(product);
            var product_search_string = ""
            if(str){
                product_search_string = str.split(":")[1];
                if(product.price){
                    product_search_string = product.price + "|" + product_search_string;
                }
                if(product.attr){
                    product_search_string = product.attr + "|" + product_search_string;
                }
            }
            product_search_string = product.id + ':' + product_search_string.replace(/:/g,'') + '\n';
            this.product_search_string = product_search_string;
            return this.product_search_string;
        },
        get_attribute_value_by_id: function(id){
            return this.product_attribute_value_by_id[id];
        },
        get_attribute_by_id: function(id){
            return this.product_attribute_by_id[id];
        }
    });
});