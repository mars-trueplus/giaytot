odoo.define('magestore_pos_stock.BaseWidget', function(require){
    'use strict';

    var MagePosBaseWidget = require('magestore_point_of_sale.BaseWidget');

    MagePosBaseWidget.PromotionLeftMenu.include({
        renderElement: function () {
            var self = this;
            this._super();
            this.$('.prom-check-stock').click(function () {
                self.gui.show_screen('checkstockscreen', {});
            });
        }
    });
});