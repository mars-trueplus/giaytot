odoo.define('magestore_pos_stock.models', function (require) {
    "use strict";

    var models = require('point_of_sale.models');
    var Model = require('web.DataModel');
    var _t = require('web.core')._t;

    // load product fields
    require('magestore_pos_speedup_v2.optimize');
    models.load_product_fields(['type', 'attribute_value_ids', 'attr_label', 'qty_available']);

    models.load_models([
        {
            model: 'product.attribute',
            fields: [
                'name'
            ],
            domain: null,
            loaded: function (self, product_attributes) {
                self.db.product_attributes = product_attributes;
                var len = product_attributes.length;
                for (var i = 0; i < len; i++) {
                    var attribute = product_attributes[i];
                    self.db.product_attribute_by_id[attribute.id] = attribute;
                }
            }
        },
        {
            model: 'product.attribute.value',
            fields: [
                'attribute_id',
                'name',
            ],
            domain: null,
            loaded: function (self, all_product_attribute_values) {
                self.db.product_attribute_values = all_product_attribute_values;
                var len = all_product_attribute_values.length;
                for (var i = 0; i < len; i++) {
                    var value = all_product_attribute_values[i];
                    self.db.product_attribute_value_by_id[value.id] = value;
                }
            }
        },
        {
            model: 'stock.quant',
            fields: [
                'product_id',
                'location_id',
                'name',
                'qty'
            ],
            domain: function (self) {
                var domain_list = [];
                var location_ids = [];
                var product_ids = self.db.product_by_category_id[0];
                if (self.config.display_qty_stock) {
                    location_ids = self.config.location_ids;
                }
                var stock_location_id = self.config.stock_location_id[0];
                if (location_ids.indexOf(stock_location_id) === -1) {
                    location_ids.push(stock_location_id);
                }
                //
                // Walter fix load product
                if (product_ids !== undefined) {
                    domain_list = [['location_id', 'in', location_ids], ['product_id', 'in', product_ids]];
                }
                // DUST
                // domain_list = [['id', '=', 1]];
                return domain_list;
            },
            loaded: function (self, stock_quants) {
                self.checkStockQuant = [];
                self.stock_quant_by_id = {};
                self.checkStockQuant = stock_quants;
                var len = stock_quants.length;

                for (var i = 0; i < len; i++) {
                    var stock_quant = stock_quants[i];
                    var product = self.db.get_product_by_id(stock_quant.product_id[0]);
                    //
                    // Walter fix load product
                    if (product !== undefined) {
                        var stock = {
                            "id": stock_quant.id,
                            "location_id": stock_quant.location_id[0],
                            "location_name": stock_quant.location_id[1],
                            "product_id": stock_quant.product_id[0],
                            "product_name": stock_quant.product_id[1],
                            "product_barcode": product.barcode,
                            "qty": stock_quant.qty
                        };

                        // Set qty available for product
                        self.stock_quant_by_id[stock_quant.id] = stock;
                        if (stock_quant.location_id[0] === self.config.stock_location_id[0]) {
                            product.qty_available = stock_quant.qty;
                        }
                    }
                }
                var pos_stock_quants = stock_quants.filter(function (stock_quant) {
                    return stock_quant.location_id[0] === self.config.stock_location_id[0]
                });

                // Get only stock_quant of POS location
                // to calculate product qty available
                // set product qty_available to 0
                for (var j = 0; j < pos_stock_quants.length; j++){
                    var stock_quant_reset = pos_stock_quants[j];
                    var product_reset = self.db.get_product_by_id(stock_quant_reset.product_id[0]);
                    if (product_reset){
                        product_reset.qty_available = 0;
                    }
                }
                // get product qty_available = SUM(stock_quants)
                for (var k = 0; k < pos_stock_quants.length; k++){
                    var pos_stock_quant = pos_stock_quants[k];
                    var product_available = self.db.get_product_by_id(pos_stock_quant.product_id[0]);
                    if (product_available){
                        product_available.qty_available += pos_stock_quant.qty;
                    }
                }
            }
        }
    ]);

    var _super_pos_model = models.PosModel.prototype;
    models.PosModel = models.PosModel.extend({
        load_server_data: function () {
            var self = this;

            //Adam - Fix loi check stock tren live site
            return _super_pos_model.load_server_data.apply(this, arguments).then(function () {
                var configModel = new Model('pos.config');
                self.allLocationWarehourse = [];
                configModel.call('get_all_location_warehourse', [self.pos_session.config_id[0]]).then(function (result) {
                    self.allLocationWarehourse = result;
                });
            });
        },
        _save_to_server: function (orders, options) {
            var self = this;
            var base = _super_pos_model._save_to_server.call(this, orders, options);
            base.done(function (server_ids) {
                _.each(orders, function (order) {
                    var stock_quant_fields = models.get_fields('stock.quant');
                    new Model('pos.order').call('get_stock_quants_pos_order', [stock_quant_fields, order.id]).then(function (stock_quants) {
                        _.each(stock_quants, function (stock_quant) {
                            self.stock_quant_by_id[stock_quant.id] = stock_quant;
                        });
                    });
                });
            });
            return base;
        }
    });


    var _super_order = models.Order.prototype;
    models.Order = models.Order.extend({
        // add enable_out_stock_key_values to order model - by ace
        initialize: function (attributes, options) {
            this.enable_out_of_stock = [];
            this.is_stock_checking = false;
            _super_order.initialize.call(this, attributes, options);

        },

        init_from_JSON: function (json) {
            this.enable_out_of_stock = json.enable_out_of_stock;
            this.is_stock_checking = json.is_stock_checking;
            _super_order.init_from_JSON.call(this, json);

        },
        export_as_JSON: function () {
            var loaded = _super_order.export_as_JSON.call(this);
            loaded.enable_out_of_stock = this.enable_out_of_stock ? this.enable_out_of_stock : [];
            return loaded;
        }
    });

    // Added and Fix Bug by Craig - US57
    var _super_order_line = models.Orderline.prototype;
    models.Orderline = models.Orderline.extend({
        set_quantity: function (quantity) {

            _super_order_line.set_quantity.call(this, quantity);

            var self = this;

            // Composed by Craig
            // This boolean variable to check if product type is service
            // Not apply to product with type is service
            // Because in JS False equal to 0, product service type is false\
            // So it equal 0
            var exclude_service_product = this.product.type !== "service";
            //end
            // check if order != return_order
            if (quantity > 0 && this.pos.config.check_stock_mode) {

                // show popup check stock in two cases- by ace
                if (this.pos.config.allow_to_order_out_of_stock_product) {
                    if (exclude_service_product && quantity > this.product.qty_available && this.order.enable_out_of_stock && this.order.enable_out_of_stock.indexOf(this.product.id) === -1) {
                        // mark order is stock checking
                        this.order.is_stock_checking = true;
                        // show popup
                        this.pos.gui.show_popup('out_of_stock_popup',
                            {
                                'message': _t("The quantity of this product is out of stock. Do you want to order it? "),
                                'my_confirm': _super_order_line.set_quantity,
                                'out_stock': quantity,
                                'context': self,
                                'product_id': this.product.id
                            });
                    }

                } else {

                    if (quantity > this.product.qty_available && exclude_service_product && this.order.enable_out_of_stock && this.order.enable_out_of_stock.indexOf(this.product.id) === -1) {
                        if (this.product.qty_available <= 0) {
                            // mark order is stock checking
                            this.order.is_stock_checking = true;
                            this.pos.gui.show_popup('out_of_stock_popup', {
                                'message': "This product is out of stock. You can't order it!",
                                'context': self,
                                'ok': true
                            });
                        } else {
                            // mark order is stock checking
                            this.order.is_stock_checking = true;
                            this.pos.gui.show_popup('out_of_stock_popup', {
                                'message': _t("The quantity of this product is only " + this.product.qty_available + ". Do you want to order it? "),
                                'my_confirm': _super_order_line.set_quantity,
                                'max_available': this.product.qty_available,
                                'context': self
                            });
                        }
                    }
                }
            }
        }
    });
});
