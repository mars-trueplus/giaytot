odoo.define('magestore_pos_stock.pos_stock', function (require) {
    "use strict";
    var models = require('point_of_sale.models');
    var screens = require('point_of_sale.screens');
    var _super_posmodel = models.PosModel.prototype;
    var _t = require('web.core')._t;

    screens.PaymentScreenWidget.include({
        refresh_qty_available: function (product) {
            var $elem = $("[data-product-id='" + product.id + "'] .product-status");
            if (product.qty_available <= 0) {
                $elem.addClass('product-not-available');
                $elem.text(_t("SOLD OUT"));
            } else {
                $elem.text(_t('Qty: ') + product.qty_available);
            }
        },
        validate_order: function (force_validation) {
            this._super();
            var self = this;
            var order = this.pos.get_order();
            if (order) {
                order.orderlines.each(function (line) {
                    var product = line.get_product();
                    var location = self.pos.shop;
                    var stock_quant = self.get_stock_quant_by_barcode_name(product.id, product.display_name, location.id);
                    product.qty_available -= line.get_quantity();
                    if(stock_quant && stock_quant.lenght > 0){
                        stock_quant[0].qty = product.qty_available
                    }
                    self.refresh_qty_available(product);
                });
            }
        },
        get_stock_quant_by_barcode_name: function (barcode, product_name, location_id) {
            if (barcode) {
                return Object.values(this.pos.stock_quant_by_id).filter(function (t) {
                    return t.product_id === barcode && t.location_id === location_id;
                });
            } else {
                return Object.values(this.pos.stock_quant_by_id).filter(function (t) {
                    return t.product_name === product_name && t.location_id === location_id;
                });
            }
        }
    });

    screens.ProductListWidget.include({
        renderElement: function (reset) {
            this._super();
            var self = this;
            $('.product-status').prev().css('bottom', '20px');
            if (this.pos.config.press_product_status_to_view_stock) {
                $('.product-available, .product-not-available').bind('click', function (event) {
                    // Click to button SOLD OUT, PRODUCT QTY
                    event.stopPropagation();
                    var barcode = $(this).attr('data-product-barcode');
                    var product_id = $(this).parent().attr('data-product-id');
                    if (barcode != false) {
                        self.gui.show_screen('checkstockscreen', {name: barcode, product_id: product_id});
                    } else {
                        var name = $(this).attr('data-product-name');
                        self.gui.show_screen('checkstockscreen', {name: name, product_id: product_id});
                    }
                });
            }
        }
    });
});