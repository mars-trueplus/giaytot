# -*- coding: utf-8 -*-

from odoo import api, fields, models


class PosOrder(models.Model):
    _inherit = 'pos.order'

    @api.model
    def get_stock_quants_pos_order(self, stock_quant_fields,
                                   pos_reference=None,
                                   order_id=None):
        # if ('id' not in stock_quant_fields):
        #     stock_quant_fields.append('id')
        # if pos_reference:
        #     order = self.search([('pos_reference', 'like', pos_reference)])
        # elif order_id:
        #     order = self.browse(order_id)
        # else:
        #     return False
        #
        # stock_quants = []
        # picking_id = order.picking_id
        # move_lines = picking_id.move_lines
        # for move in move_lines:
        #     quants = move.quant_ids
        #     for quant in quants:
        #         filter_stock_quant_fields = {key: quant[key] for key in
        #                                      stock_quant_fields}
        #         stock_quants.append(filter_stock_quant_fields)
        #
        # for quant in stock_quants:
        #     if 'product_id' in quant:
        #         product = quant['product_id']
        #         quant['product_id'] = product.id
        #         quant['product_name'] = product.name
        #         quant['product_barcode'] = product.barcode
        #
        #     if 'location_id' in quant:
        #         location = quant['location_id']
        #         quant['location_id'] = location.id
        #         quant['location_name'] = location.name
        # return stock_quants
        return []
