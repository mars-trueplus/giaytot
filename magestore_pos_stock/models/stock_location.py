# -*- coding: utf-8 -*_

from odoo import fields, api, models

class StockLocation(models.Model):
    _inherit = 'stock.location'

    warehouse_id = fields.Many2one('stock.warehouse', compute='_get_warehouse_id')

    @api.multi
    def _get_warehouse_id(self):
        for location in self:
            location.warehouse_id = location.get_warehouse().id