# -*- coding: utf-8 -*-

from odoo import models, fields, api, _

class PosConfig(models.Model):
    _inherit = "pos.config"

    check_stock_mode = fields.Boolean(string="Check stock mode", default=True)
    display_qty_stock = fields.Boolean(string="Display qty stock", default=False)
    # stock_type = fields.Selection([
    #     ('available_qty', 'Available qty'),
    #     ('qty_on_hand', 'Qty on hand')
    # ], string="Stock type", default='qty_on_hand')
    stock_type = fields.Selection([
        ('qty_on_hand', 'Qty on hand')
    ], string="Stock type", default='qty_on_hand')
    allow_to_order_out_of_stock_product = fields.Boolean(string='Allow to order when out of stock', default=False)
    press_product_status_to_view_stock = fields.Boolean(string="Press product status to view stock", default=False)
    location_ids = fields.Many2many("stock.location", string="Related location")

    @api.onchange('check_stock_mode', 'display_qty_stock')
    def _compute_check_stock_mode(self):
        if not self.check_stock_mode:
            self.display_qty_stock = False
            self.allow_to_order_out_of_stock_product = False
            self.press_product_status_to_view_stock = False
        if not self.display_qty_stock:
            self.press_product_status_to_view_stock = False

    @api.model
    def get_check_stock(self, id, check_out_of_stock=False):
        result = []
        res = self.search([('id', '=', id)])

        ls_product_template = self.env['product.template'].search([('available_in_pos', '=', True)]).ids
        ls_product_product = self.env['product.product'].search([('product_tmpl_id', 'in', ls_product_template)]).ids

        ls_stock_quants = []

        if check_out_of_stock:
            ls_stock_quants = self.env['stock.quant'].search(
                [('product_id', 'in', ls_product_product), ('location_id', '=', res.stock_location_id.id)])
        else:
            ls_location_id = res.location_ids.ids
            if res.stock_location_id.id not in ls_location_id:
                ls_location_id.append(res.stock_location_id.id)
            ls_stock_quants = self.env['stock.quant'].search(
                [('product_id', 'in', ls_product_product), ('location_id', 'in', ls_location_id)])

        for stock_quant in ls_stock_quants:
            stock_location = self.env['stock.location'].search(
                [('id', '=', stock_quant.location_id.id), '|', ('active', '=', False), ('active', '=', True)])
            stock_warehouse = stock_location.get_warehouse()
            stock_quant.warehouse_name = stock_warehouse.name
            stock_quant.warehouse_id = stock_warehouse

            val = {}
            val.update({
                'id': stock_quant.id,
                'location_id': stock_location.id,
                'location_name': stock_location.name,
                'location_display_name': stock_location.display_name,

                'qty': stock_quant.qty,
                'barcode': stock_quant.product_id.barcode,
                'product_name': stock_quant.product_id.name,
                'product_display_name': stock_quant.product_id.display_name,
            })
            if stock_warehouse:
                val.update({
                    'warehouse_name': stock_quant.warehouse_name,
                    'phone': stock_quant.warehouse_id.partner_id.phone,
                    'mobile': stock_quant.warehouse_id.partner_id.mobile,
                    'address': stock_quant.warehouse_id.partner_id.street,
                    'email': stock_quant.warehouse_id.partner_id.email
                })
            result.append(val)

        sorted_list = sorted(result, key=lambda x: (x['location_name'], x['barcode']))
        return sorted_list
        last = sorted_list[0]['barcode']
        new_list = [sorted_list[0]]
        sorted_list.pop(0)
        for x in sorted_list:
            if x['barcode'] != last:
                new_list.append(x)
                last = x['barcode']
            else:
                new_list[len(new_list) - 1]['qty'] += x['qty']

        return new_list

    @api.model
    def get_all_location_warehourse(self, id):
        config = self.env['pos.config'].search([('id', '=', id)])
        location_ids = config.location_ids.ids
        location_ids.append(config.stock_location_id.id)
        locations = self.env['stock.location'].search([('usage', '=', 'internal'), ('id', 'in', location_ids)])
        result = []
        for location in locations:
            val = {}
            warehouse = location.get_warehouse()
            val.update({
                'location_id': location.id,
                'location_name': location.name,
                'location_display_name': location.display_name,
                'qty': 0
            })

            if warehouse:
                val.update({
                    'warehourse_id': warehouse.id,
                    'warehouse_name': warehouse.display_name,
                    'phone': warehouse.partner_id.phone,
                    'mobile': warehouse.partner_id.mobile,
                    'address': warehouse.partner_id.street,
                    'email': warehouse.partner_id.email,
                })
            result.append(val)
        return result
