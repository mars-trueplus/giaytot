# -*- coding: utf-8 -*-
{
    'name': "Magestore Pos Stock",

    'summary': """
    """,

    'description': """
    """,

    'author': "Magestore",
    'website': "https://www.magestore.com",

    'category': 'Point of Sale',
    'version': '1.0',

    'depends': [
        'base',
        'magestore_point_of_sale',
    ],

    'data': [
        'views/templates.xml',
        'views/pos_config_view.xml',
    ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
    "application": True,
    "installable": True,
    "auto_install": False,
}
