# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (c) 2015-Present Webkul Software Pvt. Ltd. (<https://webkul.com/>)
#
#################################################################################
import models
from odoo import api, SUPERUSER_ID

def pre_init_check(cr):
	from openerp.service import common
	from openerp.exceptions import Warning
	version_info = common.exp_version()
	server_serie =version_info.get('server_serie')
	if server_serie!='10.0':raise Warning('Module support Odoo series 10.0 found {}.'.format(server_serie))
	return True

def post_init_hook(cr, registry):
	env = api.Environment(cr, SUPERUSER_ID, {})
	sale_orders = env['pos.order'].search([])
	for sale_order in sale_orders:
		try:
			if sale_order.partner_id:
				sale_order.customer_phone = sale_order.partner_id.phone or sale_order.partner_id.mobile or '1'
		except Exception as e:
			continue