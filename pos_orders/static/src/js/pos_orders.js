odoo.define('pos_orders.pos_orders', function (require) {
    "use strict"
    var Model = require('web.DataModel');
    var screens = require('point_of_sale.screens');
    var gui = require('point_of_sale.gui');
    var models = require('point_of_sale.models');
    var utils = require('web.utils');
    var core = require('web.core');
    var QWeb = core.qweb;
    var SuperPosModel = models.PosModel.prototype;
    var MageBaseWidget = require('magestore_point_of_sale.BaseWidget');

    models.load_models([{
        model: 'pos.order',
        fields: ['id', 'name', 'date_order', 'partner_id', 'lines', 'pos_reference', 'invoice_id', 'customer_phone', 'location_id', 'statement_ids', 'amount_total'],
        domain: function (self) {
            var date_from = new Date();
            date_from.setDate(date_from.getDate() - 30);
            var dd = date_from.getDate();
            var mm = date_from.getMonth() + 1; //January is 0!
            var yyyy = date_from.getFullYear();
            if (dd < 10) {
                dd = '0' + dd;
            }
            if (mm < 10) {
                mm = '0' + mm;
            }
            date_from = yyyy + '-' + mm + '-' + dd;

            var domain_list = [['location_id', '=', self.config.stock_location_id[0]], ['date_order', '>', date_from], ['state', 'not in', ['draft', 'cancel']]]
            return domain_list;
        },
        loaded: function (self, wk_order) {
            self.db.pos_all_orders = wk_order;
            self.db.order_by_id = {};
            wk_order.forEach(function (order) {
                self.db.order_by_id[order.id] = order;
            });
        }
    }, {
        model: 'pos.order.line',
        fields: ['product_id', 'order_id', 'qty', 'discount', 'price_unit', 'price_tax', 'price_subtotal_incl', 'price_subtotal'],
        domain: function (self) {
            var order_lines = [];
            var orders = self.db.pos_all_orders;
            for (var i = 0; i < orders.length; i++) {
                order_lines = order_lines.concat(orders[i]['lines']);
            }
            return [
                ['id', 'in', order_lines]
            ];
        },
        loaded: function (self, wk_order_lines) {
            self.db.pos_all_order_lines = wk_order_lines;
            self.db.line_by_id = {};
            wk_order_lines.forEach(function (line) {
                self.db.line_by_id[line.id] = line;
            });
        }
    }], {
        'after': 'product.product'
    });

    models.get_fields = function (model_name) {
        var pos_models = models.PosModel.prototype.models;
        for (var i = 0; i < pos_models.length; i++){
            var model = pos_models[i];
            if (model.model === model_name){
                return model.fields;
            }
        }
        return [];
    };

    models.PosModel = models.PosModel.extend({
        push_and_invoice_order: function (order) {
            var self = this;
            var invoiced = new $.Deferred();
            if (!order.get_client()) {
                invoiced.reject({
                    code: 400,
                    message: 'Missing Customer',
                    data: {}
                });
                return invoiced;
            }
            var order_id = this.db.add_order(order.export_as_JSON());
            this.flush_mutex.exec(function () {
                var done = new $.Deferred();
                var transfer = self._flush_orders([self.db.get_order(order_id)], {
                    timeout: 30000,
                    to_invoice: true
                });
                transfer.fail(function (error) {
                    invoiced.reject(error);
                    done.reject();
                });
                transfer.pipe(function (order_server_id) {
                    self.chrome.do_action('point_of_sale.pos_invoice_report', {
                        additional_context: {
                            //Code chenged for POS All Orders List --START--
                            active_ids: [order_server_id[0].id],
                            //Code chenged for POS All Orders List --END--
                        }
                    });
                    invoiced.resolve();
                    done.resolve();
                });
                return done;
            });
            return invoiced;
        },


        _save_to_server: function (orders, options) {
            var self = this;
            var base = SuperPosModel._save_to_server.call(this, orders, options);
            base.done(function (server_ids) {
                _.each(orders, function (order) {
                    var pos_order_fields = models.get_fields('pos.order');
                    new Model('pos.order').call('search_read', [[['pos_reference', 'like', order.id]], pos_order_fields]).then(function (lst_order) {
                        var order_data = lst_order[0];
                        self.db.pos_all_orders.unshift(order_data);
                        self.db.order_by_id[order_data.id] = order_data;

                        // add statement, order line of order to db
                        new Model('account.bank.statement.line').call('search_read', [[['id', 'in', order_data.statement_ids]]]).then(function (statement_lines) {
                            self.db.all_statements.push(statement_lines);
                            _.each(statement_lines, function (val) {
                                self.db.statement_by_id[val.id] = val;
                            });
                        });
                        new Model('pos.order.line').call('search_read', [[['id', 'in', order_data.lines]]]).then(function (order_lines) {
                            self.db.pos_all_order_lines.push(order_lines);
                            _.each(order_lines, function (val) {
                                self.db.line_by_id[val.id] = val;
                            })
                        });
                    });
                });

            });
            return base;
        }

        // _save_to_server: function (orders, options) {
        //     if (!orders || !orders.length) {
        //         var result = $.Deferred();
        //         result.resolve([]);
        //         return result;
        //     }
        //     options = options || {};
        //     var self = this;
        //     var timeout = typeof options.timeout === 'number' ? options.timeout : 7500 * orders.length;
        //     var posOrderModel = new Model('pos.order');
        //     return posOrderModel.call('create_from_ui',
        //         [_.map(orders, function (order) {
        //             order.to_invoice = options.to_invoice || false;
        //             return order;
        //         })],
        //         undefined,
        //         {
        //             shadow: !options.to_invoice,
        //             timeout: timeout
        //         }
        //     ).then(function (return_dict) {
        //         _.each(orders, function (order) {
        //             self.db.remove_order(order.id);
        //         });
        //         //Code for POS All Orders List --start--
        //         return_dict.orders.forEach(function (order) {
        //             self.db.pos_all_orders.unshift(order);
        //             self.db.order_by_id[order.id] = order;
        //         });
        //         //Code for POS All Orders List --start--
        //         self.set('failed', false);
        //         return return_dict.orders;
        //     }).fail(function (error, event) {
        //         if (error.code === 200) {
        //             if (error.data.exception_type == 'warning') {
        //                 delete error.data.debug;
        //             }
        //             if ((!self.get('failed') || options.show_error) && !options.to_invoice) {
        //                 self.gui.show_popup('error-traceback', {
        //                     'title': error.data.message,
        //                     'body': error.data.debug
        //                 });
        //             }
        //             self.set('failed', error)
        //         }
        //         event.preventDefault();
        //         // console.error('Failed to send orders:', orders);
        //     });
        // }
    });

    var OrdersScreenWidget = screens.ScreenWidget.extend({
        template: 'OrdersScreenWidget',

        init: function (parent, options) {
            this._super(parent, options);
        },
        get_customer: function (customer_id) {
            var self = this;
            if (self.gui)
                return self.gui.get_current_screen_param('customer_id');
            else
                return undefined;
        },
        render_list: function (order, input_txt) {
            var self = this;
            var customer_id = this.get_customer();
            var new_order_data = [];
            if (customer_id != undefined) {
                for (var i = 0; i < order.length; i++) {
                    if (order[i].partner_id[0] == customer_id)
                        new_order_data = new_order_data.concat(order[i]);
                }
                order = new_order_data;
            }
            if (input_txt != undefined && input_txt != '') {
                var new_order_data = [];
                var search_text = input_txt.toLowerCase()
                for (var i = 0; i < order.length; i++) {
                    if (order[i].partner_id == '') {
                        order[i].partner_id = [0, '-'];
                    }
                    if (order[i].customer_phone == false) {
                        order[i].customer_phone = '';
                    }
                    // console.log(order[i].customer_phone);
                    if (((order[i].name.toLowerCase()).indexOf(search_text) != -1) || ((order[i].partner_id[1].toLowerCase()).indexOf(search_text) != -1) || ((order[i].customer_phone.toLowerCase()).indexOf(search_text) != -1)) {
                        new_order_data = new_order_data.concat(order[i]);
                    }
                }
                order = new_order_data;
            }
            var contents = this.$el[0].querySelector('.wk-order-list-contents');
            contents.innerHTML = "";
            var wk_orders = order;
            for (var i = 0, len = Math.min(wk_orders.length, 1000); i < len; i++) {
                var wk_order = wk_orders[i];
                var orderline_html = QWeb.render('WkOrderLine', {
                    widget: this,
                    order: wk_orders[i],
                    customer_id: wk_orders[i].partner_id[0],
                });
                var orderline = document.createElement('tbody');
                orderline.innerHTML = orderline_html;
                orderline = orderline.childNodes[1];
                contents.appendChild(orderline);
            }
        },
        show: function () {
            var self = this;
            this._super();
            var orders = self.pos.db.pos_all_orders;
            this.render_list(orders, undefined);
            this.$('.order_search').keyup(function () {
                self.render_list(orders, this.value);
            });
            this.$('.back').on('click', function () {
                self.gui.show_screen('products');
            });
        },
        close: function () {
            this._super();
            this.$('.wk-order-list-contents').undelegate();
        },
    });
    gui.define_screen({name: 'wk_order', widget: OrdersScreenWidget});
    screens.ProductScreenWidget.include({
        show: function () {
            var self = this;
            this._super();
            this.product_categories_widget.reset_category();
            this.numpad.state.reset();
            // All orders button was moved to left menu
            // $('#all_orders').on('click',function(){
            // 	self.gui.show_screen('wk_order',{});
            // });
        },
    });
    screens.ClientListScreenWidget.include({
        show: function () {
            var self = this;
            self._super();
            $('.view_all_order').on('click', function () {
                self.gui.show_screen('wk_order', {
                    'customer_id': this.id
                });
            });
        }
    });


    MageBaseWidget.PromotionLeftMenu.include({
        renderElement: function () {
            var self = this;
            this._super();
            this.$('.prom-history').on('click', function () {
                self.gui.show_screen('wk_order', {});
            });
        }
    });

    return OrdersScreenWidget;
});