# -*- coding: utf-8 -*-

{
    'name': 'Magestore Warranty',
    'version': '1.0.0',
    'category': 'Tools',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Feature Warranty',
    'description': """""",
    'depends': [
        'base', 'sale', 'point_of_sale', 'account',
    ],
    'data': [
        'views/sale_warranty_views.xml',
        'data/print_warranty.xml',
        'report/report_warranty_receipt_print.xml',
        'security/warranty_security.xml',
        'security/ir.model.access.csv'
    ],
    'sequence': 1,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
