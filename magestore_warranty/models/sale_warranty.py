# -*- coding: utf-8 -*-
from odoo import api, models, fields, _, exceptions
from odoo.osv import osv
from odoo.exceptions import UserError


class SaleWarranty(models.Model):
    _name = "sale.warranty"

    name = fields.Char()
    sale_bill_type = fields.Selection([('pos', 'POS'), ('sale', 'Sale')], default="sale")
    sale_bill_code_pos = fields.Many2one('pos.order', domain="[('state', 'in', ['paid', 'done', 'invoiced'])]")
    sale_bill_code_sale = fields.Many2one('sale.order', domain="[('state', 'not in', ['draft', 'sent', 'cancel'])]")
    warranty_receive_loc = fields.Many2one('pos.config', required=True, string='Location')
    sale_warranty_line = fields.One2many('warranty.sale.order.line', 'sale_warranty_id')
    pos_warranty_line = fields.One2many('warranty.pos.order.line', 'pos_warranty_id')
    customer_ids = fields.Many2one("res.partner", string="Customer ID",
                                   compute="_get_customer_id")  # --> added by Adams
    currency_id = fields.Many2one('res.currency', 'Currency', required=True,
                                  default=lambda self: self.env.user.company_id.currency_id.id)
    company_id = fields.Char(default="", compute="_compute_company_id")

    customer_id = fields.Many2one("res.partner", compute="_get_customer_id", store=True)
    sale_date = fields.Char(default="", compute="_compute_sale_date_id")  # --> added by Adams
    warranty_day = fields.Date()
    warranty_order_day = fields.Date()
    warranty_delivered_day = fields.Date()
    product_all_code = fields.Char(default="")
    feedback_reason = fields.Text(string="Phản hồi của khách hàng")
    state = fields.Selection([
        ('draft', 'Bản thảo'),
        ('received', 'Đang bảo hành'),
        ('completed', 'Hoàn thành'),
        ('returned', 'Đã trả bảo hành'),
        ('cancelled', 'Hủy'),
        ('feedback', 'Đã phản hồi'),
    ], default="draft")
    total_warranty = fields.Integer(default=0)

    # Added by Mars
    phone_number = fields.Char('Số điện thoại')
    customer_name = fields.Char('Tên khách hàng', related='customer_id.name', readonly=True, store=True)

    @api.onchange("sale_bill_code_pos", "sale_bill_code_sale")
    def _get_customer_id(self):
        for item in self:
            if item.sale_bill_type == 'pos':
                tmp = self.env['pos.order'].sudo().browse(item.sale_bill_code_pos.id)
                if tmp.partner_id.id:
                    partner_id = tmp.partner_id.id
                    item.customer_ids = partner_id
                    item.customer_id = partner_id
                    list_phone = []
                    for phone in [tmp.partner_id.phone, tmp.partner_id.mobile]:
                        if phone:
                            list_phone.append(phone)
                    item.phone_number = ' , '.join(list_phone)

            if item.sale_bill_type == 'sale':
                tmp = self.env['sale.order'].sudo().browse(item.sale_bill_code_sale.id)
                item.currency_ids = self.env['sale.order'].sudo().search([
                    ('id', '=', item.sale_bill_code_sale.id)], limit=1).currency_id
                if tmp.partner_id.id:
                    partner_id = tmp.partner_id.id
                    item.customer_ids = partner_id
                    item.customer_id = partner_id
                    list_phone = []
                    for phone in [tmp.partner_id.phone, tmp.partner_id.mobile]:
                        if phone:
                            list_phone.append(phone)
                    item.phone_number = ' , '.join(list_phone)

    @api.multi
    def _prepare_invoice(self):
        self.ensure_one()
        journal_id = self.env['account.invoice'].default_get(['journal_id'])['journal_id']
        if not journal_id:
            raise UserError(_('Please define an accounting sale journal for this company.'))
        invoice_vals = {
            'partner_id': self.partner_invoice_id.id,
        }
        return invoice_vals

    @api.multi
    def receive_warranty(self):
        # Create new invoice
        # vals.update({
        #     'partner_id': self.customer_id
        # })
        customer_inv = self.env['account.invoice'].search([])
        max_customer_inv = len(customer_inv)

        self.state = 'received'
        print("Receive")

    @api.multi
    def completed_warranty(self):
        self.state = 'completed'
        print("Complete")

    @api.multi
    def returned_warranty(self):
        self.state = 'returned'

    @api.multi
    def cancelled_warranty(self):
        self.state = "draft"

    @api.multi
    def feedback_warranty(self):
        self.state = "feedback"

    @api.onchange('sale_bill_code_sale')
    def _compute_sale_warranty_line(self):
        if self.sale_bill_code_sale:
            list_sale_order_line_vals = []
            self.product_all_code = ""
            sale_order_line = self.env['sale.order.line'].sudo().search(
                [('order_id', '=', self.sale_bill_code_sale.id)])
            for item_sale_order_line in sale_order_line:
                if item_sale_order_line.product_uom_qty > 0:
                    sale_order_line_vals = {}
                    searched_product = self.env['product.product'].sudo().search(
                        [('id', '=', item_sale_order_line.product_id.id)])
                    self.product_all_code = ""
                    if not searched_product.default_code:
                        self.product_all_code = self.product_all_code + ""
                    else:
                        self.product_all_code = self.product_all_code + searched_product.default_code
                    sale_order_line_vals.update({
                        'product_code': searched_product.default_code,
                        'product_name': searched_product.name,
                        'product_qty': item_sale_order_line.product_uom_qty,
                    })
                    list_sale_order_line_vals.append(sale_order_line_vals)

            self.sale_warranty_line = list_sale_order_line_vals

    @api.onchange('sale_bill_code_pos')
    def _compute_pos_warranty_line(self):
        if self.sale_bill_code_pos:
            list_pos_order_line_vals = []
            pos_order_line = self.env['pos.order.line'].search([('order_id', '=', self.sale_bill_code_pos.id)])
            for item_pos_order_line in pos_order_line:
                if item_pos_order_line.qty > 0:
                    pos_order_line_vals = {}
                    searched_product = self.env['product.product'].sudo().search(
                        [('id', '=', item_pos_order_line.product_id.id)])
                    self.product_all_code = ""
                    if not searched_product.default_code:
                        self.product_all_code = self.product_all_code + ""
                    else:
                        self.product_all_code = self.product_all_code + searched_product.default_code
                    pos_order_line_vals.update({
                        'product_code': searched_product.default_code,
                        'product_name': searched_product.name,
                        'product_qty': item_pos_order_line.qty,
                    })
                    list_pos_order_line_vals.append(pos_order_line_vals)

            self.pos_warranty_line = list_pos_order_line_vals

    @api.onchange("sale_bill_code_pos", "sale_bill_code_sale", "sale_bill_type")
    def _compute_company_id(self):
        for item in self:
            if item.sale_bill_type == 'pos':
                item.company_id = self.env['pos.order'].search([('id', '=', item.sale_bill_code_pos.id)],
                                                               limit=1).location_id.name
            if item.sale_bill_type == 'sale':
                item.company_id = self.env['sale.order'].search([('id', '=', item.sale_bill_code_sale.id)],
                                                                limit=1).warehouse_id.name

    @api.onchange("sale_bill_code_pos", "sale_bill_code_sale", "sale_bill_type")
    def _compute_sale_date_id(self):
        for item in self:
            if item.sale_bill_type == 'pos':
                item.sale_date = self.env['pos.order'].search([('id', '=', item.sale_bill_code_pos.id)],
                                                              limit=1).date_order
            if item.sale_bill_type == 'sale':
                item.sale_date = self.env['sale.order'].search([('id', '=', item.sale_bill_code_sale.id)],
                                                               limit=1).date_order

    @api.model
    def create(self, vals):
        res = super(SaleWarranty, self).create(vals)

        # Recalculate bill name
        _prefix = "BH"

        res.name = ""
        res.name += _prefix

        _bill_num = res.id

        if _bill_num < 10:
            for i in range(5):
                res.name += "0"
            res.name += str(_bill_num)
        elif _bill_num < 100:
            for i in range(4):
                res.name += "0"
            res.name += str(_bill_num)
        elif _bill_num < 1000:
            for i in range(3):
                res.name += "0"
            res.name += str(_bill_num)
        elif _bill_num < 10000:
            for i in range(2):
                res.name += "0"
            res.name += str(_bill_num)
        else:
            res.name += str(_bill_num)

        if vals.get('sale_bill_code_sale'):
            sum_fee = 0
            sale_warranty_line = vals.get('sale_warranty_line')
            for item_sale_warranty_line in sale_warranty_line:
                for rec in item_sale_warranty_line:
                    if rec != 0 or rec:
                        if rec.get('warranty_qty') > rec.get('product_qty'):
                            raise exceptions.ValidationError(_("Warranty qty is larger than product qty"))
                        else:
                            sum_fee += rec.get('warranty_fee')
                            rec.update({
                                'sale_warranty_id': res.id,
                            })
            res.total_warranty = sum_fee
            return res

        if vals.get('sale_bill_code_pos'):
            sum_fee = 0
            pos_warranty_line = vals.get('pos_warranty_line')
            for item_pos_warranty_line in pos_warranty_line:
                for rec in item_pos_warranty_line:
                    if rec != 0 or rec:
                        if rec.get('warranty_qty') > rec.get('product_qty'):
                            raise exceptions.ValidationError(_("Warranty qty is larger than product qty"))
                        else:
                            sum_fee += rec.get('warranty_fee')
                            rec.update({
                                'pos_warranty_id': res.id,
                            })
            res.total_warranty = sum_fee
            return res

    @api.multi
    def write(self, vals):
        flag = 0

        for item in self:
            if item.sale_bill_code_sale:
                sale_warranty_line = item.sale_warranty_line
                for item_sale_warranty_line in sale_warranty_line:
                    if item_sale_warranty_line.warranty_qty > item_sale_warranty_line.product_qty:
                        raise exceptions.ValidationError(_("Warranty qty is larger than product qty"))
                    if item_sale_warranty_line != 0 and item_sale_warranty_line:
                        item_sale_warranty_line.sale_warranty_id = item.id

            if item.sale_bill_code_pos:
                pos_warranty_line = item.pos_warranty_line
                for item_pos_warranty_line in pos_warranty_line:
                    if item_pos_warranty_line.warranty_qty > item_pos_warranty_line.product_qty:
                        raise exceptions.ValidationError(_("Warranty qty is larger than product qty"))
                    if item_pos_warranty_line != 0 and item_pos_warranty_line:
                        item_pos_warranty_line.pos_warranty_id = item.id
        if flag == 1:
            raise exceptions.ValidationError(_("Warranty qty is larger than product qty"))
        else:
            return super(SaleWarranty, self).write(vals)

    @api.multi
    def unlink(self):
        for warranty in self:
            if not warranty.state in ['cancelled', 'draft']:
                raise UserError(_('In order to delete a warranty order, you must cancel it first.'))
        return super(SaleWarranty, self).unlink()

    @api.multi
    def action_view_invoice(self):
        action = self.env.ref('account.action_invoice_tree1')
        result = action.read()[0]

        return result


class SaleWarrantyLine(models.Model):
    _name = "warranty.sale.order.line"

    sale_warranty_id = fields.Many2one('sale.warranty')

    product_name = fields.Char()
    product_qty = fields.Integer()
    product_code = fields.Char()

    warranty_fee = fields.Float(default=0.0)
    warranty_note = fields.Char(required=True, string="Lý do bảo hành")
    warranty_qty = fields.Integer('Warranty Qty')


class PosWarrantyLine(models.Model):
    _name = "warranty.pos.order.line"

    pos_warranty_id = fields.Many2one('sale.warranty')

    product_code = fields.Char()
    product_name = fields.Char()
    product_qty = fields.Integer()

    warranty_fee = fields.Float(default=0.0)
    warranty_note = fields.Char(required=True, string="Lý do bảo hành")
    warranty_qty = fields.Integer()
