from odoo import models, fields, api


class MageInstance(models.Model):
    _inherit = "mage.instance"

    product_fee_shipment = fields.Many2one('product.product', string="Fee Shipment Default", required=True)
    product_discount = fields.Many2one('product.product', string="Discount Default", required=True)
    sale_person = fields.Many2one('res.users', string="Salesperson", required=True)
    sale_team = fields.Many2one('crm.team', string="Sales Team", required=True)
    sale_company_id = fields.Many2one('res.company', string='Sale Company', required=True)