from odoo import api, fields, models, _


class MageSaleOrder(models.Model):
    _inherit = "sale.order"

    instance_id = fields.Many2one('mage.instance', string='Instance')
    source_document = fields.Char(string="Source Document")

    @api.depends('team_id', 'instance_id')
    def _apply_require_attribute(self):
        name_team = None
        if self.team_id:
            name_team = self.team_id.name.lower()
        if name_team and ('online' in name_team or 'website' in name_team) and not self.instance_id:
            self.apply_require_carrier_supplier = True
        else:
            self.apply_require_carrier_supplier = False
