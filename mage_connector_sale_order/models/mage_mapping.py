from odoo import models, fields, api, tools
import json

import logging
_logger = logging.getLogger(__name__)


class MageMappingSaleOrder(models.Model):
    _name = "mage.mapping.sale.order"

    odoo_sale_order_id = fields.Many2one('sale.order', string="Odoo Sale Order")
    mage_sale_order_id = fields.Integer(string="Magento Sale Order")

    @api.model
    def create_sale_order(self, order_data):
        try:
            context = self._context
            customer_id = 0
            order_id = 0
            if context.get('instance_type') == 'magento':
                vals = json.loads(order_data)
                customer_info = vals.get('customer_info')
                email = customer_info.get('customer_email', False)
                if email:
                    customer_obj = self.env['res.partner'].search([('email', '=', email)], limit=1)
                    if not customer_obj:
                        if not customer_info.get('customer_id') and customer_info.get('customer_name'):
                            odoo_country_id = False
                            res_country_obj = self.env['res.country'].search(
                                [('code', '=', customer_info.get('customer_country_id', False))])
                            if res_country_obj:
                                odoo_country_id = res_country_obj.id

                            state_id = False
                            mage_city = customer_info.get('customer_city', False)
                            if mage_city:
                                res_country_state_obj = self.env['res.country.state'].search([
                                    ('name', '=', mage_city),
                                    ('country_id', '=', odoo_country_id)
                                ])
                                if res_country_state_obj:
                                    state_id = res_country_state_obj.id

                            customer_new_obj = self.env['res.partner'].create({
                                'name': customer_info.get('customer_name'),
                                'email': email,
                                'source': 'Magento',
                                'phone': customer_info.get('customer_telephone', False),
                                'street': customer_info.get('customer_street', False),
                                'city': mage_city,
                                'state_id': state_id,
                                'country_id': odoo_country_id,
                                'customer': True
                            })
                            customer_id = customer_new_obj.id
                    else:
                        customer_id = customer_obj.id

                    if customer_id:
                        customer = self.env['res.partner'].browse(customer_id)
                        if not customer.country_id:
                            #get default country_id of Vietnam
                            default_country_id = self.env['res.country'].search([('code', '=', 'VN')], limit=1)
                            odoo_country_id = default_country_id.id
                            customer.write({'country_id': odoo_country_id})

                        order_line = vals.get('order_items')
                        instance_id = None
                        user_id = None
                        team_id = None
                        warehouse_id = None
                        company_id = None
                        instance_url = context.get('instance_url')
                        if instance_url:
                            instance_obj = self.env['mage.instance'].get_instance_by_url(instance_url)
                            if instance_obj:
                                instance_id = instance_obj.id
                                if instance_obj.sale_person:
                                    user_id = instance_obj.sale_person.id
                                if instance_obj.sale_team:
                                    team_id = instance_obj.sale_team.id
                                if instance_obj.stock_location:
                                    warehouse_id = instance_obj.stock_location.id
                                if instance_obj.sale_company_id:
                                    company_id = instance_obj.sale_company_id.id
                                # shipment_amount = vals.get('shipment_amount')
                                # if shipment_amount > 0:
                                #     product_fee_shipment = instance_obj.product_fee_shipment
                                #     if product_fee_shipment:
                                #         order_line.append([0, False, {
                                #             'product_id': product_fee_shipment.id,
                                #             'product_uom_qty': 1,
                                #             'price_unit': shipment_amount,
                                #             'product_uom': 1
                                #         }])
                                discount_amount = vals.get('discount_amount')
                                if discount_amount and float(discount_amount) != 0:
                                    product_discount = instance_obj.product_discount
                                    if product_discount:
                                        order_line.append([0, False, {
                                            'product_id': product_discount.id,
                                            'product_uom_qty': 1,
                                            'price_unit': abs(float(discount_amount)) * -1,
                                            'product_uom': 1
                                        }])
                                onestepcheckout_discount_amount = vals.get('onestepcheckout_discount_amount')
                                if onestepcheckout_discount_amount and float(onestepcheckout_discount_amount) != 0:
                                    product_onestepcheckout_discount = instance_obj.product_discount
                                    if product_onestepcheckout_discount:
                                        order_line.append([0, False, {
                                            'product_id': product_onestepcheckout_discount.id,
                                            'product_uom_qty': 1,
                                            'price_unit': abs(float(onestepcheckout_discount_amount)) * -1,
                                            'product_uom': 1
                                        }])

                        sale_order_data = {
                            'partner_id': customer_id,
                            'order_line': order_line,
                            'instance_id': instance_id,
                            'user_id': user_id,
                            'team_id': team_id,
                            'warehouse_id': warehouse_id,
                            'company_id': company_id,
                            'note': vals.get('note', False),
                            'delivery_type': 'cod' if vals.get('delivery_type', False) == 'freeshipping_freeshipping' else 'store',
                            'source_document': vals.get('order_increment_id', False)
                        }
                        sale_order_obj = self.env['sale.order'].create(sale_order_data)
                        if sale_order_obj:
                            order_id = sale_order_obj.id
                            mage_order_id = vals.get('order_id')
                            self.env['mage.mapping.sale.order'].create({
                                'odoo_sale_order_id': sale_order_obj.id,
                                'mage_sale_order_id': mage_order_id,
                                'status': 'done'
                            })
            return order_id
        except Exception as ex:
            _logger.exception('Sale Order - Error when create sale order from Magento: %s' % tools.ustr(ex))
            return 0


class MageMappingInvoice(models.Model):
    _name = "mage.mapping.invoice"

    odoo_sale_order_id = fields.Many2one('sale.order', string="Odoo ID")
    mage_sale_order_id = fields.Integer(string="Magento ID")
