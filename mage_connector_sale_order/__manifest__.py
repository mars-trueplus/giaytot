# -*- coding: utf-8 -*-

{
    'name': 'Magento Connector Sale Order',
    'version': '1.0.0',
    'category': 'Connector Modules',
    'sequence': 1,
    'author': 'Magestore',
    'website': 'http://www.magestore.com',
    'summary': 'Magento Connector Sale Order',
    'description': """""",
    'depends': [
        'base',
        'mage_connector_base',
        'magestore_sale'
    ],
    'data': [
        # 'data/sale_order_cron.xml',
        'security/ir.model.access.csv',
        'views/mage_mapping_view.xml',
        'views/sale_order_view.xml',
        'views/mage_instance_view.xml'
    ],
    'sequence': 2,
    'application': True,
    'installable': True,
    'auto_install': False,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
