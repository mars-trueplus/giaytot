# -*- coding: utf-8 -*-
from odoo import api, fields, models


class PosPromotionConfiguration(models.TransientModel):

    _name = 'pos.promotion.config.settings'
    _inherit = 'res.config.settings'

    apply_pos_promotion_program = fields.Selection([
        (1, "Single"),
        (3, "Multi")
    ], string="POS Promotion Programs For Each Product",
        help='')
    allow_second_time_sellection = fields.Selection([
        (1, "Có"),
        (2, "Không")
    ], string="Tự động áp dụng khuyến mãi",
        help='')

    # @api.model
    # def get_default_company_share_partner(self, fields):
    #     return {
    #         'company_share_partner': not self.env.ref('base.res_partner_rule').active
    #     }
    #
    # @api.multi
    # def set_default_company_share_partner(self):
    #     partner_rule = self.env.ref('base.res_partner_rule')
    #     for config in self:
    #         partner_rule.write({'active': not config.company_share_partner})

    @api.multi
    def set_apply_pos_promotion_program(self):
        Values = self.env['ir.values'].sudo()
        for config in self:
            # print config.apply_pos_promotion_program
            Values.set_default('pos.promotion.config.settings', 'apply_pos_promotion_program', config.apply_pos_promotion_program)

    @api.model
    def get_default_apply_pos_promotion_program(self, fields):
        ir_values = self.env['ir.values'].sudo().get_default('pos.promotion.config.settings', 'apply_pos_promotion_program')
        return {
            'apply_pos_promotion_program': 1 if ir_values == 1 else 3,
        }

    @api.model
    def get_apply_pos_promotion_program(self):
        ir_values = self.env['ir.values'].sudo().get_default('pos.promotion.config.settings', 'apply_pos_promotion_program')
        if ir_values:
            return ir_values
        return 1

    @api.multi
    def set_allow_second_time_sellection(self):
        Values = self.env['ir.values'].sudo()
        for config in self:
            # print config.apply_pos_promotion_program
            Values.set_default('pos.promotion.config.settings', 'allow_second_time_sellection',
                               config.allow_second_time_sellection)

    @api.model
    def get_default_allow_second_time_sellection(self, fields):
        ir_values = self.env['ir.values'].sudo().get_default('pos.promotion.config.settings', 'allow_second_time_sellection')
        return {
            'allow_second_time_sellection': 1 if ir_values == 1 else 2,
        }

    @api.model
    def get_allow_second_time_sellection(self):
        ir_values = self.env['ir.values'].sudo().get_default('pos.promotion.config.settings', 'allow_second_time_sellection')
        if ir_values:
            return ir_values
        return 1

