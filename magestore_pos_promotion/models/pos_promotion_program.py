# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
from odoo.exceptions import UserError


class PosPromotionProgram(models.Model):
    _name = 'pos.promotion.program'

    @api.model
    def get_type_selection(self):
        promotion_types = self.env['pos.promotion.type'].search([('status', '=', 'active')])
        return [(promotion_type.code, promotion_type.name) for promotion_type in promotion_types]

    # def _get_default_location(self):
    #     return self.env['stock.warehouse'].search([('company_id', '=', self.env.user.company_id.id)], limit=1).lot_stock_id

    name = fields.Char(string='Name', required=True,
                       states={'active': [('readonly', True)]})
    description = fields.Text(string='Description', states={'active': [('readonly', True)]})
    type = fields.Selection(get_type_selection, 'Type', required=True, states={'active': [('readonly', True)]})
    sequence = fields.Integer(string='Sequence', required=True, states={'active': [('readonly', True)]})
    state = fields.Selection([('draft', 'Bản thảo'), ('active', 'Kích hoạt'), ('cancel', 'Hủy')], default='draft', states={'active': [('readonly', True)]})
    start_date = fields.Date(string='Start Date', states={'active': [('readonly', True)]})
    end_date = fields.Date(string='End Date', states={'active': [('readonly', True)]})
    day_apply = fields.Many2many('pos.promotion.day', states={'active': [('readonly', True)]}, string="Ngày được áp dụng")
    # stock_location_id = fields.Many2many('stock.location', string='Stock Location', required=True, domain=[('usage', '=', 'internal')], default=_get_default_location, states={'active': [('readonly', True)]})
    stock_location_id = fields.Many2many('stock.location', string='Stock Location', required=True, domain=[('usage', '=', 'internal')], states={'active': [('readonly', True)]})
    discount_product_id = fields.Many2one('product.product', string='Discount Product',
                                          domain="[('available_in_pos', '=', True)]",
                                          help='The product used to model the discount',
                                          states={'active': [('readonly', True)]})
    coupon_code = fields.Boolean(string="Áp dụng mã khuyến mãi", default=False, states={'active': [('readonly', True)]})
    code = fields.Char(string="Mã khuyến mãi", states={'active': [('readonly', True)]})
    quantity = fields.Integer(string="Phát hành", states={'active': [('readonly', True)]})
    used = fields.Integer(string="Đã sử dụng", readonly=True)
    left = fields.Integer(string="Còn lại", readonly=True, compute="_compute_code_left")
    code_per_customer = fields.Integer(string="Áp dụng cho mỗi khách hàng", states={'active': [('readonly', True)]})

    _sql_constraints = [
        ('sequence_uniq', 'unique (sequence)', "Sequence đã tồn tại !"),
        ('code_uniq', 'unique (code)', "Code đã tồn tại !"),
        ('sequence_gt_zero', 'CHECK (sequence>0)', 'Sequence phải lớn hơn 0 !'),
    ]

    @api.multi
    def action_draft(self):
        self.state = 'draft'

    @api.multi
    def action_active(self):
        self.state = 'active'

    @api.multi
    def action_cancel(self):
        self.state = 'cancel'

    @api.depends('used','quantity')
    def _compute_code_left(self):
        for program in self:
            if program.quantity > 0:
                program.left = program.quantity - program.used

    @api.model
    def get_coupon_code_left(self, code, customer):
        # print 'get_coupon_code_left'
        # print code
        # print customer
        programs = self.search([('coupon_code', '=', True), ('code', '=', code), ('state', '=', 'active')])
        if len(programs) > 1:
            return -1
        if len(programs) == 1:
            if programs.quantity == 0:
                if customer != -1:
                    used_code_customer = self.env['pos.promotion.coupon.code'].search(
                        [('partner_id', '=', customer), ('pos_promotion_program_id', '=', programs.id)])
                    if len(used_code_customer) > 0:
                        if used_code_customer.allow_use_coupon_code < programs.code_per_customer:
                            return 'unlimitted'
                        else:
                            return -1
                    else:
                        return 'unlimitted'
                return 'unlimitted'
            elif customer != -1:
                used_code_customer = self.env['pos.promotion.coupon.code'].search([('partner_id', '=', customer), ('pos_promotion_program_id', '=', programs.id)])
                if len(used_code_customer) > 0:
                    if used_code_customer.allow_use_coupon_code < programs.code_per_customer:
                        return programs.left
                    else:
                        return -1
                else:
                    return programs.left
            else:
                return programs.left
        if len(programs) == 0:
            return -1

    @api.model
    def set_coupon_code_left_1(self, id):
        # if self.browse(id).quantity != 0:
        self.browse(id).used += 1

    @api.model
    def set_coupon_code_left_after_change_order(self, id):
        if self.browse(id).quantity != 0:
            self.browse(id).used -= 1

    # @api.model
    # def create(self, vals):
    #     if vals.get('coupon_code'):
    #         partners = self.env['res.partner'].search([]).ids
    #         self.env['pos.promotion.coupon.code'].create({
    #             'partner_id': partners,
    #             'pos_promotion_program_id': self.id,
    #             'allow_use_coupon_code': 0
    #         })
    #
    #     return super(PosPromotionProgram, self).create(vals)

    # @api.multi
    # def write(self, vals):
    #     if vals.get('code_per_customer'):
    #         self.env['pos.promotion.coupon.code'].search([('pos_promotion_program_id', '=', self.id)]).allow_use_coupon_code = vals.get('code_per_customer')
    #     return super(PosPromotionProgram, self).write(vals)

class Partner(models.Model):
    _name = "pos.promotion.coupon.code"

    partner_id = fields.Integer(string='Partner')
    pos_promotion_program_id = fields.Integer(string='POS Promotion')
    allow_use_coupon_code = fields.Integer(string='Number')

    @api.model
    def check_customer_with_coupon_code(self, partner, program):
        # print 'check_customer_with_coupon_code'
        # print partner
        # print program
        used_code = self.search([('partner_id', '=', partner),('pos_promotion_program_id', '=', program)])
        # print used_code
        program_code_allow = self.env['pos.promotion.program'].search([('coupon_code', '=', True), ('id', '=', program), ('state', '=', 'active')])
        if len(used_code) > 0:
            if program_code_allow.code_per_customer != 0:
                if used_code.allow_use_coupon_code < program_code_allow.code_per_customer:
                    self.search(
                        [('partner_id', '=', partner),
                         ('pos_promotion_program_id', '=', program)]).allow_use_coupon_code += 1
                    return 1
                else:
                    return -1
            if program_code_allow.code_per_customer == 0:
                return 1
        if len(used_code) == 0:
            if program_code_allow.code_per_customer != 0:
                self.create({
                    'partner_id': partner,
                    'pos_promotion_program_id': program,
                    'allow_use_coupon_code': 1
                })
                return 1
            if program_code_allow.code_per_customer == 0:
                return 1

    # @api.model
    # def set_used_coupon_code(self, partner, program):
    #     self.search([('partner_id', '=', partner), ('pos_promotion_program_id', '=', program)]).allow_use_coupon_code += 1

