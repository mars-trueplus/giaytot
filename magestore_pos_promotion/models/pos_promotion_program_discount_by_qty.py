# -*- coding: utf-8 -*-
from odoo import models, fields, api


# Discount by quantity
class IrPosPromotionDiscountByQty(models.Model):
    _name = 'ir.pos.promotion.discount.by.qty'
    _order = 'total_qty asc'

    promotion_program_id = fields.Many2one('pos.promotion.program', string='Promotion Program', ondelete='cascade', index=True)
    total_qty = fields.Float(string='Quantity ', required=True, default=2)
    value = fields.Float(string='Discount (%)', required=True)


class PosPromotionProgram(models.Model):
    _inherit = 'pos.promotion.program'

    pos_promotion_discount_by_qty_access = fields.One2many('ir.pos.promotion.discount.by.qty',
                                                             'promotion_program_id',
                                                             string='Discount by Quantity',
                                                             copy=True, states={'active': [('readonly', True)]})
    # discount_by_qty_product_id = fields.Many2one('product.product', string='Discount Product',
    #                                       domain="[('available_in_pos', '=', True)]",
    #                                       help='The product used to model the discount',
    #                                       states={'active': [('readonly', True)]})
