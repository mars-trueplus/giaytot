# -*- coding: utf-8 -*-
from odoo import models, fields, api


#Discount on total
class IrPosPromotionDiscountOnTotal(models.Model):
    _name = 'ir.pos.promotion.discount.on.total'
    _order = 'total_order asc'

    promotion_program_id = fields.Many2one('pos.promotion.program', string='Promotion Program', ondelete='cascade', index=True)
    total_order = fields.Float(string='Total order amount', required=True, default=1)
    type = fields.Selection([('percent', 'Percentage'), ('fixed', 'Fixed')], string='Type (Percentage| Fixed Amount)', required=True, default='percent')
    value = fields.Float(string='Value', required=True, default=1)


class PosPromotionProgram(models.Model):
    _inherit = 'pos.promotion.program'

    pos_promotion_discount_on_total_access = fields.One2many('ir.pos.promotion.discount.on.total', 'promotion_program_id',
                                                           string='Discount On Total',
                                                           copy=True, states={'active': [('readonly', True)]})

# class PosOrder(models.Model):
#     _inherit = "pos.order"
#
#     amount_promotion_discount = fields.Float(string="Promotion Discount", readonly=True, digits=0)

    # @api.model
    # def _order_fields(self, ui_order):
    #     fields = super(PosOrder, self)._order_fields(ui_order)
    #     fields['amount_promotion_discount'] = ui_order.get('amount_promotion_discount', 0)
    #     return fields
    #
    # @api.depends('statement_ids', 'lines.price_subtotal_incl', 'lines.discount')
    # def _compute_amount_all(self):
    #     for order in self:
    #         order.amount_paid = order.amount_return = order.amount_tax = 0.0
    #         currency = order.pricelist_id.currency_id
    #         order.amount_paid = sum(payment.amount for payment in order.statement_ids)
    #         order.amount_return = sum(payment.amount < 0 and payment.amount or 0 for payment in order.statement_ids)
    #         order.amount_tax = currency.round(
    #             sum(self._amount_line_tax(line, order.fiscal_position_id) for line in order.lines))
    #         amount_untaxed = currency.round(sum(line.price_subtotal for line in order.lines))
    #         order.amount_total = order.amount_tax + amount_untaxed - order.amount_promotion_discount
