# -*- coding: utf-8 -*-

from . import pos_promotion_type
from . import pos_promotion_program
from . import pos_promotion_program_discount
from . import pos_promotion_day
from . import pos_promotion_program_bxpy
from . import pos_promotion_program_bxgy
from . import pos_promotion_program_special_price
from . import pos_promotion_program_discount_on_cat
from . import pos_promotion_program_give_product
from . import pos_promotion_program_discount_on_total
from . import pos_promotion_program_discount_by_qty
from . import res_config

