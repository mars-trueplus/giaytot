odoo.define('pos_promotion.popup', function (require) {
    "use strict";

    var posbasewidget = require('point_of_sale.BaseWidget');
    var gui = require('point_of_sale.gui');
    var basepopup = require('point_of_sale.popups');
    var screens = require('point_of_sale.screens');
    var core = require('web.core');
    var pos_promotion = require('pos_promotion.pos_promotion');
    var Model = require('web.DataModel');

    var QWeb = core.qweb;

    var FillInCouponCodePopupWidget = basepopup.extend({
        template: 'FillInCouponCodePopupWidget',
        events: _.extend({}, basepopup.prototype.events, {
            'click .validate-coupon-code': 'click_validate_coupon_code'
        }),
        renderElement: function(){
            this._super();

            this.el.querySelector('.popup-coupon-code .popup-coupon-code-body input').addEventListener('keypress',this.fill_barcode_handler);
            this.el.querySelector('.popup-coupon-code .popup-coupon-code-body input').addEventListener('keydown',this.fill_barcode_handler);

            if (this.pos.get_order().coupon_code_string){
                this.el.querySelector('.popup-coupon-code .popup-coupon-code-body input').value = this.pos.get_order().coupon_code_string;
                this.fill_barcode_handler();
            }

        },
        show: function (options) {
            options = options || {};
            var self = this;
            this._super(options);
            this.code = '';
            this.check = 0;

            // if(!this.pos.get_order().is_used_coupon_code && this.pos.get_order().program_used_by_coupon_code != -1){
            //     new Model('pos.promotion.program').call('set_coupon_code_left_after_change_order',[this.pos.get_order().program_used_by_coupon_code]).then(function () {
            //         self.pos.get_order().program_used_by_coupon_code = -1;
            //     });
            // }

            var search_timeout  = null;
            this.fill_barcode_handler = function(event){
                if (typeof (coupon) === 'undefined' ){
                    clearTimeout(search_timeout);

                    search_timeout = setTimeout(function(){
                        self.perform_check_coupon_code(self.el.querySelector('.popup-coupon-code .popup-coupon-code-body input').value);
                    },70);
                }
                else if(event.type == "keypress" || event.keyCode === 46 || event.keyCode === 8){
                    clearTimeout(search_timeout);

                    var codebox = this;

                    search_timeout = setTimeout(function(){
                        self.perform_check_coupon_code(codebox.value);
                    },70);
                }
            };

            this.renderElement();
        },
        perform_check_coupon_code: function (codebox) {
            var self = this;
            var checkExist = false;
            var couponCodeModel = new Model('pos.promotion.program');
            var customer = this.pos.get_order().get_client() ? this.pos.get_order().get_client().id : -1;
            couponCodeModel.call('get_coupon_code_left',[codebox,customer]).then(function (return_value) {
                if(return_value > 0 || return_value == 'unlimitted'){
                    for(var i = 0 ; i < self.pos.coupon_code.length ; i++){
                        if(self.pos.coupon_code[i].code == codebox){
                            checkExist = true;
                            break;
                        }
                    }
                    if(checkExist){
                        self.el.querySelector('.coupon-code-message-content').textContent = '';
                        self.check = 1;
                        self.code = codebox;
                    }
                    else{
                        self.check = 0;
                        self.code = self.el.querySelector('.popup-coupon-code .popup-coupon-code-body input').value;
                        self.el.querySelector('.coupon-code-message-content').textContent = 'Mã khuyến mại không hợp lệ';
                    }
                }
                if(return_value <= 0){
                    self.check = 0;
                    self.code = self.el.querySelector('.popup-coupon-code .popup-coupon-code-body input').value;
                    self.el.querySelector('.coupon-code-message-content').textContent = 'Mã khuyến mại không hợp lệ';
                }
            });
        },
        click_validate_coupon_code: function () {
            var self = this;
            // console.log('click_validate_coupon_code');
            // console.log(self.check);
            var coupon_code_promotion = new pos_promotion.PosPromotionButton(this,{});
            if((self.check > 0 && self.pos.get_order().coupon_code_string == '') || (self.check > 0 && self.pos.get_order().coupon_code_string == this.code)){
                console.log('1');
                console.log(self.pos.get_order().coupon_code_string);
                console.log(self.code);
                self.pos.get_order().coupon_code_string = self.code;
                coupon_code_promotion.button_click(this.code,true,false);
            }else if(self.check == 0 && self.pos.get_order().coupon_code_string && this.code == ''){
                console.log('2');
                console.log(self.pos.get_order().coupon_code_string);
                console.log(self.code);
                // self.pos.get_order().coupon_code_string = this.code;
                coupon_code_promotion.button_click(self.pos.get_order().coupon_code_string,true,true);
                self.pos.get_order().coupon_code_string = this.code;
            }else if(self.check > 0 && self.pos.get_order().coupon_code_string != this.code){
                console.log('3');
                console.log(self.pos.get_order().coupon_code_string);
                console.log(self.code);
                coupon_code_promotion.button_click(self.pos.get_order().coupon_code_string,true,true);
                self.pos.get_order().coupon_code_string = this.code;
                coupon_code_promotion.button_click(this.code,true,false);
            }else{
                return;
            }
        }
    });
    gui.define_popup({name:'fillincouponcode', widget: FillInCouponCodePopupWidget});

    var GetFreeProductPopupWidget = basepopup.extend({
        template: 'GetFreeProductPopupWidget',
        events: _.extend({}, basepopup.prototype.events, {
            'click .free-product-title-selection-item':    'click_free_product_title',
            'click .free-product-selection-item': 'click_choose_free_product',
            'click .confirm-free-product': 'click_confirm_free_product'
        }),
        renderElement: function(list_title,check,coupon){
            this._super();

            var contents = this.$el[0].querySelector('.pos-promotion-program-product-list');
            var clientline_html = QWeb.render('ProductOfProgramWidget',{widget: this, products:this.list});
            contents.innerHTML = clientline_html;

            if(coupon){
                var new_free_product_chosen_count = this.pos.get_order().new_free_product_chosen_count_coupon;
                var new_free_product_chosen_title = this.pos.get_order().new_free_product_chosen_title_coupon;
            }else{
                var new_free_product_chosen_count = this.pos.get_order().new_free_product_chosen_count;
                var new_free_product_chosen_title = this.pos.get_order().new_free_product_chosen_title;
            }
            if(check == 0 && new_free_product_chosen_count.length > 0 && new_free_product_chosen_title.length > 0){
                for(var i = 0 ; i < new_free_product_chosen_title.length ; i++){
                    if(new_free_product_chosen_title[i].count_product != 0){
                        $(".show-item-chosen[data-id="+new_free_product_chosen_title[i].id+"]").text(new_free_product_chosen_title[i].count_product);
                        for(var j = 0 ; j < new_free_product_chosen_count.length ; j++){
                            if(new_free_product_chosen_title[i].id == new_free_product_chosen_count[j].program_id){
                                // $(":input[data-product-checkbox-id="+new_free_product_chosen_count[j].product_id+"][data-product-bxgy-id="+new_free_product_chosen_count[j].program_id+"][data-item-index="+new_free_product_chosen_count[j].product_index+"]").prop('checked', true);
                                $(".get-pos-promotion-product[data-product-checkbox-id="+new_free_product_chosen_count[j].product_id+"][data-item-index="+new_free_product_chosen_count[j].product_index+"][data-product-bxgy-id="+new_free_product_chosen_count[j].program_id+"]").css('background-color','rgb(58, 204, 15)');
                            }
                        }
                    }
                }
            }
        },
        show: function(options){
            options = options || {};
            var self = this;
            this._super(options);

            this.old_title_index = -1;
            this.old_title_id = -1;
            var first_title = [];
            if(options.list.length > 0){
                first_title.push(options.list[0]);
                for(var i = 1 ; i < options.list.length ; i++){
                    if(options.list[0].product_bxgy_id == options.list[i].product_bxgy_id){
                         first_title.push(options.list[i]);
                    }
                }
                this.list = first_title || [];
            }else{
                this.list = options.list || [];
            }
            this.list_title = options.list_title || [];
            this.is_add_by_coupon_code = options.is_coupon || false;
            this.count_chosen_product = [];
            if(options.is_coupon){
                var new_free_product_chosen_title = [];
                var new_free_product_chosen_count = [];

                if (this.pos.get_order().new_free_product_chosen_title_coupon.length > 0){
                    new_free_product_chosen_title = this.pos.get_order().new_free_product_chosen_title_coupon;
                }
                if (this.pos.get_order().new_free_product_chosen_count_coupon.length > 0){
                    new_free_product_chosen_count = this.pos.get_order().new_free_product_chosen_count_coupon;
                }

                if(this.list_title.length == new_free_product_chosen_title.length){
                    this.list_title = new_free_product_chosen_title;
                    this.count_chosen_product = new_free_product_chosen_count;
                }
                if(this.list_title.length != new_free_product_chosen_title.length && new_free_product_chosen_title.length > 0){
                    for(var i = 0 ; i < new_free_product_chosen_title.length ; i++){
                        for(var j = 0 ; j < this.list_title.length ; j++){
                            if(new_free_product_chosen_title[i].id == this.list_title[j].id){
                                this.list_title[j].count_product = new_free_product_chosen_title[i].count_product
                            }
                        }
                    }
                    for(var j = 0 ; j < this.list_title.length ; j++){
                        for(var k = 0 ; k < new_free_product_chosen_count.length ; k++){
                            if(new_free_product_chosen_count[k].program_id == this.list_title[j].id){
                                this.count_chosen_product.push(new_free_product_chosen_count[k]);
                            }
                        }
                    }
                    this.pos.get_order().new_free_product_chosen_title_coupon = this.list_title;
                }
                this.renderElement([],0,options.is_coupon);
            }else{
                var new_free_product_chosen_title = this.pos.get_order().new_free_product_chosen_title;
                var new_free_product_chosen_count = this.pos.get_order().new_free_product_chosen_count;

                if(this.list_title.length == new_free_product_chosen_title.length){
                    this.list_title = new_free_product_chosen_title;
                    this.count_chosen_product = new_free_product_chosen_count;
                }
                if(this.list_title.length != new_free_product_chosen_title.length && new_free_product_chosen_title.length > 0){
                    for(var i = 0 ; i < new_free_product_chosen_title.length ; i++){
                        for(var j = 0 ; j < this.list_title.length ; j++){
                            if(new_free_product_chosen_title[i].id == this.list_title[j].id){
                                this.list_title[j].count_product = new_free_product_chosen_title[i].count_product
                            }
                        }
                    }
                    for(var j = 0 ; j < this.list_title.length ; j++){
                        for(var k = 0 ; k < new_free_product_chosen_count.length ; k++){
                            if(new_free_product_chosen_count[k].program_id == this.list_title[j].id){
                                this.count_chosen_product.push(new_free_product_chosen_count[k]);
                            }
                        }
                    }
                    this.pos.get_order().new_free_product_chosen_title = this.list_title;
                }
                this.renderElement([],0,options.is_coupon);
            }

            // console.log('show');
            // console.log(new_free_product_chosen_title);
            // console.log(new_free_product_chosen_count);
            // console.log(this.list_title);
            // console.log(this.list);
            // console.log(options.is_coupon);

            // if(!options.is_coupon && self.pos.get_order().program_used_by_coupon_code > 0){
            //     var index = -1;
            //     for(var i = 0 ; i < new_free_product_chosen_title.length ; i++){
            //         if(self.pos.get_order().program_used_by_coupon_code == new_free_product_chosen_title[i].id){
            //             index = new_free_product_chosen_title.indexOf(new_free_product_chosen_title[i]);
            //             break;
            //         }
            //     }
            //     if(index >= 0){
            //         new_free_product_chosen_title.splice(index,1);
            //     }
                // index = -1;
                // for(var i = 0 ; i < this.list_title.length ; i++){
                //     if(self.pos.get_order().program_used_by_coupon_code == this.list_title[i].id){
                //         index = this.list_title.indexOf(this.list_title[i]);
                //         break;
                //     }
                // }
                // if(index >= 0){
                //     this.list_title.splice(index,1);
                // }
                // var indexCoupon = [];
                // for(var i = 0 ; i < new_free_product_chosen_count.length ; i++){
                //     if(self.pos.get_order().program_used_by_coupon_code == new_free_product_chosen_count[i].program_id){
                //         indexCoupon.push(new_free_product_chosen_count[i]);
                //     }
                // }
                // for(var i = 0 ; i < indexCoupon.length ; i++){
                //     var index = new_free_product_chosen_count.indexOf(indexCoupon[i]);
                //     if (index >= 0) {
                //         new_free_product_chosen_count.splice(index, 1);
                //     }
                // }
                // indexCoupon = [];
                // for(var i = 0 ; i < this.list.length ; i++){
                //     if(self.pos.get_order().program_used_by_coupon_code == this.list[i].product_bxgy_id){
                //         indexCoupon.push(this.list[i]);
                //     }
                // }
                // for(var i = 0 ; i < indexCoupon.length ; i++){
                //     var index = this.list.indexOf(indexCoupon[i]);
                //     if (index >= 0) {
                //         this.list.splice(index, 1);
                //     }
                // }
            // }
        },
        click_confirm_free_product: function () {
            this.options.ok.call(self,this.count_chosen_product,this.list_title,this.is_add_by_coupon_code);
        },
        click_free_product_title: function (event) {
            var self= this;
            var item = this.list_title[parseInt($(event.target).data('item-index'))];
            var title_index = parseInt($(event.target).data('item-index'));
            var title_id = parseInt($(event.target).data('id'));
            if(this.old_title_index != -1 && this.old_title_id != -1){
                $(".pos-promotion-program-button.free-product-title-selection-item[data-id="+this.old_title_id+"][data-item-index="+this.old_title_index+"]").css('background', '#EEEEEE');
            }
            this.old_title_index = title_index;
            this.old_title_id = title_id;
            $(".pos-promotion-program-button.free-product-title-selection-item[data-id="+title_id+"][data-item-index="+title_index+"]").css('background', '#8b8b8b');
            this.list = this.options.choose_title.call(self,item);
            var contents = this.$el[0].querySelector('.pos-promotion-program-product-list');
            var clientline_html = QWeb.render('ProductOfProgramWidget',{widget: this, products:this.list});
            contents.innerHTML = clientline_html;
            var title_list = this.list_title;
            var chosen_product = this.count_chosen_product;
            for(var i = 0 ; i < title_list.length ; i++){
                if(title_list[i].count_product != 0){
                    // $(".show-item-chosen[data-id="+title_list[i].id+"]").text(title_list[i].count_product);
                    for(var j = 0 ; j < chosen_product.length ; j++){
                        if(title_list[i].id == chosen_product[j].program_id){
                            // $(":input[data-product-checkbox-id="+chosen_product[j].product_id+"][data-product-bxgy-id="+chosen_product[j].program_id+"][data-item-index="+chosen_product[j].product_index+"]").prop('checked', true);
                            $(".get-pos-promotion-product[data-product-checkbox-id="+chosen_product[j].product_id+"][data-item-index="+chosen_product[j].product_index+"][data-product-bxgy-id="+chosen_product[j].program_id+"]").css('background-color','rgb(58, 204, 15)');
                        }
                    }
                }
            }
        },
        click_choose_free_product: function (event) {
            var item_id = parseInt($(event.target).data('product-checkbox-id'));
            var item_index = parseInt($(event.target).data('item-index'));
            var item_bxgy_id = parseInt($(event.target).data('product-bxgy-id'));
            // var node = ":input[data-product-checkbox-id="+item_id+"][data-item-index="+item_index+"][data-product-bxgy-id="+item_bxgy_id+"]";
            var node = ".get-pos-promotion-product[data-product-checkbox-id="+item_id+"][data-item-index="+item_index+"][data-product-bxgy-id="+item_bxgy_id+"]";
            // if($(node).is(':checked')){
            if($(node).css('background-color')=='rgb(58, 204, 15)'){
                for(var i = 0 ; i < this.list_title.length ; i++){
                    if(this.list_title[i].id == item_bxgy_id){
                        if(this.list_title[i].chosen_product >= this.list_title[i].count_product && this.list_title[i].count_product > 0){
                            // $(node).prop('checked', false);
                            $(node).css('background-color', '');
                            for(var j = 0 ; j < this.count_chosen_product.length ; j++){
                                if(this.count_chosen_product[j].program_id == item_bxgy_id && this.count_chosen_product[j].product_id == item_id){
                                    this.count_chosen_product.splice(j,1);
                                }
                            }
                            this.list_title[i].count_product--;
                            $(".show-item-chosen[data-id="+item_bxgy_id+"]").text(this.list_title[i].count_product);
                        }
                    }
                }
            }else{
                for(var i = 0 ; i < this.list_title.length ; i++){
                    if(this.list_title[i].id == item_bxgy_id){
                        if(this.list_title[i].chosen_product > this.list_title[i].count_product){
                            // $(node).prop('checked', true);
                            $(node).css('background-color', 'rgb(58, 204, 15)');
                            var check = true;
                            for(var j = 0 ; j < this.count_chosen_product.length ; j++){
                                if(this.count_chosen_product[j].program_id == item_bxgy_id && this.count_chosen_product[j].product_id == item_id){
                                    check = false;
                                }
                            }
                            if(check){
                                this.count_chosen_product.push({
                                    program_id : item_bxgy_id,
                                    product_id : item_id,
                                    product_index : item_index
                                });
                            }
                            this.list_title[i].count_product++;
                            $(".show-item-chosen[data-id="+item_bxgy_id+"]").text(this.list_title[i].count_product);
                        }
                    }
                }
            }
        },
        get_product_image_url: function(product){
            return window.location.origin + '/web/image?model=product.product&field=image_medium&id='+product;
        },
    });
    gui.define_popup({name:'getfreeproduct', widget: GetFreeProductPopupWidget});
});