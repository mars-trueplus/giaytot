odoo.define('pos_promotion.models', function (require) {
    "use strict";

    var gui = require('point_of_sale.gui');
    var models = require('point_of_sale.models');
    var alldays = ['sunday','monday','tuesday','wednesday','thursday','friday','saturday'];
    var nowDate = new Date().getTime();
    var utils = require('web.utils');
    var core = require('web.core');
    var Model = require('web.DataModel');
    var _t = core._t;

    var round_di = utils.round_decimals;
    var round_pr = utils.round_precision;

    // var posmodel_super = models.PosModel.prototype;
    // models.PosModel = models.PosModel.extend({
    //     initialize: function(session, attributes) {
    //         this.background_product = [];
    //         return posmodel_super.initialize.call(this,session,attributes);
    //     },
    //
    //     load_server_data: function () {
    //         var self = this;
    //         var product_index = _.findIndex(this.models, function (model) {
    //             return model.model === "product.product";
    //         });
    //         var product_model = this.models[product_index];
    //         product_model.limit = 5;
    //         product_model.loaded_background = function(self, products){
    //                                                 // self.db.add_products_background(products);
    //                                                 self.db.add_products(products);
    //                                             };
    //
    //         var loaded = new $.Deferred();
    //         var progress = 0;
    //         var progress_step = 1.0 / self.models.length;
    //         var tmp = {}; // this is used to share a temporary state between models loaders
    //
    //         function load_model(index){
    //             if(index >= self.models.length){
    //                 loaded.resolve();
    //             }else{
    //                 var model = self.models[index];
    //                 self.chrome.loading_message(_t('Loading')+' '+(model.label || model.model || ''), progress);
    //
    //                 var cond = typeof model.condition === 'function'  ? model.condition(self,tmp) : true;
    //                 if (!cond) {
    //                     load_model(index+1);
    //                     return;
    //                 }
    //
    //                 var limit =  typeof model.limit === 'function'  ? model.limit(self,tmp)  : model.limit;
    //                 var fields =  typeof model.fields === 'function'  ? model.fields(self,tmp)  : model.fields;
    //                 var domain =  typeof model.domain === 'function'  ? model.domain(self,tmp)  : model.domain;
    //                 var context = typeof model.context === 'function' ? model.context(self,tmp) : model.context;
    //                 var ids     = typeof model.ids === 'function'     ? model.ids(self,tmp) : model.ids;
    //                 var order   = typeof model.order === 'function'   ? model.order(self,tmp):    model.order;
    //                 progress += progress_step;
    //
    //                 var records;
    //                 if( model.model ){
    //                     if (model.ids) {
    //                         records = new Model(model.model).call('read',[ids,fields],context);
    //                     } else {
    //                         records = new Model(model.model)
    //                             .query(fields)
    //                             .filter(domain)
    //                             .order_by(order)
    //                             .limit(limit)
    //                             .context(context)
    //                             .all();
    //                     }
    //                     records.then(function(result){
    //                             try{    // catching exceptions in model.loaded(...)
    //                                 $.when(model.loaded(self,result,tmp))
    //                                     .then(function(){ load_model(index + 1); },
    //                                           function(err){ loaded.reject(err); });
    //                             }catch(err){
    //                                 console.error(err.stack);
    //                                 loaded.reject(err);
    //                             }
    //                         },function(err){
    //                             loaded.reject(err);
    //                         });
    //                     if(model.model == 'product.product'){
    //                         var posPromotionConfigModel = new Model('pos.promotion.config.settings');
    //                         posPromotionConfigModel.call('get_apply_pos_promotion_program').then(function (return_value) {
    //                             var all_product_count = new Model(model.model)
    //                                 .query(fields)
    //                                 .filter(domain)
    //                                 .order_by(order)
    //                                 .context(context)
    //                                 .count();
    //                             all_product_count.then(function (result) {
    //                                 var count_product = result - limit;
    //                                 var offset_product = limit;
    //                                 function load_background(count_product,offset_product) {
    //                                     if(count_product > 0){
    //                                         var records_product_background = new Model(model.model)
    //                                         .query(fields)
    //                                         .filter(domain)
    //                                         .order_by(order)
    //                                         .limit(limit)
    //                                         .offset(offset_product)
    //                                         .context(context)
    //                                         .all();
    //                                         records_product_background.then(function(result_product){
    //                                             $.when(model.loaded_background(self,result_product,tmp))
    //                                                 .then(function(){ load_background(count_product - limit,offset_product + limit); });
    //                                         });
    //                                     }
    //                                 }
    //                                 load_background(count_product,offset_product);
    //                             });
    //                         });
    //                     }
    //                 }else if( model.loaded ){
    //                     try{    // catching exceptions in model.loaded(...)
    //                         $.when(model.loaded(self,tmp))
    //                             .then(  function(){ load_model(index +1); },
    //                                     function(err){ loaded.reject(err); });
    //                     }catch(err){
    //                         loaded.reject(err);
    //                     }
    //                 }else{
    //                     load_model(index + 1);
    //                 }
    //             }
    //         }
    //
    //         try{
    //             load_model(0);
    //         }catch(err){
    //             loaded.reject(err);
    //         }
    //
    //         return loaded;
    //     }
    // });

    //pos.promotion.day model
    models.load_models({
        model: 'pos.promotion.day',
        fields: ['code'],
        loaded: function (self, promotion_days) {
            self.promotion_days = [];
            for (var i=0; i<promotion_days.length; i++) {
                self.promotion_days[promotion_days[i].id] = promotion_days[i].code;
            }
        }
    });

    //pos.promotion.program model
    models.load_models({
        model: 'pos.promotion.program',
        fields: [
            'id',
            'name',
            'description',
            'type',
            'stock_location_id',
            'discount_by_qty_value',
            // 'discount_by_qty_product_id',
            'sequence',
            'state',
            'start_date',
            'end_date',
            'day_apply',
            'number_select_free',
            'pos_promotion_bxgy_buy',
            'pos_promotion_bxgy_get',
            'total_order',
            'number_select_gift',
            'discount_product_id',
            'coupon_code',
            'code',
            'quantity',
            'used',
            'left',
            'code_per_customer'
        ],
        domain: [['state','=','active']],
        loaded: function (self, programs) {
            var pos_stock_id = self.config.stock_location_id[0];
            var d = new Date();
            var n = d.getDay();
            self.programs = [];
            // tab13 coupon code
            self.programs_back_up = [];

            self.program_ids = [];
            self.priority_category = [];
            self.priority_product = [];
            self.priority_total = [];
            self.priority_discount_by_qty = [];
            self.coupon_code = [];
            for (var i=0; i<programs.length; i++) {
                if(programs[i].stock_location_id.length > 0){
                    for(var k = 0 ; k < programs[i].stock_location_id.length ; k++){
                        if(programs[i].stock_location_id[k] == pos_stock_id){
                            var days = programs[i].day_apply;
                            var day_available = false;
                            for (var j=0; j<days.length; j++) {
                                if (self.promotion_days[days[j]] == 'everyday' || self.promotion_days[days[j]] == alldays[n]) {
                                    day_available = true;
                                    break;
                                }
                            }
                            if (day_available) {
                                var start_date = programs[i].start_date;
                                var end_date = programs[i].end_date;
                                if ((start_date == '' && end_date == '') || (start_date == '' && new Date(end_date + ' 23:59:59').getTime() >= nowDate) || (new Date(start_date).getTime() <= nowDate && end_date == '') || (new Date(start_date).getTime() <= nowDate && new Date(end_date + ' 23:59:59').getTime() >= nowDate)) {
                                    self.programs.push(programs[i]);
                                    // tab13 coupon code
                                    self.programs_back_up.push(programs[i]);

                                    self.program_ids.push(programs[i].id);
                                    if(programs[i].type == 'give_product' || programs[i].type == 'discount_on_total'){
                                        self.priority_total.push({
                                            'promotion_program_id': programs[i].id,
                                            'sequence': programs[i].sequence
                                        })
                                    }
                                    if(programs[i].type == 'discount_by_qty'){
                                        self.priority_discount_by_qty.push({
                                            'promotion_program_id': programs[i].id,
                                            'sequence': programs[i].sequence
                                        });
                                    }
                                    // Tab13 coupon code
                                    if(programs[i].coupon_code){
                                        self.coupon_code.push({
                                            'promotion_program_id': programs[i].id,
                                            'code': programs[i].code,
                                            'quantity': programs[i].quantity,
                                            'used': programs[i].used,
                                            'left': programs[i].left,
                                            'code_per_customer': programs[i].code_per_customer
                                        });
                                    }
                                }
                            }
                            break;
                        }
                    }
                }
            }
        }
    });

    //ir.pos.promotion.discount model - danh sach cac san pham duoc discount theo chuong trinh khuyen mai
    models.load_models({
        model: 'ir.pos.promotion.discount',
        fields: ['product_id', 'promotion_program_id', 'min_qty', 'discount'],
        loaded: function (self, promotion_discount) {
            self.pos_promotion_product_discount = [];

            self.promotion_discount_approved = [];
            self.promotion_discount_product_approved = [];
            for (var i=0; i<promotion_discount.length; i++) {
                if (self.program_ids.indexOf(promotion_discount[i].promotion_program_id[0]) != -1) {
                    self.pos_promotion_product_discount.push({
                        'product_id': promotion_discount[i].product_id[0],
                        'promotion_program_id': promotion_discount[i].promotion_program_id[0],
                        'min_qty': promotion_discount[i].min_qty,
                        'discount': promotion_discount[i].discount
                    });
                    // sequence
                    self.priority_product.push({
                        'is_product': true,
                        'product_id': promotion_discount[i].product_id[0],
                        'promotion_program_id': promotion_discount[i].promotion_program_id[0],
                        'sequence': self.programs[self.program_ids.indexOf(promotion_discount[i].promotion_program_id[0])].sequence
                    });
                }
            }
        }
    });

    // ir.pos.promotion.bxgy model - danh sach cac san pham khuyen mai va duoc khuyen mai theo chuong trinh buy x get y
    models.load_models({
        model: 'ir.pos.promotion.bxgy',
        fields: [
            'id',
            'product_id',
            'min_qty',
            'promotion_program_id_buy',
            'promotion_program_id_get'
        ],
        loaded: function (self, bxgys) {
            self.bxgy_bx = [];
            self.bxgy_gy = [];
            self.bxgy_programs = [];
            self.bxgy = bxgys;
            // self.new_order_free_product = [];
            // self.new_free_product_chosen_title = [];
            // self.new_free_product_chosen_count = [];
            var check;
            for(var i = 0 ; i < self.programs.length ; i++){
                if(self.programs[i].type == 'bxgy'){
                    self.bxgy_programs.push(self.programs[i]);
                    if(self.programs[i].pos_promotion_bxgy_buy.length > 0){
                        for(var j = 0 ; j < self.programs[i].pos_promotion_bxgy_buy.length ; j++) {
                            for (var k = 0; k < bxgys.length; k++) {
                                if (self.programs[i].pos_promotion_bxgy_buy[j] == bxgys[k].id && self.programs[i].id == bxgys[k].promotion_program_id_buy[0]) {
                                    self.bxgy_bx.push({
                                        'bxgy_program_id' : bxgys[k].promotion_program_id_buy[0],
                                        'id': bxgys[k].product_id[0],
                                        'qty': bxgys[k].min_qty
                                    });
                                    // sequence
                                    self.priority_product.push({
                                        'is_product': true,
                                        'product_id': bxgys[k].product_id[0],
                                        'promotion_program_id': bxgys[k].promotion_program_id_buy[0],
                                        'sequence': self.programs[i].sequence
                                    });
                                }
                            }
                        }
                    }
                    if(self.programs[i].pos_promotion_bxgy_get.length > 0){
                        for(var j = 0 ; j < self.programs[i].pos_promotion_bxgy_get.length ; j++){
                            for (var k = 0; k < bxgys.length; k++) {
                                if (self.programs[i].pos_promotion_bxgy_get[j] == bxgys[k].id && self.programs[i].id == bxgys[k].promotion_program_id_get[0]) {
                                    self.bxgy_gy.push({
                                        'bxgy_program_id' : bxgys[k].promotion_program_id_get[0],
                                        'id': bxgys[k].product_id[0]
                                    });
                                }
                            }
                        }
                    }
                }
            }
        }
    });

    //ir.pos.promotion.give.product model - danh sach cac san pham duoc discount theo chuong trinh khuyen mai
    models.load_models({
        model: 'ir.pos.promotion.give.product',
        fields: ['id','product_id', 'promotion_program_id'],
        loaded: function (self, promotion_give_product) {
            self.pos_promotion_product_give_product = [];
            for (var i=0; i<promotion_give_product.length; i++) {
                if (self.program_ids.indexOf(promotion_give_product[i].promotion_program_id[0]) != -1) {
                    self.pos_promotion_product_give_product.push({
                        'product_id': promotion_give_product[i].product_id[0],
                        'promotion_program_id': promotion_give_product[i].promotion_program_id[0]
                    });
                }
            }
        }
    });

    // ir.pos.promotion.specialprice model - danh sach cac san pham khuyen mai theo gia dac biet
    models.load_models({
        model: 'ir.pos.promotion.specialprice',
        fields: ['id','product_id','promotion_program_id','spec_price'],
        loaded: function (self, special_prices) {
            self.special_prices = [];
            for (var i=0; i<special_prices.length; i++) {
                self.special_prices.push({
                    'product_id': special_prices[i].product_id[0],
                    'promotion_program_id': special_prices[i].promotion_program_id[0],
                    'spec_price': special_prices[i].spec_price
                });
                // sequence
                if (self.program_ids.indexOf(special_prices[i].promotion_program_id[0]) != -1) {
                    self.priority_product.push({
                        'is_product': true,
                        'product_id': special_prices[i].product_id[0],
                        'promotion_program_id': special_prices[i].promotion_program_id[0],
                        'sequence': self.programs[self.program_ids.indexOf(special_prices[i].promotion_program_id[0])].sequence
                    });
                }
            }
        }
    });

    //ir.pos.promotion.discount.on.cat model - danh sach cac khuyen mai theo category
    models.load_models({
        model: 'ir.pos.promotion.discount.on.cat',
        fields: ['category_id', 'promotion_program_id', 'type', 'value'],
        loaded: function (self, discount_on_cat) {
            self.discount_on_cat = [];
            for (var i=0; i<discount_on_cat.length; i++) {
                self.discount_on_cat.push({
                    'category_id': discount_on_cat[i].category_id[0],
                    'promotion_program_id': discount_on_cat[i].promotion_program_id[0],
                    'type': discount_on_cat[i].type,
                    'value': discount_on_cat[i].value
                });
                // sequence
                if (self.program_ids.indexOf(discount_on_cat[i].promotion_program_id[0]) != -1) {
                    self.priority_category.push({
                        'is_product': false,
                        'category_id': discount_on_cat[i].category_id[0],
                        'promotion_program_id': discount_on_cat[i].promotion_program_id[0],
                        'sequence': self.programs[self.program_ids.indexOf(discount_on_cat[i].promotion_program_id[0])].sequence
                    });
                }
            }
        }
    });

    //ir.pos.promotion.discount.on.total model - danh sach cac khuyen mai theo total order
    models.load_models({
        model: 'ir.pos.promotion.discount.on.total',
        fields: ['promotion_program_id', 'total_order', 'type', 'value'],
        loaded: function (self, discount_on_total) {
            // self.discount_on_total_program = [];
            self.discount_on_total = [];
            // self.discount_on_total_program_product = [];
            // self.promotion_discount_on_total_approved = [];
            // self.promotion_discount_on_total_product_approved = [];
            for(var i = 0 ; i < self.programs.length ; i++){
                if(self.programs[i].type == 'discount_on_total'){
                    // self.discount_on_total_program.push({
                    //     'id': self.programs[i].id,
                    //     'name': self.programs[i].name,
                    //     'product_id': self.programs[i].discount_product_id[0],
                    //     'product_name': self.programs[i].discount_product_id[1],
                    //     'sequence': self.programs[i].sequence
                    // });
                    for(var j = 0 ; j < discount_on_total.length ; j++){
                        if(self.programs[i].id == discount_on_total[j].promotion_program_id[0]){
                            self.discount_on_total.push({
                                'id': discount_on_total[j].id,
                                'total_order': discount_on_total[j].total_order,
                                'promotion_program_id': discount_on_total[j].promotion_program_id[0],
                                'promotion_program_name': discount_on_total[j].promotion_program_id[1],
                                'type': discount_on_total[j].type,
                                'value': discount_on_total[j].value
                            });
                        }
                    }
                    // self.discount_on_total_program_product.push({
                    //     'id': self.programs[i].id,
                    //     'name': self.programs[i].name,
                    //     'product_id': self.programs[i].discount_product_id[0],
                    //     'product_name': self.programs[i].discount_product_id[1]
                    // });
                }
            }
            // console.log(self.discount_on_total_program);
            // console.log(self.discount_on_total);
            // console.log(self.discount_on_total_program_product);
        }
    });

    //ir.pos.promotion.discount.by.qty model - danh sach cac khuyen mai theo quantity
    models.load_models({
        model: 'ir.pos.promotion.discount.by.qty',
        fields: ['promotion_program_id', 'total_qty','value'],
        loaded: function (self, discount_by_qtys) {
            // self.discount_on_total_program = [];
            self.discount_by_qty = [];
            // self.discount_on_total_program_product = [];
            // self.promotion_discount_on_total_approved = [];
            // self.promotion_discount_on_total_product_approved = [];
            for(var i = 0 ; i < self.programs.length ; i++){
                if(self.programs[i].type == 'discount_by_qty'){
                    // self.discount_on_total_program.push({
                    //     'id': self.programs[i].id,
                    //     'name': self.programs[i].name,
                    //     'product_id': self.programs[i].discount_product_id[0],
                    //     'product_name': self.programs[i].discount_product_id[1],
                    //     'sequence': self.programs[i].sequence
                    // });
                    for(var j = 0 ; j < discount_by_qtys.length ; j++){
                        if(self.programs[i].id == discount_by_qtys[j].promotion_program_id[0]){
                            self.discount_by_qty.push({
                                'id': discount_by_qtys[j].id,
                                'total_qty': discount_by_qtys[j].total_qty,
                                'promotion_program_id': discount_by_qtys[j].promotion_program_id[0],
                                'promotion_program_name': discount_by_qtys[j].promotion_program_id[1],
                                'value': discount_by_qtys[j].value
                            });
                        }
                    }
                    // self.discount_on_total_program_product.push({
                    //     'id': self.programs[i].id,
                    //     'name': self.programs[i].name,
                    //     'product_id': self.programs[i].discount_product_id[0],
                    //     'product_name': self.programs[i].discount_product_id[1]
                    // });
                }
            }
            // console.log(self.discount_on_total_program);
            // console.log(self.discount_on_total);
            // console.log(self.discount_on_total_program_product);
        }
    });

    var _super_posmodel = models.PosModel.prototype;
    models.PosModel = models.PosModel.extend({
        //removes the current order
        delete_current_order: function(){
            var order = this.get_order();
            if (order) {
                if(order.is_used_coupon_code){
                    new Model('pos.promotion.program').call('set_coupon_code_left_after_change_order',[order.program_used_by_coupon_code]).then(function () {
                        order.destroy({'reason':'abandon'});
                    });
                }else{
                    order.destroy({'reason':'abandon'});
                }
            }
        },
    });

    var _orderlineproto = models.Orderline.prototype;
    models.Orderline = models.Orderline.extend({
        initialize: function(attr,options){
            _orderlineproto.initialize.call(this,attr,options);
            this.pos_promotion_message = this.pos_promotion_message || [];
            this.is_pos_promotion_product = this.is_pos_promotion_product || 0;
            this.is_product_by_coupon_code = this.is_product_by_coupon_code || false;
            this.discount_by_pos_promotion = this.discount_by_pos_promotion || [];
            this.discount_by_coupon_code = this.discount_by_coupon_code || [];
        },
        init_from_JSON: function(json) {
            _orderlineproto.init_from_JSON.apply(this, arguments);
            this.pos_promotion_message = json.pos_promotion_message;
            this.is_pos_promotion_product = json.is_pos_promotion_product;
            this.is_product_by_coupon_code = json.is_product_by_coupon_code;
            this.discount_by_pos_promotion = json.discount_by_pos_promotion;
            this.discount_by_coupon_code = json.discount_by_coupon_code;
        },
        export_as_JSON: function() {
            var json = _orderlineproto.export_as_JSON.call(this,arguments);
            json.pos_promotion_message = this.pos_promotion_message;
            json.is_pos_promotion_product = this.is_pos_promotion_product;
            json.is_product_by_coupon_code = this.is_product_by_coupon_code;
            return json;
        },
        set_pos_promotion_message: function (message) {
            var self = this;
            if( typeof this.order.get_last_orderline().pos_promotion_message != 'undefined'){
                this.order.get_last_orderline().pos_promotion_message += '\"\<br\/\>\"' +message;
            }else{
                this.order.get_last_orderline().pos_promotion_message = message;
            }

            this.trigger('change',this);
        },
        set_discount_by_qty_message: function (message) {
            var self = this;
            if( typeof this.order.get_last_orderline().pos_promotion_message != 'undefined'){
                // this.order.get_last_orderline().pos_promotion_message += message;
                this.order.get_last_orderline().pos_promotion_message += '\"\<br\/\>\"' + message;
            }else{
                this.order.get_last_orderline().pos_promotion_message = message;
            }

            this.trigger('change',this);
        }

    });

    var _orderproto = models.Order.prototype;
    models.Order = models.Order.extend({
        initialize: function(attributes,options){
            _orderproto.initialize.call(this,attributes,options);
            this.click_pos_promotion = this.click_pos_promotion || false;
            // this.show_pop_up_promotion = this.show_pop_up_promotion || false;

            this.new_order_free_product = this.new_order_free_product || [];
            this.new_free_product_chosen_title = this.new_free_product_chosen_title || [];
            this.new_free_product_chosen_count = this.new_free_product_chosen_count || [];
            this.pos_promotion_note = this.pos_promotion_note || '';
            // tab13 coupon code
            this.coupon_code_string = this.coupon_code_string || '';
            this.before_use_coupon = this.before_use_coupon || [];
            this.is_used_coupon_code = this.is_used_coupon_code || false;
            this.program_used_by_coupon_code = this.program_used_by_coupon_code || -1;
            this.new_order_free_product_coupon = this.new_order_free_product_coupon || [];
            this.new_free_product_chosen_title_coupon = this.new_free_product_chosen_title_coupon || [];
            this.new_free_product_chosen_count_coupon = this.new_free_product_chosen_count_coupon || [];


            // console.log('initialize1');
            // console.log(this.is_used_coupon_code);
            // console.log(this.program_used_by_coupon_code);
            // console.log(this.new_order_free_product_coupon);
            // console.log(this.new_free_product_chosen_title_coupon);
            // console.log(this.new_free_product_chosen_count_coupon);

        },
        init_from_JSON: function(json) {

            _orderproto.init_from_JSON.apply(this, arguments);
            this.click_pos_promotion = json.click_pos_promotion;
            // this.show_pop_up_promotion = json.show_pop_up_promotion;

            this.new_order_free_product = json.new_order_free_product;
            this.new_free_product_chosen_title = json.new_free_product_chosen_title;
            this.new_free_product_chosen_count = json.new_free_product_chosen_count;
            this.pos_promotion_note = json.pos_promotion_note;
            // tab13 coupon code
            this.coupon_code_string = json.coupon_code_string;
            this.before_use_coupon = json.before_use_coupon;
            this.is_used_coupon_code = json.is_used_coupon_code;
            this.program_used_by_coupon_code = json.program_used_by_coupon_code;
            this.new_order_free_product_coupon = json.new_order_free_product_coupon;
            this.new_free_product_chosen_title_coupon = json.new_free_product_chosen_title_coupon;
            this.new_free_product_chosen_count_coupon = json.new_free_product_chosen_count_coupon;

            // console.log('init_from_JSON1');
            // console.log(this.is_used_coupon_code);
            // console.log(this.program_used_by_coupon_code);
            // console.log(this.new_order_free_product_coupon);
            // console.log(this.new_free_product_chosen_title_coupon);
            // console.log(this.new_free_product_chosen_count_coupon);
        },
        // export_as_JSON: function() {
        //     var json = _orderproto.export_as_JSON.call(this,arguments);
        //     json.new_order_free_product     = this.new_order_free_product;
        //     json.new_free_product_chosen_title     = this.new_free_product_chosen_title;
        //     json.new_free_product_chosen_count     = this.new_free_product_chosen_count;
        //     console.log('export_as_JSON');
        //     console.log(json);
        //     return json;
        // }
        set_pos_promotion_note: function(note){
            this.pos_promotion_note = note;
            // this.trigger('change',this);
        },
        // set_client: function(client){
        //     var self = this;
        //     console.log('set_client')
        //     console.log(client);
        //     if(this.is_used_coupon_code){
        //         new Model('pos.promotion.coupon.code').call('check_customer_with_coupon_code',[client.id,this.program_used_by_coupon_code]).then(function (result) {
        //             if(result == 1){
        //                 self.assert_editable();
        //                 self.set('client',client);
        //             }
        //             if(result == -1){
        //                 alert('Can not use coupon code!!!');
        //                 return;
        //             }
        //         })
        //     }else{
        //         this.assert_editable();
        //         this.set('client',client);
        //     }
        // }
    });

    //ir.pos.promotion.give.product model - danh sach cac san pham duoc discount theo chuong trinh khuyen mai
    // models.load_models({
    //     model: 'ir.pos.promotion.give.product',
    //     fields: ['product_id', 'promotion_program_id'],
    //     loaded: function (self, promotion_give_product) {
    //         self.pos_promotion_product_give_product = [];
    //         //
    //         // self.promotion_discount_approved = [];
    //         // self.promotion_discount_product_approved = [];
    //         for (var i=0; i<promotion_give_product.length; i++) {
    //             if (self.program_ids.indexOf(promotion_give_product[i].promotion_program_id[0]) != -1) {
    //                 self.pos_promotion_product_give_product.push({
    //                     'product_id': promotion_give_product[i].product_id[0],
    //                     'promotion_program_id': promotion_give_product[i].promotion_program_id[0]
    //                 });
    //             }
    //         }
    //     }
    // });
});
