odoo.define('pos_promotion.pos_promotion', function (require) {
    "use strict";

    var screens = require('point_of_sale.screens');
    var models = require('point_of_sale.models');
    var gui = require('point_of_sale.gui');
    var core = require('web.core');
    var BaseWidget = require('point_of_sale.BaseWidget');
    var Model = require('web.DataModel');
    var MageBaseWidget = require('magestore_point_of_sale.BaseWidget');

    var _t = core._t;

    // var CouponCodeButton = screens.ActionButtonWidget.extend({
    //     template: 'CouponCodeButton',
    //
    //     button_click: function () {
    //         this.gui.show_popup('fillincouponcode',{
    //
    //         });
    //     },
    // });
    // screens.define_action_button({
    //     'name': 'coupon_code',
    //     'widget': CouponCodeButton,
    //     'condition': function(){
    //         return true;
    //         // return this.pos.config.iface_discount && this.pos.config.discount_product_id;
    //     }
    // });

    // var PosPromotionButton = screens.ActionButtonWidget.extend({
    //     template: 'PosPromotionButton',
    //     button_click: function (code,coupon,clear_coupon) {
    //         var self = this;
    //
    //         if(typeof (coupon) === 'undefined') coupon = false;
    //         if(typeof (code) === 'undefined') code = '';
    //         if(typeof (clear_coupon) === 'undefined') clear_coupon = false;
    //
    //         self.clear = clear_coupon;
    //
    //         var new_program_available = [];
    //         var new_discount_on_total = [];
    //         var new_discount_by_qty = [];
    //
    //         self.program_available = this.pos.programs;
    //         // tab13 coupon code
    //         self.program_available_back_up = this.pos.programs_back_up;
    //         //
    //         self.priority = this.pos.priority_product;
    //         self.priority_total = this.pos.priority_total;
    //         self.discount_on_total = this.pos.discount_on_total;
    //         self.priority_discount_by_qty = this.pos.priority_discount_by_qty;
    //         self.discount_by_qty = this.pos.discount_by_qty;
    //
    //         // self.pos.note_content = document.getElementById('note').value;
    //
    //         // chi lay 1 total discount
    //         // if(self.priority_total.length > 0){
    //         //     var min = self.priority_total[0];
    //         //     var delete_total_program = [];
    //         //     var new_delete_total_program = [];
    //         //     for(var i = 0 ; i < self.priority_total.length ; i++){
    //         //         if(self.priority_total[i].sequence < min.sequence){
    //         //             delete_total_program.push(min);
    //         //             min = self.priority_total[i];
    //         //         }
    //         //         if(self.priority_total[i].sequence > min.sequence){
    //         //             delete_total_program.push(self.priority_total[i]);
    //         //         }
    //         //     }
    //         //     // console.log(delete_total_program);
    //         //     if(delete_total_program.length > 0){
    //         //         for(var k = 0 ; k < self.program_available.length ; k++){
    //         //             for(var l = 0 ; l < delete_total_program.length ; l++){
    //         //                 if(delete_total_program[l].promotion_program_id == self.program_available[k].id){
    //         //                     new_delete_total_program.push(self.program_available[k]);
    //         //                 }
    //         //             }
    //         //         }
    //         //         for(var k = 0 ; k < new_delete_total_program.length ; k++){
    //         //             var index = self.program_available.indexOf(new_delete_total_program[k]);
    //         //             if(index >= 0){
    //         //                 self.program_available.splice(index,1);
    //         //             }
    //         //         }
    //         //     }
    //         // }
    //         // het
    //
    //         // if (self.pos.get_order().new_order_free_product.length > 0) {
    //         //     var re_orderline = self.pos.get_order().get_orderlines();
    //         //     var new_order = self.pos.get_order().new_order_free_product;
    //         //     for (var i = 0; i < new_order.length; i++) {
    //         //         for (var j = 0; j < re_orderline.length; j++) {
    //         //             if (re_orderline[j].id == new_order[i].order_line_position) {
    //         //                 self.pos.get_order().remove_orderline(re_orderline[j]);
    //         //             }
    //         //         }
    //         //     }
    //         // }
    //
    //
    //         if(self.pos.get_order().get_orderlines().length > 0) {
    //             var product_price;
    //             var checked_orderline = [];
    //             var product_qty;
    //             var check;
    //
    //             for (var i = 0; i < self.pos.get_order().get_orderlines().length; i++) {
    //                 check = 0;
    //                 product_price = self.pos.get_order().get_orderlines()[i].product.price;
    //                 product_qty = self.pos.get_order().get_orderlines()[i].quantity;
    //                 if (checked_orderline.length == 0 && product_price > 0 && product_qty > 0 && self.pos.get_order().get_orderlines()[i].is_pos_promotion_product != 1) {
    //                     checked_orderline.push({
    //                         'orderline_id': self.pos.get_order().get_orderlines()[i].id,
    //                         'product_id': self.pos.get_order().get_orderlines()[i].product.id,
    //                         'product_name': self.pos.get_order().get_orderlines()[i].product.display_name,
    //                         'product_price': self.pos.get_order().get_orderlines()[i].product.price,
    //                         'product_qty': self.pos.get_order().get_orderlines()[i].quantity
    //                     });
    //                     continue;
    //                 }
    //                 if (checked_orderline.length > 0 && self.pos.get_order().get_orderlines()[i].product.price > 0 && product_qty > 0 && self.pos.get_order().get_orderlines()[i].is_pos_promotion_product != 1) {
    //                     checked_orderline.push({
    //                         'orderline_id': self.pos.get_order().get_orderlines()[i].id,
    //                         'product_id': self.pos.get_order().get_orderlines()[i].product.id,
    //                         'product_name': self.pos.get_order().get_orderlines()[i].product.display_name,
    //                         'product_price': self.pos.get_order().get_orderlines()[i].product.price,
    //                         'product_qty': self.pos.get_order().get_orderlines()[i].quantity
    //                     });
    //                 }
    //             }
    //             if (checked_orderline.length > 0) {
    //                 checked_orderline.sort(function (a, b) {
    //                     return (a.product_price < b.product_price) ? 1 : ((b.product_price < a.product_price) ? -1 : 0);
    //                 });
    //                 for (var j = 0; j < checked_orderline.length; j++) {
    //                     if (checked_orderline[j].product_qty > 1) {
    //                         for (var s = 1; s < checked_orderline[j].product_qty; s++) {
    //                             var clone_orderline = self.pos.get_order().get_orderline(checked_orderline[j].orderline_id).clone();
    //                             self.pos.get_order().add_orderline(clone_orderline);
    //                             self.pos.get_order().get_orderlines()[self.pos.get_order().get_orderlines().indexOf(clone_orderline)].set_quantity(1);
    //                             checked_orderline.push({
    //                                 'orderline_id': self.pos.get_order().get_orderlines()[self.pos.get_order().get_orderlines().indexOf(clone_orderline)].id,
    //                                 'product_id': self.pos.get_order().get_orderlines()[self.pos.get_order().get_orderlines().indexOf(clone_orderline)].product.id,
    //                                 'product_name': self.pos.get_order().get_orderlines()[self.pos.get_order().get_orderlines().indexOf(clone_orderline)].product.display_name,
    //                                 'product_price': self.pos.get_order().get_orderlines()[self.pos.get_order().get_orderlines().indexOf(clone_orderline)].product.price,
    //                                 'product_qty': self.pos.get_order().get_orderlines()[self.pos.get_order().get_orderlines().indexOf(clone_orderline)].quantity
    //                             });
    //                         }
    //                         self.pos.get_order().get_orderline(checked_orderline[j].orderline_id).set_quantity(1);
    //                     }
    //                 }
    //             }
    //         }
    //
    //         // if(code && !self.pos.get_order().is_used_coupon_code && coupon){
    //
    //         // if (code == '' && clear_coupon) {
    //         //         var re_orderline = self.pos.get_order().get_orderlines();
    //         //         var new_order = self.pos.get_order().new_order_free_product_coupon;
    //         //         for (var i = 0; i < new_order.length; i++) {
    //         //             for (var j = 0; j < re_orderline.length; j++) {
    //         //                 if (re_orderline[j].id == new_order[i].order_line_position) {
    //         //                     // console.log('remove product coupon 1');
    //         //                     self.pos.get_order().remove_orderline(re_orderline[j]);
    //         //                 }
    //         //             }
    //         //         }
    //         //     }
    //
    //         if(code != '' && coupon){
    //             // tab13 sum discount
    //             self.pos.get_order().get_orderlines().forEach(function (line) {
    //                 // line.discount_by_pos_promotion = [];
    //                 line.discount_by_coupon_code = [];
    //                 // line.pos_promotion_message = [];
    //             });
    //             if (self.clear){
    //                 var allLine = self.pos.get_order().get_orderlines();
    //                 for (var o = 0 ; o < allLine.length ; o++){
    //                     for (var i = 0 ; i < allLine[o].pos_promotion_message.length ; i++){
    //                         if (allLine[o].pos_promotion_message[i].coupon == 1){
    //                             var index = allLine[o].pos_promotion_message.indexOf(allLine[o].pos_promotion_message[i]);
    //                             if (index >= 0){
    //                                 allLine[o].pos_promotion_message.splice(index,1);
    //                                 break;
    //                             }
    //                         }
    //                     }
    //                     break;
    //                 }
    //             }
    //             // if (self.pos.get_order().new_order_free_product_coupon.length > 0) {
    //             //     var re_orderline = self.pos.get_order().get_orderlines();
    //             //     var new_order = self.pos.get_order().new_order_free_product_coupon;
    //             //     for (var i = 0; i < new_order.length; i++) {
    //             //         for (var j = 0; j < re_orderline.length; j++) {
    //             //             if (re_orderline[j].id == new_order[i].order_line_position) {
    //             //                 // console.log('remove product coupon 1');
    //             //                 self.pos.get_order().remove_orderline(re_orderline[j]);
    //             //             }
    //             //         }
    //             //     }
    //             // }
    //             // console.log(code);
    //             self.order = self.pos.get_order();
    //             self.order_orderlines = self.order.get_orderlines();
    //             self.orderlines = new Array();
    //                 _.each(self.order_orderlines, function (orderline) {
    //                     self.orderlines.push(orderline);
    //                 });
    //             self.order.pos_promotion_message = [];
    //             self.new_promotion_discount_approved = [];
    //             self.new_promotion_discount_product_approved = [];
    //             self.product_list = self.pos.db.get_product_by_category(0);
    //             // self.bxgy_gy_list = [];
    //             // self.bxgy_gy_title = [];
    //             self.popup_promotion = false;
    //             self.use_program = 0;
    //             var use_program_type = '';
    //             // console.log('this.pos.programs');
    //             // console.log(this.pos.programs);
    //             // console.log(self.order.save_delete_coupon_program);
    //             // console.log(self.order.program_used_by_coupon_code);
    //             // console.log(self.order.save_delete_coupon_program_bxgy);
    //             // console.log(self.order.save_delete_coupon_program_bx);
    //             // console.log(self.order.save_delete_coupon_program_gy);
    //             // if(self.order.program_used_by_coupon_code > 0){
    //             //     if(self.order.save_delete_coupon_program.length > 0){
    //             //         this.pos.programs.push(self.order.save_delete_coupon_program[0]);
    //             //     }
    //             //     if(self.order.save_delete_coupon_program_bxgy.length > 0){
    //             //         for(var i = 0 ; i < self.order.save_delete_coupon_program_bxgy.length ; i++){
    //             //             this.pos.bxgy_programs.push(self.order.save_delete_coupon_program_bxgy[i]);
    //             //         }
    //             //     }
    //             //     if(self.order.save_delete_coupon_program_bx.length > 0){
    //             //         for(var i = 0 ; i < self.order.save_delete_coupon_program_bx.length ; i++){
    //             //             this.pos.bxgy_bx.push(self.order.save_delete_coupon_program_bx[i]);
    //             //         }
    //             //     }
    //             //     if(self.order.save_delete_coupon_program_gy.length > 0){
    //             //         for(var i = 0 ; i < self.order.save_delete_coupon_program_gy.length ; i++){
    //             //             this.pos.bxgy_gy.push(self.order.save_delete_coupon_program_gy[i]);
    //             //         }
    //             //     }
    //             // }
    //             for (var i = 0 ; i < this.pos.programs_back_up.length ; i++){
    //                 var index = this.pos.programs.indexOf(this.pos.programs_back_up[i]);
    //                 if (index < 0) {
    //                     this.pos.programs.push(this.pos.programs_back_up[i]);
    //                 }
    //             }
    //             this.pos.programs.sort(function(a,b) {return (a.sequence > b.sequence) ? 1 : ((b.sequence > a.sequence) ? -1 : 0);} );
    //             for (var i = 0; i < this.pos.programs.length; i++) {
    //                 if (this.pos.programs[i].code == code) {
    //                     if(self.order.before_use_coupon.length == 0){
    //                         self.order.before_use_coupon.push({
    //                             total : self.order.get_total_with_tax(),
    //                             quantity : self.order.get_orderlines().length
    //                         });
    //                     }else {
    //                         self.order.before_use_coupon = [];
    //                         self.order.before_use_coupon.push({
    //                             total : self.order.get_total_with_tax(),
    //                             quantity : self.order.get_orderlines().length
    //                         });
    //                         // self.order.before_use_coupon[0].total = self.order.get_total_with_tax();
    //                         // self.order.before_use_coupon[0].quantity = self.order.get_orderlines().length;
    //                     }
    //                     // use_program = true;
    //                     self.bxgy_gy_list = [];
    //                     self.bxgy_gy_title = [];
    //                     switch (this.pos.programs[i].type) {
    //                         case 'discount':
    //                             self.use_program = this.pos.programs[i].id;
    //                             use_program_type = this.pos.programs[i].type;
    //                             self.apply_pos_promotion_discount(this.pos.programs[i], self.clear);
    //                             break;
    //                         case 'special_price':
    //                             self.use_program = this.pos.programs[i].id;
    //                             use_program_type = this.pos.programs[i].type;
    //                             self.apply_pos_promotion_special_price(this.pos.programs[i], self.clear);
    //                             break;
    //                         case 'give_product':
    //                             self.use_program = this.pos.programs[i].id;
    //                             use_program_type = this.pos.programs[i].type;
    //                             self.popup_promotion = true;
    //                             self.apply_pos_promotion_give_product(this.pos.programs[i], self.clear);
    //                             break;
    //                         case 'discount_on_cat':
    //                             self.use_program = this.pos.programs[i].id;
    //                             use_program_type = this.pos.programs[i].type;
    //                             self.apply_pos_promotion_discount_on_cat(this.pos.programs[i], self.clear);
    //                             break;
    //                         case 'discount_on_total':
    //                             self.use_program = this.pos.programs[i].id;
    //                             use_program_type = this.pos.programs[i].type;
    //                             self.apply_pos_promotion_discount_on_total(this.pos.programs[i],self.discount_on_total, self.clear);
    //                             break;
    //                         case 'discount_by_qty':
    //                             var new_discount_by_qty = [];
    //                             for(var k = 0 ; k < self.discount_by_qty.length ; k++){
    //                                 if(self.discount_by_qty[k].promotion_program_id == this.pos.programs[i].id){
    //                                     new_discount_by_qty.push(self.discount_by_qty[k]);
    //                                 }
    //                             }
    //                             self.use_program = this.pos.programs[i].id;
    //                             use_program_type = this.pos.programs[i].type;
    //                             self.popup_promotion = true;
    //                             self.apply_pos_promotion_discount_by_qty(this.pos.programs[i],new_discount_by_qty, self.clear);
    //                             break;
    //                         case 'bxgy':
    //                             self.use_program = this.pos.programs[i].id;
    //                             use_program_type = this.pos.programs[i].type;
    //                             self.apply_pos_promotion_bxgy(self.clear);
    //                             break;
    //                     }
    //                 }
    //             }
    //             this.show_product_popup(true);
    //             // console.log('coupon code');
    //             // console.log(self.pos.get_order().get_orderlines());
    //             if(self.order.before_use_coupon.length > 0){
    //                 if(self.order.get_total_with_tax() != self.order.before_use_coupon[0].total || self.order.get_orderlines().length > self.order.before_use_coupon[0].quantity){
    //                     self.order.is_used_coupon_code = true;
    //                     self.order.program_used_by_coupon_code = self.use_program;
    //                     new Model('pos.promotion.program').call('set_coupon_code_left_1',[self.use_program]);
    //                 }
    //             }
    //         }
    //         if(!coupon){
    //             // tab13 sum discount
    //             self.pos.get_order().get_orderlines().forEach(function (line) {
    //                 line.discount_by_pos_promotion = [];
    //                 // line.discount_by_coupon_code = [];
    //                 // line.pos_promotion_message = [];
    //             });
    //             var allLine = self.pos.get_order().get_orderlines();
    //             var spliceIndex = [];
    //             for (var i = 0 ; i < allLine.length ; i++){
    //                 if (allLine[i].pos_promotion_message.length > 0){
    //                     for (var j = 0 ; j < allLine[i].pos_promotion_message.length ; j++){
    //                         if (allLine[i].pos_promotion_message[j].coupon == 0){
    //                             spliceIndex.push(allLine[i].pos_promotion_message[j]);
    //                         }
    //                     }
    //                 }
    //             }
    //             if (spliceIndex.length > 0){
    //                 for (var i = 0 ; i < allLine.length ; i++){
    //                     if (allLine[i].pos_promotion_message.length > 0){
    //                         for (var j = 0 ; j < allLine[i].pos_promotion_message.length ; j++){
    //                             for (var k = 0 ; k < spliceIndex.length ; k++){
    //                                 if (spliceIndex[k].orderline_id == allLine[i].id){
    //                                     var index = allLine[i].pos_promotion_message.indexOf(spliceIndex[k]);
    //                                     if (index >= 0){
    //                                         allLine[i].pos_promotion_message.splice(index,1);
    //                                     }
    //                                 }
    //                             }
    //                         }
    //                     }
    //                 }
    //             }
    //
    //             if (self.pos.get_order().new_order_free_product.length > 0) {
    //                 var re_orderline = self.pos.get_order().get_orderlines();
    //                 var new_order = self.pos.get_order().new_order_free_product;
    //                 for (var i = 0; i < new_order.length; i++) {
    //                     for (var j = 0; j < re_orderline.length; j++) {
    //                         if (re_orderline[j].id == new_order[i].order_line_position) {
    //                             // console.log('remove product coupon 2');
    //                             self.pos.get_order().remove_orderline(re_orderline[j]);
    //                         }
    //                     }
    //                 }
    //             }
    //             // Tab13 coupon code
    //             // if(this.pos.get_order().program_used_by_coupon_code > 0){
    //             //     if (self.pos.get_order().program_used_by_coupon_code > 0){
    //             //         var indexCoupon = -1;
    //             //         for (var i = 0 ; i < self.program_available.length ; i++){
    //             //             if(self.program_available[i].id == self.pos.get_order().program_used_by_coupon_code){
    //             //                 indexCoupon = self.program_available.indexOf(self.program_available[i]);
    //             //                 if(self.program_available[i].type == 'bxgy'){
    //             //                     // var bxgy_programs = this.pos.bxgy_programs;
    //             //                     // var bxgy_bx = this.pos.bxgy_bx;
    //             //                     // var bxgy_gy = this.pos.bxgy_gy;
    //             //                     var bxgy_delete_by_coupon = [];
    //             //                     var bxgy_delete_by_coupon_id = [];
    //             //                     for(var j = 0 ; j < this.pos.bxgy_programs.length ; j++){
    //             //                         if(self.program_available[i].id == this.pos.bxgy_programs[j].id){
    //             //                             bxgy_delete_by_coupon.push(this.pos.bxgy_programs[j]);
    //             //                             bxgy_delete_by_coupon_id.push(self.program_available[i].id);
    //             //                         }
    //             //                     }
    //             //                     if(bxgy_delete_by_coupon.length > 0){
    //             //                         for(var k = 0 ; k < bxgy_delete_by_coupon.length ; k++){
    //             //                             var index = this.pos.bxgy_programs.indexOf(bxgy_delete_by_coupon[k]);
    //             //                             if(index >= 0){
    //             //                                 if(this.pos.get_order().save_delete_coupon_program_bxgy.length > 0){
    //             //                                     this.pos.get_order().save_delete_coupon_program_bxgy = [];
    //             //                                     this.pos.get_order().save_delete_coupon_program_bxgy.push(bxgy_delete_by_coupon[k]);
    //             //                                 }else{
    //             //                                     this.pos.get_order().save_delete_coupon_program_bxgy.push(bxgy_delete_by_coupon[k]);
    //             //                                 }
    //             //                                 this.pos.bxgy_programs.splice(index,1);
    //             //                             }
    //             //                         }
    //             //                     }
    //             //                     if(bxgy_delete_by_coupon_id.length > 0){
    //             //                         for(var k = 0 ; k < bxgy_delete_by_coupon_id.length ; k++){
    //             //                             for(var g = 0 ; g < this.pos.bxgy_bx.length ; g++){
    //             //                                 if(bxgy_delete_by_coupon_id[k] == this.pos.bxgy_bx[g].bxgy_program_id){
    //             //                                     var index = this.pos.bxgy_bx.indexOf(this.pos.bxgy_bx[g]);
    //             //                                     if(index >= 0){
    //             //                                         if(this.pos.get_order().save_delete_coupon_program_bx.length > 0){
    //             //                                             this.pos.get_order().save_delete_coupon_program_bx = [];
    //             //                                             this.pos.get_order().save_delete_coupon_program_bx.push(this.pos.bxgy_bx[g]);
    //             //                                         }else{
    //             //                                             this.pos.get_order().save_delete_coupon_program_bx.push(this.pos.bxgy_bx[g]);
    //             //                                         }
    //             //                                         this.pos.bxgy_bx.splice(index,1);
    //             //                                     }
    //             //                                 }
    //             //                             }
    //             //                         }
    //             //                         for(var k = 0 ; k < bxgy_delete_by_coupon_id.length ; k++){
    //             //                             for(var g = 0 ; g < this.pos.bxgy_gy.length ; g++){
    //             //                                 if(bxgy_delete_by_coupon_id[k] == this.pos.bxgy_gy[g].bxgy_program_id){
    //             //                                     var index = this.pos.bxgy_gy.indexOf(this.pos.bxgy_gy[g]);
    //             //                                     if(index >= 0){
    //             //                                         if(this.pos.get_order().save_delete_coupon_program_gy.length > 0){
    //             //                                             this.pos.get_order().save_delete_coupon_program_gy = [];
    //             //                                             this.pos.get_order().save_delete_coupon_program_gy.push(this.pos.bxgy_gy[g]);
    //             //                                         }else{
    //             //                                             this.pos.get_order().save_delete_coupon_program_gy.push(this.pos.bxgy_gy[g]);
    //             //                                         }
    //             //                                         this.pos.bxgy_gy.splice(index,1);
    //             //                                     }
    //             //                                 }
    //             //                             }
    //             //                         }
    //             //                     }
    //             //                 }
    //             //                 if(this.pos.get_order().save_delete_coupon_program.length > 0){
    //             //                     this.pos.get_order().save_delete_coupon_program = [];
    //             //                     this.pos.get_order().save_delete_coupon_program.push(self.program_available[i]);
    //             //                 }else{
    //             //                     this.pos.get_order().save_delete_coupon_program.push(self.program_available[i]);
    //             //                 }
    //             //                 break;
    //             //             }
    //             //         }
    //             //         if (indexCoupon >= 0) {
    //             //             self.program_available.splice(indexCoupon, 1);
    //             //         }
    //             //     }
    //             // }
    //             // console.log('self.program_available');
    //             // console.log(self.program_available);
    //             // chi lay 1 discount by qty
    //             if (self.priority_discount_by_qty.length > 0) {
    //                 var min_discount_by_qty_program = self.priority_discount_by_qty[0];
    //                 var delete_discount_by_qty_program = [];
    //                 var new_delete_discount_by_qty_program = [];
    //                 for (var i = 0; i < self.priority_discount_by_qty.length; i++) {
    //                     if (self.priority_discount_by_qty[i].sequence < min_discount_by_qty_program.sequence) {
    //                         delete_discount_by_qty_program.push(min_discount_by_qty_program.promotion_program_id);
    //                         min_discount_by_qty_program = self.priority_discount_by_qty[i];
    //                     }
    //                     if (self.priority_discount_by_qty[i].sequence > min_discount_by_qty_program.sequence) {
    //                         delete_discount_by_qty_program.push(self.priority_discount_by_qty[i].promotion_program_id);
    //                     }
    //                 }
    //                 if (delete_discount_by_qty_program.length > 0) {
    //                     for (var i = 0; i < self.program_available.length; i++) {
    //                         for (var j = 0; j < delete_discount_by_qty_program.length; j++) {
    //                             if (delete_discount_by_qty_program[j] == self.program_available[i].id) {
    //                                 new_delete_discount_by_qty_program.push(self.program_available[i]);
    //                             }
    //                         }
    //                     }
    //                     for (var k = 0; k < new_delete_discount_by_qty_program.length; k++) {
    //                         var index = self.program_available.indexOf(new_delete_discount_by_qty_program[k]);
    //                         if (index >= 0) {
    //                             self.program_available.splice(index, 1);
    //                         }
    //                     }
    //                 }
    //             }
    //             // het
    //             for (var i = 0; i < this.pos.priority_category.length; i++) {
    //                 self.priority.push(this.pos.priority_category[i]);
    //             }
    //             var posPromotionConfigModel = new Model('pos.promotion.config.settings');
    //             posPromotionConfigModel.call('get_apply_pos_promotion_program').then(function (return_value) {
    //                 var discount_by_qty_position = -1;
    //                 if (self.program_available.length > 0) {
    //                     // sap xep chuong trinh theo sequence
    //                     for (var i = 0; i < self.program_available.length - 1; i++) {
    //                         for (var j = i + 1; j < self.program_available.length; j++) {
    //                             if (self.program_available[j].sequence < self.program_available[i].sequence) {
    //                                 var temp = self.program_available[i];
    //                                 self.program_available[i] = self.program_available[j];
    //                                 self.program_available[j] = temp;
    //                             }
    //                         }
    //                     }
    //                     for (var i = 0; i < self.program_available.length; i++) {
    //                         if (self.program_available[i].type == 'discount_by_qty') {
    //                             discount_by_qty_position = i;
    //                             // self.new_discount_by_qty = [];
    //                             new_discount_by_qty = [];
    //                             for (var k = 0; k < self.discount_by_qty.length; k++) {
    //                                 if (self.discount_by_qty[k].promotion_program_id == self.program_available[i].id) {
    //                                     // self.new_discount_by_qty.push(self.discount_by_qty[k]);
    //                                     new_discount_by_qty.push(self.discount_by_qty[k]);
    //                                 }
    //                             }
    //                         }
    //                         // if(self.program_available[i].type == 'discount_on_total'){
    //                         //     // self.new_discount_on_total = [];
    //                         //     new_discount_on_total = [];
    //                         //     for(var k = 0 ; k < self.discount_on_total.length ; k++){
    //                         //         if(self.discount_on_total[k].promotion_program_id == self.program_available[i].id){
    //                         //             // self.new_discount_on_total.push(self.discount_on_total[k]);
    //                         //             new_discount_on_total.push(self.discount_on_total[k]);
    //                         //         }
    //                         //     }
    //                         // }
    //                     }
    //                     // het sap xep
    //                     // kiem tra chuong trinh chay theo config
    //                     if (return_value == 3) {
    //                         if(self.program_available.length > 1){
    //                             var program_available_with_coupon = [];
    //                             for(var i = 0 ; i < self.program_available.length ; i++){
    //                                 if(self.program_available[i].coupon_code){
    //                                     program_available_with_coupon.push(self.program_available[i]);
    //                                 }
    //                             }
    //                             if(program_available_with_coupon.length > 0){
    //                                 for(var i = 0 ; i < program_available_with_coupon.length ; i++){
    //                                     var index = self.program_available.indexOf(program_available_with_coupon[i]);
    //                                     if(index >= 0){
    //                                         self.program_available.splice(index,1);
    //                                     }
    //                                 }
    //                             }
    //                             // self.program_available.splice(1,self.program_available.length-1);
    //                         }
    //                     }
    //                     if (return_value == 1) {
    //                         if(self.program_available.length > 1){
    //                             var program_available_but_coupon = [];
    //                             var program_available_with_coupon = [];
    //                             for(var i = 0 ; i < self.program_available.length ; i++){
    //                                 if(!self.program_available[i].coupon_code){
    //                                     program_available_but_coupon.push(self.program_available[i]);
    //                                 }
    //                                 if(self.program_available[i].coupon_code){
    //                                     program_available_with_coupon.push(self.program_available[i]);
    //                                 }
    //                             }
    //                             if(program_available_with_coupon.length > 0){
    //                                 for(var i = 0 ; i < program_available_with_coupon.length ; i++){
    //                                     var index = self.program_available.indexOf(program_available_with_coupon[i]);
    //                                     if(index >= 0){
    //                                         self.program_available.splice(index,1);
    //                                     }
    //                                 }
    //                             }
    //                             if(program_available_but_coupon.length > 1){
    //                                 for(var i = 1 ; i < program_available_but_coupon.length ; i++){
    //                                     var index = self.program_available.indexOf(program_available_but_coupon[i]);
    //                                     if(index >= 0){
    //                                         self.program_available.splice(index,1);
    //                                     }
    //                                 }
    //                             }
    //                             // self.program_available.splice(1,self.program_available.length-1);
    //                         }
    //
    //                         // chi lay 1 total discount
    //                         // if (self.priority_total.length > 0) {
    //                         //     var min = self.priority_total[0];
    //                         //     var delete_total_program = [];
    //                         //     var new_delete_total_program = [];
    //                         //     for (var i = 0; i < self.priority_total.length; i++) {
    //                         //         if (self.priority_total[i].sequence < min.sequence) {
    //                         //             delete_total_program.push(min);
    //                         //             min = self.priority_total[i];
    //                         //         }
    //                         //         if (self.priority_total[i].sequence > min.sequence) {
    //                         //             delete_total_program.push(self.priority_total[i]);
    //                         //         }
    //                         //     }
    //                         //     if (delete_total_program.length > 0) {
    //                         //         for (var k = 0; k < self.program_available.length; k++) {
    //                         //             for (var l = 0; l < delete_total_program.length; l++) {
    //                         //                 if (delete_total_program[l].promotion_program_id == self.program_available[k].id) {
    //                         //                     new_delete_total_program.push(self.program_available[k]);
    //                         //                 }
    //                         //             }
    //                         //         }
    //                         //         for (var k = 0; k < new_delete_total_program.length; k++) {
    //                         //             var index = self.program_available.indexOf(new_delete_total_program[k]);
    //                         //             if (index >= 0) {
    //                         //                 self.program_available.splice(index, 1);
    //                         //             }
    //                         //         }
    //                         //     }
    //                         // }
    //                         // het
    //
    //                         // var priority_products = [];
    //                         // var delete_program_available_id = [];
    //                         // for (var i = 0; i < self.program_available.length; i++) {
    //                         //     if (self.program_available[i].type == 'discount' || self.program_available[i].type == 'special_price' || self.program_available[i].type == 'bxgy' || self.program_available[i].type == 'discount_on_cat') {
    //                         //         if (discount_by_qty_position > i) {
    //                         //             delete_program_available_id.push(self.program_available[discount_by_qty_position]);
    //                         //             break;
    //                         //         }
    //                         //         if (discount_by_qty_position < i && discount_by_qty_position != -1) {
    //                         //             delete_program_available_id.push(self.program_available[i]);
    //                         //         }
    //                         //     }
    //                         // }
    //                         // if (delete_program_available_id.length > 0) {
    //                         //     for (var k = 0; k < delete_program_available_id.length; k++) {
    //                         //         var index = self.program_available.indexOf(delete_program_available_id[k]);
    //                         //         if (index >= 0) {
    //                         //             self.program_available.splice(index, 1);
    //                         //         }
    //                         //     }
    //                         // }
    //                         // delete_program_available_id = [];
    //                         // if (self.priority.length > 0) {
    //                         //     var new_priority = [];
    //                         //     for (var i = 0; i < self.program_available.length; i++) {
    //                         //         for (var j = 0; j < self.priority.length; j++) {
    //                         //             if (self.program_available[i].id == self.priority[j].promotion_program_id) {
    //                         //                 new_priority.push(self.priority[j]);
    //                         //             }
    //                         //         }
    //                         //     }
    //                         //     for (var i = 0; i < self.program_available.length; i++) {
    //                         //         priority_products = [];
    //                         //         for (var j = 0; j < new_priority.length; j++) {
    //                         //             if (self.program_available[i].id == new_priority[j].promotion_program_id) {
    //                         //                 priority_products.push(new_priority[j]);
    //                         //             }
    //                         //         }
    //                         //         for (var j = 0; j < new_priority.length; j++) {
    //                         //             for (var k = 0; k < priority_products.length; k++) {
    //                         //                 if (new_priority[j].is_product && priority_products[k].is_product) {
    //                         //                     if (new_priority[j].product_id == priority_products[k].product_id && new_priority[j].promotion_program_id != priority_products[k].promotion_program_id) {
    //                         //                         if (new_priority[j].sequence > priority_products[k].sequence) {
    //                         //                             delete_program_available_id.push(new_priority[j].promotion_program_id);
    //                         //                         }
    //                         //                     }
    //                         //                 }
    //                         //                 if (new_priority[j].is_product && !priority_products[k].is_product) {
    //                         //                     if (new_priority[j].promotion_program_id != priority_products[k].promotion_program_id && self.pos.db.is_product_in_category(priority_products[k].category_id, new_priority[j].product_id)) {
    //                         //                         if (new_priority[j].sequence > priority_products[k].sequence) {
    //                         //                             delete_program_available_id.push(new_priority[j].promotion_program_id);
    //                         //                         }
    //                         //                     }
    //                         //                 }
    //                         //                 if (!new_priority[j].is_product && !priority_products[k].is_product) {
    //                         //                     if (new_priority[j].promotion_program_id != priority_products[k].promotion_program_id && new_priority[j].category_id != priority_products[k].category_id) {
    //                         //                         if (new_priority[j].sequence > priority_products[k].sequence) {
    //                         //                             delete_program_available_id.push(new_priority[j].promotion_program_id);
    //                         //                         }
    //                         //                     }
    //                         //                 }
    //                         //                 if (!new_priority[j].is_product && priority_products[k].is_product) {
    //                         //                     if (self.pos.db.is_product_in_category(new_priority[j].category_id, priority_products[k].product_id) && new_priority[j].promotion_program_id != priority_products[k].promotion_program_id) {
    //                         //                         if (new_priority[j].sequence > priority_products[k].sequence) {
    //                         //                             delete_program_available_id.push(new_priority[j].promotion_program_id);
    //                         //                         }
    //                         //                     }
    //                         //                 }
    //                         //             }
    //                         //         }
    //                         //     }
    //                         //     // self.delete_program_available(delete_program_available_id,self.program_available);
    //                         //     // co the chi can push vao delete_program_available_id vi tri cua chuong trinh
    //                         //     if (delete_program_available_id.length > 0) {
    //                         //         var new_delete_program_available_id = [];
    //                         //         for (var j = 0; j < self.program_available.length; j++) {
    //                         //             for (var i = 0; i < delete_program_available_id.length; i++) {
    //                         //                 if (delete_program_available_id[i] == self.program_available[j].id) {
    //                         //                     new_delete_program_available_id.push(self.program_available[j]);
    //                         //                 }
    //                         //             }
    //                         //         }
    //                         //         if (new_delete_program_available_id.length > 0) {
    //                         //             for (var k = 0; k < new_delete_program_available_id.length; k++) {
    //                         //                 var index = self.program_available.indexOf(new_delete_program_available_id[k]);
    //                         //                 if (index >= 0) {
    //                         //                     self.program_available.splice(index, 1);
    //                         //                 }
    //                         //             }
    //                         //         }
    //                         //     }
    //                         // }
    //                     }
    //                 }
    //                 // return;
    //                 // het kiem tra chuong trinh
    //
    //                 self.order = self.pos.get_order();
    //                 self.order_orderlines = self.order.get_orderlines();
    //                 self.orderlines = new Array();
    //                 _.each(self.order_orderlines, function (orderline) {
    //                     self.orderlines.push(orderline);
    //                 });
    //
    //                 self.bxgy_gy_list = [];
    //                 self.bxgy_gy_title = [];
    //                 self.product_list = self.pos.db.get_product_by_category(0);
    //
    //                 // if (self.order.new_order_free_product.length > 0) {
    //                 //     var re_orderline = self.order_orderlines;
    //                 //     var new_order = self.order.new_order_free_product;
    //                 //     for (var i = 0; i < new_order.length; i++) {
    //                 //         for (var j = 0; j < re_orderline.length; j++) {
    //                 //             if (re_orderline[j].id == new_order[i].order_line_position) {
    //                 //                 self.order.remove_orderline(re_orderline[j]);
    //                 //             }
    //                 //         }
    //                 //     }
    //                 // }
    //
    //                 self.apply_promotion(new_discount_by_qty, self.discount_on_total);
    //
    //                 // console.log('pos promotion');
    //                 // console.log(self.pos.get_order().get_orderlines());
    //                 // for (var i = 0 ; i < self.pos.get_order().get_orderlines().length ; i++){
    //                 //     var total = 0;
    //                 //     if (self.pos.get_order().get_orderlines()[i].discount_by_pos_promotion.length > 1){
    //                 //         self.pos.get_order().get_orderlines()[i].discount_by_pos_promotion.forEach(function (item) {
    //                 //             total += item.value;
    //                 //         });
    //                 //         console.log(total);
    //                 //     }
    //                 //     if (self.pos.get_order().get_orderlines()[i].discount_by_coupon_code.length > 1){
    //                 //         self.pos.get_order().get_orderlines()[i].discount_by_coupon_code.forEach(function (item) {
    //                 //             total += item.value;
    //                 //         });
    //                 //         console.log(total);
    //                 //     }
    //                 //     self.pos.get_order().get_orderlines()[i].set_discount(total);
    //                 // }
    //
    //                 // Tab13 auto check pos_promotion
    //                 self.pos.get_order().click_pos_promotion = true;
    //                 // self.pos.click_pos_promotion = true;
    //             });
    //         }
    //
    //     },
    //
    //     apply_promotion: function (a,b) {
    //         var self = this;
    //         self.order.pos_promotion_message = [];
    //         var check_bxgy = 0;
    //         self.new_promotion_discount_approved = [];
    //         self.new_promotion_discount_product_approved = [];
    //         // var check_total_program = false;
    //         // for (var i = 0; i < self.program_available.length; i++) {
    //         //     if (self.program_available[i].type == 'discount_on_total'){
    //         //         check_total_program = true;
    //         //         var index = self.program_available.indexOf(self.program_available[i]);
    //         //         if (index >= 0){
    //         //             self.program_available.slice(index,1);
    //         //             self.program_available.push(self.program_available[i]);
    //         //         }
    //         //         break;
    //         //     }
    //         // }
    //         for (var i = 0; i < self.program_available.length; i++) {
    //             // check promotion progam type discount %
    //
    //             if (self.program_available[i].type == 'discount') {
    //                 self.apply_pos_promotion_discount(self.program_available[i]);
    //             }
    //             if (self.program_available[i].type == 'special_price') {
    //                 self.apply_pos_promotion_special_price(self.program_available[i]);
    //             }
    //             if (self.program_available[i].type == 'bxgy') {
    //                 check_bxgy++;
    //             }
    //             if(i == self.program_available.length - 1 && check_bxgy != 0){
    //                 self.apply_pos_promotion_bxgy();
    //             }
    //             // if (program_available[i].type == 'bxpy') {
    //             //     self.apply_pos_promotion_bxpy(program_available[i])
    //             // }
    //             if (self.program_available[i].type == 'give_product') {
    //                 self.apply_pos_promotion_give_product(self.program_available[i]);
    //             }
    //             if (self.program_available[i].type == 'discount_on_cat') {
    //                 self.apply_pos_promotion_discount_on_cat(self.program_available[i]);
    //             }
    //             //discount on total
    //             if (self.program_available[i].type == 'discount_on_total' ) {
    //                 self.apply_pos_promotion_discount_on_total(self.program_available[i],b);
    //             }
    //             if(self.program_available[i].type == 'discount_by_qty'){
    //                 self.apply_pos_promotion_discount_by_qty(self.program_available[i],a);
    //             }
    //         }
    //
    //         this.show_product_popup(false);
    //         //cancel promotion type discount not available
    //         this.cancel_pos_promotion_discount();
    //     },
    //
    //     // start promotion type: discount by qty
    //     apply_pos_promotion_discount_by_qty: function (program,a, clear_coupon) {
    //         var self = this;
    //         // if(typeof (clear_coupon) === 'undefined') clear_coupon = false;
    //         // var product  = this.pos.db.get_product_by_id(program.discount_product_id[0]);
    //         var discount_by_qty = a;
    //
    //         if(self.order_orderlines.length > 0){
    //             var product_price;
    //             var checked_orderline = [];
    //             var product_qty;
    //             var check;
    //
    //             for(var i = 0 ; i < self.order_orderlines.length; i++){
    //                 check = 0;
    //                 product_price = self.order_orderlines[i].product.price;
    //                 product_qty = self.order_orderlines[i].quantity;
    //                 if(checked_orderline.length == 0 && product_price > 0 && product_qty > 0 && self.order_orderlines[i].is_pos_promotion_product != 1){
    //                     checked_orderline.push({
    //                         'orderline_id': self.order_orderlines[i].id,
    //                         'product_id': self.order_orderlines[i].product.id,
    //                         'product_name': self.order_orderlines[i].product.display_name,
    //                         'product_price': self.order_orderlines[i].product.price,
    //                         'product_qty': self.order_orderlines[i].quantity
    //                     });
    //                     continue;
    //                 }
    //                 if(checked_orderline.length > 0 && self.order_orderlines[i].product.price > 0 && product_qty > 0 && self.order_orderlines[i].is_pos_promotion_product != 1){
    //                     checked_orderline.push({
    //                         'orderline_id': self.order_orderlines[i].id,
    //                         'product_id': self.order_orderlines[i].product.id,
    //                         'product_name':self.order_orderlines[i].product.display_name,
    //                         'product_price': self.order_orderlines[i].product.price,
    //                         'product_qty': self.order_orderlines[i].quantity
    //                     });
    //                 }
    //             }
    //             if(checked_orderline.length > 0){
    //                 checked_orderline.sort(function(a,b) {return (a.product_price < b.product_price) ? 1 : ((b.product_price < a.product_price) ? -1 : 0);} );
    //                 for(var j = 0 ; j < checked_orderline.length ; j++){
    //                     if(checked_orderline[j].product_qty > 1){
    //                         for(var s = 1 ; s < checked_orderline[j].product_qty ; s++){
    //                             var clone_orderline = self.order.get_orderline(checked_orderline[j].orderline_id).clone();
    //                             self.order.add_orderline(clone_orderline);
    //                             self.order_orderlines[self.order_orderlines.indexOf(clone_orderline)].set_quantity(1);
    //                             checked_orderline.push({
    //                                 'orderline_id': self.order_orderlines[self.order_orderlines.indexOf(clone_orderline)].id,
    //                                 'product_id': self.order_orderlines[self.order_orderlines.indexOf(clone_orderline)].product.id,
    //                                 'product_name':self.order_orderlines[self.order_orderlines.indexOf(clone_orderline)].product.display_name,
    //                                 'product_price': self.order_orderlines[self.order_orderlines.indexOf(clone_orderline)].product.price,
    //                                 'product_qty': self.order_orderlines[self.order_orderlines.indexOf(clone_orderline)].quantity
    //                             });
    //                         }
    //                         self.order.get_orderline(checked_orderline[j].orderline_id).set_quantity(1);
    //                     }
    //                 }
    //                 checked_orderline.sort(function(a,b) {return (a.product_price < b.product_price) ? 1 : ((b.product_price < a.product_price) ? -1 : 0);} );
    //                 var count_prod=0;
    //                 var message;
    //                 for (var i = 0 ; i < checked_orderline.length ; i++) {
    //                     for(var j = 0 ; j < self.order_orderlines.length ; j++){
    //                         if(self.order_orderlines[j].id == checked_orderline[i].orderline_id){
    //                             // self.order_orderlines[j].set_discount(0);
    //                             count_prod++;
    //                             for(var p = 0 ; p < discount_by_qty.length ; p++){
    //                                 if(count_prod >= discount_by_qty[p].total_qty && typeof discount_by_qty[p+1] != 'undefined' && count_prod < discount_by_qty[p+1].total_qty && self.order_orderlines[j].price > 0){
    //                                     if(typeof (clear_coupon) !== 'undefined' && clear_coupon){
    //                                         for (var k = 0 ; k < self.order_orderlines[j].discount_by_coupon_code.length ; k++){
    //                                             if (self.order_orderlines[j].id == self.order_orderlines[j].discount_by_coupon_code[k].orderline_id){
    //                                                 self.order_orderlines[j].set_discount(self.order_orderlines[j].get_discount() - self.order_orderlines[j].discount_by_coupon_code[k].value);
    //                                                 break;
    //                                             }
    //                                         }
    //                                         // self.order_orderlines[j].set_discount(0);
    //                                         message = null;
    //                                     }else{
    //                                         if (typeof (clear_coupon) === 'undefined'){
    //                                             self.order_orderlines[j].discount_by_pos_promotion.push({
    //                                                 'orderline_id': self.order_orderlines[j].id,
    //                                                 'program_id': program.id,
    //                                                 'value': discount_by_qty[p].value
    //                                             });
    //                                         }
    //                                         if (typeof (clear_coupon) !== 'undefined' && !clear_coupon){
    //                                             self.order_orderlines[j].discount_by_coupon_code.push({
    //                                                 'orderline_id': self.order_orderlines[j].id,
    //                                                 'program_id': program.id,
    //                                                 'value': discount_by_qty[p].value
    //                                             });
    //                                         }
    //                                         self.order_orderlines[j].set_discount(discount_by_qty[p].value);
    //                                         if (!message) {
    //                                             message = program.name;
    //                                         }
    //                                     }
    //                                 }
    //                                 if(count_prod >= discount_by_qty[p].total_qty && p == discount_by_qty.length - 1 && self.order_orderlines[j].price > 0){
    //                                     if(clear_coupon){
    //                                         for (var k = 0 ; k < self.order_orderlines[j].discount_by_coupon_code.length ; k++){
    //                                             if (self.order_orderlines[j].id == self.order_orderlines[j].discount_by_coupon_code[k].orderline_id){
    //                                                 self.order_orderlines[j].set_discount(self.order_orderlines[j].get_discount() - self.order_orderlines[j].discount_by_coupon_code[k].value);
    //                                                 break;
    //                                             }
    //                                         }
    //                                         // self.order_orderlines[j].set_discount(0);
    //                                         message = null;
    //                                     }else{
    //                                         if (typeof (clear_coupon) === 'undefined'){
    //                                             self.order_orderlines[j].discount_by_pos_promotion.push({
    //                                                 'orderline_id': self.order_orderlines[j].id,
    //                                                 'program_id': program.id,
    //                                                 'value': discount_by_qty[p].value
    //                                             });
    //                                         }
    //                                         if (typeof (clear_coupon) !== 'undefined' && !clear_coupon){
    //                                             self.order_orderlines[j].discount_by_coupon_code.push({
    //                                                 'orderline_id': self.order_orderlines[j].id,
    //                                                 'program_id': program.id,
    //                                                 'value': discount_by_qty[p].value
    //                                             });
    //                                         }
    //                                         self.order_orderlines[j].set_discount(discount_by_qty[p].value);
    //                                         if (!message) {
    //                                             message = program.name;
    //                                         }
    //                                     }
    //                                 }
    //                             }
    //                         }
    //                     }
    //                 }
    //                 if(!clear_coupon){
    //                     self.order.pos_promotion_message.push(message);
    //                 }
    //             }
    //         }
    //     },
    //     // end promotion type: discount by qty
    //
    //     //start promotion type: discount
    //     get_product_promotion_discount: function (promotion_id) {
    //         var self = this;
    //         var pos_promotion_product_discount = self.pos.pos_promotion_product_discount;
    //         var promotion_product = [];
    //         for (var i = 0; i < pos_promotion_product_discount.length; i++) {
    //             if (pos_promotion_product_discount[i].promotion_program_id == promotion_id) {
    //                 promotion_product.push(pos_promotion_product_discount[i]);
    //             }
    //         }
    //         return promotion_product;
    //     },
    //     apply_pos_promotion_discount: function (program, clear_coupon) {
    //         var self = this;
    //         // if(typeof (clear_coupon) === 'undefined') clear_coupon = false;
    //         var pos_promotion_product_discount = self.get_product_promotion_discount(program.id);
    //         var orderlines = self.orderlines;
    //         self.promotions_product_discount_apply = [];
    //         for (var condition_count = 0; condition_count < pos_promotion_product_discount.length; condition_count++) {
    //             var apply = true;
    //             var min_qty = pos_promotion_product_discount[condition_count].min_qty;
    //             var product_id = pos_promotion_product_discount[condition_count].product_id;
    //             var p_qty = 0;
    //             var orderline_ids = [];
    //             for (var item_count = 0; item_count < orderlines.length; item_count++) {
    //                 if (orderlines[item_count].product.id == product_id) {
    //                     p_qty = parseFloat(p_qty) + parseFloat(orderlines[item_count].quantity);
    //                     orderline_ids.push(orderlines[item_count].id);
    //                 }
    //             }
    //             if (parseFloat(p_qty) <= 0 || parseFloat(p_qty) < parseFloat(min_qty)) {
    //                 apply = false;
    //                 // break;
    //             }
    //             if (apply === true) {
    //                 apply = false;
    //                 for (var j = 0; j < orderline_ids.length; j++) {
    //                     self.promotions_product_discount_apply.push({
    //                         'orderline_id': orderline_ids[j],
    //                         'discount': pos_promotion_product_discount[condition_count].discount
    //                     });
    //                 }
    //             }
    //         }
    //         var message = [];
    //         var coupon = 0;
    //         if (typeof (clear_coupon) !== 'undefined' && !clear_coupon){
    //             coupon = 1;
    //         }
    //         if (self.promotions_product_discount_apply) {
    //             for (var i = 0; i < orderlines.length; i++) {
    //                 for (var j = 0; j < self.promotions_product_discount_apply.length; j++) {
    //                     if (self.promotions_product_discount_apply[j].orderline_id == orderlines[i].id) {
    //                         if(clear_coupon){
    //                             for (var k = 0 ; k < orderlines[i].pos_promotion_message.length ; k++){
    //                                 if (orderlines[i].id == orderlines[i].pos_promotion_message[k].orderline_id && orderlines[i].pos_promotion_message[k].coupon == 1){
    //                                     var index = orderlines[i].pos_promotion_message.indexOf(orderlines[i].pos_promotion_message[k]);
    //                                     if (index >= 0){
    //                                         orderlines[i].pos_promotion_message.splice(index,1);
    //                                         break;
    //                                     }
    //                                 }
    //                             }
    //                             // orderlines[i].pos_promotion_message = null;
    //                             for (var k = 0 ; k < orderlines[i].discount_by_coupon_code.length ; k++){
    //                                 if (orderlines[i].id == orderlines[i].discount_by_coupon_code[k].orderline_id){
    //                                     orderlines[i].set_discount(orderlines[i].get_discount() - orderlines[i].discount_by_coupon_code[k].value);
    //                                     break;
    //                                 }
    //                             }
    //                             // orderlines[i].set_discount(0);
    //                         }else{
    //                             if( typeof orderlines[i].pos_promotion_message != 'undefined'){
    //                                 // message = orderlines[i].pos_promotion_message;
    //                                 orderlines[i].pos_promotion_message.push({
    //                                     'coupon': coupon,
    //                                     'orderline_id': orderlines[i].id,
    //                                     'message': program.name + ': discount ' + self.promotions_product_discount_apply[j].discount + '%'
    //                                 });
    //                                 // orderlines[i].pos_promotion_message = program.name + ': discount ' + self.promotions_product_discount_apply[j].discount + '%';
    //                             }else{
    //                                 orderlines[i].pos_promotion_message.push({
    //                                     'coupon': coupon,
    //                                     'orderline_id': orderlines[i].id,
    //                                     'message': program.name + ': discount ' + self.promotions_product_discount_apply[j].discount + '%'
    //                                 });
    //                                 // orderlines[i].pos_promotion_message = program.name + ': discount ' + self.promotions_product_discount_apply[j].discount + '%';
    //                             }
    //                             if (typeof (clear_coupon) === 'undefined'){
    //                                 orderlines[i].discount_by_pos_promotion.push({
    //                                     'orderline_id': orderlines[i].id,
    //                                     'program_id': program.id,
    //                                     'value': self.promotions_product_discount_apply[j].discount
    //                                 });
    //                             }
    //                             if (typeof (clear_coupon) !== 'undefined' && !clear_coupon){
    //                                 orderlines[i].discount_by_coupon_code.push({
    //                                     'orderline_id': orderlines[i].id,
    //                                     'program_id': program.id,
    //                                     'value': self.promotions_product_discount_apply[j].discount
    //                                 });
    //                             }
    //                             orderlines[i].set_discount(self.promotions_product_discount_apply[j].discount);
    //                             self.new_promotion_discount_product_approved.push(orderlines[i].id);
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     },
    //     cancel_pos_promotion_discount: function () {
    //         var self = this;
    //         var orderlines = self.orderlines;
    //         var old_promotion_discount_approved = self.pos.promotion_discount_approved;
    //         var old_promotion_discount_product_approved = self.pos.promotion_discount_product_approved;
    //         if (old_promotion_discount_approved) {
    //             var new_promotion_discount_approve = self.new_promotion_discount_approved;
    //         }
    //         if (old_promotion_discount_product_approved) {
    //             var new_promotion_discount_product_approved = self.new_promotion_discount_product_approved;
    //             var cancel_product = [];
    //             for (var i = 0; i < old_promotion_discount_product_approved.length; i++) {
    //                 if (new_promotion_discount_product_approved.indexOf(old_promotion_discount_product_approved[i]) == -1) {
    //                     cancel_product.push(old_promotion_discount_product_approved[i]);
    //                     delete new_promotion_discount_product_approved[new_promotion_discount_product_approved.indexOf(old_promotion_discount_product_approved[i])];
    //                     // new_promotion_discount_product_approved.splice(new_promotion_discount_product_approved.indexOf(old_promotion_discount_product_approved[i]), 1);
    //                 }
    //             }
    //             self.pos.promotion_discount_product_approved = new_promotion_discount_product_approved;
    //             if (cancel_product) {
    //                 for (var i = 0; i < orderlines.length; i++) {
    //                     if (cancel_product.indexOf(orderlines[i].id) != -1) {
    //                         orderlines[i].pos_promotion_message = null;
    //                         orderlines[i].set_discount(0);
    //                     }
    //                 }
    //             }
    //         }
    //     },
    //     //end promotion type: discount
    //
    //     //start promotion type: Buy X Pay Y
    //     apply_pos_promotion_bxpy: function (program) {
    //         return;
    //         var self = this;
    //         var pos_promotion_product_discount = self.get_product_promotion_discount(program.id);
    //         var orderlines = self.orderlines;
    //         self.promotions_product_discount_apply = [];
    //         for (var condition_count = 0; condition_count < pos_promotion_product_discount.length; condition_count++) {
    //             var apply = true;
    //             var min_qty = pos_promotion_product_discount[condition_count].min_qty;
    //             var product_id = pos_promotion_product_discount[condition_count].product_id;
    //             var p_qty = 0;
    //             var orderline_ids = [];
    //             for (var item_count = 0; item_count < orderlines.length; item_count++) {
    //                 if (orderlines[item_count].product.id == product_id) {
    //                     p_qty = parseFloat(p_qty) + parseFloat(orderlines[item_count].quantity);
    //                     orderline_ids.push(orderlines[item_count].id);
    //                 }
    //             }
    //             if (parseFloat(p_qty) <= 0 || parseFloat(p_qty) < parseFloat(min_qty)) {
    //                 apply = false;
    //                 break;
    //             }
    //             if (apply === true) {
    //                 apply = false;
    //                 for (var j = 0; j < orderline_ids.length; j++) {
    //                     self.promotions_product_discount_apply.push({
    //                         'orderline_id': orderline_ids[j],
    //                         'discount': pos_promotion_product_discount[condition_count].discount
    //                     });
    //                 }
    //             }
    //         }
    //         if (self.promotions_product_discount_apply) {
    //             for (var i = 0; i < orderlines.length; i++) {
    //                 for (var j = 0; j < self.promotions_product_discount_apply.length; j++) {
    //                     if (self.promotions_product_discount_apply[j].orderline_id == orderlines[i].id) {
    //                         orderlines[i].set_discount(self.promotions_product_discount_apply[j].discount);
    //                         self.new_promotion_discount_product_approved.push(orderlines[i].id);
    //                     }
    //                 }
    //             }
    //             if (program_available[i].type == 'discount_on_cat') {
    //                 self.apply_pos_promotion_discount_on_cat();
    //             }
    //         }
    //     },
    //     //end Buy X pay Y
    //
    //     //
    //     show_product_popup:function (coupon) {
    //         var self = this;
    //         if(!coupon && self.pos.get_order().program_used_by_coupon_code > 0){
    //             var index = -1;
    //             for(var i = 0 ; i < self.bxgy_gy_title.length ; i++){
    //                 if(self.pos.get_order().program_used_by_coupon_code == self.bxgy_gy_title[i].id){
    //                     index = self.bxgy_gy_title.indexOf(self.bxgy_gy_title[i]);
    //                     break;
    //                 }
    //             }
    //             if(index >= 0){
    //                 self.bxgy_gy_title.splice(index,1);
    //             }
    //             var indexCoupon = [];
    //             for(var i = 0 ; i < self.bxgy_gy_list.length ; i++){
    //                 if(self.pos.get_order().program_used_by_coupon_code == self.bxgy_gy_list[i].product_bxgy_id){
    //                     indexCoupon.push(self.bxgy_gy_list[i]);
    //                 }
    //             }
    //             for(var i = 0 ; i < indexCoupon.length ; i++){
    //                 var index = self.bxgy_gy_list.indexOf(indexCoupon[i]);
    //                 if (index >= 0) {
    //                     self.bxgy_gy_list.splice(index, 1);
    //                 }
    //             }
    //         }
    //         if(self.bxgy_gy_title.length > 0){
    //             this.gui.show_popup('getfreeproduct',{
    //                 'list_title':self.bxgy_gy_title,
    //                 'is_coupon':coupon,
    //                 list: self.bxgy_gy_list,
    //                 choose_title: function (item) {
    //                     var free_product_by_program = [];
    //                     for(var i = 0 ; i < self.bxgy_gy_list.length ; i++){
    //                         if(item.id == self.bxgy_gy_list[i].product_bxgy_id){
    //                             free_product_by_program.push(self.bxgy_gy_list[i]);
    //                         }
    //                     }
    //                     return free_product_by_program;
    //                 },
    //                 ok: function(item,item1,item2) {
    //                     if(item2){
    //                         self.order.new_free_product_chosen_count_coupon = item;
    //                         self.order.new_free_product_chosen_title_coupon = item1;
    //                         if(self.order.new_order_free_product_coupon.length > 0){
    //                             var re_orderline = self.order_orderlines;
    //                             var new_order = self.order.new_order_free_product_coupon;
    //                             for(var i = 0 ; i < new_order.length ; i++){
    //                                 for(var j = 0 ; j < re_orderline.length ; j++){
    //                                     if(re_orderline[j].id == new_order[i].order_line_position && !re_orderline[j].is_product_by_coupon_code){
    //                                         // console.log('remove product coupon 3');
    //                                         self.order.remove_orderline(re_orderline[j]);
    //                                     }
    //                                 }
    //                             }
    //                         }
    //                     }else{
    //                         self.order.new_free_product_chosen_count = item;
    //                         self.order.new_free_product_chosen_title = item1;
    //                         if(self.order.new_order_free_product.length > 0){
    //                             var re_orderline = self.order_orderlines;
    //                             var new_order = self.order.new_order_free_product;
    //                             for(var i = 0 ; i < new_order.length ; i++){
    //                                 for(var j = 0 ; j < re_orderline.length ; j++){
    //                                     if(re_orderline[j].id == new_order[i].order_line_position && !re_orderline[j].is_product_by_coupon_code){
    //                                         // console.log('remove product coupon 4');
    //                                         self.order.remove_orderline(re_orderline[j]);
    //                                     }
    //                                 }
    //                             }
    //                         }
    //                     }
    //
    //                     var message = [];
    //                     for(var i = 0 ; i < item.length ; i++){
    //                         for(var j = 0 ; j < self.bxgy_gy_list.length ; j++){
    //                             if(item[i].product_id == self.bxgy_gy_list[j].product_list.id && item[i].program_id == self.bxgy_gy_list[j].product_bxgy_id){
    //                                 self.order.add_product(self.pos.db.get_product_by_id(item[i].product_id));
    //                                 var last_orderline = self.order_orderlines[self.order_orderlines.length-1];
    //                                 if(item2){
    //                                     self.order.new_order_free_product_coupon.push({
    //                                         order_line_position : last_orderline.id,
    //                                         bxgy_program_id : item[i].program_id
    //                                     });
    //                                 }else{
    //                                     self.order.new_order_free_product.push({
    //                                         order_line_position : last_orderline.id,
    //                                         bxgy_program_id : item[i].program_id
    //                                     });
    //                                 }
    //                                 if( typeof last_orderline.pos_promotion_message != 'undefined'){
    //                                     // message = self.orderlines[i].pos_promotion_message;
    //                                     last_orderline.pos_promotion_message.push({
    //                                         'message': self.bxgy_gy_list[j].product_bxgy_name
    //                                     });
    //                                     // last_orderline.pos_promotion_message = self.bxgy_gy_list[j].product_bxgy_name;
    //                                 }else{
    //                                     last_orderline.pos_promotion_message.push({
    //                                         'message': self.bxgy_gy_list[j].product_bxgy_name
    //                                     });
    //                                     // last_orderline.pos_promotion_message = self.bxgy_gy_list[j].product_bxgy_name;
    //                                 }
    //                                 last_orderline.is_pos_promotion_product = 1;
    //                                 last_orderline.set_unit_price(0);
    //                             }
    //                         }
    //                     }
    //                     // self.pos.get_order().show_pop_up_promotion = true;
    //                     // self.pos.show_pop_up_promotion = true;
    //                     self.pos.get_order().click_pos_promotion = true;
    //                     // self.pos.click_pos_promotion = true;
    //                     // tab13 coupon code
    //                     if(coupon){
    //                         if(self.order.before_use_coupon.length > 0 && !self.order.is_used_coupon_code){
    //                             if(self.order.get_total_with_tax() != self.order.before_use_coupon[0].total || self.order.get_orderlines().length > self.order.before_use_coupon[0].quantity){
    //                                 self.order.is_used_coupon_code = true;
    //                                 self.order.program_used_by_coupon_code = self.use_program;
    //                                 new Model('pos.promotion.program').call('set_coupon_code_left_1',[self.use_program]);
    //                             }
    //                             // console.log('before_use_coupon1');
    //                             // console.log(self.order.is_used_coupon_code);
    //                             // console.log(self.order.program_used_by_coupon_code);
    //                             // self.pos.get_order().show_pop_up_promotion = false;
    //                             // self.pos.show_pop_up_promotion = false;
    //                             self.pos.get_order().click_pos_promotion = false;
    //                             // self.pos.click_pos_promotion = false;
    //                         }
    //                     }
    //                 },
    //                 cancel: function(){
    //                     // user chose nothing
    //                 }
    //             });
    //         }
    //         if(!coupon && self.bxgy_gy_list.length == 0 && self.order.new_order_free_product.length > 0){
    //             var re_orderline = self.order_orderlines;
    //             var new_order = self.order.new_order_free_product;
    //             for(var i = 0 ; i < new_order.length ; i++){
    //                 for(var j = 0 ; j < re_orderline.length ; j++){
    //                     if(re_orderline[j].id == new_order[i].order_line_position){
    //                         // console.log('remove product coupon 5');
    //                         self.order.remove_orderline(re_orderline[j]);
    //                     }
    //                 }
    //             }
    //         }
    //         if(coupon && self.bxgy_gy_list.length == 0 && self.order.new_order_free_product_coupon.length > 0){
    //             var re_orderline = self.order_orderlines;
    //             var new_order = self.order.new_order_free_product_coupon;
    //             for(var i = 0 ; i < new_order.length ; i++){
    //                 for(var j = 0 ; j < re_orderline.length ; j++){
    //                     if(re_orderline[j].id == new_order[i].order_line_position){
    //                         // console.log('remove product coupon 6');
    //                         self.order.remove_orderline(re_orderline[j]);
    //                     }
    //                 }
    //             }
    //         }
    //
    //         // tab13 sum discount
    //         // console.log('pos promotion');
    //         // console.log(self.pos.get_order().get_orderlines());
    //         self.pos.get_order().get_orderlines().forEach(function (line) {
    //             var total = 0;
    //             if (line.discount_by_pos_promotion.length >= 1){
    //                 line.discount_by_pos_promotion.forEach(function (item) {
    //                     total += item.value;
    //                 });
    //                 // console.log(total);
    //             }
    //             if (line.discount_by_coupon_code.length >= 1){
    //                 line.discount_by_coupon_code.forEach(function (item) {
    //                     total += item.value;
    //                 });
    //                 // console.log(total);
    //             }
    //             line.set_discount(total);
    //         });
    //     },
    //     //
    //
    //     //Start give product
    //     get_product_promotion_give_product: function (promotion_id) {
    //         var self = this;
    //         var all_programs = self.pos.programs;
    //         var pos_promotion_product_give_product = self.pos.pos_promotion_product_give_product;
    //         var promotion_product = [];
    //         for(var k = 0 ; k < self.product_list.length ; k++){
    //             for (var i = 0; i < all_programs.length; i++) {
    //                 for(var j = 0 ; j <pos_promotion_product_give_product.length ; j++){
    //                     if(all_programs[i].id == promotion_id && pos_promotion_product_give_product[j].promotion_program_id == promotion_id){
    //                         if(self.product_list[k].id == pos_promotion_product_give_product[j].product_id){
    //                             self.bxgy_gy_list.push({
    //                                 product_bxgy_id: all_programs[i].id,
    //                                 product_bxgy_name: all_programs[i].name,
    //                                 product_list : self.product_list[k]
    //                             });
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     },
    //     apply_pos_promotion_give_product: function (program, clear_coupon) {
    //         var self = this;
    //         if(typeof (clear_coupon) === 'undefined') clear_coupon = false;
    //         if (clear_coupon) {
    //             var re_orderlinee = self.pos.get_order().get_orderlines();
    //             var new_orderr = self.pos.get_order().new_order_free_product_coupon;
    //             for (var i = 0; i < new_orderr.length; i++) {
    //                 for (var j = 0; j < re_orderlinee.length; j++) {
    //                     if (re_orderlinee[j].id == new_orderr[i].order_line_position) {
    //                         // console.log('remove product coupon 1');
    //                         self.pos.get_order().remove_orderline(re_orderlinee[j]);
    //                     }
    //                 }
    //             }
    //             if (this.pos.get_order().new_free_product_chosen_title_coupon.length > 0){
    //                 this.pos.get_order().new_free_product_chosen_title_coupon = [];
    //             }
    //             if (this.pos.get_order().new_free_product_chosen_count_coupon.length > 0){
    //                 this.pos.get_order().new_free_product_chosen_count_coupon = [];
    //             }
    //         }
    //
    //         var order = self.order;
    //         var min_total_order = program.total_order;
    //         var current_total_order = order.get_total_with_tax();
    //         if (parseFloat(current_total_order) >= parseFloat(min_total_order)) {
    //             self.bxgy_gy_title.push({
    //                 id : program.id,
    //                 name : program.name,
    //                 chosen_product : program.number_select_gift,
    //                 count_product : 0
    //             });
    //             self.get_product_promotion_give_product(program.id);
    //         }
    //         if(clear_coupon){
    //             self.bxgy_gy_title = [];
    //             self.bxgy_gy_list = [];
    //         }
    //     },
    //     //end promotion give product
    //
    //     // start promotion buy x get y product
    //     apply_pos_promotion_bxgy: function (clear_coupon) {
    //         var self = this;
    //         if(typeof (clear_coupon) === 'undefined') clear_coupon = false;
    //         if (clear_coupon) {
    //             var re_orderlinee = self.pos.get_order().get_orderlines();
    //             var new_orderr = self.pos.get_order().new_order_free_product_coupon;
    //             for (var i = 0; i < new_orderr.length; i++) {
    //                 for (var j = 0; j < re_orderlinee.length; j++) {
    //                     if (re_orderlinee[j].id == new_orderr[i].order_line_position) {
    //                         // console.log('remove product coupon 1');
    //                         self.pos.get_order().remove_orderline(re_orderlinee[j]);
    //                     }
    //                 }
    //             }
    //             if (this.pos.get_order().new_free_product_chosen_title_coupon.length > 0){
    //                 this.pos.get_order().new_free_product_chosen_title_coupon = [];
    //             }
    //             if (this.pos.get_order().new_free_product_chosen_count_coupon.length > 0){
    //                 this.pos.get_order().new_free_product_chosen_count_coupon = [];
    //             }
    //         }
    //
    //         // var order = this.pos.get_order();
    //         var orderlines = self.order.get_orderlines();
    //         if(orderlines.length > 0){
    //
    //             // gop san pham trong order
    //             var orderlines_object = [];
    //             var check;
    //             for(var i = 0 ; i < orderlines.length ; i++){
    //                 check = false;
    //                 if(i == 0){
    //                     orderlines_object.push({
    //                         'id': orderlines[i].product.id,
    //                         'qty': orderlines[i].quantity,
    //                         'is_promotion_product': orderlines[i].is_pos_promotion_product
    //                     });
    //                     continue;
    //                 }
    //                 for(var j = 0 ; j < orderlines_object.length ; j++) {
    //                     if (orderlines_object[j].id == orderlines[i].product.id) {
    //                         orderlines_object[j].qty += orderlines[i].quantity;
    //                         check = true;
    //                         break;
    //                     }
    //                 }
    //                 if(!check){
    //                     orderlines_object.push({
    //                         'id': orderlines[i].product.id,
    //                         'qty': orderlines[i].quantity,
    //                         'is_promotion_product': orderlines[i].is_pos_promotion_product
    //                     });
    //                 }
    //             }
    //
    //             // kiem tra san pham theo chuong trinh khuyen mai
    //             var bxgy_programs = this.pos.bxgy_programs;
    //             var bxgy_bx = this.pos.bxgy_bx;
    //             var bxgy_gy = this.pos.bxgy_gy;
    //             var check_bxgy_bx,bxgy_bx_total;
    //             var bxgy_bx_product = false,bxgy_bx_product_index;
    //             for(var i = 0 ; i <  bxgy_programs.length; i++){
    //                 check_bxgy_bx = true;
    //                 bxgy_bx_total = 0;
    //                 for(var j = 0 ; j < bxgy_bx.length ; j++){
    //                     bxgy_bx_product = false;
    //                     if(bxgy_programs[i].id == bxgy_bx[j].bxgy_program_id){
    //                         // kiem tra ton tai
    //                         for(var k = 0 ; k < orderlines_object.length ; k++){
    //                             if(bxgy_bx[j].id == orderlines_object[k].id && orderlines_object[k].is_promotion_product != 1){
    //                                 bxgy_bx_product = true;
    //                                 bxgy_bx_product_index = k;
    //                                 break;
    //                             }
    //                         }
    //                         // kiem tra min qty
    //                         if(bxgy_bx_product){
    //                             if(bxgy_bx[j].qty <= orderlines_object[bxgy_bx_product_index].qty) {
    //                                     bxgy_bx_total++;
    //                             }else{
    //                                 check_bxgy_bx = false;
    //                                 break;
    //                             }
    //                         }
    //                         if(!bxgy_bx_product){
    //                             check_bxgy_bx = false;
    //                             break;
    //                         }
    //                         if(check_bxgy_bx && bxgy_bx_total == bxgy_programs[i].pos_promotion_bxgy_buy.length){
    //                             break;
    //                         }
    //                     }
    //                 }
    //                 if(check_bxgy_bx){
    //                     for(var k = 0 ; k < self.product_list.length ; k++){
    //                         for(var j = 0 ; j < bxgy_gy.length ; j++){
    //                             if(bxgy_programs[i].id == bxgy_gy[j].bxgy_program_id){
    //                                 if(self.product_list[k].id == bxgy_gy[j].id){
    //                                     self.bxgy_gy_list.push({
    //                                         product_bxgy_id: bxgy_programs[i].id,
    //                                         product_bxgy_name: bxgy_programs[i].name,
    //                                         product_list : self.product_list[k]
    //                                     });
    //                                 }
    //                             }
    //                         }
    //                     }
    //                     self.bxgy_gy_title.push({
    //                         id : bxgy_programs[i].id,
    //                         name : bxgy_programs[i].name,
    //                         chosen_product : bxgy_programs[i].number_select_free,
    //                         count_product : 0
    //                     });
    //                 }
    //             }
    //             if (clear_coupon){
    //                 self.bxgy_gy_list = [];
    //                 self.bxgy_gy_title = [];
    //             }
    //         }
    //     },
    //     // end promotion buy x get y product
    //
    //     //start promotion type special price
    //     get_product_promotion_special_price: function (promotion_id) {
    //         var self = this;
    //         var pos_promotion_special_price = self.pos.special_prices;
    //         var promotion_product = [];
    //         for (var i = 0; i < pos_promotion_special_price.length; i++) {
    //             if (pos_promotion_special_price[i].promotion_program_id == promotion_id) {
    //                 promotion_product.push(pos_promotion_special_price[i]);
    //             }
    //         }
    //         return promotion_product;
    //     },
    //     apply_pos_promotion_special_price: function (program, clear_coupon) {
    //         var self = this;
    //         // if(typeof (clear_coupon) === 'undefined') clear_coupon = false;
    //         var orderlines = self.orderlines;
    //         var promotion_product = self.get_product_promotion_special_price(program.id);
    //         var message = [];
    //         var coupon = 0;
    //         if (typeof (clear_coupon) !== 'undefined' && !clear_coupon){
    //             coupon = 1;
    //         }
    //         for (var i=0; i<promotion_product.length; i++) {
    //             for (var item_count = 0; item_count < orderlines.length; item_count++) {
    //                 if (orderlines[item_count].product.id == promotion_product[i].product_id && orderlines[item_count].quantity > 0) {
    //                     if(clear_coupon){
    //                         for (var k = 0 ; k < orderlines[item_count].pos_promotion_message.length ; k++){
    //                             if (orderlines[item_count].id == orderlines[item_count].pos_promotion_message[k].orderline_id && orderlines[item_count].pos_promotion_message[k].coupon == 1){
    //                                 var index = orderlines[item_count].pos_promotion_message.indexOf(orderlines[item_count].pos_promotion_message[k]);
    //                                 if (index >= 0){
    //                                     orderlines[item_count].pos_promotion_message.splice(index,1);
    //                                     break;
    //                                 }
    //                             }
    //                         }
    //                         // orderlines[item_count].pos_promotion_message = null;
    //                         orderlines[item_count].set_unit_price(orderlines[item_count].product.price);
    //                     }else{
    //                         if( typeof orderlines[item_count].pos_promotion_message != 'undefined'){
    //                             // message = orderlines[i].pos_promotion_message;
    //                             orderlines[item_count].pos_promotion_message.push({
    //                                 'coupon': coupon,
    //                                 'orderline_id': orderlines[item_count].id,
    //                                 'message': program.name + ': special price ' + self.chrome.format_currency(promotion_product[i].spec_price)
    //                             });
    //                             // orderlines[item_count].pos_promotion_message = program.name + ': special price ' + self.chrome.format_currency(promotion_product[i].spec_price);
    //                         }else{
    //                             orderlines[item_count].pos_promotion_message.push({
    //                                 'coupon': coupon,
    //                                 'orderline_id': orderlines[item_count].id,
    //                                 'message': program.name + ': special price ' + self.chrome.format_currency(promotion_product[i].spec_price)
    //                             });
    //                             // orderlines[item_count].pos_promotion_message = program.name + ': special price ' + self.chrome.format_currency(promotion_product[i].spec_price);
    //                         }
    //                         orderlines[item_count].set_unit_price(promotion_product[i].spec_price);
    //                     }
    //                 }
    //             }
    //         }
    //     },
    //     //end promotion type special price
    //
    //     //start promotion type discount on cat
    //     get_category_promotion_discount_on_cat: function (promotion_id) {
    //         var self = this;
    //         var pos_promotion_discount_on_cat = self.pos.discount_on_cat;
    //         var promotion_category = [];
    //         for (var i = 0; i < pos_promotion_discount_on_cat.length; i++) {
    //             if (pos_promotion_discount_on_cat[i].promotion_program_id == promotion_id) {
    //                 promotion_category.push(pos_promotion_discount_on_cat[i]);
    //             }
    //         }
    //         return promotion_category;
    //     },
    //
    //     apply_pos_promotion_discount_on_cat: function (program, clear_coupon) {
    //         var self = this;
    //         // if(typeof (clear_coupon) === 'undefined') clear_coupon = false;
    //         var orderlines = self.orderlines;
    //         var promotion_category = self.get_category_promotion_discount_on_cat(program.id);
    //         var message = [];
    //         var coupon = 0;
    //         if (typeof (clear_coupon) !== 'undefined' && !clear_coupon){
    //             coupon = 1;
    //         }
    //         for (var i=0; i<promotion_category.length; i++) {
    //             for (var item_count = 0; item_count < orderlines.length; item_count++) {
    //                 if (orderlines[item_count].product.pos_categ_id[0] == promotion_category[i].category_id && orderlines[item_count].quantity > 0) {
    //                     if (promotion_category[i].type == 'percent') {
    //                         if(clear_coupon){
    //                             for (var k = 0 ; k < orderlines[item_count].pos_promotion_message.length ; k++){
    //                                 if (orderlines[item_count].id == orderlines[item_count].pos_promotion_message[k].orderline_id && orderlines[item_count].pos_promotion_message[k].coupon == 1){
    //                                     var index = orderlines[item_count].pos_promotion_message.indexOf(orderlines[item_count].pos_promotion_message[k]);
    //                                     if (index >= 0){
    //                                         orderlines[item_count].pos_promotion_message.splice(index,1);
    //                                         break;
    //                                     }
    //                                 }
    //                             }
    //                             // orderlines[item_count].pos_promotion_message = null;
    //                             for (var k = 0 ; k < orderlines[item_count].discount_by_coupon_code.length ; k++){
    //                                 if (orderlines[item_count].id == orderlines[item_count].discount_by_coupon_code[k].orderline_id){
    //                                     orderlines[item_count].set_discount(orderlines[item_count].get_discount() - orderlines[item_count].discount_by_coupon_code[k].value);
    //                                     break;
    //                                 }
    //                             }
    //                             // orderlines[item_count].set_discount(0);
    //                         }else{
    //                             if( typeof orderlines[item_count].pos_promotion_message != 'undefined'){
    //                                 // message = orderlines[item_count].pos_promotion_message;
    //                                 orderlines[item_count].pos_promotion_message.push({
    //                                     'coupon': coupon,
    //                                     'orderline_id': orderlines[item_count].id,
    //                                     'message': program.name + ': discount ' + promotion_category[i].value + '%'
    //                                 });
    //                                 // orderlines[item_count].pos_promotion_message = program.name + ': discount ' + promotion_category[i].value + '%';
    //                             }else{
    //                                 orderlines[item_count].pos_promotion_message.push({
    //                                     'coupon': coupon,
    //                                     'orderline_id': orderlines[item_count].id,
    //                                     'message': program.name + ': discount ' + promotion_category[i].value + '%'
    //                                 });
    //                                 // orderlines[item_count].pos_promotion_message = program.name + ': discount ' + promotion_category[i].value + '%';
    //                             }
    //                             if (typeof (clear_coupon) === 'undefined'){
    //                                 orderlines[item_count].discount_by_pos_promotion.push({
    //                                     'orderline_id': orderlines[item_count].id,
    //                                     'program_id': program.id,
    //                                     'value': promotion_category[i].value
    //                                 });
    //                             }
    //                             if (typeof (clear_coupon) !== 'undefined' && !clear_coupon){
    //                                 orderlines[item_count].discount_by_coupon_code.push({
    //                                     'orderline_id': orderlines[item_count].id,
    //                                     'program_id': program.id,
    //                                     'value': promotion_category[i].value
    //                                 });
    //                             }
    //                             orderlines[item_count].set_discount(promotion_category[i].value);
    //                         }
    //                     }
    //                     if (promotion_category[i].type == 'fixed') {
    //                         if(clear_coupon){
    //                             for (var k = 0 ; k < orderlines[item_count].pos_promotion_message.length ; k++){
    //                                 if (orderlines[item_count].id == orderlines[item_count].pos_promotion_message[k].orderline_id && orderlines[item_count].pos_promotion_message[k].coupon == 1){
    //                                     var index = orderlines[item_count].pos_promotion_message.indexOf(orderlines[item_count].pos_promotion_message[k]);
    //                                     if (index >= 0){
    //                                         orderlines[item_count].pos_promotion_message.splice(index,1);
    //                                         break;
    //                                     }
    //                                 }
    //                             }
    //                             // orderlines[item_count].pos_promotion_message = null;
    //                             orderlines[item_count].set_unit_price(orderlines[item_count].product.price);
    //                         }else{
    //                             if( typeof orderlines[item_count].pos_promotion_message != 'undefined'){
    //                                 // message = orderlines[item_count].pos_promotion_message;
    //                                 orderlines[item_count].pos_promotion_message.push({
    //                                     'coupon': coupon,
    //                                     'orderline_id': orderlines[item_count].id,
    //                                     'message': program.name + ': discount ' + self.chrome.format_currency(promotion_category[i].value)
    //                                 });
    //                                 // orderlines[item_count].pos_promotion_message = program.name + ': discount ' + self.chrome.format_currency(promotion_category[i].value);
    //                             }else{
    //                                 orderlines[item_count].pos_promotion_message.push({
    //                                     'coupon': coupon,
    //                                     'orderline_id': orderlines[item_count].id,
    //                                     'message': program.name + ': discount ' + self.chrome.format_currency(promotion_category[i].value)
    //                                 });
    //                                 // orderlines[item_count].pos_promotion_message = program.name + ': discount ' + self.chrome.format_currency(promotion_category[i].value);
    //                             }
    //                             orderlines[item_count].set_unit_price(orderlines[item_count].product.price - promotion_category[i].value);
    //                         }
    //                     }
    //                 }
    //             }
    //         }
    //     },
    //     //end promotion type discount on cat
    //
    //     //start promotion type discount on total
    //     apply_pos_promotion_discount_on_total: function (program,b,clear_coupon) {
    //         var self = this;
    //         // if(typeof (clear_coupon) === 'undefined') clear_coupon = false;
    //         var product  = this.pos.db.get_product_by_id(program.discount_product_id[0]);
    //
    //         if(typeof (clear_coupon) !== 'undefined' && clear_coupon){
    //             var i = 0;
    //             while ( i < self.order_orderlines.length ) {
    //                 if (self.order_orderlines[i].get_product() === product) {
    //                     self.order.remove_orderline(self.order_orderlines[i]);
    //                 } else {
    //                     i++;
    //                 }
    //             }
    //         }else{
    //             // Remove existing discounts
    //             var i = 0;
    //             while ( i < self.order_orderlines.length ) {
    //                 if (self.order_orderlines[i].get_product() === product) {
    //                     self.order.remove_orderline(self.order_orderlines[i]);
    //                 } else {
    //                     i++;
    //                 }
    //             }
    //
    //             // Add discount
    //             var discount = 0;
    //             var type = '', value = '';
    //             // var discount_on_total = self.new_discount_on_total;
    //             var discount_on_total = [];
    //             var total = self.order.get_total_with_tax();
    //             for(var k = 0 ; k < b.length ; k++){
    //                 if(b[k].promotion_program_id == program.id){
    //                     discount_on_total.push(b[k]);
    //                 }
    //             }
    //             for(var j = discount_on_total.length-1 ; j >= 0  ; j--){
    //                 if(total >= discount_on_total[j].total_order){
    //                     if(discount_on_total[j].type == 'percent'){
    //                         value = discount_on_total[j].value + '%';
    //                         discount = (- total * (discount_on_total[j].value / 100));
    //                         break;
    //                     }else{
    //                         value = self.chrome.format_currency(discount_on_total[j].value);
    //                         discount = (- discount_on_total[j].value);
    //                         break;
    //                     }
    //                 }
    //             }
    //             // var message = [];
    //             var coupon = 0;
    //             if (typeof (clear_coupon) !== 'undefined' && !clear_coupon){
    //                 coupon = 1;
    //             }
    //             if( discount < 0 ){
    //                 self.order.add_product(product, {
    //                     price: discount
    //                 });
    //                 for ( var j = 0 ; j < self.order_orderlines.length ; j++){
    //                     if (self.order_orderlines[j].get_product() === product){
    //                         self.order_orderlines[j].pos_promotion_message.push({
    //                             'coupon': coupon,
    //                             'orderline_id': self.order_orderlines[j].id,
    //                             'message': program.name + ': ' + value
    //                         });
    //                         // self.order_orderlines[self.order_orderlines.length - 1].pos_promotion_message = program.name + ': ' + value;
    //                         // self.order_orderlines[self.order_orderlines.length - 1].product.display_name = 'Total Discount';
    //                         self.order_orderlines[j].set_unit_price(discount);
    //                         break;
    //                     }
    //                 }
    //                 // self.order_orderlines[self.order_orderlines.length - 1].pos_promotion_message.push({
    //                 //     'coupon': coupon,
    //                 //     'orderline_id': self.order_orderlines[self.order_orderlines.length - 1].id,
    //                 //     'message': program.name + ': ' + value
    //                 // });
    //                 // // self.order_orderlines[self.order_orderlines.length - 1].pos_promotion_message = program.name + ': ' + value;
    //                 // // self.order_orderlines[self.order_orderlines.length - 1].product.display_name = 'Total Discount';
    //                 // self.order_orderlines[self.order_orderlines.length - 1].set_unit_price(discount);
    //             }
    //         }
    //     }
    //
    // //     compare: function compare(a,b) {
    // //         if (a.total_order < b.total_order)
    // //             return -1;
    // //         if (a.total_order > b.total_order)
    // //             return 1;
    // //         return 0;
    // //     },
    // //
    // //     get_promotion_discount_on_total: function (promotion_id) {
    // //         var self = this;
    // //         var pos_promotion_discount_on_total = self.pos.discount_on_total;
    // //         var promotion_discount_on_total = [];
    // //         for (var i = 0; i < pos_promotion_discount_on_total.length; i++) {
    // //             if (pos_promotion_discount_on_total[i].promotion_program_id == promotion_id) {
    // //                 promotion_discount_on_total.push(pos_promotion_discount_on_total[i]);
    // //             }
    // //         }
    // //         promotion_discount_on_total.sort(function compare(a,b) {
    // //             if (a.total_order > b.total_order)
    // //                 return -1;
    // //             if (a.total_order < b.total_order)
    // //                 return 1;
    // //             return 0;
    // //         });
    // //         return promotion_discount_on_total;
    // //     },
    // //
    // //     apply_pos_promotion_discount_on_total: function (program) {
    // //         var self = this;
    // //         var order = self.order;
    // //         var current_total_order = order.get_total_with_tax();
    // //         var promotion_discount_on_total = self.get_promotion_discount_on_total(program.id);
    // //         var promotion_id = '';
    // //         for (var i=0; i<promotion_discount_on_total.length; i++) {
    // //             if (parseFloat(current_total_order) >= parseFloat(promotion_discount_on_total[i].total_order)) {
    // //                 var type = promotion_discount_on_total[i].type;
    // //                 var value = promotion_discount_on_total[i].value;
    // //                 promotion_id = promotion_discount_on_total[i].promotion_program_id;
    // //                 if (self.pos.promotion_discount_on_total_approved.indexOf(program.id) == -1) {
    // //                     self.pos.promotion_discount_on_total_approved.push(program.id);
    // //                     order.add_product(self.pos.db.get_product_by_id(68));
    // //                     // console.log(self.pos.db.get_product_by_id(68));
    // //                     var last_orderline = self.pos.get_order().get_orderlines()[self.pos.get_order().get_orderlines().length-1];
    // //                     var pos_promotion_message = 'test';
    // //                     last_orderline.pos_promotion_message = pos_promotion_message;
    // //                     last_orderline.product.display_name = 'Discount Product';
    // //                     // console.log(last_orderline);
    // //                     last_orderline.set_unit_price(-10);
    // //                     var item_line = last_orderline.id;
    // //                     self.pos.promotion_discount_product_approved.push({
    // //                         'pos_program_id': program.id,
    // //                         'item_line': item_line
    // //                     })
    // //                 } else {
    // //
    // //                 }
    // //                 break;
    // //             }
    // //         }
    // //         if (!promotion_id) {
    // //             if (self.pos.promotion_discount_on_total_approved.indexOf(program.id) != -1) {
    // //                 delete self.pos.promotion_discount_on_total_approved[self.pos.promotion_discount_on_total_approved.indexOf(program.id)];
    // //             }
    // //         }
    // //     },
    // //     //end promotion type discount on total
    // });

    //  screens.define_action_button({
    //     'name': 'pos_promotion',
    //     'widget': PosPromotionButton,
    //     'condition': function(){
    //         return true;
    //         // return this.pos.config.iface_discount && this.pos.config.discount_product_id;
    //     }
    // });


    // Move coupon button to left menu
    var CouponCodeButton = BaseWidget.extend({
        template: 'CouponCodeButton',
        renderElement: function () {
            var self = this;
            this._super();
        },
    });

    // Move promotion button to left menu
    var PosPromotionButton = BaseWidget.extend({
        template: 'PosPromotionButton',
        button_click: function (code, coupon, clear_coupon) {
            var self = this;

            if (typeof (coupon) === 'undefined') coupon = false;
            if (typeof (code) === 'undefined') code = '';
            if (typeof (clear_coupon) === 'undefined') clear_coupon = false;

            self.clear = clear_coupon;

            var new_program_available = [];
            var new_discount_on_total = [];
            var new_discount_by_qty = [];

            self.program_available = this.pos.programs;
            // tab13 coupon code
            self.program_available_back_up = this.pos.programs_back_up;
            //
            self.priority = this.pos.priority_product;
            self.priority_total = this.pos.priority_total;
            self.discount_on_total = this.pos.discount_on_total;
            self.priority_discount_by_qty = this.pos.priority_discount_by_qty;
            self.discount_by_qty = this.pos.discount_by_qty;

            // self.pos.note_content = document.getElementById('note').value;

            // chi lay 1 total discount
            // if(self.priority_total.length > 0){
            //     var min = self.priority_total[0];
            //     var delete_total_program = [];
            //     var new_delete_total_program = [];
            //     for(var i = 0 ; i < self.priority_total.length ; i++){
            //         if(self.priority_total[i].sequence < min.sequence){
            //             delete_total_program.push(min);
            //             min = self.priority_total[i];
            //         }
            //         if(self.priority_total[i].sequence > min.sequence){
            //             delete_total_program.push(self.priority_total[i]);
            //         }
            //     }
            //     // console.log(delete_total_program);
            //     if(delete_total_program.length > 0){
            //         for(var k = 0 ; k < self.program_available.length ; k++){
            //             for(var l = 0 ; l < delete_total_program.length ; l++){
            //                 if(delete_total_program[l].promotion_program_id == self.program_available[k].id){
            //                     new_delete_total_program.push(self.program_available[k]);
            //                 }
            //             }
            //         }
            //         for(var k = 0 ; k < new_delete_total_program.length ; k++){
            //             var index = self.program_available.indexOf(new_delete_total_program[k]);
            //             if(index >= 0){
            //                 self.program_available.splice(index,1);
            //             }
            //         }
            //     }
            // }
            // het

            // if (self.pos.get_order().new_order_free_product.length > 0) {
            //     var re_orderline = self.pos.get_order().get_orderlines();
            //     var new_order = self.pos.get_order().new_order_free_product;
            //     for (var i = 0; i < new_order.length; i++) {
            //         for (var j = 0; j < re_orderline.length; j++) {
            //             if (re_orderline[j].id == new_order[i].order_line_position) {
            //                 self.pos.get_order().remove_orderline(re_orderline[j]);
            //             }
            //         }
            //     }
            // }


            if (self.pos.get_order().get_orderlines().length > 0) {
                var product_price;
                var checked_orderline = [];
                var product_qty;
                var check;

                for (var i = 0; i < self.pos.get_order().get_orderlines().length; i++) {
                    check = 0;
                    product_price = self.pos.get_order().get_orderlines()[i].product.price;
                    product_qty = self.pos.get_order().get_orderlines()[i].quantity;
                    if (checked_orderline.length == 0 && product_price > 0 && product_qty > 0 && self.pos.get_order().get_orderlines()[i].is_pos_promotion_product != 1) {
                        checked_orderline.push({
                            'orderline_id': self.pos.get_order().get_orderlines()[i].id,
                            'product_id': self.pos.get_order().get_orderlines()[i].product.id,
                            'product_name': self.pos.get_order().get_orderlines()[i].product.display_name,
                            'product_price': self.pos.get_order().get_orderlines()[i].product.price,
                            'product_qty': self.pos.get_order().get_orderlines()[i].quantity
                        });
                        continue;
                    }
                    if (checked_orderline.length > 0 && self.pos.get_order().get_orderlines()[i].product.price > 0 && product_qty > 0 && self.pos.get_order().get_orderlines()[i].is_pos_promotion_product != 1) {
                        checked_orderline.push({
                            'orderline_id': self.pos.get_order().get_orderlines()[i].id,
                            'product_id': self.pos.get_order().get_orderlines()[i].product.id,
                            'product_name': self.pos.get_order().get_orderlines()[i].product.display_name,
                            'product_price': self.pos.get_order().get_orderlines()[i].product.price,
                            'product_qty': self.pos.get_order().get_orderlines()[i].quantity
                        });
                    }
                }
                if (checked_orderline.length > 0) {
                    checked_orderline.sort(function (a, b) {
                        return (a.product_price < b.product_price) ? 1 : ((b.product_price < a.product_price) ? -1 : 0);
                    });
                    for (var j = 0; j < checked_orderline.length; j++) {
                        if (checked_orderline[j].product_qty > 1) {
                            for (var s = 1; s < checked_orderline[j].product_qty; s++) {
                                var clone_orderline = self.pos.get_order().get_orderline(checked_orderline[j].orderline_id).clone();
                                self.pos.get_order().add_orderline(clone_orderline);
                                self.pos.get_order().get_orderlines()[self.pos.get_order().get_orderlines().indexOf(clone_orderline)].set_quantity(1);
                                checked_orderline.push({
                                    'orderline_id': self.pos.get_order().get_orderlines()[self.pos.get_order().get_orderlines().indexOf(clone_orderline)].id,
                                    'product_id': self.pos.get_order().get_orderlines()[self.pos.get_order().get_orderlines().indexOf(clone_orderline)].product.id,
                                    'product_name': self.pos.get_order().get_orderlines()[self.pos.get_order().get_orderlines().indexOf(clone_orderline)].product.display_name,
                                    'product_price': self.pos.get_order().get_orderlines()[self.pos.get_order().get_orderlines().indexOf(clone_orderline)].product.price,
                                    'product_qty': self.pos.get_order().get_orderlines()[self.pos.get_order().get_orderlines().indexOf(clone_orderline)].quantity
                                });
                            }
                            self.pos.get_order().get_orderline(checked_orderline[j].orderline_id).set_quantity(1);
                        }
                    }
                }
            }

            // if(code && !self.pos.get_order().is_used_coupon_code && coupon){

            // if (code == '' && clear_coupon) {
            //         var re_orderline = self.pos.get_order().get_orderlines();
            //         var new_order = self.pos.get_order().new_order_free_product_coupon;
            //         for (var i = 0; i < new_order.length; i++) {
            //             for (var j = 0; j < re_orderline.length; j++) {
            //                 if (re_orderline[j].id == new_order[i].order_line_position) {
            //                     // console.log('remove product coupon 1');
            //                     self.pos.get_order().remove_orderline(re_orderline[j]);
            //                 }
            //             }
            //         }
            //     }

            if (code != '' && coupon) {
                // tab13 sum discount
                self.pos.get_order().get_orderlines().forEach(function (line) {
                    // line.discount_by_pos_promotion = [];
                    line.discount_by_coupon_code = [];
                    // line.pos_promotion_message = [];
                });
                if (self.clear) {
                    var allLine = self.pos.get_order().get_orderlines();
                    for (var o = 0; o < allLine.length; o++) {
                        for (var i = 0; i < allLine[o].pos_promotion_message.length; i++) {
                            if (allLine[o].pos_promotion_message[i].coupon == 1) {
                                var index = allLine[o].pos_promotion_message.indexOf(allLine[o].pos_promotion_message[i]);
                                if (index >= 0) {
                                    allLine[o].pos_promotion_message.splice(index, 1);
                                    break;
                                }
                            }
                        }
                        break;
                    }
                }
                // if (self.pos.get_order().new_order_free_product_coupon.length > 0) {
                //     var re_orderline = self.pos.get_order().get_orderlines();
                //     var new_order = self.pos.get_order().new_order_free_product_coupon;
                //     for (var i = 0; i < new_order.length; i++) {
                //         for (var j = 0; j < re_orderline.length; j++) {
                //             if (re_orderline[j].id == new_order[i].order_line_position) {
                //                 // console.log('remove product coupon 1');
                //                 self.pos.get_order().remove_orderline(re_orderline[j]);
                //             }
                //         }
                //     }
                // }
                // console.log(code);
                self.order = self.pos.get_order();
                self.order_orderlines = self.order.get_orderlines();
                self.orderlines = new Array();
                _.each(self.order_orderlines, function (orderline) {
                    self.orderlines.push(orderline);
                });
                self.order.pos_promotion_message = [];
                self.new_promotion_discount_approved = [];
                self.new_promotion_discount_product_approved = [];
                self.product_list = self.pos.db.get_product_by_category(0);
                // self.bxgy_gy_list = [];
                // self.bxgy_gy_title = [];
                self.popup_promotion = false;
                self.use_program = 0;
                var use_program_type = '';
                // console.log('this.pos.programs');
                // console.log(this.pos.programs);
                // console.log(self.order.save_delete_coupon_program);
                // console.log(self.order.program_used_by_coupon_code);
                // console.log(self.order.save_delete_coupon_program_bxgy);
                // console.log(self.order.save_delete_coupon_program_bx);
                // console.log(self.order.save_delete_coupon_program_gy);
                // if(self.order.program_used_by_coupon_code > 0){
                //     if(self.order.save_delete_coupon_program.length > 0){
                //         this.pos.programs.push(self.order.save_delete_coupon_program[0]);
                //     }
                //     if(self.order.save_delete_coupon_program_bxgy.length > 0){
                //         for(var i = 0 ; i < self.order.save_delete_coupon_program_bxgy.length ; i++){
                //             this.pos.bxgy_programs.push(self.order.save_delete_coupon_program_bxgy[i]);
                //         }
                //     }
                //     if(self.order.save_delete_coupon_program_bx.length > 0){
                //         for(var i = 0 ; i < self.order.save_delete_coupon_program_bx.length ; i++){
                //             this.pos.bxgy_bx.push(self.order.save_delete_coupon_program_bx[i]);
                //         }
                //     }
                //     if(self.order.save_delete_coupon_program_gy.length > 0){
                //         for(var i = 0 ; i < self.order.save_delete_coupon_program_gy.length ; i++){
                //             this.pos.bxgy_gy.push(self.order.save_delete_coupon_program_gy[i]);
                //         }
                //     }
                // }
                for (var i = 0; i < this.pos.programs_back_up.length; i++) {
                    var index = this.pos.programs.indexOf(this.pos.programs_back_up[i]);
                    if (index < 0) {
                        this.pos.programs.push(this.pos.programs_back_up[i]);
                    }
                }
                this.pos.programs.sort(function (a, b) {
                    return (a.sequence > b.sequence) ? 1 : ((b.sequence > a.sequence) ? -1 : 0);
                });
                for (var i = 0; i < this.pos.programs.length; i++) {
                    if (this.pos.programs[i].code == code) {
                        if (self.order.before_use_coupon.length == 0) {
                            self.order.before_use_coupon.push({
                                total: self.order.get_total_with_tax(),
                                quantity: self.order.get_orderlines().length
                            });
                        } else {
                            self.order.before_use_coupon = [];
                            self.order.before_use_coupon.push({
                                total: self.order.get_total_with_tax(),
                                quantity: self.order.get_orderlines().length
                            });
                            // self.order.before_use_coupon[0].total = self.order.get_total_with_tax();
                            // self.order.before_use_coupon[0].quantity = self.order.get_orderlines().length;
                        }
                        // use_program = true;
                        self.bxgy_gy_list = [];
                        self.bxgy_gy_title = [];
                        switch (this.pos.programs[i].type) {
                            case 'discount':
                                self.use_program = this.pos.programs[i].id;
                                use_program_type = this.pos.programs[i].type;
                                self.apply_pos_promotion_discount(this.pos.programs[i], self.clear);
                                break;
                            case 'special_price':
                                self.use_program = this.pos.programs[i].id;
                                use_program_type = this.pos.programs[i].type;
                                self.apply_pos_promotion_special_price(this.pos.programs[i], self.clear);
                                break;
                            case 'give_product':
                                self.use_program = this.pos.programs[i].id;
                                use_program_type = this.pos.programs[i].type;
                                self.popup_promotion = true;
                                self.apply_pos_promotion_give_product(this.pos.programs[i], self.clear);
                                break;
                            case 'discount_on_cat':
                                self.use_program = this.pos.programs[i].id;
                                use_program_type = this.pos.programs[i].type;
                                self.apply_pos_promotion_discount_on_cat(this.pos.programs[i], self.clear);
                                break;
                            case 'discount_on_total':
                                self.use_program = this.pos.programs[i].id;
                                use_program_type = this.pos.programs[i].type;
                                self.apply_pos_promotion_discount_on_total(this.pos.programs[i], self.discount_on_total, self.clear);
                                break;
                            case 'discount_by_qty':
                                var new_discount_by_qty = [];
                                for (var k = 0; k < self.discount_by_qty.length; k++) {
                                    if (self.discount_by_qty[k].promotion_program_id == this.pos.programs[i].id) {
                                        new_discount_by_qty.push(self.discount_by_qty[k]);
                                    }
                                }
                                self.use_program = this.pos.programs[i].id;
                                use_program_type = this.pos.programs[i].type;
                                self.popup_promotion = true;
                                self.apply_pos_promotion_discount_by_qty(this.pos.programs[i], new_discount_by_qty, self.clear);
                                break;
                            case 'bxgy':
                                self.use_program = this.pos.programs[i].id;
                                use_program_type = this.pos.programs[i].type;
                                self.apply_pos_promotion_bxgy(self.clear);
                                break;
                        }
                    }
                }
                this.show_product_popup(true);
                // console.log('coupon code');
                // console.log(self.pos.get_order().get_orderlines());
                if (self.order.before_use_coupon.length > 0) {
                    if (self.order.get_total_with_tax() != self.order.before_use_coupon[0].total || self.order.get_orderlines().length > self.order.before_use_coupon[0].quantity) {
                        self.order.is_used_coupon_code = true;
                        self.order.program_used_by_coupon_code = self.use_program;
                        new Model('pos.promotion.program').call('set_coupon_code_left_1', [self.use_program]);
                    }
                }
            }
            if (!coupon) {
                // tab13 sum discount
                self.pos.get_order().get_orderlines().forEach(function (line) {
                    line.discount_by_pos_promotion = [];
                    // line.discount_by_coupon_code = [];
                    // line.pos_promotion_message = [];
                });
                var allLine = self.pos.get_order().get_orderlines();
                var spliceIndex = [];
                for (var i = 0; i < allLine.length; i++) {
                    if (allLine[i].pos_promotion_message.length > 0) {
                        for (var j = 0; j < allLine[i].pos_promotion_message.length; j++) {
                            if (allLine[i].pos_promotion_message[j].coupon == 0) {
                                spliceIndex.push(allLine[i].pos_promotion_message[j]);
                            }
                        }
                    }
                }
                if (spliceIndex.length > 0) {
                    for (var i = 0; i < allLine.length; i++) {
                        if (allLine[i].pos_promotion_message.length > 0) {
                            for (var j = 0; j < allLine[i].pos_promotion_message.length; j++) {
                                for (var k = 0; k < spliceIndex.length; k++) {
                                    if (spliceIndex[k].orderline_id == allLine[i].id) {
                                        var index = allLine[i].pos_promotion_message.indexOf(spliceIndex[k]);
                                        if (index >= 0) {
                                            allLine[i].pos_promotion_message.splice(index, 1);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if (self.pos.get_order().new_order_free_product.length > 0) {
                    var re_orderline = self.pos.get_order().get_orderlines();
                    var new_order = self.pos.get_order().new_order_free_product;
                    for (var i = 0; i < new_order.length; i++) {
                        for (var j = 0; j < re_orderline.length; j++) {
                            if (re_orderline[j].id == new_order[i].order_line_position) {
                                // console.log('remove product coupon 2');
                                self.pos.get_order().remove_orderline(re_orderline[j]);
                            }
                        }
                    }
                }
                // Tab13 coupon code
                // if(this.pos.get_order().program_used_by_coupon_code > 0){
                //     if (self.pos.get_order().program_used_by_coupon_code > 0){
                //         var indexCoupon = -1;
                //         for (var i = 0 ; i < self.program_available.length ; i++){
                //             if(self.program_available[i].id == self.pos.get_order().program_used_by_coupon_code){
                //                 indexCoupon = self.program_available.indexOf(self.program_available[i]);
                //                 if(self.program_available[i].type == 'bxgy'){
                //                     // var bxgy_programs = this.pos.bxgy_programs;
                //                     // var bxgy_bx = this.pos.bxgy_bx;
                //                     // var bxgy_gy = this.pos.bxgy_gy;
                //                     var bxgy_delete_by_coupon = [];
                //                     var bxgy_delete_by_coupon_id = [];
                //                     for(var j = 0 ; j < this.pos.bxgy_programs.length ; j++){
                //                         if(self.program_available[i].id == this.pos.bxgy_programs[j].id){
                //                             bxgy_delete_by_coupon.push(this.pos.bxgy_programs[j]);
                //                             bxgy_delete_by_coupon_id.push(self.program_available[i].id);
                //                         }
                //                     }
                //                     if(bxgy_delete_by_coupon.length > 0){
                //                         for(var k = 0 ; k < bxgy_delete_by_coupon.length ; k++){
                //                             var index = this.pos.bxgy_programs.indexOf(bxgy_delete_by_coupon[k]);
                //                             if(index >= 0){
                //                                 if(this.pos.get_order().save_delete_coupon_program_bxgy.length > 0){
                //                                     this.pos.get_order().save_delete_coupon_program_bxgy = [];
                //                                     this.pos.get_order().save_delete_coupon_program_bxgy.push(bxgy_delete_by_coupon[k]);
                //                                 }else{
                //                                     this.pos.get_order().save_delete_coupon_program_bxgy.push(bxgy_delete_by_coupon[k]);
                //                                 }
                //                                 this.pos.bxgy_programs.splice(index,1);
                //                             }
                //                         }
                //                     }
                //                     if(bxgy_delete_by_coupon_id.length > 0){
                //                         for(var k = 0 ; k < bxgy_delete_by_coupon_id.length ; k++){
                //                             for(var g = 0 ; g < this.pos.bxgy_bx.length ; g++){
                //                                 if(bxgy_delete_by_coupon_id[k] == this.pos.bxgy_bx[g].bxgy_program_id){
                //                                     var index = this.pos.bxgy_bx.indexOf(this.pos.bxgy_bx[g]);
                //                                     if(index >= 0){
                //                                         if(this.pos.get_order().save_delete_coupon_program_bx.length > 0){
                //                                             this.pos.get_order().save_delete_coupon_program_bx = [];
                //                                             this.pos.get_order().save_delete_coupon_program_bx.push(this.pos.bxgy_bx[g]);
                //                                         }else{
                //                                             this.pos.get_order().save_delete_coupon_program_bx.push(this.pos.bxgy_bx[g]);
                //                                         }
                //                                         this.pos.bxgy_bx.splice(index,1);
                //                                     }
                //                                 }
                //                             }
                //                         }
                //                         for(var k = 0 ; k < bxgy_delete_by_coupon_id.length ; k++){
                //                             for(var g = 0 ; g < this.pos.bxgy_gy.length ; g++){
                //                                 if(bxgy_delete_by_coupon_id[k] == this.pos.bxgy_gy[g].bxgy_program_id){
                //                                     var index = this.pos.bxgy_gy.indexOf(this.pos.bxgy_gy[g]);
                //                                     if(index >= 0){
                //                                         if(this.pos.get_order().save_delete_coupon_program_gy.length > 0){
                //                                             this.pos.get_order().save_delete_coupon_program_gy = [];
                //                                             this.pos.get_order().save_delete_coupon_program_gy.push(this.pos.bxgy_gy[g]);
                //                                         }else{
                //                                             this.pos.get_order().save_delete_coupon_program_gy.push(this.pos.bxgy_gy[g]);
                //                                         }
                //                                         this.pos.bxgy_gy.splice(index,1);
                //                                     }
                //                                 }
                //                             }
                //                         }
                //                     }
                //                 }
                //                 if(this.pos.get_order().save_delete_coupon_program.length > 0){
                //                     this.pos.get_order().save_delete_coupon_program = [];
                //                     this.pos.get_order().save_delete_coupon_program.push(self.program_available[i]);
                //                 }else{
                //                     this.pos.get_order().save_delete_coupon_program.push(self.program_available[i]);
                //                 }
                //                 break;
                //             }
                //         }
                //         if (indexCoupon >= 0) {
                //             self.program_available.splice(indexCoupon, 1);
                //         }
                //     }
                // }
                // console.log('self.program_available');
                // console.log(self.program_available);
                // chi lay 1 discount by qty
                if (self.priority_discount_by_qty.length > 0) {
                    var min_discount_by_qty_program = self.priority_discount_by_qty[0];
                    var delete_discount_by_qty_program = [];
                    var new_delete_discount_by_qty_program = [];
                    for (var i = 0; i < self.priority_discount_by_qty.length; i++) {
                        if (self.priority_discount_by_qty[i].sequence < min_discount_by_qty_program.sequence) {
                            delete_discount_by_qty_program.push(min_discount_by_qty_program.promotion_program_id);
                            min_discount_by_qty_program = self.priority_discount_by_qty[i];
                        }
                        if (self.priority_discount_by_qty[i].sequence > min_discount_by_qty_program.sequence) {
                            delete_discount_by_qty_program.push(self.priority_discount_by_qty[i].promotion_program_id);
                        }
                    }
                    if (delete_discount_by_qty_program.length > 0) {
                        for (var i = 0; i < self.program_available.length; i++) {
                            for (var j = 0; j < delete_discount_by_qty_program.length; j++) {
                                if (delete_discount_by_qty_program[j] == self.program_available[i].id) {
                                    new_delete_discount_by_qty_program.push(self.program_available[i]);
                                }
                            }
                        }
                        for (var k = 0; k < new_delete_discount_by_qty_program.length; k++) {
                            var index = self.program_available.indexOf(new_delete_discount_by_qty_program[k]);
                            if (index >= 0) {
                                self.program_available.splice(index, 1);
                            }
                        }
                    }
                }
                // het
                for (var i = 0; i < this.pos.priority_category.length; i++) {
                    self.priority.push(this.pos.priority_category[i]);
                }
                var posPromotionConfigModel = new Model('pos.promotion.config.settings');
                posPromotionConfigModel.call('get_apply_pos_promotion_program').then(function (return_value) {
                    var discount_by_qty_position = -1;
                    if (self.program_available.length > 0) {
                        // sap xep chuong trinh theo sequence
                        for (var i = 0; i < self.program_available.length - 1; i++) {
                            for (var j = i + 1; j < self.program_available.length; j++) {
                                if (self.program_available[j].sequence < self.program_available[i].sequence) {
                                    var temp = self.program_available[i];
                                    self.program_available[i] = self.program_available[j];
                                    self.program_available[j] = temp;
                                }
                            }
                        }
                        for (var i = 0; i < self.program_available.length; i++) {
                            if (self.program_available[i].type == 'discount_by_qty') {
                                discount_by_qty_position = i;
                                // self.new_discount_by_qty = [];
                                new_discount_by_qty = [];
                                for (var k = 0; k < self.discount_by_qty.length; k++) {
                                    if (self.discount_by_qty[k].promotion_program_id == self.program_available[i].id) {
                                        // self.new_discount_by_qty.push(self.discount_by_qty[k]);
                                        new_discount_by_qty.push(self.discount_by_qty[k]);
                                    }
                                }
                            }
                            // if(self.program_available[i].type == 'discount_on_total'){
                            //     // self.new_discount_on_total = [];
                            //     new_discount_on_total = [];
                            //     for(var k = 0 ; k < self.discount_on_total.length ; k++){
                            //         if(self.discount_on_total[k].promotion_program_id == self.program_available[i].id){
                            //             // self.new_discount_on_total.push(self.discount_on_total[k]);
                            //             new_discount_on_total.push(self.discount_on_total[k]);
                            //         }
                            //     }
                            // }
                        }
                        // het sap xep
                        // kiem tra chuong trinh chay theo config
                        if (return_value == 3) {
                            if (self.program_available.length > 1) {
                                var program_available_with_coupon = [];
                                for (var i = 0; i < self.program_available.length; i++) {
                                    if (self.program_available[i].coupon_code) {
                                        program_available_with_coupon.push(self.program_available[i]);
                                    }
                                }
                                if (program_available_with_coupon.length > 0) {
                                    for (var i = 0; i < program_available_with_coupon.length; i++) {
                                        var index = self.program_available.indexOf(program_available_with_coupon[i]);
                                        if (index >= 0) {
                                            self.program_available.splice(index, 1);
                                        }
                                    }
                                }
                                // self.program_available.splice(1,self.program_available.length-1);
                            }
                        }
                        if (return_value == 1) {
                            if (self.program_available.length > 1) {
                                var program_available_but_coupon = [];
                                var program_available_with_coupon = [];
                                for (var i = 0; i < self.program_available.length; i++) {
                                    if (!self.program_available[i].coupon_code) {
                                        program_available_but_coupon.push(self.program_available[i]);
                                    }
                                    if (self.program_available[i].coupon_code) {
                                        program_available_with_coupon.push(self.program_available[i]);
                                    }
                                }
                                if (program_available_with_coupon.length > 0) {
                                    for (var i = 0; i < program_available_with_coupon.length; i++) {
                                        var index = self.program_available.indexOf(program_available_with_coupon[i]);
                                        if (index >= 0) {
                                            self.program_available.splice(index, 1);
                                        }
                                    }
                                }
                                if (program_available_but_coupon.length > 1) {
                                    for (var i = 1; i < program_available_but_coupon.length; i++) {
                                        var index = self.program_available.indexOf(program_available_but_coupon[i]);
                                        if (index >= 0) {
                                            self.program_available.splice(index, 1);
                                        }
                                    }
                                }
                                // self.program_available.splice(1,self.program_available.length-1);
                            }

                            // chi lay 1 total discount
                            // if (self.priority_total.length > 0) {
                            //     var min = self.priority_total[0];
                            //     var delete_total_program = [];
                            //     var new_delete_total_program = [];
                            //     for (var i = 0; i < self.priority_total.length; i++) {
                            //         if (self.priority_total[i].sequence < min.sequence) {
                            //             delete_total_program.push(min);
                            //             min = self.priority_total[i];
                            //         }
                            //         if (self.priority_total[i].sequence > min.sequence) {
                            //             delete_total_program.push(self.priority_total[i]);
                            //         }
                            //     }
                            //     if (delete_total_program.length > 0) {
                            //         for (var k = 0; k < self.program_available.length; k++) {
                            //             for (var l = 0; l < delete_total_program.length; l++) {
                            //                 if (delete_total_program[l].promotion_program_id == self.program_available[k].id) {
                            //                     new_delete_total_program.push(self.program_available[k]);
                            //                 }
                            //             }
                            //         }
                            //         for (var k = 0; k < new_delete_total_program.length; k++) {
                            //             var index = self.program_available.indexOf(new_delete_total_program[k]);
                            //             if (index >= 0) {
                            //                 self.program_available.splice(index, 1);
                            //             }
                            //         }
                            //     }
                            // }
                            // het

                            // var priority_products = [];
                            // var delete_program_available_id = [];
                            // for (var i = 0; i < self.program_available.length; i++) {
                            //     if (self.program_available[i].type == 'discount' || self.program_available[i].type == 'special_price' || self.program_available[i].type == 'bxgy' || self.program_available[i].type == 'discount_on_cat') {
                            //         if (discount_by_qty_position > i) {
                            //             delete_program_available_id.push(self.program_available[discount_by_qty_position]);
                            //             break;
                            //         }
                            //         if (discount_by_qty_position < i && discount_by_qty_position != -1) {
                            //             delete_program_available_id.push(self.program_available[i]);
                            //         }
                            //     }
                            // }
                            // if (delete_program_available_id.length > 0) {
                            //     for (var k = 0; k < delete_program_available_id.length; k++) {
                            //         var index = self.program_available.indexOf(delete_program_available_id[k]);
                            //         if (index >= 0) {
                            //             self.program_available.splice(index, 1);
                            //         }
                            //     }
                            // }
                            // delete_program_available_id = [];
                            // if (self.priority.length > 0) {
                            //     var new_priority = [];
                            //     for (var i = 0; i < self.program_available.length; i++) {
                            //         for (var j = 0; j < self.priority.length; j++) {
                            //             if (self.program_available[i].id == self.priority[j].promotion_program_id) {
                            //                 new_priority.push(self.priority[j]);
                            //             }
                            //         }
                            //     }
                            //     for (var i = 0; i < self.program_available.length; i++) {
                            //         priority_products = [];
                            //         for (var j = 0; j < new_priority.length; j++) {
                            //             if (self.program_available[i].id == new_priority[j].promotion_program_id) {
                            //                 priority_products.push(new_priority[j]);
                            //             }
                            //         }
                            //         for (var j = 0; j < new_priority.length; j++) {
                            //             for (var k = 0; k < priority_products.length; k++) {
                            //                 if (new_priority[j].is_product && priority_products[k].is_product) {
                            //                     if (new_priority[j].product_id == priority_products[k].product_id && new_priority[j].promotion_program_id != priority_products[k].promotion_program_id) {
                            //                         if (new_priority[j].sequence > priority_products[k].sequence) {
                            //                             delete_program_available_id.push(new_priority[j].promotion_program_id);
                            //                         }
                            //                     }
                            //                 }
                            //                 if (new_priority[j].is_product && !priority_products[k].is_product) {
                            //                     if (new_priority[j].promotion_program_id != priority_products[k].promotion_program_id && self.pos.db.is_product_in_category(priority_products[k].category_id, new_priority[j].product_id)) {
                            //                         if (new_priority[j].sequence > priority_products[k].sequence) {
                            //                             delete_program_available_id.push(new_priority[j].promotion_program_id);
                            //                         }
                            //                     }
                            //                 }
                            //                 if (!new_priority[j].is_product && !priority_products[k].is_product) {
                            //                     if (new_priority[j].promotion_program_id != priority_products[k].promotion_program_id && new_priority[j].category_id != priority_products[k].category_id) {
                            //                         if (new_priority[j].sequence > priority_products[k].sequence) {
                            //                             delete_program_available_id.push(new_priority[j].promotion_program_id);
                            //                         }
                            //                     }
                            //                 }
                            //                 if (!new_priority[j].is_product && priority_products[k].is_product) {
                            //                     if (self.pos.db.is_product_in_category(new_priority[j].category_id, priority_products[k].product_id) && new_priority[j].promotion_program_id != priority_products[k].promotion_program_id) {
                            //                         if (new_priority[j].sequence > priority_products[k].sequence) {
                            //                             delete_program_available_id.push(new_priority[j].promotion_program_id);
                            //                         }
                            //                     }
                            //                 }
                            //             }
                            //         }
                            //     }
                            //     // self.delete_program_available(delete_program_available_id,self.program_available);
                            //     // co the chi can push vao delete_program_available_id vi tri cua chuong trinh
                            //     if (delete_program_available_id.length > 0) {
                            //         var new_delete_program_available_id = [];
                            //         for (var j = 0; j < self.program_available.length; j++) {
                            //             for (var i = 0; i < delete_program_available_id.length; i++) {
                            //                 if (delete_program_available_id[i] == self.program_available[j].id) {
                            //                     new_delete_program_available_id.push(self.program_available[j]);
                            //                 }
                            //             }
                            //         }
                            //         if (new_delete_program_available_id.length > 0) {
                            //             for (var k = 0; k < new_delete_program_available_id.length; k++) {
                            //                 var index = self.program_available.indexOf(new_delete_program_available_id[k]);
                            //                 if (index >= 0) {
                            //                     self.program_available.splice(index, 1);
                            //                 }
                            //             }
                            //         }
                            //     }
                            // }
                        }
                    }
                    // return;
                    // het kiem tra chuong trinh

                    self.order = self.pos.get_order();
                    self.order_orderlines = self.order.get_orderlines();
                    self.orderlines = new Array();
                    _.each(self.order_orderlines, function (orderline) {
                        self.orderlines.push(orderline);
                    });

                    self.bxgy_gy_list = [];
                    self.bxgy_gy_title = [];
                    self.product_list = self.pos.db.get_product_by_category(0);

                    // if (self.order.new_order_free_product.length > 0) {
                    //     var re_orderline = self.order_orderlines;
                    //     var new_order = self.order.new_order_free_product;
                    //     for (var i = 0; i < new_order.length; i++) {
                    //         for (var j = 0; j < re_orderline.length; j++) {
                    //             if (re_orderline[j].id == new_order[i].order_line_position) {
                    //                 self.order.remove_orderline(re_orderline[j]);
                    //             }
                    //         }
                    //     }
                    // }

                    self.apply_promotion(new_discount_by_qty, self.discount_on_total);

                    // console.log('pos promotion');
                    // console.log(self.pos.get_order().get_orderlines());
                    // for (var i = 0 ; i < self.pos.get_order().get_orderlines().length ; i++){
                    //     var total = 0;
                    //     if (self.pos.get_order().get_orderlines()[i].discount_by_pos_promotion.length > 1){
                    //         self.pos.get_order().get_orderlines()[i].discount_by_pos_promotion.forEach(function (item) {
                    //             total += item.value;
                    //         });
                    //         console.log(total);
                    //     }
                    //     if (self.pos.get_order().get_orderlines()[i].discount_by_coupon_code.length > 1){
                    //         self.pos.get_order().get_orderlines()[i].discount_by_coupon_code.forEach(function (item) {
                    //             total += item.value;
                    //         });
                    //         console.log(total);
                    //     }
                    //     self.pos.get_order().get_orderlines()[i].set_discount(total);
                    // }

                    // Tab13 auto check pos_promotion
                    self.pos.get_order().click_pos_promotion = true;
                    // self.pos.click_pos_promotion = true;
                });
            }

        },

        apply_promotion: function (a, b) {
            var self = this;
            self.order.pos_promotion_message = [];
            var check_bxgy = 0;
            self.new_promotion_discount_approved = [];
            self.new_promotion_discount_product_approved = [];
            // var check_total_program = false;
            // for (var i = 0; i < self.program_available.length; i++) {
            //     if (self.program_available[i].type == 'discount_on_total'){
            //         check_total_program = true;
            //         var index = self.program_available.indexOf(self.program_available[i]);
            //         if (index >= 0){
            //             self.program_available.slice(index,1);
            //             self.program_available.push(self.program_available[i]);
            //         }
            //         break;
            //     }
            // }
            for (var i = 0; i < self.program_available.length; i++) {
                // check promotion progam type discount %

                if (self.program_available[i].type == 'discount') {
                    self.apply_pos_promotion_discount(self.program_available[i]);
                }
                if (self.program_available[i].type == 'special_price') {
                    self.apply_pos_promotion_special_price(self.program_available[i]);
                }
                if (self.program_available[i].type == 'bxgy') {
                    check_bxgy++;
                }
                if (i == self.program_available.length - 1 && check_bxgy != 0) {
                    self.apply_pos_promotion_bxgy();
                }
                // if (program_available[i].type == 'bxpy') {
                //     self.apply_pos_promotion_bxpy(program_available[i])
                // }
                if (self.program_available[i].type == 'give_product') {
                    self.apply_pos_promotion_give_product(self.program_available[i]);
                }
                if (self.program_available[i].type == 'discount_on_cat') {
                    self.apply_pos_promotion_discount_on_cat(self.program_available[i]);
                }
                //discount on total
                if (self.program_available[i].type == 'discount_on_total') {
                    self.apply_pos_promotion_discount_on_total(self.program_available[i], b);
                }
                if (self.program_available[i].type == 'discount_by_qty') {
                    self.apply_pos_promotion_discount_by_qty(self.program_available[i], a);
                }
            }

            this.show_product_popup(false);
            //cancel promotion type discount not available
            this.cancel_pos_promotion_discount();
        },

        // start promotion type: discount by qty
        apply_pos_promotion_discount_by_qty: function (program, a, clear_coupon) {
            var self = this;
            // if(typeof (clear_coupon) === 'undefined') clear_coupon = false;
            // var product  = this.pos.db.get_product_by_id(program.discount_product_id[0]);
            var discount_by_qty = a;

            if (self.order_orderlines.length > 0) {
                var product_price;
                var checked_orderline = [];
                var product_qty;
                var check;

                for (var i = 0; i < self.order_orderlines.length; i++) {
                    check = 0;
                    product_price = self.order_orderlines[i].product.price;
                    product_qty = self.order_orderlines[i].quantity;
                    if (checked_orderline.length == 0 && product_price > 0 && product_qty > 0 && self.order_orderlines[i].is_pos_promotion_product != 1) {
                        checked_orderline.push({
                            'orderline_id': self.order_orderlines[i].id,
                            'product_id': self.order_orderlines[i].product.id,
                            'product_name': self.order_orderlines[i].product.display_name,
                            'product_price': self.order_orderlines[i].product.price,
                            'product_qty': self.order_orderlines[i].quantity
                        });
                        continue;
                    }
                    if (checked_orderline.length > 0 && self.order_orderlines[i].product.price > 0 && product_qty > 0 && self.order_orderlines[i].is_pos_promotion_product != 1) {
                        checked_orderline.push({
                            'orderline_id': self.order_orderlines[i].id,
                            'product_id': self.order_orderlines[i].product.id,
                            'product_name': self.order_orderlines[i].product.display_name,
                            'product_price': self.order_orderlines[i].product.price,
                            'product_qty': self.order_orderlines[i].quantity
                        });
                    }
                }
                if (checked_orderline.length > 0) {
                    checked_orderline.sort(function (a, b) {
                        return (a.product_price < b.product_price) ? 1 : ((b.product_price < a.product_price) ? -1 : 0);
                    });
                    for (var j = 0; j < checked_orderline.length; j++) {
                        if (checked_orderline[j].product_qty > 1) {
                            for (var s = 1; s < checked_orderline[j].product_qty; s++) {
                                var clone_orderline = self.order.get_orderline(checked_orderline[j].orderline_id).clone();
                                self.order.add_orderline(clone_orderline);
                                self.order_orderlines[self.order_orderlines.indexOf(clone_orderline)].set_quantity(1);
                                checked_orderline.push({
                                    'orderline_id': self.order_orderlines[self.order_orderlines.indexOf(clone_orderline)].id,
                                    'product_id': self.order_orderlines[self.order_orderlines.indexOf(clone_orderline)].product.id,
                                    'product_name': self.order_orderlines[self.order_orderlines.indexOf(clone_orderline)].product.display_name,
                                    'product_price': self.order_orderlines[self.order_orderlines.indexOf(clone_orderline)].product.price,
                                    'product_qty': self.order_orderlines[self.order_orderlines.indexOf(clone_orderline)].quantity
                                });
                            }
                            self.order.get_orderline(checked_orderline[j].orderline_id).set_quantity(1);
                        }
                    }
                    checked_orderline.sort(function (a, b) {
                        return (a.product_price < b.product_price) ? 1 : ((b.product_price < a.product_price) ? -1 : 0);
                    });
                    var count_prod = 0;
                    var message;
                    for (var i = 0; i < checked_orderline.length; i++) {
                        for (var j = 0; j < self.order_orderlines.length; j++) {
                            if (self.order_orderlines[j].id == checked_orderline[i].orderline_id) {
                                // self.order_orderlines[j].set_discount(0);
                                count_prod++;
                                for (var p = 0; p < discount_by_qty.length; p++) {
                                    if (count_prod >= discount_by_qty[p].total_qty && typeof discount_by_qty[p + 1] != 'undefined' && count_prod < discount_by_qty[p + 1].total_qty && self.order_orderlines[j].price > 0) {
                                        if (typeof (clear_coupon) !== 'undefined' && clear_coupon) {
                                            for (var k = 0; k < self.order_orderlines[j].discount_by_coupon_code.length; k++) {
                                                if (self.order_orderlines[j].id == self.order_orderlines[j].discount_by_coupon_code[k].orderline_id) {
                                                    self.order_orderlines[j].set_discount(self.order_orderlines[j].get_discount() - self.order_orderlines[j].discount_by_coupon_code[k].value);
                                                    break;
                                                }
                                            }
                                            // self.order_orderlines[j].set_discount(0);
                                            message = null;
                                        } else {
                                            if (typeof (clear_coupon) === 'undefined') {
                                                self.order_orderlines[j].discount_by_pos_promotion.push({
                                                    'orderline_id': self.order_orderlines[j].id,
                                                    'program_id': program.id,
                                                    'value': discount_by_qty[p].value
                                                });
                                            }
                                            if (typeof (clear_coupon) !== 'undefined' && !clear_coupon) {
                                                self.order_orderlines[j].discount_by_coupon_code.push({
                                                    'orderline_id': self.order_orderlines[j].id,
                                                    'program_id': program.id,
                                                    'value': discount_by_qty[p].value
                                                });
                                            }
                                            self.order_orderlines[j].set_discount(discount_by_qty[p].value);
                                            if (!message) {
                                                message = program.name;
                                            }
                                        }
                                    }
                                    if (count_prod >= discount_by_qty[p].total_qty && p == discount_by_qty.length - 1 && self.order_orderlines[j].price > 0) {
                                        if (clear_coupon) {
                                            for (var k = 0; k < self.order_orderlines[j].discount_by_coupon_code.length; k++) {
                                                if (self.order_orderlines[j].id == self.order_orderlines[j].discount_by_coupon_code[k].orderline_id) {
                                                    self.order_orderlines[j].set_discount(self.order_orderlines[j].get_discount() - self.order_orderlines[j].discount_by_coupon_code[k].value);
                                                    break;
                                                }
                                            }
                                            // self.order_orderlines[j].set_discount(0);
                                            message = null;
                                        } else {
                                            if (typeof (clear_coupon) === 'undefined') {
                                                self.order_orderlines[j].discount_by_pos_promotion.push({
                                                    'orderline_id': self.order_orderlines[j].id,
                                                    'program_id': program.id,
                                                    'value': discount_by_qty[p].value
                                                });
                                            }
                                            if (typeof (clear_coupon) !== 'undefined' && !clear_coupon) {
                                                self.order_orderlines[j].discount_by_coupon_code.push({
                                                    'orderline_id': self.order_orderlines[j].id,
                                                    'program_id': program.id,
                                                    'value': discount_by_qty[p].value
                                                });
                                            }
                                            self.order_orderlines[j].set_discount(discount_by_qty[p].value);
                                            if (!message) {
                                                message = program.name;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!clear_coupon) {
                        self.order.pos_promotion_message.push(message);
                    }
                }
            }
        },
        // end promotion type: discount by qty

        //start promotion type: discount
        get_product_promotion_discount: function (promotion_id) {
            var self = this;
            var pos_promotion_product_discount = self.pos.pos_promotion_product_discount;
            var promotion_product = [];
            for (var i = 0; i < pos_promotion_product_discount.length; i++) {
                if (pos_promotion_product_discount[i].promotion_program_id == promotion_id) {
                    promotion_product.push(pos_promotion_product_discount[i]);
                }
            }
            return promotion_product;
        },
        apply_pos_promotion_discount: function (program, clear_coupon) {
            var self = this;
            // if(typeof (clear_coupon) === 'undefined') clear_coupon = false;
            var pos_promotion_product_discount = self.get_product_promotion_discount(program.id);
            var orderlines = self.orderlines;
            self.promotions_product_discount_apply = [];
            for (var condition_count = 0; condition_count < pos_promotion_product_discount.length; condition_count++) {
                var apply = true;
                var min_qty = pos_promotion_product_discount[condition_count].min_qty;
                var product_id = pos_promotion_product_discount[condition_count].product_id;
                var p_qty = 0;
                var orderline_ids = [];
                for (var item_count = 0; item_count < orderlines.length; item_count++) {
                    if (orderlines[item_count].product.id == product_id) {
                        p_qty = parseFloat(p_qty) + parseFloat(orderlines[item_count].quantity);
                        orderline_ids.push(orderlines[item_count].id);
                    }
                }
                if (parseFloat(p_qty) <= 0 || parseFloat(p_qty) < parseFloat(min_qty)) {
                    apply = false;
                    // break;
                }
                if (apply === true) {
                    apply = false;
                    for (var j = 0; j < orderline_ids.length; j++) {
                        self.promotions_product_discount_apply.push({
                            'orderline_id': orderline_ids[j],
                            'discount': pos_promotion_product_discount[condition_count].discount
                        });
                    }
                }
            }
            var message = [];
            var coupon = 0;
            if (typeof (clear_coupon) !== 'undefined' && !clear_coupon) {
                coupon = 1;
            }
            if (self.promotions_product_discount_apply) {
                for (var i = 0; i < orderlines.length; i++) {
                    for (var j = 0; j < self.promotions_product_discount_apply.length; j++) {
                        if (self.promotions_product_discount_apply[j].orderline_id == orderlines[i].id) {
                            if (clear_coupon) {
                                for (var k = 0; k < orderlines[i].pos_promotion_message.length; k++) {
                                    if (orderlines[i].id == orderlines[i].pos_promotion_message[k].orderline_id && orderlines[i].pos_promotion_message[k].coupon == 1) {
                                        var index = orderlines[i].pos_promotion_message.indexOf(orderlines[i].pos_promotion_message[k]);
                                        if (index >= 0) {
                                            orderlines[i].pos_promotion_message.splice(index, 1);
                                            break;
                                        }
                                    }
                                }
                                // orderlines[i].pos_promotion_message = null;
                                for (var k = 0; k < orderlines[i].discount_by_coupon_code.length; k++) {
                                    if (orderlines[i].id == orderlines[i].discount_by_coupon_code[k].orderline_id) {
                                        orderlines[i].set_discount(orderlines[i].get_discount() - orderlines[i].discount_by_coupon_code[k].value);
                                        break;
                                    }
                                }
                                // orderlines[i].set_discount(0);
                            } else {
                                if (typeof orderlines[i].pos_promotion_message != 'undefined') {
                                    // message = orderlines[i].pos_promotion_message;
                                    orderlines[i].pos_promotion_message.push({
                                        'coupon': coupon,
                                        'orderline_id': orderlines[i].id,
                                        'message': program.name + ': discount ' + self.promotions_product_discount_apply[j].discount + '%'
                                    });
                                    // orderlines[i].pos_promotion_message = program.name + ': discount ' + self.promotions_product_discount_apply[j].discount + '%';
                                } else {
                                    orderlines[i].pos_promotion_message.push({
                                        'coupon': coupon,
                                        'orderline_id': orderlines[i].id,
                                        'message': program.name + ': discount ' + self.promotions_product_discount_apply[j].discount + '%'
                                    });
                                    // orderlines[i].pos_promotion_message = program.name + ': discount ' + self.promotions_product_discount_apply[j].discount + '%';
                                }
                                if (typeof (clear_coupon) === 'undefined') {
                                    orderlines[i].discount_by_pos_promotion.push({
                                        'orderline_id': orderlines[i].id,
                                        'program_id': program.id,
                                        'value': self.promotions_product_discount_apply[j].discount
                                    });
                                }
                                if (typeof (clear_coupon) !== 'undefined' && !clear_coupon) {
                                    orderlines[i].discount_by_coupon_code.push({
                                        'orderline_id': orderlines[i].id,
                                        'program_id': program.id,
                                        'value': self.promotions_product_discount_apply[j].discount
                                    });
                                }
                                orderlines[i].set_discount(self.promotions_product_discount_apply[j].discount);
                                self.new_promotion_discount_product_approved.push(orderlines[i].id);
                            }
                        }
                    }
                }
            }
        },
        cancel_pos_promotion_discount: function () {
            var self = this;
            var orderlines = self.orderlines;
            var old_promotion_discount_approved = self.pos.promotion_discount_approved;
            var old_promotion_discount_product_approved = self.pos.promotion_discount_product_approved;
            if (old_promotion_discount_approved) {
                var new_promotion_discount_approve = self.new_promotion_discount_approved;
            }
            if (old_promotion_discount_product_approved) {
                var new_promotion_discount_product_approved = self.new_promotion_discount_product_approved;
                var cancel_product = [];
                for (var i = 0; i < old_promotion_discount_product_approved.length; i++) {
                    if (new_promotion_discount_product_approved.indexOf(old_promotion_discount_product_approved[i]) == -1) {
                        cancel_product.push(old_promotion_discount_product_approved[i]);
                        delete new_promotion_discount_product_approved[new_promotion_discount_product_approved.indexOf(old_promotion_discount_product_approved[i])];
                        // new_promotion_discount_product_approved.splice(new_promotion_discount_product_approved.indexOf(old_promotion_discount_product_approved[i]), 1);
                    }
                }
                self.pos.promotion_discount_product_approved = new_promotion_discount_product_approved;
                if (cancel_product) {
                    for (var i = 0; i < orderlines.length; i++) {
                        if (cancel_product.indexOf(orderlines[i].id) != -1) {
                            orderlines[i].pos_promotion_message = null;
                            orderlines[i].set_discount(0);
                        }
                    }
                }
            }
        },
        //end promotion type: discount

        //start promotion type: Buy X Pay Y
        apply_pos_promotion_bxpy: function (program) {
            return;
            var self = this;
            var pos_promotion_product_discount = self.get_product_promotion_discount(program.id);
            var orderlines = self.orderlines;
            self.promotions_product_discount_apply = [];
            for (var condition_count = 0; condition_count < pos_promotion_product_discount.length; condition_count++) {
                var apply = true;
                var min_qty = pos_promotion_product_discount[condition_count].min_qty;
                var product_id = pos_promotion_product_discount[condition_count].product_id;
                var p_qty = 0;
                var orderline_ids = [];
                for (var item_count = 0; item_count < orderlines.length; item_count++) {
                    if (orderlines[item_count].product.id == product_id) {
                        p_qty = parseFloat(p_qty) + parseFloat(orderlines[item_count].quantity);
                        orderline_ids.push(orderlines[item_count].id);
                    }
                }
                if (parseFloat(p_qty) <= 0 || parseFloat(p_qty) < parseFloat(min_qty)) {
                    apply = false;
                    break;
                }
                if (apply === true) {
                    apply = false;
                    for (var j = 0; j < orderline_ids.length; j++) {
                        self.promotions_product_discount_apply.push({
                            'orderline_id': orderline_ids[j],
                            'discount': pos_promotion_product_discount[condition_count].discount
                        });
                    }
                }
            }
            if (self.promotions_product_discount_apply) {
                for (var i = 0; i < orderlines.length; i++) {
                    for (var j = 0; j < self.promotions_product_discount_apply.length; j++) {
                        if (self.promotions_product_discount_apply[j].orderline_id == orderlines[i].id) {
                            orderlines[i].set_discount(self.promotions_product_discount_apply[j].discount);
                            self.new_promotion_discount_product_approved.push(orderlines[i].id);
                        }
                    }
                }
                if (program_available[i].type == 'discount_on_cat') {
                    self.apply_pos_promotion_discount_on_cat();
                }
            }
        },
        //end Buy X pay Y

        //
        show_product_popup: function (coupon) {
            var self = this;
            if (!coupon && self.pos.get_order().program_used_by_coupon_code > 0) {
                var index = -1;
                for (var i = 0; i < self.bxgy_gy_title.length; i++) {
                    if (self.pos.get_order().program_used_by_coupon_code == self.bxgy_gy_title[i].id) {
                        index = self.bxgy_gy_title.indexOf(self.bxgy_gy_title[i]);
                        break;
                    }
                }
                if (index >= 0) {
                    self.bxgy_gy_title.splice(index, 1);
                }
                var indexCoupon = [];
                for (var i = 0; i < self.bxgy_gy_list.length; i++) {
                    if (self.pos.get_order().program_used_by_coupon_code == self.bxgy_gy_list[i].product_bxgy_id) {
                        indexCoupon.push(self.bxgy_gy_list[i]);
                    }
                }
                for (var i = 0; i < indexCoupon.length; i++) {
                    var index = self.bxgy_gy_list.indexOf(indexCoupon[i]);
                    if (index >= 0) {
                        self.bxgy_gy_list.splice(index, 1);
                    }
                }
            }
            if (self.bxgy_gy_title.length > 0) {
                this.gui.show_popup('getfreeproduct', {
                    'list_title': self.bxgy_gy_title,
                    'is_coupon': coupon,
                    list: self.bxgy_gy_list,
                    choose_title: function (item) {
                        var free_product_by_program = [];
                        for (var i = 0; i < self.bxgy_gy_list.length; i++) {
                            if (item.id == self.bxgy_gy_list[i].product_bxgy_id) {
                                free_product_by_program.push(self.bxgy_gy_list[i]);
                            }
                        }
                        return free_product_by_program;
                    },
                    ok: function (item, item1, item2) {
                        if (item2) {
                            self.order.new_free_product_chosen_count_coupon = item;
                            self.order.new_free_product_chosen_title_coupon = item1;
                            if (self.order.new_order_free_product_coupon.length > 0) {
                                var re_orderline = self.order_orderlines;
                                var new_order = self.order.new_order_free_product_coupon;
                                for (var i = 0; i < new_order.length; i++) {
                                    for (var j = 0; j < re_orderline.length; j++) {
                                        if (re_orderline[j].id == new_order[i].order_line_position && !re_orderline[j].is_product_by_coupon_code) {
                                            // console.log('remove product coupon 3');
                                            self.order.remove_orderline(re_orderline[j]);
                                        }
                                    }
                                }
                            }
                        } else {
                            self.order.new_free_product_chosen_count = item;
                            self.order.new_free_product_chosen_title = item1;
                            if (self.order.new_order_free_product.length > 0) {
                                var re_orderline = self.order_orderlines;
                                var new_order = self.order.new_order_free_product;
                                for (var i = 0; i < new_order.length; i++) {
                                    for (var j = 0; j < re_orderline.length; j++) {
                                        if (re_orderline[j].id == new_order[i].order_line_position && !re_orderline[j].is_product_by_coupon_code) {
                                            // console.log('remove product coupon 4');
                                            self.order.remove_orderline(re_orderline[j]);
                                        }
                                    }
                                }
                            }
                        }

                        var message = [];
                        for (var i = 0; i < item.length; i++) {
                            for (var j = 0; j < self.bxgy_gy_list.length; j++) {
                                if (item[i].product_id == self.bxgy_gy_list[j].product_list.id && item[i].program_id == self.bxgy_gy_list[j].product_bxgy_id) {
                                    self.order.add_product(self.pos.db.get_product_by_id(item[i].product_id));
                                    var last_orderline = self.order_orderlines[self.order_orderlines.length - 1];
                                    if (item2) {
                                        self.order.new_order_free_product_coupon.push({
                                            order_line_position: last_orderline.id,
                                            bxgy_program_id: item[i].program_id
                                        });
                                    } else {
                                        self.order.new_order_free_product.push({
                                            order_line_position: last_orderline.id,
                                            bxgy_program_id: item[i].program_id
                                        });
                                    }
                                    if (typeof last_orderline.pos_promotion_message != 'undefined') {
                                        // message = self.orderlines[i].pos_promotion_message;
                                        last_orderline.pos_promotion_message.push({
                                            'message': self.bxgy_gy_list[j].product_bxgy_name
                                        });
                                        // last_orderline.pos_promotion_message = self.bxgy_gy_list[j].product_bxgy_name;
                                    } else {
                                        last_orderline.pos_promotion_message.push({
                                            'message': self.bxgy_gy_list[j].product_bxgy_name
                                        });
                                        // last_orderline.pos_promotion_message = self.bxgy_gy_list[j].product_bxgy_name;
                                    }
                                    last_orderline.is_pos_promotion_product = 1;
                                    last_orderline.set_unit_price(0);
                                }
                            }
                        }
                        // self.pos.get_order().show_pop_up_promotion = true;
                        // self.pos.show_pop_up_promotion = true;
                        self.pos.get_order().click_pos_promotion = true;
                        // self.pos.click_pos_promotion = true;
                        // tab13 coupon code
                        if (coupon) {
                            if (self.order.before_use_coupon.length > 0 && !self.order.is_used_coupon_code) {
                                if (self.order.get_total_with_tax() != self.order.before_use_coupon[0].total || self.order.get_orderlines().length > self.order.before_use_coupon[0].quantity) {
                                    self.order.is_used_coupon_code = true;
                                    self.order.program_used_by_coupon_code = self.use_program;
                                    new Model('pos.promotion.program').call('set_coupon_code_left_1', [self.use_program]);
                                }
                                // console.log('before_use_coupon1');
                                // console.log(self.order.is_used_coupon_code);
                                // console.log(self.order.program_used_by_coupon_code);
                                // self.pos.get_order().show_pop_up_promotion = false;
                                // self.pos.show_pop_up_promotion = false;
                                self.pos.get_order().click_pos_promotion = false;
                                // self.pos.click_pos_promotion = false;
                            }
                        }
                    },
                    cancel: function () {
                        // user chose nothing
                    }
                });
            }
            if (!coupon && self.bxgy_gy_list.length == 0 && self.order.new_order_free_product.length > 0) {
                var re_orderline = self.order_orderlines;
                var new_order = self.order.new_order_free_product;
                for (var i = 0; i < new_order.length; i++) {
                    for (var j = 0; j < re_orderline.length; j++) {
                        if (re_orderline[j].id == new_order[i].order_line_position) {
                            // console.log('remove product coupon 5');
                            self.order.remove_orderline(re_orderline[j]);
                        }
                    }
                }
            }
            if (coupon && self.bxgy_gy_list.length == 0 && self.order.new_order_free_product_coupon.length > 0) {
                var re_orderline = self.order_orderlines;
                var new_order = self.order.new_order_free_product_coupon;
                for (var i = 0; i < new_order.length; i++) {
                    for (var j = 0; j < re_orderline.length; j++) {
                        if (re_orderline[j].id == new_order[i].order_line_position) {
                            // console.log('remove product coupon 6');
                            self.order.remove_orderline(re_orderline[j]);
                        }
                    }
                }
            }

            // tab13 sum discount
            // console.log('pos promotion');
            // console.log(self.pos.get_order().get_orderlines());
            self.pos.get_order().get_orderlines().forEach(function (line) {
                var total = 0;
                if (line.discount_by_pos_promotion.length >= 1) {
                    line.discount_by_pos_promotion.forEach(function (item) {
                        total += item.value;
                    });
                    // console.log(total);
                }
                if (line.discount_by_coupon_code.length >= 1) {
                    line.discount_by_coupon_code.forEach(function (item) {
                        total += item.value;
                    });
                    // console.log(total);
                }
                line.set_discount(total);
            });
        },
        //

        //Start give product
        get_product_promotion_give_product: function (promotion_id) {
            var self = this;
            var all_programs = self.pos.programs;
            var pos_promotion_product_give_product = self.pos.pos_promotion_product_give_product;
            var promotion_product = [];
            for (var k = 0; k < self.product_list.length; k++) {
                for (var i = 0; i < all_programs.length; i++) {
                    for (var j = 0; j < pos_promotion_product_give_product.length; j++) {
                        if (all_programs[i].id == promotion_id && pos_promotion_product_give_product[j].promotion_program_id == promotion_id) {
                            if (self.product_list[k].id == pos_promotion_product_give_product[j].product_id) {
                                self.bxgy_gy_list.push({
                                    product_bxgy_id: all_programs[i].id,
                                    product_bxgy_name: all_programs[i].name,
                                    product_list: self.product_list[k]
                                });
                            }
                        }
                    }
                }
            }
        },
        apply_pos_promotion_give_product: function (program, clear_coupon) {
            var self = this;
            if (typeof (clear_coupon) === 'undefined') clear_coupon = false;
            if (clear_coupon) {
                var re_orderlinee = self.pos.get_order().get_orderlines();
                var new_orderr = self.pos.get_order().new_order_free_product_coupon;
                for (var i = 0; i < new_orderr.length; i++) {
                    for (var j = 0; j < re_orderlinee.length; j++) {
                        if (re_orderlinee[j].id == new_orderr[i].order_line_position) {
                            // console.log('remove product coupon 1');
                            self.pos.get_order().remove_orderline(re_orderlinee[j]);
                        }
                    }
                }
                if (this.pos.get_order().new_free_product_chosen_title_coupon.length > 0) {
                    this.pos.get_order().new_free_product_chosen_title_coupon = [];
                }
                if (this.pos.get_order().new_free_product_chosen_count_coupon.length > 0) {
                    this.pos.get_order().new_free_product_chosen_count_coupon = [];
                }
            }

            var order = self.order;
            var min_total_order = program.total_order;
            var current_total_order = order.get_total_with_tax();
            if (parseFloat(current_total_order) >= parseFloat(min_total_order)) {
                self.bxgy_gy_title.push({
                    id: program.id,
                    name: program.name,
                    chosen_product: program.number_select_gift,
                    count_product: 0
                });
                self.get_product_promotion_give_product(program.id);
            }
            if (clear_coupon) {
                self.bxgy_gy_title = [];
                self.bxgy_gy_list = [];
            }
        },
        //end promotion give product

        // start promotion buy x get y product
        apply_pos_promotion_bxgy: function (clear_coupon) {
            var self = this;
            if (typeof (clear_coupon) === 'undefined') clear_coupon = false;
            if (clear_coupon) {
                var re_orderlinee = self.pos.get_order().get_orderlines();
                var new_orderr = self.pos.get_order().new_order_free_product_coupon;
                for (var i = 0; i < new_orderr.length; i++) {
                    for (var j = 0; j < re_orderlinee.length; j++) {
                        if (re_orderlinee[j].id == new_orderr[i].order_line_position) {
                            // console.log('remove product coupon 1');
                            self.pos.get_order().remove_orderline(re_orderlinee[j]);
                        }
                    }
                }
                if (this.pos.get_order().new_free_product_chosen_title_coupon.length > 0) {
                    this.pos.get_order().new_free_product_chosen_title_coupon = [];
                }
                if (this.pos.get_order().new_free_product_chosen_count_coupon.length > 0) {
                    this.pos.get_order().new_free_product_chosen_count_coupon = [];
                }
            }

            // var order = this.pos.get_order();
            var orderlines = self.order.get_orderlines();
            if (orderlines.length > 0) {

                // gop san pham trong order
                var orderlines_object = [];
                var check;
                for (var i = 0; i < orderlines.length; i++) {
                    check = false;
                    if (i == 0) {
                        orderlines_object.push({
                            'id': orderlines[i].product.id,
                            'qty': orderlines[i].quantity,
                            'is_promotion_product': orderlines[i].is_pos_promotion_product
                        });
                        continue;
                    }
                    for (var j = 0; j < orderlines_object.length; j++) {
                        if (orderlines_object[j].id == orderlines[i].product.id) {
                            orderlines_object[j].qty += orderlines[i].quantity;
                            check = true;
                            break;
                        }
                    }
                    if (!check) {
                        orderlines_object.push({
                            'id': orderlines[i].product.id,
                            'qty': orderlines[i].quantity,
                            'is_promotion_product': orderlines[i].is_pos_promotion_product
                        });
                    }
                }

                // kiem tra san pham theo chuong trinh khuyen mai
                var bxgy_programs = this.pos.bxgy_programs;
                var bxgy_bx = this.pos.bxgy_bx;
                var bxgy_gy = this.pos.bxgy_gy;
                var check_bxgy_bx, bxgy_bx_total;
                var bxgy_bx_product = false, bxgy_bx_product_index;
                for (var i = 0; i < bxgy_programs.length; i++) {
                    check_bxgy_bx = true;
                    bxgy_bx_total = 0;
                    for (var j = 0; j < bxgy_bx.length; j++) {
                        bxgy_bx_product = false;
                        if (bxgy_programs[i].id == bxgy_bx[j].bxgy_program_id) {
                            // kiem tra ton tai
                            for (var k = 0; k < orderlines_object.length; k++) {
                                if (bxgy_bx[j].id == orderlines_object[k].id && orderlines_object[k].is_promotion_product != 1) {
                                    bxgy_bx_product = true;
                                    bxgy_bx_product_index = k;
                                    break;
                                }
                            }
                            // kiem tra min qty
                            if (bxgy_bx_product) {
                                if (bxgy_bx[j].qty <= orderlines_object[bxgy_bx_product_index].qty) {
                                    bxgy_bx_total++;
                                } else {
                                    check_bxgy_bx = false;
                                    break;
                                }
                            }
                            if (!bxgy_bx_product) {
                                check_bxgy_bx = false;
                                break;
                            }
                            if (check_bxgy_bx && bxgy_bx_total == bxgy_programs[i].pos_promotion_bxgy_buy.length) {
                                break;
                            }
                        }
                    }
                    if (check_bxgy_bx) {
                        for (var k = 0; k < self.product_list.length; k++) {
                            for (var j = 0; j < bxgy_gy.length; j++) {
                                if (bxgy_programs[i].id == bxgy_gy[j].bxgy_program_id) {
                                    if (self.product_list[k].id == bxgy_gy[j].id) {
                                        self.bxgy_gy_list.push({
                                            product_bxgy_id: bxgy_programs[i].id,
                                            product_bxgy_name: bxgy_programs[i].name,
                                            product_list: self.product_list[k]
                                        });
                                    }
                                }
                            }
                        }
                        self.bxgy_gy_title.push({
                            id: bxgy_programs[i].id,
                            name: bxgy_programs[i].name,
                            chosen_product: bxgy_programs[i].number_select_free,
                            count_product: 0
                        });
                    }
                }
                if (clear_coupon) {
                    self.bxgy_gy_list = [];
                    self.bxgy_gy_title = [];
                }
            }
        },
        // end promotion buy x get y product

        //start promotion type special price
        get_product_promotion_special_price: function (promotion_id) {
            var self = this;
            var pos_promotion_special_price = self.pos.special_prices;
            var promotion_product = [];
            for (var i = 0; i < pos_promotion_special_price.length; i++) {
                if (pos_promotion_special_price[i].promotion_program_id == promotion_id) {
                    promotion_product.push(pos_promotion_special_price[i]);
                }
            }
            return promotion_product;
        },
        apply_pos_promotion_special_price: function (program, clear_coupon) {
            var self = this;
            // if(typeof (clear_coupon) === 'undefined') clear_coupon = false;
            var orderlines = self.orderlines;
            var promotion_product = self.get_product_promotion_special_price(program.id);
            var message = [];
            var coupon = 0;
            if (typeof (clear_coupon) !== 'undefined' && !clear_coupon) {
                coupon = 1;
            }
            for (var i = 0; i < promotion_product.length; i++) {
                for (var item_count = 0; item_count < orderlines.length; item_count++) {
                    if (orderlines[item_count].product.id == promotion_product[i].product_id && orderlines[item_count].quantity > 0) {
                        if (clear_coupon) {
                            for (var k = 0; k < orderlines[item_count].pos_promotion_message.length; k++) {
                                if (orderlines[item_count].id == orderlines[item_count].pos_promotion_message[k].orderline_id && orderlines[item_count].pos_promotion_message[k].coupon == 1) {
                                    var index = orderlines[item_count].pos_promotion_message.indexOf(orderlines[item_count].pos_promotion_message[k]);
                                    if (index >= 0) {
                                        orderlines[item_count].pos_promotion_message.splice(index, 1);
                                        break;
                                    }
                                }
                            }
                            // orderlines[item_count].pos_promotion_message = null;
                            orderlines[item_count].set_unit_price(orderlines[item_count].product.price);
                        } else {
                            if (typeof orderlines[item_count].pos_promotion_message != 'undefined') {
                                // message = orderlines[i].pos_promotion_message;
                                orderlines[item_count].pos_promotion_message.push({
                                    'coupon': coupon,
                                    'orderline_id': orderlines[item_count].id,
                                    'message': program.name + ': special price ' + self.chrome.format_currency(promotion_product[i].spec_price)
                                });
                                // orderlines[item_count].pos_promotion_message = program.name + ': special price ' + self.chrome.format_currency(promotion_product[i].spec_price);
                            } else {
                                orderlines[item_count].pos_promotion_message.push({
                                    'coupon': coupon,
                                    'orderline_id': orderlines[item_count].id,
                                    'message': program.name + ': special price ' + self.chrome.format_currency(promotion_product[i].spec_price)
                                });
                                // orderlines[item_count].pos_promotion_message = program.name + ': special price ' + self.chrome.format_currency(promotion_product[i].spec_price);
                            }
                            orderlines[item_count].set_unit_price(promotion_product[i].spec_price);
                        }
                    }
                }
            }
        },
        //end promotion type special price

        //start promotion type discount on cat
        get_category_promotion_discount_on_cat: function (promotion_id) {
            var self = this;
            var pos_promotion_discount_on_cat = self.pos.discount_on_cat;
            var promotion_category = [];
            for (var i = 0; i < pos_promotion_discount_on_cat.length; i++) {
                if (pos_promotion_discount_on_cat[i].promotion_program_id == promotion_id) {
                    promotion_category.push(pos_promotion_discount_on_cat[i]);
                }
            }
            return promotion_category;
        },

        apply_pos_promotion_discount_on_cat: function (program, clear_coupon) {
            var self = this;
            // if(typeof (clear_coupon) === 'undefined') clear_coupon = false;
            var orderlines = self.orderlines;
            var promotion_category = self.get_category_promotion_discount_on_cat(program.id);
            var message = [];
            var coupon = 0;
            if (typeof (clear_coupon) !== 'undefined' && !clear_coupon) {
                coupon = 1;
            }
            for (var i = 0; i < promotion_category.length; i++) {
                for (var item_count = 0; item_count < orderlines.length; item_count++) {
                    if (orderlines[item_count].product.pos_categ_id[0] == promotion_category[i].category_id && orderlines[item_count].quantity > 0) {
                        if (promotion_category[i].type == 'percent') {
                            if (clear_coupon) {
                                for (var k = 0; k < orderlines[item_count].pos_promotion_message.length; k++) {
                                    if (orderlines[item_count].id == orderlines[item_count].pos_promotion_message[k].orderline_id && orderlines[item_count].pos_promotion_message[k].coupon == 1) {
                                        var index = orderlines[item_count].pos_promotion_message.indexOf(orderlines[item_count].pos_promotion_message[k]);
                                        if (index >= 0) {
                                            orderlines[item_count].pos_promotion_message.splice(index, 1);
                                            break;
                                        }
                                    }
                                }
                                // orderlines[item_count].pos_promotion_message = null;
                                for (var k = 0; k < orderlines[item_count].discount_by_coupon_code.length; k++) {
                                    if (orderlines[item_count].id == orderlines[item_count].discount_by_coupon_code[k].orderline_id) {
                                        orderlines[item_count].set_discount(orderlines[item_count].get_discount() - orderlines[item_count].discount_by_coupon_code[k].value);
                                        break;
                                    }
                                }
                                // orderlines[item_count].set_discount(0);
                            } else {
                                if (typeof orderlines[item_count].pos_promotion_message != 'undefined') {
                                    // message = orderlines[item_count].pos_promotion_message;
                                    orderlines[item_count].pos_promotion_message.push({
                                        'coupon': coupon,
                                        'orderline_id': orderlines[item_count].id,
                                        'message': program.name + ': discount ' + promotion_category[i].value + '%'
                                    });
                                    // orderlines[item_count].pos_promotion_message = program.name + ': discount ' + promotion_category[i].value + '%';
                                } else {
                                    orderlines[item_count].pos_promotion_message.push({
                                        'coupon': coupon,
                                        'orderline_id': orderlines[item_count].id,
                                        'message': program.name + ': discount ' + promotion_category[i].value + '%'
                                    });
                                    // orderlines[item_count].pos_promotion_message = program.name + ': discount ' + promotion_category[i].value + '%';
                                }
                                if (typeof (clear_coupon) === 'undefined') {
                                    orderlines[item_count].discount_by_pos_promotion.push({
                                        'orderline_id': orderlines[item_count].id,
                                        'program_id': program.id,
                                        'value': promotion_category[i].value
                                    });
                                }
                                if (typeof (clear_coupon) !== 'undefined' && !clear_coupon) {
                                    orderlines[item_count].discount_by_coupon_code.push({
                                        'orderline_id': orderlines[item_count].id,
                                        'program_id': program.id,
                                        'value': promotion_category[i].value
                                    });
                                }
                                orderlines[item_count].set_discount(promotion_category[i].value);
                            }
                        }
                        if (promotion_category[i].type == 'fixed') {
                            if (clear_coupon) {
                                for (var k = 0; k < orderlines[item_count].pos_promotion_message.length; k++) {
                                    if (orderlines[item_count].id == orderlines[item_count].pos_promotion_message[k].orderline_id && orderlines[item_count].pos_promotion_message[k].coupon == 1) {
                                        var index = orderlines[item_count].pos_promotion_message.indexOf(orderlines[item_count].pos_promotion_message[k]);
                                        if (index >= 0) {
                                            orderlines[item_count].pos_promotion_message.splice(index, 1);
                                            break;
                                        }
                                    }
                                }
                                // orderlines[item_count].pos_promotion_message = null;
                                orderlines[item_count].set_unit_price(orderlines[item_count].product.price);
                            } else {
                                if (typeof orderlines[item_count].pos_promotion_message != 'undefined') {
                                    // message = orderlines[item_count].pos_promotion_message;
                                    orderlines[item_count].pos_promotion_message.push({
                                        'coupon': coupon,
                                        'orderline_id': orderlines[item_count].id,
                                        'message': program.name + ': discount ' + self.chrome.format_currency(promotion_category[i].value)
                                    });
                                    // orderlines[item_count].pos_promotion_message = program.name + ': discount ' + self.chrome.format_currency(promotion_category[i].value);
                                } else {
                                    orderlines[item_count].pos_promotion_message.push({
                                        'coupon': coupon,
                                        'orderline_id': orderlines[item_count].id,
                                        'message': program.name + ': discount ' + self.chrome.format_currency(promotion_category[i].value)
                                    });
                                    // orderlines[item_count].pos_promotion_message = program.name + ': discount ' + self.chrome.format_currency(promotion_category[i].value);
                                }
                                orderlines[item_count].set_unit_price(orderlines[item_count].product.price - promotion_category[i].value);
                            }
                        }
                    }
                }
            }
        },
        //end promotion type discount on cat

        //start promotion type discount on total
        apply_pos_promotion_discount_on_total: function (program, b, clear_coupon) {
            var self = this;
            // if(typeof (clear_coupon) === 'undefined') clear_coupon = false;
            var product = this.pos.db.get_product_by_id(program.discount_product_id[0]);

            if (typeof (clear_coupon) !== 'undefined' && clear_coupon) {
                var i = 0;
                while (i < self.order_orderlines.length) {
                    if (self.order_orderlines[i].get_product() === product) {
                        self.order.remove_orderline(self.order_orderlines[i]);
                    } else {
                        i++;
                    }
                }
            } else {
                // Remove existing discounts
                var i = 0;
                while (i < self.order_orderlines.length) {
                    if (self.order_orderlines[i].get_product() === product) {
                        self.order.remove_orderline(self.order_orderlines[i]);
                    } else {
                        i++;
                    }
                }

                // Add discount
                var discount = 0;
                var type = '', value = '';
                // var discount_on_total = self.new_discount_on_total;
                var discount_on_total = [];
                var total = self.order.get_total_with_tax();
                for (var k = 0; k < b.length; k++) {
                    if (b[k].promotion_program_id == program.id) {
                        discount_on_total.push(b[k]);
                    }
                }
                for (var j = discount_on_total.length - 1; j >= 0; j--) {
                    if (total >= discount_on_total[j].total_order) {
                        if (discount_on_total[j].type == 'percent') {
                            value = discount_on_total[j].value + '%';
                            discount = (-total * (discount_on_total[j].value / 100));
                            break;
                        } else {
                            value = self.chrome.format_currency(discount_on_total[j].value);
                            discount = (-discount_on_total[j].value);
                            break;
                        }
                    }
                }
                // var message = [];
                var coupon = 0;
                if (typeof (clear_coupon) !== 'undefined' && !clear_coupon) {
                    coupon = 1;
                }
                if (discount < 0) {
                    self.order.add_product(product, {
                        price: discount
                    });
                    for (var j = 0; j < self.order_orderlines.length; j++) {
                        if (self.order_orderlines[j].get_product() === product) {
                            self.order_orderlines[j].pos_promotion_message.push({
                                'coupon': coupon,
                                'orderline_id': self.order_orderlines[j].id,
                                'message': program.name + ': ' + value
                            });
                            // self.order_orderlines[self.order_orderlines.length - 1].pos_promotion_message = program.name + ': ' + value;
                            // self.order_orderlines[self.order_orderlines.length - 1].product.display_name = 'Total Discount';
                            self.order_orderlines[j].set_unit_price(discount);
                            break;
                        }
                    }
                    // self.order_orderlines[self.order_orderlines.length - 1].pos_promotion_message.push({
                    //     'coupon': coupon,
                    //     'orderline_id': self.order_orderlines[self.order_orderlines.length - 1].id,
                    //     'message': program.name + ': ' + value
                    // });
                    // // self.order_orderlines[self.order_orderlines.length - 1].pos_promotion_message = program.name + ': ' + value;
                    // // self.order_orderlines[self.order_orderlines.length - 1].product.display_name = 'Total Discount';
                    // self.order_orderlines[self.order_orderlines.length - 1].set_unit_price(discount);
                }
            }
        }

        //     compare: function compare(a,b) {
        //         if (a.total_order < b.total_order)
        //             return -1;
        //         if (a.total_order > b.total_order)
        //             return 1;
        //         return 0;
        //     },
        //
        //     get_promotion_discount_on_total: function (promotion_id) {
        //         var self = this;
        //         var pos_promotion_discount_on_total = self.pos.discount_on_total;
        //         var promotion_discount_on_total = [];
        //         for (var i = 0; i < pos_promotion_discount_on_total.length; i++) {
        //             if (pos_promotion_discount_on_total[i].promotion_program_id == promotion_id) {
        //                 promotion_discount_on_total.push(pos_promotion_discount_on_total[i]);
        //             }
        //         }
        //         promotion_discount_on_total.sort(function compare(a,b) {
        //             if (a.total_order > b.total_order)
        //                 return -1;
        //             if (a.total_order < b.total_order)
        //                 return 1;
        //             return 0;
        //         });
        //         return promotion_discount_on_total;
        //     },
        //
        //     apply_pos_promotion_discount_on_total: function (program) {
        //         var self = this;
        //         var order = self.order;
        //         var current_total_order = order.get_total_with_tax();
        //         var promotion_discount_on_total = self.get_promotion_discount_on_total(program.id);
        //         var promotion_id = '';
        //         for (var i=0; i<promotion_discount_on_total.length; i++) {
        //             if (parseFloat(current_total_order) >= parseFloat(promotion_discount_on_total[i].total_order)) {
        //                 var type = promotion_discount_on_total[i].type;
        //                 var value = promotion_discount_on_total[i].value;
        //                 promotion_id = promotion_discount_on_total[i].promotion_program_id;
        //                 if (self.pos.promotion_discount_on_total_approved.indexOf(program.id) == -1) {
        //                     self.pos.promotion_discount_on_total_approved.push(program.id);
        //                     order.add_product(self.pos.db.get_product_by_id(68));
        //                     // console.log(self.pos.db.get_product_by_id(68));
        //                     var last_orderline = self.pos.get_order().get_orderlines()[self.pos.get_order().get_orderlines().length-1];
        //                     var pos_promotion_message = 'test';
        //                     last_orderline.pos_promotion_message = pos_promotion_message;
        //                     last_orderline.product.display_name = 'Discount Product';
        //                     // console.log(last_orderline);
        //                     last_orderline.set_unit_price(-10);
        //                     var item_line = last_orderline.id;
        //                     self.pos.promotion_discount_product_approved.push({
        //                         'pos_program_id': program.id,
        //                         'item_line': item_line
        //                     })
        //                 } else {
        //
        //                 }
        //                 break;
        //             }
        //         }
        //         if (!promotion_id) {
        //             if (self.pos.promotion_discount_on_total_approved.indexOf(program.id) != -1) {
        //                 delete self.pos.promotion_discount_on_total_approved[self.pos.promotion_discount_on_total_approved.indexOf(program.id)];
        //             }
        //         }
        //     },
        //     //end promotion type discount on total
    });

    MageBaseWidget.PromotionLeftMenu.include({
        renderElement: function () {
            var self = this;
            this._super();
            this.$('.prom-coupon').click(function () {
                self.gui.show_popup('fillincouponcode', {});
            });
        }
    });

    return {
        CouponCodeButton: CouponCodeButton,
        PosPromotionButton: PosPromotionButton
    };

});
